
from util.util import get_input


puzzle_input = get_input()
test_input = [
    "$ cd /",
    "$ ls",
    "dir a",
    "14848514 b.txt",
    "8504156 c.dat",
    "dir d",
    "$ cd a",
    "$ ls",
    "dir e",
    "29116 f",
    "2557 g",
    "62596 h.lst",
    "$ cd e",
    "$ ls",
    "584 i",
    "$ cd ..",
    "$ cd ..",
    "$ cd d",
    "$ ls",
    "4060174 j",
    "8033020 d.log",
    "5626152 d.ext",
    "7214296 k",
]


class DiskObject:

    def __init__(self, name: str, object_type: str, size: int = 0, parent=None):
        self.parent = parent
        self.name = name
        self.object_type = object_type
        self.size = size
        self.calculated_size = None
        self.children = {}

    def __repr__(self):
        if self.object_type == "dir":
            return f"{self.name} ({self.object_type})"
        return f"{self.name} ({self.object_type}, {self.size})"

    def add(self, args):
        if args[0] == "dir":
            self.children[args[1]] = DiskObject(args[1], "dir", parent=self)
        else:
            self.children[args[1]] = DiskObject(args[1], "file", int(args[0]), parent=self)

    def get_size(self):
        if self.object_type == "file":
            return self.size
        if self.calculated_size is not None:
            return self.calculated_size
        self.calculated_size = sum([x.get_size() for x in self.children.values()])
        return self.calculated_size

    def get_sizes(self, threshold: int = 100000):
        if self.object_type == "file":
            return []

        size_list = []
        own_size = self.get_size()
        if own_size < threshold:
            size_list.append(own_size)

        for x in self.children.values():
            size_list = size_list + x.get_sizes(threshold)

        return size_list

    def cd(self, command):
        if command == ".." and not self.parent:
            return self
        if command == "..":
            return self.parent
        if command in self.children:
            return self.children[command]
        raise ValueError(f"incorrect command: {command}")

    def print(self, level: int = 0, indent: int = 4):
        print(" " * level * indent + "- " + str(self))
        for o in self.children.values():
            o.print(level+1)


def process_input(lines=puzzle_input):
    root = DiskObject("/", "dir")
    current_location = root
    for line in lines[1:]:
        if "$ ls" in line:
            continue
        if "$ cd" in line:
            current_location = current_location.cd(line.replace("$ cd ", ""))
            continue
        current_location.add(line.split(" "))
    return root


def solution1(problem=puzzle_input, print_structure=False):
    root = process_input(problem)
    if print_structure:
        root.print()
    return sum(root.get_sizes())
    

def solution2(problem=puzzle_input, total_disk_space: int = 70000000, space_needed: int = 30000000):
    root = process_input(problem)
    current_unused = total_disk_space - root.get_size()
    extra_space_needed = space_needed - current_unused
    if extra_space_needed < 0:
        return 0
    sizes = sorted(root.get_sizes(total_disk_space))
    for size in sizes:
        if size > extra_space_needed:
            return size
    return None
    
    
print(f"test 1: {solution1(test_input, True)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
