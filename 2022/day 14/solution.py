
from util.util import get_input, sign


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            [x.split(",") for x in line.split(" -> ")]
        )
    return processed_input


class Cave:

    def __init__(self, layout, sand_origin=(500,0)):
        self.sand_origin = sand_origin
        self.y_min = sand_origin[1]
        self.y_max = 0
        self.x_min = float('inf')
        self.x_max = 0
        self.map = {sand_origin: "+"}
        self.sand_particles = 0
        self._init_map(layout)

    def _update_boundaries(self, x, y):
        if x < self.x_min:
            self.x_min = x
        if x > self.x_max:
            self.x_max = x
        if y < self.y_min:
            self.y_min = y
        if y > self.y_max:
            self.y_max = y

    def _init_map(self, layout):
        for rock_lines in layout:
            for i in range(len(rock_lines)-1):
                x1, y1 = [int(j) for j in rock_lines[i]]
                x2, y2 = [int(j) for j in rock_lines[i+1]]
                options = set([(x2, y2)])
                if x1 == x2:
                    for offset in range(0, y2-y1, sign(y2-y1)):
                        options.add((x1, y1 + offset))
                if y1 == y2:
                    for offset in range(0, x2-x1, sign(x2-x1)):
                        options.add((x1 + offset, y1))

                for option in options:
                    self._update_boundaries(*option)
                    self.map[option] = "#"

    def print(self):
        for y in range(self.y_min, self.y_max+1):
            line = []
            for x in range(self.x_min, self.x_max+1):
                line.append(self.map.get((x, y), "."))
            print("".join(line))

    def sand_option(self, x, y, max_depth):
        if max_depth is not None:
            if y+1 >= max_depth:
                return None
        options = [(x, y+1), (x-1, y+1), (x+1, y+1)]
        for option in options:
            if option not in self.map:
                return option
        return None

    def out_of_bounds(self, x, y, max_depth):
        if max_depth is None:
            if x < self.x_min or x > self.x_max or y >= self.y_max:
                return True
            return False
        return False

    def flow_sand(self, max_depth=None):
        while True:
            sand_position = self.sand_origin
            option = self.sand_option(*sand_position, max_depth)
            while not self.out_of_bounds(*sand_position, max_depth) and option is not None:
                sand_position = option
                option = self.sand_option(*sand_position, max_depth)

            # Sand must have come to a halt or is out of bounds
            if not self.out_of_bounds(*sand_position, max_depth) and self.map[self.sand_origin] != "o":
                self.map[sand_position] = "o"
                self._update_boundaries(*sand_position)
                self.sand_particles += 1
            else:
                return


def solution1(problem=puzzle_input):
    cave = Cave(process_input(problem))
    cave.flow_sand()
    cave.print()
    return cave.sand_particles
    

def solution2(problem=puzzle_input):
    cave = Cave(process_input(problem))
    cave.flow_sand(cave.y_max + 2)
    cave.print()
    return cave.sand_particles
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
