
from util.util import get_input
from parse import parse
import mip as mip
import numpy as np


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        ores = parse(
            "Blueprint {i}: Each ore robot costs {ore_robot_ore} ore. "
            "Each clay robot costs {clay_robot_ore} ore. "
            "Each obsidian robot costs {obsidian_robot_ore} ore and {obsidian_robot_clay} clay. "
            "Each geode robot costs {geode_robot_ore} ore and {geode_robot_obsidian} obsidian.",
            line,
        ).named
        processed_input.append({k: int(v) for k, v in ores.items()})
    return processed_input


def score_blueprint(blueprint, minutes):
    # Create Mixed Integer Model and turn off output
    model = mip.Model(sense=mip.MAXIMIZE)
    model.verbose = 0

    # Objective function
    objective = mip.LinExpr(0)

    # Populate variables
    all_variables = []
    for minute in range(minutes):
        variables = {}

        # Robot creation variables at specific minute
        variables.update({"ore": model.add_var(f"ore_{minute}", var_type=mip.BINARY)})
        variables.update({"clay": model.add_var(f"clay_{minute}", var_type=mip.BINARY)})
        variables.update({"obsidian": model.add_var(f"obsidian_{minute}", var_type=mip.BINARY)})
        variables.update({"geode": model.add_var(f"geode_{minute}", var_type=mip.BINARY)})
        all_variables.append(variables)

        # There can only be one robot created at each step
        model.add_constr(variables["ore"] + variables["clay"] + variables["obsidian"] + variables["geode"] <= 1)

        # Decrease in resource constraints
        ore = mip.LinExpr(const=minute)
        clay = mip.LinExpr(const=0)
        obsidian = mip.LinExpr(const=0)

        for passed_time in range(minute):
            # Decrease of ore if a certain robot is created
            ore.add_var(all_variables[passed_time]["ore"], minute - passed_time - 1)
            ore.add_var(all_variables[passed_time]["ore"], -blueprint["ore_robot_ore"])
            ore.add_var(all_variables[passed_time]["clay"], -blueprint["clay_robot_ore"])
            ore.add_var(all_variables[passed_time]["obsidian"], -blueprint["obsidian_robot_ore"])
            ore.add_var(all_variables[passed_time]["geode"], -blueprint["geode_robot_ore"])

            # Decrease of clay if a certain robot is created
            clay.add_var(all_variables[passed_time]["clay"], minute - passed_time - 1)
            clay.add_var(all_variables[passed_time]["obsidian"], -blueprint["obsidian_robot_clay"])

            # Decrease of obsidian if a certain robot is created
            obsidian.add_var(all_variables[passed_time]["obsidian"], minute - passed_time - 1)
            obsidian.add_var(all_variables[passed_time]["geode"], -blueprint["geode_robot_obsidian"])

        # Total ores created should be higher than or equal to all consumed ores to create robots
        model.add_constr(
            ore >= variables["ore"] * blueprint["ore_robot_ore"] + variables["clay"] * blueprint["clay_robot_ore"] +
            variables["obsidian"] * blueprint["obsidian_robot_ore"] + variables["geode"] * blueprint["geode_robot_ore"]
        )
        model.add_constr(clay >= variables["obsidian"] * blueprint["obsidian_robot_clay"])
        model.add_constr(obsidian >= variables["geode"] * blueprint["geode_robot_obsidian"])

        # Add current geode count to the objective function to maximise
        objective.add_var(variables["geode"], minutes - minute - 1)

    # Load the objective function and optimize
    model.objective = objective
    model.optimize()

    return int(model.objective_value)


def solution1(problem=puzzle_input):
    blueprints = process_input(problem)
    scores = []
    for blueprint in blueprints:
        scores.append(blueprint["i"] * score_blueprint(blueprint, 24))
    return sum(scores)
    

def solution2(problem=puzzle_input):
    blueprints = process_input(problem)
    scores = []
    for blueprint in blueprints[:3]:
        scores.append(score_blueprint(blueprint, 32))
    print(scores)
    return np.prod(scores)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
