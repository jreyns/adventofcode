
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        op, x = "noop", False
        if line != op:
            op, x = line.split(" ")
        processed_input.append(
            (op, int(x))
        )
    return processed_input


class CPU:

    cycles = {
        "noop": 1,
        "addx": 2
    }

    def __init__(self, operations):
        self.cycle = 0
        self.operations = operations
        self.fp = 0
        self.x = 1
        self.timer = 0
        self.locked = False
        self.add = False
        self.to_add = 0

    def tick(self):
        self.cycle += 1

        # Shortcut because the result of addx only occurs after 2 cycles
        if self.add:
            self.add = False
            self.x += self.to_add

        if self.locked:
            self.timer -= 1

        if self.timer > 0:
            return True

        # Check current operation
        op, x = self.operations[self.fp]

        # The noop operation is one cycle but doesn't change anything so just handle it
        if op == "noop":
            self.fp += 1
            return self.fp < len(self.operations)

        # The addx takes 2 cycles so can only complete if it was locked before
        if op == "addx" and self.locked:
            self.locked = False
            self.add = True
            self.to_add = x
            self.fp += 1
            return self.fp < len(self.operations)

        # Can only reach this location if op == addx and locked == False so set the timer and set the cpu to locked
        self.timer = self.cycles[op] - 1
        self.locked = True
        return True


def solution1(problem=puzzle_input):
    cpu = CPU(process_input(problem))
    cycles = list(range(20, 260, 40))
    total = 0
    for i in range(1, 221):
        cpu.tick()
        if i in cycles:
            total += i * cpu.x
    return total
    

def solution2(problem=puzzle_input):
    cpu = CPU(process_input(problem))
    options = [".", "#"]
    buffer = ""
    can_continue = True
    position = 0
    while can_continue:
        if position >= 40:
            position = 0
            print(buffer)
            buffer = ""

        can_continue = cpu.tick()
        output = (cpu.x - 1) <= position <= (cpu.x + 1)
        buffer += options[output]
        position += 1
    print(buffer)
    return
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
