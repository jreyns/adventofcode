from util.util import get_input

puzzle_input = get_input()

test_input = [
    ("A", "Y"),
    ("B", "X"),
    ("C", "Z")
]


def process_input():
    processed_input = []
    for line in puzzle_input:
        processed_input.append(line.split(" "))
    return processed_input


def check_score(
        elf: str, you: str, mapping=[("A", "X"), ("B", "Y"), ("C", "Z")],
        symbol_order=["A", "B", "C"], symbol_scores=[1, 2, 3], win_scores=[0, 3, 6],
        rules={
            "A": lambda x: True if x == "B" else False,
            "B": lambda x: True if x == "C" else False,
            "C": lambda x: True if x == "A" else False
        }):
    translation = {
        p2: p1 for p1, p2 in mapping
    }
    your_play = translation[you]
    score = symbol_scores[symbol_order.index(your_play)]
    if elf == your_play:
        score += win_scores[1]
    elif rules[elf](your_play):
        score += win_scores[2]
    else:
        score += win_scores[0]

    return score


def enforce_score(
        elf: str, you: str, mapping={"A": "CAB", "B": "ABC", "C": "BCA"},
        symbol_order=["A", "B", "C"], symbol_scores=[1, 2, 3], win_scores=[0, 3, 6],
        goals={
            "X": 0,
            "Y": 1,
            "Z": 2
        }):
    goal = goals[you]
    your_play = mapping[elf][goal]
    symbol_score = symbol_scores[symbol_order.index(your_play)]
    action_score = win_scores[goal]
    return symbol_score + action_score


def solution1(game_inputs=process_input()):
    total_score = 0
    for elf, you in game_inputs:
        total_score += check_score(elf, you)
    return total_score


def solution2(game_inputs=process_input()):
    total_score = 0
    for elf, you in game_inputs:
        total_score += enforce_score(elf, you)
    return total_score


print(f"test 1: {solution1(test_input)}")
print(f"test 1: {solution2(test_input)}")

print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
