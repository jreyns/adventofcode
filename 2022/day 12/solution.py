
from util.util import get_input, get_adjacent_coordinates


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            [x for x in line]
        )
    return processed_input


class Grid:

    def __init__(self, lines):
        self.height = len(lines)
        self.width = len(lines[0])

        self.start = None
        self.end = None

        self.map = lines
        self.a_positions = set([])

        self._init_state()

    def _init_state(self):
        for j in range(self.height):
            for i in range(self.width):
                if self.map[j][i] == "S":
                    self.start = i, j
                    self.map[j][i] = "a"
                if self.map[j][i] == "E":
                    self.end = i, j
                    self.map[j][i] = "z"
                if self.map[j][i] == "a":
                    self.a_positions.add((i, j))

    def hill_climb_1(self):
        passed = set([])
        points = [self.end]
        distance = 0
        while points:
            distance += 1
            new_points = []
            for i, j in points:
                if (i, j) in passed:
                    continue
                passed.add((i, j))
                current_elevation = ord(self.map[j][i])
                neighbours = get_adjacent_coordinates(i, j, (self.width, self.height))
                for x, y in neighbours:
                    if (x, y) in passed:
                        continue
                    if current_elevation - ord(self.map[y][x]) > 1:
                        continue
                    if (x, y) == self.start:
                        return distance
                    new_points.append((x, y))
            points = new_points

    def hill_climb_2(self):
        passed = set([])
        points = [self.end]
        distance = 0
        a_paths = []
        while points:
            distance += 1
            new_points = []
            for i, j in points:
                if (i, j) in passed:
                    continue
                passed.add((i, j))
                current_elevation = ord(self.map[j][i])
                neighbours = get_adjacent_coordinates(i, j, (self.width, self.height))
                for x, y in neighbours:
                    if (x, y) in passed:
                        continue
                    if current_elevation - ord(self.map[y][x]) > 1:
                        continue
                    if (x, y) in self.a_positions:
                        a_paths.append(distance)
                    new_points.append((x, y))
            points = new_points
        return min(*a_paths)


def solution1(problem=puzzle_input):
    lines = process_input(problem)
    grid = Grid(lines)
    return grid.hill_climb_1()
    

def solution2(problem=puzzle_input):
    lines = process_input(problem)
    grid = Grid(lines)
    return grid.hill_climb_2()
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
