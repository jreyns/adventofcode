
from util.util import get_input


puzzle_input = get_input()
test_input = [
    "2-4,6-8",
    "2-3,4-5",
    "5-7,7-9",
    "2-8,3-7",
    "6-6,4-6",
    "2-6,4-8"
]


def ranges_fully_overlap(range1, range2):
    if range1[0] <= range2[0] and range1[1] >= range2[1]:
        return True
    elif range1[0] >= range2[0] and range1[1] <= range2[1]:
        return True
    else:
        return False


def ranges_overlap(range1, range2):
    if range1[0] <= range2[0] <= range1[1]:
        return True
    elif range2[0] <= range1[0] <= range2[1]:
        return True
    elif range1[0] <= range2[1] <= range1[1]:
        return True
    elif range2[0] <= range1[1] <= range2[1]:
        return True
    else:
        return False


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        range1, range2 = line.split(",")
        processed_input.append(
            ([int(x) for x in range1.split("-")], [int(x) for x in range2.split("-")])
        )
    return processed_input


def solution1(problem=puzzle_input):
    return sum([ranges_fully_overlap(*x) for x in process_input(problem)])
    

def solution2(problem=puzzle_input):
    return sum([ranges_overlap(*x) for x in process_input(problem)])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
