
from util.util import get_input


puzzle_input = get_input()

test_input = [
    "vJrwpWtwJgWrhcsFMMfFFhFp",
    "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
    "PmmdzqPrVvPwwTWBwg",
    "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
    "ttgJtRGJQctTZtZT",
    "CrZsJsPPZsGzwwsLwLmpwMDw"
]


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        mid = int(len(line) / 2)
        processed_input.append((
            list(set(line[:mid]).intersection(set(line[mid:])))
        ))
    return processed_input


def score(character):
    values = [""] + [chr(x) for x in range(97, 123)] + [chr(x) for x in range(65, 91)]
    return values.index(character)


def solution1(problem=puzzle_input):
    return sum([score(x[0]) for x in process_input(problem)])
    

def solution2(problem=puzzle_input):
    return sum([
        score(x[0]) for x in [
            list(set(problem[index]).intersection(set(problem[index+1])).intersection(set(problem[index+2])))
            for index in range(0, len(problem), 3)
        ]
    ])


print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
