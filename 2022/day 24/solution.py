
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


class BlizzardMap:

    def __init__(self, lines):
        self.height = len(lines)
        self.width = len(lines[0])
        self.blizzards = {}
        self.start_position = (1, 0)
        self.end_position = (self.width-2, self.height - 1)
        self.walls = {}

        for y in range(self.height):
            for x in range(self.width):
                c = lines[y][x]
                if c == ".":
                    continue
                if c == "#":
                    self.walls[(x, y)] = c
                    continue
                self.blizzards[(x, y)] = [c]

    def move_blizzard(self, x, y, direction):
        new_position = x, y
        if direction == ">":
            new_position = (x+1, y)
        if direction == "v":
            new_position = (x, y+1)
        if direction == "<":
            new_position = (x-1, y)
        if direction == "^":
            new_position = (x, y-1)

        if new_position[0] == 0:
            return self.width - 2, new_position[1]
        if new_position[0] == self.width - 1:
            return 1, new_position[1]
        if new_position[1] == 0:
            return new_position[0], self.height - 2
        if new_position[1] == self.height - 1:
            return new_position[0], 1

        return new_position

    def move_blizzards(self):
        new_blizzards = {}
        for position, blizzards in self.blizzards.items():
            for blizzard in blizzards:
                new_position = self.move_blizzard(*position, blizzard)
                if new_position not in new_blizzards:
                    new_blizzards[new_position] = []
                new_blizzards[new_position].append(blizzard)
        self.blizzards = new_blizzards

    def get_adjacent(self, x, y):
        options = [
            (x + 1, y), (x, y + 1), (x - 1, y), (x, y - 1), (x, y)
        ]

        return [
            option for option in options
            if 0 <= option[0] < self.width and 0 <= option[1] < self.height and option not in self.walls
        ]

    def prune(self, positions):
        pruned = []
        for position in positions:
            if position not in self.blizzards:
                pruned.append(position)
        return pruned

    def search(self):
        steps = 0
        positions = [self.start_position]
        while self.end_position not in positions and len(positions) > 0:
            new_positions = []

            for position in positions:
                new_positions += self.get_adjacent(*position)

            self.move_blizzards()

            positions = set(self.prune(new_positions))

            steps += 1

        return steps

    def to_back_and_to_again(self):
        steps = 0
        steps += self.search()

        # Swap start and end
        tmp = self.end_position
        self.end_position = self.start_position
        self.start_position = tmp
        steps += self.search()

        # Swap start and end
        tmp = self.end_position
        self.end_position = self.start_position
        self.start_position = tmp
        steps += self.search()

        return steps

    def print(self, positions=()):
        for y in range(self.height):
            line = []
            for x in range(self.width):
                if (x, y) in positions:
                    line.append("o")
                    continue
                if (x, y) == self.start_position:
                    line.append("S")
                    continue
                if (x, y) == self.end_position:
                    line.append("E")
                    continue
                if (x, y) in self.walls:
                    line.append("#")
                    continue
                if (x, y) in self.blizzards:
                    b = self.blizzards[(x, y)]
                    line.append(b[0] if len(b) == 1 else "X")
                    continue

                line.append(".")
            print("".join(line))


def solution1(problem=puzzle_input):
    blizzard_map = BlizzardMap(process_input(problem))
    return blizzard_map.search()
    

def solution2(problem=puzzle_input):
    blizzard_map = BlizzardMap(process_input(problem))
    return blizzard_map.to_back_and_to_again()
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
