
from util.util import get_input
import numpy as np


puzzle_input = get_input()
test_input = [
    "30373",
    "25512",
    "65332",
    "33549",
    "35390"
]


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            [int(x) for x in line]
        )
    return processed_input, len(processed_input[0]), len(processed_input)


def look_over_rows(layout, width, height):
    visible = set([])

    # Check rows
    for j in range(height):
        max_row = -1
        max_reversed = -1
        reverse = list(reversed(layout[j]))
        for i in range(width):
            if layout[j][i] > max_row:
                visible.add((i, j))
                max_row = layout[j][i]
            if reverse[i] > max_reversed:
                visible.add((width - i - 1, j))
                max_reversed = reverse[i]

    return visible


def look_over_columns(layout, width, height):
    visible = set([])

    # Check columns
    for i in range(width):
        max_column = -1
        max_reversed = -1
        for j in range(height):
            if layout[j][i] > max_column:
                visible.add((i, j))
                max_column = layout[j][i]
            if layout[height - j - 1][i] > max_reversed:
                visible.add((i, height - j - 1))
                max_reversed = layout[height - j - 1][i]

    return visible


def find_visible_trees(layout, width, height):
    return look_over_rows(layout, width, height).union(look_over_columns(layout, height, width))


def solution1(problem=puzzle_input):
    return len(find_visible_trees(*process_input(problem)))


def scenic_score(layout, x, y, width, height):
    scores = []

    # To the right
    index = x + 1
    view = 0
    while x < index < width:
        view += 1
        if layout[y][index] >= layout[y][x]:
            break
        index += 1
    scores.append(view)

    # To the left
    index = x - 1
    view = 0
    while -1 < index < x:
        view += 1
        if layout[y][index] >= layout[y][x]:
            break
        index -= 1
    scores.append(view)

    # To the top
    index = y - 1
    view = 0
    while -1 < index < y:
        view += 1
        if layout[index][x] >= layout[y][x]:
            break
        index -= 1
    scores.append(view)

    # To the bottom
    index = y + 1
    view = 0
    while y < index < height:
        view += 1
        if layout[index][x] >= layout[y][x]:
            break
        index += 1
    scores.append(view)

    return np.prod(scores)


def solution2(problem=puzzle_input):
    layout, width, height = process_input(problem)
    max_scenic_score = 0
    max_position = None
    for y in range(height):
        for x in range(width):
            score = scenic_score(layout, x, y, width, height)
            if score > max_scenic_score:
                max_scenic_score = score
                max_position = (x, y)
    return max_scenic_score, max_position
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
