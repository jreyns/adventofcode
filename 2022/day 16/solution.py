
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        values = [
            x
            for x in line.replace("Valve ", "").replace(" has flow rate=", ",")
                .replace("; tunnels lead to valves ", ",").replace("; tunnel leads to valve ", ",").replace(", ", ",")
            .split(",")
        ]
        processed_input.append(
            [values[0], int(values[1]), values[2:]]
        )
    return processed_input


class Valve:

    def __init__(self, name, flow, connections):
        self.name = name
        self.flow = flow
        self.connections = connections


class ValveMap:

    def __init__(self, valves):
        self.valves = {
           n: Valve(n, f, c) for n, f, c in valves
        }
        self.not_zero = [
            n for n in self.valves if self.valves[n].flow > 0
        ]
        self.steps_needed = {
            n: {
                v: self.time_to_move(n, v)
                for v in self.not_zero
            }
            for n in self.valves
        }

    def time_to_move(self, from_node, to_node):
        if from_node == to_node:
            return 0

        passed = set([from_node])

        time = 1
        options = self.valves[from_node].connections
        while options:
            new_options = []
            for option in options:
                if option in passed:
                    continue
                if option == to_node:
                    return time

                passed.add(option)
                new_options += self.valves[option].connections
            options = new_options
            time += 1
        return 0

    def max_cost_to_move(self, from_node, to_node):
        return self.valves[to_node].flow * self.time_to_move(from_node, to_node)

    def calculate_best_valves(self, start="AA", time_steps=30, open_valves=[]):
        if time_steps <= 0:
            return 0, []

        max_score = 0
        max_path = []

        for option in self.not_zero:
            if option == start or option in open_valves:
                continue

            time = self.steps_needed[start][option]

            score = self.valves[option].flow * (time_steps - (time + 1))
            s, p = self.calculate_best_valves(option, time_steps - (time + 1), open_valves + [option])
            if (score + s) > max_score:
                max_score = score + s
                max_path = [option] + p

        return max_score, max_path

    def calculate_best_valves_multiple_players(self, positions=[("AA", 26), ("AA", 26)], open_valves=[], steps=0):
        i = steps % len(positions)
        start, time_steps = positions[i]

        if time_steps <= 0 or len(open_valves) == len(self.not_zero):
            return 0, []

        max_score = 0
        max_path = []
        for option in self.not_zero:
            if option == start or option in open_valves:
                continue

            time = self.steps_needed[start][option]
            if time >= time_steps:
                continue

            score = self.valves[option].flow * (time_steps - (time + 1))
            s, p = self.calculate_best_valves_multiple_players(
                positions[:i] + [(option, time_steps - (time + 1))] + positions[i+1:],
                open_valves + [option],
                steps + 1
            )
            if (score + s) > max_score:
                max_score = score + s
                max_path = [option] + p
            if steps == 0:
                print(f"current max: {max_score}, {max_path}")

        return max_score, max_path


def solution1(problem=puzzle_input):
    valves = ValveMap(process_input(problem))
    max_score, best_path = valves.calculate_best_valves()
    print(best_path)
    return max_score
    

def solution2(problem=puzzle_input):
    valves = ValveMap(process_input(problem))
    max_score, best_path = valves.calculate_best_valves_multiple_players()
    print(best_path)
    return max_score
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
