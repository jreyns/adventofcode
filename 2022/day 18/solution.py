
from util.util import get_input, get_adjacent_vector


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            [int(x) for x in line.split(",")]
        )
    return processed_input


class LavaMap:

    def __init__(self, cubes):
        self.cubes = {
            tuple(c): 1
            for c in cubes
        }

    def find_sides(self):
        total_sides = 0
        for cube in self.cubes:
            options = get_adjacent_vector(cube)
            total_sides += len(options) - sum([self.cubes.get(option, 0) for option in options])
        return total_sides

    def filter_cubes(self, cubes: list, seen: dict):
        filtered = {}
        for cube in cubes:
            if cube in self.cubes or cube in seen:
                continue

            options = get_adjacent_vector(cube)
            for option in options:
                if option in self.cubes:
                    filtered[cube] = filtered.get(cube, 0) + 1
        return filtered

    def find_encapsulated_sides(self, print_trace=False):
        start = sorted(self.cubes)[0]
        seen = {}
        filtered: dict = self.filter_cubes(get_adjacent_vector(start), seen)
        options = filtered.items()
        while options:
            new_options = []
            for option, sides in options:
                seen[option] = sides
                neighbours = []
                for position in get_adjacent_vector(option):
                    if position in self.cubes:
                        continue
                    neighbours += [position] + get_adjacent_vector(position)
                filtered: dict = self.filter_cubes(list(set(neighbours)), seen)
                new_options += list(filtered.items())

            options = list(set(new_options))

        if print_trace:
            self.print_chunk(self.cubes, seen)
        return sum(seen.values())

    @staticmethod
    def print_chunk(chunk, trace=None):
        x_range = min([c[0] for c in chunk])-1, max([c[0] for c in chunk])+2
        y_range = min([c[1] for c in chunk])-1, max([c[1] for c in chunk])+2
        z_range = min([c[2] for c in chunk])-1, max([c[2] for c in chunk])+2

        print("#####################")
        print(chunk)
        for z in range(*z_range):
            print(f"--- {z} ---")
            for y in range(*y_range):
                line = []
                for x in range(*x_range):
                    line.append(" #"[(x, y, z) in chunk])
                    if trace and (x, y, z) in trace:
                        line[-1] = str(trace[(x, y, z)])
                print("".join(line))
        print("#####################")


def solution1(problem=puzzle_input):
    lava_map = LavaMap(process_input(problem))
    return lava_map.find_sides()
    

def solution2(problem=puzzle_input, print_trace=False):
    lava_map = LavaMap(process_input(problem))
    return lava_map.find_encapsulated_sides(print_trace)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
