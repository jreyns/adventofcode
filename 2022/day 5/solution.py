
from util.util import get_input


puzzle_input = get_input()
test_input = [
    "    [D]    ",
    "[N] [C]    ",
    "[Z] [M] [P]",
    " 1   2   3 ",
    "",
    "move 1 from 2 to 1",
    "move 3 from 1 to 3",
    "move 2 from 2 to 1",
    "move 1 from 1 to 2"
]


def process_input(lines=puzzle_input):
    processed_input = [[], []]
    index = 0
    for line in lines:
        if line == "":
            index = 1
            continue
        processed_input[index].append(
            line
        )
    return processed_input


class ShipCrane:

    def __init__(self, stacks, moves):
        self._positions = None
        self._stacks = None
        self._moves = None

        self._parse_stacks(stacks)
        self._parse_moves(moves)

    def _parse_stacks(self, stacks):
        self._positions = [(int(c), i) for i, c in enumerate(stacks[-1]) if c != " "]
        self._stacks = {k: [] for k, _ in self._positions}

        for line in reversed(stacks[:-1]):
            for c, i in self._positions:
                if line[i] != " ":
                    self._stacks[c].append(line[i])

    def _parse_moves(self, moves):
        self._moves = []
        for line in moves:
            self._moves.append([
                int(c) for c in
                line.replace("move ", "")
                    .replace(" from ", " ")
                    .replace(" to ", " ")
                    .split(" ")
                ]
            )

    def _do_move(self, index, group):
        amount, from_stack, to_stack = self._moves[index]
        if group:
            values = self._stacks[from_stack][-amount:]
            self._stacks[from_stack] = self._stacks[from_stack][:-amount]
            self._stacks[to_stack] = self._stacks[to_stack] + values
        else:
            for _ in range(amount):
                value = self._stacks[from_stack].pop()
                self._stacks[to_stack].append(value)

    def play_moves(self, group=False):
        for i in range(len(self._moves)):
            self._do_move(i, group)

    def read_stacks(self):
        return "".join([self._stacks[i][-1] for i in sorted(self._stacks.keys())])


def solution1(problem=puzzle_input):
    crane = ShipCrane(*process_input(problem))
    crane.play_moves()
    return crane.read_stacks()
    

def solution2(problem=puzzle_input):
    crane = ShipCrane(*process_input(problem))
    crane.play_moves(True)
    return crane.read_stacks()
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
