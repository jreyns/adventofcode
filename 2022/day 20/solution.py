
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            int(line)
        )
    return processed_input


def mix_number(number, numbers):
    if number[1] == 0:
        return numbers

    index = numbers.index(number)
    del numbers[index]
    index = (index + number[1]) % len(numbers)
    return numbers[:index] + [number] + numbers[index:]


def mix_numbers(numbers, amount = 1):
    numbers = [(i, x) for i, x in enumerate(numbers)]
    mixed = [x for x in numbers]
    for _ in range(amount):
        for n in numbers:
            mixed = mix_number(n, mixed)
    return [x[1] for x in mixed]


def solution1(problem=puzzle_input):
    mixed = mix_numbers(process_input(problem))
    total = 0
    index_offset = mixed.index(0)
    for i in [1000, 2000, 3000]:
        index = i + index_offset
        total += mixed[index % len(mixed)]
    return total
    

def solution2(problem=puzzle_input):
    mixed = [811589153 * x for x in process_input(problem)]
    mixed = mix_numbers(mixed, 10)
    total = 0
    index_offset = mixed.index(0)
    for i in [1000, 2000, 3000]:
        index = i + index_offset
        total += mixed[index % len(mixed)]
    return total
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
