
from util.util import get_input


puzzle_input = get_input()
test_input = [
    "mjqjpqmgbljsphdztnvjfqwrcgsmlb",
    "bvwbjplbgvbhsrlpgdmjqwftvncz",
    "nppdvjthqldpwncqszvftbrmjlhg",
    "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg",
    "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw",
]


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


def analyse_stream(stream, offset=4):
    for end in range(offset, len(stream)):
        if len(set(stream[end-offset:end])) == offset:
            return end
    return None


def solution1(problem=puzzle_input):
    outputs = []
    for stream in problem:
        outputs.append(analyse_stream(stream))
    return outputs
    

def solution2(problem=puzzle_input):
    outputs = []
    for stream in problem:
        outputs.append(analyse_stream(stream, 14))
    return outputs
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
