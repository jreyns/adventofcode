
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        monkey, problem = line.split(":")
        processed_input.append(
            (monkey, problem.strip().split(" "))
        )
    return {m: p for m, p in processed_input}


def calculate(node, lookup):
    operation = lookup[node]

    if len(operation) == 1:
        return int(operation[0])

    op1, op, op2 = operation
    if op == "+":
        return calculate(op1, lookup) + calculate(op2, lookup)
    if op == "-":
        return calculate(op1, lookup) - calculate(op2, lookup)
    if op == "*":
        return calculate(op1, lookup) * calculate(op2, lookup)
    if op == "/":
        return calculate(op1, lookup) / calculate(op2, lookup)


def search(node, lookup, guess):
    if node == "humn":
        return guess

    operation = lookup[node]

    if len(operation) == 1:
        return int(operation[0])

    op1, op, op2 = operation

    if node == "root":
        right = search(op2, lookup, guess)
        left = search(op1, lookup, guess)
        return right == left, left, right

    r1, r2 = search(op1, lookup, guess), search(op2, lookup, guess)
    equation = f"{r1} {op} {r2}"
    return eval(equation)


def solution1(problem=puzzle_input):
    return calculate("root", process_input(problem))
    

def solution2(problem=puzzle_input):
    lower_bound = 0
    upper_bound = 1e16
    nodes = process_input(problem)
    _, first_guess, _ = search("root", nodes, 1)
    _, second_guess, _ = search("root", nodes, 2)
    reverse = first_guess < second_guess
    while True:
        guess = (upper_bound + lower_bound) // 2
        equals, left, right = search("root", nodes, guess)

        if equals:
            return guess

        if left > right:
            if reverse:
                upper_bound = guess
            else:
                lower_bound = guess
        elif right > left:
            if reverse:
                lower_bound = guess
            else:
                upper_bound = guess
    

print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
