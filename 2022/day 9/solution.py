
from util.util import get_input, get_adjacent_coordinates, euclidean_distance


puzzle_input = get_input()
test_input = [
    "R 4",
    "U 4",
    "L 3",
    "D 1",
    "R 4",
    "D 1",
    "L 5",
    "R 2"
]


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line.split(" ")
        )
    return processed_input


class Rope:

    moves = {
        "R": lambda x, y: (x+1, y),
        "L": lambda x, y: (x-1, y),
        "U": lambda x, y: (x, y+1),
        "D": lambda x, y: (x, y-1),
    }

    def __init__(self, knots=1):
        self.knots = knots
        self.position = [(0, 0)] * (knots + 1)
        self.tail_positions = [(0, 0)]

    def move(self, direction, amount):
        for _ in range(int(amount)):
            self.position[0] = self.moves[direction](*self.position[0])
            for i in range(self.knots):
                self._move_tail(i+1)

    def _move_tail(self, index):
        # Shorter notations
        head = self.position[index-1]
        tail = self.position[index]

        # Calculate the neighbouring positions
        neighbours = get_adjacent_coordinates(*tail, None, True)

        # Don't do anything, means tail is still touching the head
        if head == tail or head in neighbours:
            return

        # Move to the closest spot next to the head
        best_move = None
        min_distance = float('inf')
        for n in neighbours:
            distance = euclidean_distance(n, head)
            if distance < min_distance:
                min_distance = distance
                best_move = n

        # Update position of the tail
        self.position[index] = best_move
        if index == self.knots:
            self.tail_positions.append(best_move)


def solution1(problem=puzzle_input):
    moves = process_input(problem)
    rope = Rope()
    for move in moves:
        rope.move(*move)
    return len(set(rope.tail_positions))
    

def solution2(problem=puzzle_input):
    moves = process_input(problem)
    rope = Rope(9)
    for move in moves:
        rope.move(*move)
    return len(set(rope.tail_positions))
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
