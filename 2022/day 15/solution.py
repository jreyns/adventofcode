
from util.util import get_input, manhattan_distance


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        values = [
            int(x)
            for x in line.replace("Sensor at x=", "").replace(" y=", "").replace(": closest beacon is at x=", ",")
            .split(",")
        ]
        processed_input.append(
            [tuple(values[:2]), tuple(values[2:]), manhattan_distance(tuple(values[:2]),  tuple(values[2:]))]
        )
    return processed_input


class Sensors:

    def __init__(self, positions):
        self.sensors = positions
        self.beacons = {sensor[1]: 1 for sensor in self.sensors}

    def find_matching(self, y):
        points = set([])
        for sensor in self.sensors:
            self._find_matching_for_single_sensor(points, sensor, y)
        return points

    def _find_matching_for_single_sensor(self, points, sensor, y):
        sensor_position = sensor[0]
        max_distance = sensor[-1]
        x_diff = 0
        current = (sensor_position[0], y)

        while manhattan_distance(sensor_position, current) <= max_distance:
            if current not in self.beacons:
                points.add(current)
            if x_diff > 0:
                left = (sensor_position[0] - x_diff, y)
                if left not in self.beacons:
                    points.add(left)

            x_diff += 1
            current = (sensor_position[0] + x_diff, y)

        return None

    def _find_bound_for_single_sensor(self, sensor, bound):
        sensor_position = sensor[0]
        max_distance = sensor[-1]
        points = set([])
        for i in range(max_distance + 2):
            options = [
                (sensor_position[0] - i, sensor_position[1] + (max_distance + 1) - i),
                (sensor_position[0] - i, sensor_position[1] - (max_distance + 1) + i),
                (sensor_position[0] + i, sensor_position[1] + (max_distance + 1) - i),
                (sensor_position[0] + i, sensor_position[1] - (max_distance + 1) + i)
            ]

            for option in options:
                if option not in self.beacons \
                        and 0 <= option[0] <= bound \
                        and 0 <= option[1] <= bound:
                    points.add(option)

        return points

    def is_find_unmapped(self, x, y):
        if (x, y) in self.beacons:
            return False
        for sensor in self.sensors:
            if manhattan_distance((x, y), sensor[0]) <= sensor[-1]:
                return False
        return True

    def find_unmapped(self, bound):
        positions = set([])
        for sensor in self.sensors:
            positions = positions.union(self._find_bound_for_single_sensor(sensor, bound))
        return positions


def solution1(problem=puzzle_input, y=10):
    sensors = Sensors(process_input(problem))
    return len(sensors.find_matching(y))


def solution2(problem=puzzle_input, bound=20):
    sensors = Sensors(process_input(problem))
    positions = list(sensors.find_unmapped(bound))
    for position in positions:
        if sensors.is_find_unmapped(*position):
            return position[0] * 4000000 + position[1]
    return 0


print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")

print(f"solution 1: {solution1(y=2000000)}")
print(f"solution 2: {solution2(bound=4000000)}")
