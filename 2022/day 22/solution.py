
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


class MonkeyMap:

    moves = {
        ">": (1, 0),
        "<": (-1, 0),
        "^": (0, -1),
        "V": (0, 1)
    }

    turn = {
        ">": lambda x: "^" if x == "L" else "V",
        "<": lambda x: "V" if x == "L" else "^",
        "^": lambda x: "<" if x == "L" else ">",
        "V": lambda x: ">" if x == "L" else "<"
    }

    score = [">", "V", "<", "^"]

    def __init__(self, lines, width=4):
        # Hardcoded Faces:
        if width == 4:
            self.faces = {
                # Face 1
                ((11, 0), (11, 3)): {">": ((15, 11), (15, 8))},
                ((8, 3), (11, 3)): {"V": ((8, 4), (11, 4))},
                ((8, 0), (8, 3)): {"<": ((4, 4), (7, 4))},
                ((8, 0), (11, 0)): {"^": ((3, 3), (0, 3))},
                # Face 5
                ((11, 8), (11, 11)): {">": ((15, 11), (15, 8))},
                ((8, 11), (11, 11)): {"V": ((8, 4), (11, 4))},
                ((8, 8), (8, 11)): {"<": ((4, 4), (7, 4))},
                ((8, 8), (11, 8)): {"^": ((3, 3), (0, 3))},
                # Face 2
                ((3, 4), (3, 7)): {">": ((4, 4), (4, 7))},
                ((0, 4), (0, 7)): {"<": ((15, 11), (12, 11))},
                # Face 4
                ((11, 4), (11, 7)): {">": ((15, 8), (12, 8))},
                ((8, 4), (8, 7)): {"<": ((7, 4), (7, 7))},
            }
        else:
            self.faces = {
                1: ([50, 0, 99, 49], []),
                2: ([100, 0, 149, 49], []),
                3: ([50, 50, 99, 99], []),
                4: ([0, 100, 49, 149], []),
                5: ([50, 100, 99, 149], []),
                6: ([0, 150, 49, 199], [])
            }

        # Path and direction
        r_splitted = lines[-1].split("R")
        self.path = []
        for r in r_splitted:
            l_splitted = [int(x) for x in r.split("L")]
            for l in l_splitted:
                self.path.append(l)
                self.path.append("L")

            self.path[-1] = "R"
        del self.path[-1]

        self.direction = ">"

        # Map info
        lines = lines[:-1]
        self.map = {}
        self.height = len(lines)
        self.width = max([len(x) for x in lines])
        self.rows = {i: [None, None] for i in range(self.height)}
        self.columns = {i: [None, None] for i in range(self.width)}

        for y, line in enumerate(lines):
            for x, c in enumerate(line):
                if lines[y][x] == " ":
                    continue

                self.map[(x, y)] = c

                if self.rows[y][0] is None:
                    self.rows[y][0] = x
                else:
                    self.rows[y][1] = x

                if self.columns[x][0] is None:
                    self.columns[x][0] = y
                else:
                    self.columns[x][1] = y

        # Starting position
        self.position = (self.rows[0][0], 0)
        self.map[self.position] = self.direction

    def do_moves(self):
        for move in self.path:
            if move in ["L", "R"]:
                self.direction = self.turn[self.direction](move)
                self.map[self.position] = self.direction
                continue

            current_move = self.moves[self.direction]
            for _ in range(move):
                new_position = self.position[0] + current_move[0], self.position[1] + current_move[1]
                point = self.map.get(new_position, " ")

                if point == " " and self.direction == ">":
                    new_position = (self.rows[new_position[1]][0], new_position[1])
                if point == " " and self.direction == "<":
                    new_position = (self.rows[new_position[1]][1], new_position[1])
                if point == " " and self.direction == "^":
                    new_position = (new_position[0], self.columns[new_position[0]][1])
                if point == " " and self.direction == "V":
                    new_position = (new_position[0], self.columns[new_position[0]][0])

                point = self.map.get(new_position, " ")
                if point == "#":
                    break

                self.position = new_position
                self.map[self.position] = self.direction
        return (self.position[1] + 1) * 1000 + (self.position[0] + 1) * 4 + self.score.index(self.direction)

    def do_moves_with_faces(self):
        for move in self.path:
            if move in ["L", "R"]:
                self.direction = self.turn[self.direction](move)
                self.map[self.position] = self.direction
                continue

            current_move = self.moves[self.direction]
            for _ in range(move):
                new_position = self.position[0] + current_move[0], self.position[1] + current_move[1]
                point = self.map.get(new_position, " ")

                if point == " " and self.direction == ">":
                    new_position = (self.rows[new_position[1]][0], new_position[1])
                if point == " " and self.direction == "<":
                    new_position = (self.rows[new_position[1]][1], new_position[1])
                if point == " " and self.direction == "^":
                    new_position = (new_position[0], self.columns[new_position[0]][1])
                if point == " " and self.direction == "V":
                    new_position = (new_position[0], self.columns[new_position[0]][0])

                point = self.map.get(new_position, " ")
                if point == "#":
                    break

                self.position = new_position
                self.map[self.position] = self.direction
        return (self.position[1] + 1) * 1000 + (self.position[0] + 1) * 4 + self.score.index(self.direction)

    def print(self):
        print(self.path)
        for y in self.rows:
            line = []
            for x in self.columns:
                line.append(self.map.get((x, y), " "))
            print("".join(line))


def solution1(problem=puzzle_input):
    monkey_map = MonkeyMap(process_input(problem))
    score = monkey_map.do_moves()
    return score
    

def solution2(problem=puzzle_input, width=4):
    monkey_map = MonkeyMap(process_input(problem), width)
    monkey_map.print()
    score = monkey_map.do_moves_with_faces()
    return score
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
# print(f"solution 2: {solution2(width=50)}")
