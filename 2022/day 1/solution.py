from util.util import get_input


puzzle_input = get_input()


def get_all_calories():
    current_calories = 0
    all_calories = []
    for calories in puzzle_input:
        if calories == '':
            all_calories.append(current_calories)
            current_calories = 0
            continue
        current_calories += int(calories)
    return all_calories


def solution1():
    calories = get_all_calories()
    return max(calories)


def solution2():
    calories = get_all_calories()
    return sum(sorted(calories)[-3:])


print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
