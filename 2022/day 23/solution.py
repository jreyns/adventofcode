
from util.util import get_input, get_adjacent_coordinates


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


class ElfMap:

    def __init__(self, lines):
        self.x_bounds = [0, len(lines[0]) - 1]
        self.y_bounds = [0, len(lines) - 1]
        self.positions = {}
        self.moves = [self.move_north, self.move_south, self.move_west, self.move_east]

        for y, line in enumerate(lines):
            for x, c in enumerate(line):
                if c == "#":
                    self.positions[(x, y)] = 1

    def occupied(self, positions):
        for position in positions:
            if position in self.positions:
                return True
        return False

    def move_north(self, x, y, moves, new_options):
        # If there is no Elf in the N, NE, or NW adjacent positions, the Elf proposes moving north one step.
        if not self.occupied([(x - 1, y - 1), (x, y - 1), (x + 1, y - 1)]):
            moves[(x, y)] = (x, y - 1)
            new_options[(x, y - 1)] = new_options.get((x, y - 1), 0) + 1
            return True
        return False

    def move_south(self, x, y, moves, new_options):
        # If there is no Elf in the S, SE, or SW adjacent positions, the Elf proposes moving south one step.
        if not self.occupied([(x - 1, y + 1), (x, y + 1), (x + 1, y + 1)]):
            moves[(x, y)] = (x, y + 1)
            new_options[(x, y + 1)] = new_options.get((x, y + 1), 0) + 1
            return True
        return False

    def move_west(self, x, y, moves, new_options):
        # If there is no Elf in the W, NW, or SW adjacent positions, the Elf proposes moving west one step.
        if not self.occupied([(x - 1, y - 1), (x - 1, y), (x - 1, y + 1)]):
            moves[(x, y)] = (x - 1, y)
            new_options[(x - 1, y)] = new_options.get((x - 1, y), 0) + 1
            return True
        return False

    def move_east(self, x, y, moves, new_options):
        # If there is no Elf in the E, NE, or SE adjacent positions, the Elf proposes moving east one step.
        if not self.occupied([(x + 1, y - 1), (x + 1, y), (x + 1, y + 1)]):
            moves[(x, y)] = (x + 1, y)
            new_options[(x + 1, y)] = new_options.get((x + 1, y), 0) + 1
            return True
        return False

    def select_move(self, *args):
        for f in self.moves:
            if f(*args):
                return

    def move(self):
        move_counts = 0
        moves = {}
        new_options = {}
        new_positions = {}

        for x, y in self.positions:
            # If no other Elves are in one of those eight positions, the Elf does not do anything during this round
            if not self.occupied(get_adjacent_coordinates(x, y, None, True)):
                continue

            self.select_move(x, y, moves, new_options)

        self.moves = self.moves[1:] + [self.moves[0]]

        for position in self.positions:
            move = moves.get(position, position)
            if new_options.get(move, 0) == 1:
                new_positions[move] = 1
                move_counts += 1
            else:
                new_positions[position] = 1

        x, y = list(new_positions.keys())[0]
        self.x_bounds = [x, x]
        self.y_bounds = [y, y]
        for x, y in new_positions:
            self.x_bounds = [min(self.x_bounds[0], x), max(self.x_bounds[1], x)]
            self.y_bounds = [min(self.y_bounds[0], y), max(self.y_bounds[1], y)]

        self.positions = new_positions
        return move_counts

    def score(self):
        total = 0
        y1, y2 = self.y_bounds
        x1, x2 = self.x_bounds
        for y in range(y1, y2 + 1):
            for x in range(x1, x2 + 1):
                if (x, y) not in self.positions:
                    total += 1
        return total

    def print(self):
        y1, y2 = self.y_bounds
        x1, x2 = self.x_bounds
        print("-" * (self.x_bounds[1] - self.x_bounds[0] + 1))
        for y in range(y1, y2 + 1):
            line = []
            for x in range(x1, x2 + 1):
                line.append(
                    "#" if (x, y) in self.positions else "."
                )
            print("".join(line))
        print("-" * (self.x_bounds[1] - self.x_bounds[0] + 1))


def solution1(problem=puzzle_input):
    elf_map = ElfMap(process_input(problem))
    for _ in range(10):
        elf_map.move()
    return elf_map.score()
    

def solution2(problem=puzzle_input):
    elf_map = ElfMap(process_input(problem))
    rounds = 1
    moved = elf_map.move()
    while moved:
        rounds += 1
        moved = elf_map.move()
    return rounds
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
