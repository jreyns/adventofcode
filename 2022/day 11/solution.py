
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    monkey_input = []
    for line in lines:
        if line == "":
            processed_input.append(Monkey(*monkey_input))
            monkey_input = []
            continue

        monkey_input.append(line)

    processed_input.append(Monkey(*monkey_input))
    return processed_input


class Monkey:

    def __init__(self, order, items, operation, test, true, false):
        self.inspected = 0
        self.order = int(order.replace("Monkey ", "")[:-1])
        self.items = [int(x) for x in items.replace("  Starting items: ", "").split(", ")]
        self.operation = operation.replace("  Operation: new = ", "")
        _, self.op, self.value = self.operation.split(" ")
        self.divisible_by = int(test.replace(" Test: divisible by ", ""))
        self.true = int(true.replace("    If true: throw to monkey ", ""))
        self.false = int(false.replace("    If false: throw to monkey ", ""))
        self.options = [self.false, self.true]

    def __repr__(self):
        return f"Monkey {self.order}: {self.inspected} -> {self.items}"

    def take_turn(self, monkeys):
        for old in self.items:
            self.inspected += 1
            new = eval(self.operation)
            new = new // 3
            option = (new % self.divisible_by == 0)
            monkeys[self.options[option]].add_item(new)

        self.items = []

    def prepare_unlimited(self, monkey_count):
        self.items = [[x] * monkey_count for x in self.items]

    def eval(self, new):
        if self.op == "+":
            return new % self.divisible_by
        new = new % self.divisible_by
        if not new:
            return self.divisible_by
        return new

    def take_turn_unlimited(self, monkeys):
        for item in self.items:
            self.inspected += 1
            new_item = []
            for i, monkey in enumerate(monkeys):
                old = item[i]
                new_item.append(monkey.eval(eval(self.operation)))
            option = (new_item[self.order] % self.divisible_by == 0)
            monkeys[self.options[option]].add_item(new_item)

        self.items = []

    def add_item(self, item):
        self.items.append(item)


def play_round(monkeys):
    for monkey in monkeys:
        monkey.take_turn(monkeys)


def play_round_unlimited(monkeys):
    for monkey in monkeys:
        monkey.take_turn_unlimited(monkeys)


def play_rounds(monkeys, rounds=20, print_monkeys=False):
    for i in range(rounds):
        play_round(monkeys)
        if print_monkeys and i % 20 == 0:
            print(monkeys)

    counts = sorted([m.inspected for m in monkeys])
    return counts[-2] * counts[-1]


def play_rounds_unlimited(monkeys, rounds=10000, print_monkeys=False):
    for m in monkeys:
        m.prepare_unlimited(len(monkeys))
    for i in range(rounds):
        play_round_unlimited(monkeys)
        if print_monkeys and i % 1000 == 0:
            print(monkeys)

    counts = sorted([m.inspected for m in monkeys])
    return counts[-2] * counts[-1]


def solution1(problem=puzzle_input, print_monkeys=False):
    monkeys = process_input(problem)
    return play_rounds(monkeys, 20, print_monkeys)
    

def solution2(problem=puzzle_input, print_monkeys=False):
    monkeys = process_input(problem)
    return play_rounds_unlimited(monkeys, 10000, print_monkeys)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input, True)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
