
from util.util import get_input
import numpy as np


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    pairs = []
    for line in lines:
        if line == "":
            processed_input.append(
                pairs
            )
            pairs = []
            continue
        pairs.append(eval(line))
    processed_input.append(
        pairs
    )
    return processed_input


def is_smaller(left, right):
    if isinstance(left, int) and isinstance(right, int):
        if left < right:
            return 1
        if left > right:
            return -1
        return 0

    if isinstance(left, int):
        return is_smaller([left], right)

    if isinstance(right, int):
        return is_smaller(left, [right])

    for index in range(max(len(left), len(right))):
        if index >= len(left):
            return 1
        if index >= len(right):
            return -1

        check = is_smaller(left[index], right[index])
        if check == 1:
            return 1
        if check == -1:
            return -1

    return 0


def solution1(problem=puzzle_input):
    all_packets = process_input(problem)
    indexes = []
    for i, packets in enumerate(all_packets):
        check = is_smaller(*packets)
        if check == 1:
            indexes.append(i+1)
    return sum(indexes)
    

def solution2(problem=puzzle_input):
    packets = [eval(line) for line in problem if line != ""]
    divider_packets = [[[2]], [[6]]]
    order = []

    """
        To know the index of a divider packet, you just need to know how many packets are smaller than that packet.
        Divider packets are already sorted so you know that the lower count needs to be incremented by their index,
        because the previous divider packets should be sorted before the current one
    """

    for i, divider_packet in enumerate(divider_packets):
        lower_count = 1 + i
        for packet in packets:
            if is_smaller(packet, divider_packet) == 1:
                lower_count += 1
        order.append(lower_count)
    print(order)
    return np.prod(order)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
