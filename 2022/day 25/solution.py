
from util.util import get_input, sign


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


SNAFU = {
    "=": -2,
    "-": -1,
    "0": 0,
    "1": 1,
    "2": 2
}

inverse_SNAFU = {
    v: k for k, v in SNAFU.items()
}


def snafu_to_decimal(line):
    total = 0
    for i, c in enumerate(reversed(line)):
        factor = pow(5, i)
        total += factor * SNAFU[c]
    return total


def decimal_to_snafu(digit, previous_power=0):
    if digit in inverse_SNAFU:
        return "0" * max(previous_power - 1, 0) + inverse_SNAFU[digit]

    power = 0
    max_previous = 0
    min_previous = 0
    options = [snafu for snafu in SNAFU]
    if sign(digit) == -1:
        options = list(reversed(options))
    while True:
        factor = pow(5, power)
        if 2 * pow(5, power+1) < abs(digit):
            max_previous += 2 * factor
            min_previous += -2 * factor
            power += 1
            continue

        max_current = 0
        min_current = 0
        for snafu in options:
            current = factor * SNAFU[snafu]
            if current == digit:
                return "0" * max(previous_power - power - 1, 0) + snafu + "0" * power

            if sign(digit) == 1 and current + max_previous >= digit:
                return "0" * max(previous_power - power - 1, 0) + snafu + decimal_to_snafu(digit - current, power)
            if sign(digit) == -1 and current + min_previous <= digit:
                return "0" * max(previous_power - power - 1, 0) + snafu + decimal_to_snafu(digit - current, power)

            if current > max_current:
                max_current = current
            if current < min_current:
                min_current = current

        max_previous += max_current
        min_previous += min_current
        power += 1


def solution1(problem=puzzle_input):
    total = 0
    for line in process_input(problem):
        total += snafu_to_decimal(line)
    return decimal_to_snafu(total)
    

def solution2(problem=puzzle_input):
    return
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
