
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input += [x for x in line]
    return processed_input


class Cave:

    shapes = [
        lambda y: [(2, y), (3, y), (4, y), (5, y)],
        lambda y: [(3, y + 2), (2, y + 1), (3, y + 1), (4, y + 1), (3, y)],
        lambda y: [(4, y + 2), (4, y + 1), (2, y), (3, y), (4, y)],
        lambda y: [(2, y + 3), (2, y + 2), (2, y + 1), (2, y)],
        lambda y: [(2, y + 1), (3, y + 1), (2, y), (3, y)]
    ]

    directions = {
        "<": (-1, 0),
        ">": (1, 0)
    }

    def __init__(self, gas_pattern, width=7):
        self.width = width
        self.rock_steps = 0
        self.steps = 0
        self.map = {}
        self.gas_pattern = gas_pattern
        self.gas_pattern_length = len(gas_pattern)
        self.max_y = 0

    def move(self, positions, vector):
        hit_wall = False
        hit_floor = False
        new_positions = []
        for position in positions:
            new_position = tuple([position[i] + vector[i] for i in range(len(position))])
            if new_position[0] < 0 or new_position[0] >= self.width:
                hit_wall = True
            if new_position in self.map or new_position[1] <= 0:
                hit_floor = True
            new_positions.append(new_position)
        return hit_wall, hit_floor, new_positions

    def drop_rock(self, show=False):
        positions = self.shapes[self.rock_steps % 5](self.max_y + 4)
        if show:
            self.print(positions)
        while True:
            # Follow jet of gas
            direction = self.directions[self.gas_pattern[self.steps % self.gas_pattern_length]]
            self.steps += 1
            hit_wall, hit_floor, new_positions = self.move(positions, direction)

            if not hit_wall and not hit_floor:
                positions = new_positions

            # Move down
            _, hit_floor, new_positions = self.move(positions, (0, -1))
            if hit_floor:
                break

            positions = new_positions
            if show:
                self.print(positions)

        self.max_y = max([self.max_y] + [position[1] for position in positions])
        for position in positions:
            self.map[position] = 1

        if show:
            self.print()
        self.rock_steps += 1
        return tuple([p[0] for p in positions])

    def count_till_repeats(self):
        # Run enough times until we assume it repeats
        for _ in range(10000):
            self.drop_rock()

        current_cycle = self.steps % self.gas_pattern_length
        while not self.steps % self.gas_pattern_length < current_cycle:
            self.drop_rock()

        rock = self.rock_steps % 5
        rocks_cycle_start = self.rock_steps
        cycle_start = self.steps % self.gas_pattern_length
        max_y = self.max_y

        self.drop_rock()
        remainders = [0, self.max_y - max_y]

        while not(self.rock_steps % 5 == rock and self.steps % self.gas_pattern_length == cycle_start):
            self.drop_rock()
            remainders.append(self.max_y - max_y)

        return rocks_cycle_start, cycle_start, max_y, remainders

    def print(self, positions=()):
        print((self.gas_pattern_length, self.rock_steps % self.gas_pattern_length, self.max_y, self.rock_steps))
        print("-" * self.width)
        max_y = max([self.max_y] + [y for x, y in positions])
        for y in range(max_y, 0, -1):
            line = []
            for x in range(self.width):
                if (x, y) in self.map:
                    line.append("#")
                    continue

                if (x, y) in positions:
                    line.append("@")
                    continue

                line.append(".")
            print("".join(line))
        print("-" * self.width)


def solution1(problem=puzzle_input):
    cave = Cave(process_input(problem))
    for _ in range(2022):
        cave.drop_rock()
    return cave.max_y
    

def solution2(problem=puzzle_input):
    cave = Cave(process_input(problem))
    rocks_cycle_start, cycle_start, max_y, y_positions_per_loop = cave.count_till_repeats()
    cycle_height_length = cave.max_y - max_y
    cycle_rock_length = cave.rock_steps - rocks_cycle_start
    rocks = (1000000000000 - rocks_cycle_start) // cycle_rock_length
    remainder = 1000000000000 - rocks_cycle_start - (rocks * cycle_rock_length)
    max_y = max_y + (rocks * cycle_height_length)
    return max_y + y_positions_per_loop[remainder]
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
