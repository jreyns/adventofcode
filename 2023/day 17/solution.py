
import heapq
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            [int(x) for x in line]
        )
    return processed_input


class HeatMap:

    def __init__(self, lines):
        self.map = lines
        self.shape = (len(self.map[0]), len(self.map))

    def valid_point(self, x, y):
        return 0 <= x < self.shape[0] and 0 <= y < self.shape[1]

    def get_directions(self, x, y, dx, dy, count, min_count, max_count):
        directions = abs(dx) * [(0, -1), (0, 1)] + abs(dy) * [(1, 0), (-1, 0)]

        options = []

        # Allowed to continue in current direction
        if count < max_count and self.valid_point(x + dx, y + dy):
            options.append((self.map[y + dy][x + dx], (x + dx, y + dy), (dx, dy), count + 1))

        if count < min_count:
            return options

        for dx1, dy1 in directions:
            x1, y1 = x + dx1, y + dy1
            if self.valid_point(x1, y1):
                options.append((self.map[y1][x1], (x1, y1), (dx1, dy1), 1))

        return options

    def find_path(self, min_count=0, max_count=3, print_nodes=False):
        start = (0, (0, 0), (0, 0), 0)
        end = (self.shape[0] - 1, self.shape[1] - 1)
        seen = {start}
        path = []
        heapq.heappush(path, (self.map[1][0], (0, 1), (0, 1), 1, [((0, 1), (0, 1))]))
        heapq.heappush(path, (self.map[0][1], (1, 0), (1, 0), 1, [((1, 0), (1, 0))]))

        while path:
            score, position, direction, count, nodes = heapq.heappop(path)

            if (position, direction, count) in seen:
                continue

            seen.add((position, direction, count))
            if position == end and count < min_count:
                continue

            if position == end and count >= min_count:
                if print_nodes:
                    self.print(nodes)
                return score

            for s, p, d, c in self.get_directions(*position, *direction, count, min_count, max_count):
                heapq.heappush(path, (score + s, p, d, c, nodes + [(p, d)]))

    def print(self, nodes):
        directions = {
            (0, -1): "^",
            (0, 0): "S",
            (0, 1): "v",
            (-1, 0): "<",
            (1, 0): ">",
        }
        to_print = {
            p: directions[d] for p, d in nodes
        }
        for y in range(self.shape[1]):
            line = []
            for x in range(self.shape[0]):
                line.append(to_print.get((x, y), str(self.map[y][x])))
            print(line)
        print()


def solution1(problem=puzzle_input, print_nodes=False):
    heat_map = HeatMap(process_input(problem))
    return heat_map.find_path(print_nodes=print_nodes)
    

def solution2(problem=puzzle_input, print_nodes=False):
    heat_map = HeatMap(process_input(problem))
    return heat_map.find_path(4, 10, print_nodes=print_nodes)
    
    
print(f"test 1: {solution1(test_input, True)}")
print(f"test 2.0: {solution2(test_input, True)}")
print(f"test 2.1: {solution2(test_input2, True)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
