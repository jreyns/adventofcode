import re
from math import prod, lcm
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def process_line(line):
    relay_type, name, connections = re.match(r"([%&]?)([a-z]*) -> (.*)", line) .groups()
    return relay_type, name, connections.split(", ")


def process_input(lines=puzzle_input):
    connections = {}
    flips = {}
    conjunctions = {}

    for line in lines:
        relay_type, name, connected = process_line(line)
        connections[name] = connected
        match relay_type:
            case "%":
                flips[name] = 0
            case "&":
                conjunctions[name] = {}

    for name, connected in connections.items():
        for relay in connected:
            if relay in conjunctions:
                conjunctions[relay][name] = 0

    return connections, flips, conjunctions


def press_button(connections, flips, conjunctions, signals):
    signals[0] += 1
    queue = [("broadcaster", 0)]
    while queue:
        name, signal = queue.pop(0)

        for relay in connections[name]:
            signals[signal] += 1
            if relay not in connections:
                continue
            relay_type = "%" if relay in flips else "&"
            match relay_type, signal:
                case "&", _:
                    conjunctions[relay][name] = signal
                    queue.append((relay, not all(conjunctions[relay].values())))
                case "%", 0:
                    flips[relay] = not flips[relay]
                    queue.append((relay, flips[relay]))
                case "%", 1:
                    continue

    return signals


def solution1(problem=puzzle_input):
    connections, flips, conjunctions = process_input(problem)
    signals = [0, 0]
    for _ in range(1000):
        signals = press_button(connections, flips, conjunctions, signals)
    return prod(signals)


def solution2(problem=puzzle_input):
    connections, flips, conjunctions = process_input(problem)
    relays_of_interest = []
    for relays in connections["broadcaster"]:
        binary = ""
        while relays:
            connected = connections[relays]
            binary += ('0', '1')[any(relay not in flips for relay in connected)]
            relays, = [relay for relay in connected if relay in flips] or [0]
        relays_of_interest.append(int(binary[::-1], 2))
    return lcm(*relays_of_interest)


print(f"test 1: {solution1(test_input)}")
print(f"test 1.1: {solution1(test_input2)}")
print(f"test 2: {solution2(test_input2)}")

print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
