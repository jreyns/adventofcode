import re
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            [int(x) for x in line.split(" ")]
        )
    return processed_input


def subtract(reading):
    total = 0
    for r in reversed(reading):
        total = r - total
    return total


def predict(reading):
    history = []
    prediction = 0
    while sum(reading) or min(reading) != 0:
        history.append(reading[0])
        prediction += reading[-1]
        reading = [y - x for x, y in zip(reading, reading[1:])]
    return prediction, subtract(history)


def solution1(problem=puzzle_input):
    return sum([predict(x)[0] for x in process_input(problem)])
    

def solution2(problem=puzzle_input):
    return sum([predict(x)[1] for x in process_input(problem)])
    

print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
