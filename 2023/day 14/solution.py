
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


class ParabolicReflectorDish:

    def __init__(self, lines):
        self.shape = len(lines[0]), len(lines)
        self.map = {}
        self.squares = set([])
        self.to_sparse(lines)

    def to_sparse(self, lines):
        for y in range(self.shape[1]):
            for x in range(self.shape[0]):
                symbol = lines[y][x]
                if symbol == ".":
                    continue

                if symbol == "#":
                    self.squares.add((x, y))

                self.map[(x, y)] = symbol

    def clean_map(self):
        return {k: "#" for k in self.squares}

    @property
    def rocks(self):
        return list(sorted(set(self.map.keys()).difference(self.squares)))

    def get_sorted(self, dx, dy):
        rocks = self.rocks
        if dx == 1 or dy == 1:
            rocks.reverse()
        return rocks

    def move_x(self, positions, x, y, dx):
        while 0 <= x + dx < self.shape[0] and (x + dx, y) not in positions:
            x = x + dx
        return x, y

    def move_y(self, positions, x, y, dy):
        while 0 <= y + dy < self.shape[1] and (x, y + dy) not in positions:
            y = y + dy
        return x, y

    def move_rock(self, positions, x, y, dx, dy):
        if dx != 0:
            x, y = self.move_x(positions, x, y, dx)
        else:
            x, y = self.move_y(positions, x, y, dy)

        positions[(x, y)] = "O"

    def move_rocks(self, dx, dy):
        new_positions = self.clean_map()
        ordered_rocks = self.get_sorted(dx, dy)
        for rock in ordered_rocks:
            self.move_rock(new_positions, *rock, dx, dy)
        self.map = new_positions

    def score(self):
        return sum([self.shape[1] - y for _, y in self.rocks])

    def print(self, to_print=None):
        if to_print is None:
            to_print = self.map
        for y in range(self.shape[1]):
            line = []
            for x in range(self.shape[0]):
                line.append(to_print.get((x, y), "."))
            print(line)
        print()


def solution1(problem=puzzle_input):
    prd = ParabolicReflectorDish(process_input(problem))
    prd.move_rocks(0, -1)
    return prd.score()
    

def solution2(problem=puzzle_input):
    prd = ParabolicReflectorDish(process_input(problem))
    positions = []
    while (current := (prd.score(), prd.map)) not in positions:
        positions.append(current)
        prd.move_rocks(0, -1)
        prd.move_rocks(-1, 0)
        prd.move_rocks(0, 1)
        prd.move_rocks(1, 0)

    start = positions.index(current)
    remainder = 1000000000 - start
    modulus = len(positions) - start
    index = start + remainder % modulus
    return positions[index][0]
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
