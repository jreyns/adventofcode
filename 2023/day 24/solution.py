from math import copysign
from z3 import Solver, Real, sat

from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def parse_line(line):
    p, v = line.split("@")
    return tuple([int(x) for x in p.split(", ")]), tuple([int(x) for x in v.split(", ")])


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            parse_line(line)
        )
    return processed_input


def get_linear_function(point, velocity):
    a = velocity[1] / velocity[0]
    b = point[1] - a * point[0]
    return a, b


def intersect(a1, b1, a2, b2):
    if a1 == a2:
        return None
    x = (b2 - b1) / (a1 - a2)
    return x, a1 * x + b1


def in_future(point, velocity, intersection):
    return (copysign(1, intersection[0] - point[0]) == copysign(1, velocity[0])
            and copysign(1, intersection[1] - point[1]) == copysign(1, velocity[1]))


def in_test_area(x, y, lower, upper):
    return lower <= x <= upper and lower <= y <= upper


def solution1(problem=puzzle_input, bounds=(200000000000000, 400000000000000)):
    total = 0
    linear_equations = [(p, v, get_linear_function(p, v)) for p, v in process_input(problem)]
    for index, h1 in enumerate(linear_equations[:-1]):
        p1, v1, eq1 = h1
        for h2 in linear_equations[index+1:]:
            p2, v2, eq2 = h2
            intersection = intersect(*eq1, *eq2)
            if eq1 == eq2:
                total += 1
            if intersection is None:
                # print(f"{p1} @ {v1} and {p2} @ {v2} are parallel")
                continue
            if not in_future(p1, v1, intersection) or not in_future(p2, v2, intersection):
                # print(f"{p1} @ {v1} and {p2} @ {v2} intersect at {intersection} in the past")
                continue
            valid = in_test_area(*intersection, *bounds)
            # print(f"{p1} @ {v1} and {p2} @ {v2} intersect at {intersection} and {valid}")
            total += valid
    return total
    

def solution2(problem=puzzle_input):
    point = [Real(f'point[{i}]') for i in range(3)]
    velocity = [Real(f'velocity[{i}]') for i in range(3)]
    definitions = process_input(problem)
    s = Solver()
    for i, (p, v) in enumerate(definitions):
        t = Real(f't{i}')
        for c in range(3):
            s.add(point[c] + t * velocity[c] == p[c] + t * v[c])
    if s.check() != sat:
        print('No solution exists')

    return sum(int(str(s.model().evaluate(x))) for x in point)
    
    
print(f"test 1: {solution1(test_input, (7, 27))}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
