from math import prod
import networkx as nx
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_line(line):
    first, connected = line.split(": ")
    return first, connected.split(" ")


def process_input(lines=puzzle_input):
    processed = []
    for line in lines:
        processed.append(process_line(line))
    return processed


def solution1(problem=puzzle_input):
    graph = nx.Graph()
    for u, vs in process_input(problem):
        for v in vs:
            graph.add_edge(u, v)
    cuts = nx.minimum_edge_cut(graph)
    graph.remove_edges_from(cuts)
    return prod(map(len, nx.connected_components(graph)))
    
    
print(f"test 1: {solution1(test_input)}")
    
print(f"solution 1: {solution1()}")
