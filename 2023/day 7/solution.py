from collections import Counter
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line.split(" ")
        )
    return processed_input


def score_individual(card):
    lookup: str = "AKQJT98765432"[::-1]
    return lookup.index(card)


def score_individual_with_j(card):
    lookup: str = "AKQT98765432J"[::-1]
    return lookup.index(card)


def compare_hand(h1, h2, scoring=score_individual):
    if h1.score > h2.score:
        return 1

    if h1.score < h2.score:
        return -1

    for i in range(5):
        score_1 = scoring(h1.hand[i])
        score_2 = scoring(h2.hand[i])
        if score_1 > score_2:
            return 1

        if score_2 > score_1:
            return -1

    return 0


class Hand:

    def __init__(self, hand, bid, part=1):
        self.part = part
        self.hand = hand
        self.bid = int(bid)
        self._compare = score_individual
        if self.part == 2:
            self._compare = score_individual_with_j

    def __repr__(self):
        return f"{self.hand} {self.bid}"

    def replace_joker(self):
        c = Counter(self.hand)
        card, count = c.most_common(1)[0]
        if card == "J" and count == 5:
            return self.hand
        if card == "J":
            card, count = c.most_common(2)[1]
        return self.hand.replace("J", card)

    @property
    def score(self):
        c = Counter(self.hand)
        if self.part == 2:
            c = Counter(self.replace_joker())
        card, count = c.most_common(1)[0]

        if count >= 4:
            return count + 2

        if sum([x for _, x in c.most_common(2)]) == 5:
            return 5

        if count == 3:
            return 4

        if sum([x for _, x in c.most_common(2)]) == 4 and count == 2:
            return 3

        return count

    def __lt__(self, other):
        return compare_hand(self, other, self._compare) == -1


def solution1(problem=puzzle_input):
    hands = sorted([Hand(*x) for x in process_input(problem)])
    return sum([h.bid * (x+1) for x, h in enumerate(hands)])
    

def solution2(problem=puzzle_input):
    hands = sorted([Hand(*x, part=2) for x in process_input(problem)])
    return sum([h.bid * (x+1) for x, h in enumerate(hands)])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
