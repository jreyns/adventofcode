import util
from util.util import get_input, get_adjacent_coordinates


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


def get_machine(lines):
    symbols = []
    machine = {}
    shape = (len(lines[0]), len(lines))
    for y in range(shape[0]):
        for x in range(shape[1]):
            character: str = lines[y][x]
            if character == ".":
                continue
            if not character.isdigit():
                symbols.append((x, y))
            machine[(x, y)] = lines[y][x]
    return machine, symbols, shape


def find_left_most(machine, position, shape):
    current = (position[0] - 1, position[1])
    if current not in machine or not machine[current].isdigit():
        return position

    while 0 <= current[0] < shape[0] and current in machine and machine[current].isdigit():
        position = current
        current = (position[0] - 1, position[1])
    return position


def get_number(machine, position, shape):
    number = machine[position]
    offset = 1
    position = (position[0] + offset, position[1])
    while 0 <= position[0] < shape[0] and position in machine and machine[position].isdigit():
        number += machine[position]
        position = (position[0] + offset, position[1])
    return int(number)


def find_part_numbers(machine, symbols, shape):
    numbers = []
    seen = set([])
    for x, y in symbols:
        neighbours = get_adjacent_coordinates(x, y, shape, True)
        for neighbour in neighbours:
            if neighbour not in machine or not machine[neighbour].isdigit():
                continue
            left_most = find_left_most(machine, neighbour, shape)
            if left_most not in seen:
                numbers.append(get_number(machine, left_most, shape))
            seen.add(left_most)
    return numbers


def find_gear_numbers(machine, symbols, shape):
    numbers = []
    for x, y in symbols:
        if machine[(x, y)] != "*":
            continue

        seen = set([])
        ratios = []
        neighbours = get_adjacent_coordinates(x, y, shape, True)
        for neighbour in neighbours:
            if neighbour not in machine or not machine[neighbour].isdigit():
                continue
            left_most = find_left_most(machine, neighbour, shape)
            if left_most not in seen:
                ratios.append(get_number(machine, left_most, shape))
            seen.add(left_most)
        if len(ratios) == 2:
            numbers.append(ratios[0] * ratios[1])
    return numbers


def solution1(problem=puzzle_input):
    part_numbers = find_part_numbers(*get_machine(process_input(problem)))
    return sum(part_numbers)
    

def solution2(problem=puzzle_input):
    gear_ratios = find_gear_numbers(*get_machine(process_input(problem)))
    return sum(gear_ratios)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
