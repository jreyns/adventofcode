
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line.split(" ")
        )
    return processed_input


class DigPattern:

    directions = {
        "R": (1, 0),
        "D": (0, 1),
        "L": (-1, 0),
        "U": (0, -1),
        "0": (1, 0),
        "1": (0, 1),
        "2": (-1, 0),
        "3": (0, -1),
    }

    def __init__(self, lines, part=1):
        self.instructions = lines
        self.part = part

    def get_instruction(self, index):
        d, s, c = self.instructions[index]
        if self.part == 1:
            x, y = self.directions[d]
            return x * int(s), y * int(s)

        x, y = self.directions[c[7]]
        steps = int(c[2:7], 16)
        return x * steps, y * steps

    def calculate_area(self):
        x, area = (0, 1)

        for i in range(len(self.instructions)):
            dx, dy = self.get_instruction(i)
            x += dx
            area += x * dy + abs(dx + dy) / 2

        return int(area)


def solution1(problem=puzzle_input):
    pattern = DigPattern(process_input(problem))
    return pattern.calculate_area()
    

def solution2(problem=puzzle_input):
    pattern = DigPattern(process_input(problem), 2)
    return pattern.calculate_area()
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")

print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
