from parse import *
from util.util import get_input
from math import lcm


puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def split_line(line):
    return tuple(parse("{} = ({}, {})", line))


def process_input(lines=puzzle_input):
    choices = ["LR".index(x) for x in lines[0]]
    processed_input = []
    for line in lines[1:]:
        if line == "":
            continue
        processed_input.append(
            split_line(line)
        )
    return choices, processed_input


def build_map(map_definition):
    desert = {}
    for current, left, right in map_definition:
        desert[current] = [left, right]
    return desert


def get_starting_positions(desert):
    positions = []
    for key in desert:
        if key[2] == "A":
            positions.append(key)
    return positions


def has_ended(positions):
    return sum([x[2] != "Z" for x in positions]) == 0


def run_until_repeats(start, desert, choices):
    current = start
    steps = []
    count = 0
    intermediate = 0
    while len(steps) < 2 or steps[-1] != steps[-2]:
        current = desert[current][choices[count % len(choices)]]
        count += 1
        intermediate += 1

        if has_ended([current]):
            steps.append(intermediate)
            intermediate = 0

    return count, steps[-1]


def solution1(problem=puzzle_input):
    choices, desert_definition = process_input(problem)
    desert = build_map(desert_definition)
    current = "AAA"
    count = 0
    while current != "ZZZ":
        current = desert[current][choices[count % len(choices)]]
        count += 1

    return count
    

def solution2(problem=puzzle_input):
    choices, desert_definition = process_input(problem)
    desert = build_map(desert_definition)
    current = get_starting_positions(desert)
    options = []
    for pos in current:
        options.append(run_until_repeats(pos, desert, choices))
    return lcm(*sorted([x[1] for x in options]))
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input2)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
