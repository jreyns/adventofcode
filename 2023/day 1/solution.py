
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def get_first_number(line: str, reverse, part2):
    steps = range(0, len(line), 1)
    if reverse:
        steps = range(-1, -len(line)-1, -1)

    for step in steps:
        if line[step].isdigit():
            return line[step]

        if not part2:
            continue

        if reverse:
            line_part = line[step:]
        else:
            line_part = line[:step+1]

        for digit, word in enumerate(["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]):
            if word in line_part:
                return str(digit + 1)


def process_input(lines=puzzle_input, part2=False):
    processed_input = []
    for line in lines:
        first = get_first_number(line, False, part2)
        last = get_first_number(line, True, part2)
        processed_input.append(
            int(first+last)
        )
    return processed_input


def solution1(problem=puzzle_input):
    return sum(process_input(problem, False))
    

def solution2(problem=puzzle_input):
    return sum(process_input(problem, True))
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input2)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
