from collections import defaultdict, deque
InputList = []
with open("input.txt", "r") as data:
    for t in data:
        Line = t.strip()
        InputList.append(Line)
Width = len(InputList[0])
Height = len(InputList)

HoriSet = set()
VertSet = set()
FSet = set()
LSet = set()
SevenSet = set()
JSet = set()
InvalidSet = set()

for y, h in enumerate(InputList):
    for x, c in enumerate(h):
        if c == "-":
            HoriSet.add((x,y))
        elif c == "|":
            VertSet.add((x,y))
        elif c == "F":
            FSet.add((x,y))
        elif c == "L":
            LSet.add((x,y))
        elif c == "7":
            SevenSet.add((x,y))
        elif c == "J":
            JSet.add((x,y))
        elif c == "S":
            StartSpace = (x,y)
        else:
            InvalidSet.add((x,y))

NeighborDict = defaultdict()
for x, y in HoriSet:
    NX1, NX2 = x+1, x-1
    NeighborDict[(x,y)] = ((NX1, y),(NX2, y))
for x, y in VertSet:
    NY1, NY2 = y+1, y-1
    NeighborDict[(x,y)] = ((x, NY1),(x, NY2))
for x, y in FSet:
    NX1, NY1 = x+1, y+1
    NeighborDict[(x,y)] = ((x, NY1),(NX1, y))
for x, y in LSet:
    NX1, NY1 = x+1, y-1
    NeighborDict[(x,y)] = ((x, NY1),(NX1, y))
for x, y in SevenSet:
    NX1, NY1 = x-1, y+1
    NeighborDict[(x,y)] = ((x, NY1),(NX1, y))
for x, y in JSet:
    NX1, NY1 = x-1, y-1
    NeighborDict[(x,y)] = ((x, NY1),(NX1, y))
X, Y = StartSpace
NeighborDict[StartSpace] = ((X+1, Y),(X-1, Y),(X, Y+1),(X, Y-1))

ImperialCore = set()
ImperialFrontier = deque()
ImperialCore.add(StartSpace)
ImperialFrontier.append((0,StartSpace))

while ImperialFrontier:
    Distance, Coords = ImperialFrontier.popleft()
    Neighbors = NeighborDict[Coords]

    for NX, NY in Neighbors:
        if NX < 0 or NY < 0 or NX >= Width or NY >= Height:
            continue
        if (NX, NY) in ImperialCore or (NX,NY) in InvalidSet:
            continue
        NewNeighbors = NeighborDict[(NX, NY)]
        if Coords not in NewNeighbors:
            continue
        NewDistance = Distance+1
        ImperialFrontier.append((NewDistance, (NX,NY)))
        ImperialCore.add((NX,NY))
    Part1Answer = Distance

ImperialFrontier2 = deque()
ImperialCore2 = set()
ImperialFrontier2.append((-0.5,-0.5))

while ImperialFrontier2:
    Coords = ImperialFrontier2.popleft()
    X, Y = Coords
    if Coords in ImperialCore2:
        continue
    ImperialCore2.add(Coords)

    Directions = [(0,1,0.5,0.5,-0.5,0.5),(0,-1,0.5,-0.5,-0.5,-0.5),(1,0,0.5,0.5,0.5,-0.5),(-1,0,-0.5,0.5,-0.5,-0.5)]
    for DX, DY, MX1, MY1, MX2, MY2 in Directions:
        NX, NY, HX1, HY1, HX2, HY2 = X+DX, Y+DY, int(X+MX1), int(Y+MY1), int(X+MX2), int(Y+MY2)
        if NX < -1 or NX > Width or NY < -1 or NY > Height:
            continue
        Half1, Half2 = (HX1, HY1), (HX2, HY2)
        NewLocation = (NX, NY)
        if Half1 not in ImperialCore or Half2 not in ImperialCore:
            ImperialFrontier2.append(NewLocation)
            continue
        Neighbors1 = set(NeighborDict[Half1])
        Neighbors2 = set(NeighborDict[Half2])
        if Half1 in Neighbors2 and Half2 in Neighbors1:
            continue
        ImperialFrontier2.append(NewLocation)

Part2Answer = 0
for x in range(Width):
    for y in range(Height):
        if (X, Y) in ImperialCore:
            continue
        TempSet = set()
        for dx, dy in [(0.5,0.5),(0.5,-0.5),(-0.5,0.5),(-0.5,-0.5)]:
            NX,NY = x+dx,y+dy
            TempSet.add((NX,NY))
        Rea = TempSet & ImperialCore2
        if len(Rea) == 0:
            Part2Answer += 1

print(f"{Part1Answer = }")
print(f"{Part2Answer = }")