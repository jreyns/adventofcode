
from util.util import get_input, get_adjacent_coordinates


puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    start = None
    for y, line in enumerate(lines):
        if "S" in line:
            start = (line.index("S"), y)
        processed_input.append(
            list(line)
        )
    return processed_input, start, (len(processed_input[0]), len(processed_input))


def filter_points(positions, shape):
    max_x, max_y = shape
    points = []
    for point in positions:
        if 0 <= point[0] < max_x and 0 <= point[1] < max_y:
            points.append(point)
    return points


def deduce_start(plan, position, shape):
    positions = get_adjacent_coordinates(*position, shape)
    options = []
    for point in positions:
        if position in get_adjacent(plan, point, shape):
            options.append(point)

    options = sorted(options)
    x, y = position
    if options == [(x, y-1), (x, y+1)]:
        return "|"
    if options == [(x-1, y), (x+1, y)]:
        return "-"
    if options == [(x, y-1), (x+1, y)]:
        return "L"
    if options == [(x-1, y), (x, y-1)]:
        return "J"
    if options == [(x-1, y), (x, y+1)]:
        return "7"
    if options == [(x, y+1), (x+1, y)]:
        return "F"

    return None


def get_adjacent(plan, position, shape, symbol = None):
    x, y = position
    if symbol is None:
        symbol = plan[y][x]
    match symbol:
        case "|":
            return filter_points([(x, y-1), (x, y+1)], shape)
        case "-":
            return filter_points([(x-1, y), (x+1, y)], shape)
        case "L":
            return filter_points([(x, y-1), (x+1, y)], shape)
        case "J":
            return filter_points([(x, y-1), (x-1, y)], shape)
        case "7":
            return filter_points([(x, y+1), (x-1, y)], shape)
        case "F":
            return filter_points([(x, y+1), (x+1, y)], shape)
    return []


def solution1(problem=puzzle_input):
    plan, start, shape = process_input(problem)
    expected_symbol_at_start = deduce_start(plan, start, shape)
    steps = 0
    current = get_adjacent(plan, start, shape, expected_symbol_at_start)
    seen = set([start])
    while current:
        new_set = []
        for pos in current:
            new_set += get_adjacent(plan, pos, shape)
            seen.add(pos)
        current = set(new_set).difference(seen)
        steps += 1
    return steps


def rewrite_plan(plan, start, shape):
    start_symbol = deduce_start(plan, start, shape)
    current = get_adjacent(plan, start, shape, start_symbol)
    seen = set([start])
    while current:
        new_set = []
        for pos in current:
            new_set += get_adjacent(plan, pos, shape)
            seen.add(pos)
        current = set(new_set).difference(seen)

    for y in range(shape[1]):
        for x in range(shape[0]):
            if (x, y) not in seen:
                plan[y][x] = "."
    return plan


def filter_out_pipes(plan, positions):
    filtered = []
    for x, y in positions:
        try:
            symbol = plan[y][x]
        except IndexError:
            filtered.append((x, y))
            continue

        if symbol == ".":
            filtered.append((x, y))
            continue

    return filtered


def get_squeeze(s1, s2):
    match s1+s2:
        case "||":
            return [(0, -1), (0, 1)]
        case "--":
            return [(-1, 0), (1, 0)]
        case "|L":
            return [(0, -1), (0, 1)]
        case "J|":
            return [(0, -1), (0, 1)]
        case "7|":
            return [(0, -1), (0, 1)]
        case "|F":
            return [(0, -1), (0, 1)]
    return []


def squeeze(plan, x, y, seen):
    try:
        symbol = plan[y][x]
    except IndexError:
        return [(x, y)]

    if symbol == ".":
        return [(x, y)]

    if (x, y) in seen:
        return []

    neighbours = get_adjacent_coordinates(x, y, None)
    total = []
    for x1, y1 in neighbours:
        symbol_2 = plan[y1][x1]
        offsets = get_squeeze(symbol, symbol_2)
        for dx, dy in offsets:
            total += squeeze(plan, x+dx, y+dy, seen + [(x, y), (x1, y1)])

    return total


def add_squeezes(plan, positions):
    for pos in positions:
        positions += squeeze(plan, *pos, [])
    return positions


def get_enclosed_points(plan, point, shape):
    current = filter_out_pipes(plan, add_squeezes(plan, get_adjacent_coordinates(*point, None)))
    seen = set([point])
    while current:
        new_set = []
        for x, y in current:
            seen.add((x, y))
            if x < 0 or x >= shape[0] or y < 0 or y >= shape[1]:
                return False, seen
            neighbours = get_adjacent_coordinates(x, y, None)
            options = filter_out_pipes(plan, add_squeezes(plan, neighbours))
            new_set += options
        current = set(new_set).difference(seen)
    return True, seen


def print_plan(plan, shape, of_interest):
    for y in range(shape[1]):
        line = []
        for x in range(shape[0]):
            if plan[y][x] != ".":
                line.append(plan[y][x])
                continue
            if (x, y) in of_interest:
                line.append("I")
                continue
            line.append("O")
        print("".join(line))


def solution2(problem=puzzle_input):
    plan, start, shape = process_input(problem)
    plan = rewrite_plan(plan, start, shape)
    count = 0
    seen = set([])
    of_interest = set([])
    for y in range(shape[1]):
        for x in range(shape[0]):
            if (x, y) not in seen and plan[y][x] == ".":
                enclosed, points = get_enclosed_points(plan, (x, y), shape)
                if enclosed:
                    seen = seen.union(points)
                    of_interest = of_interest.union(points)
                    count += len(points)
    print_plan(plan, shape, of_interest)
    return count
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input2)}")
    
print(f"solution 1: {solution1()}")
#print(f"solution 2: {solution2()}")
