
import re
from util.util import get_input

puzzle_input = get_input()
test_input = get_input("test.txt")


def parse_rule(line):
    n, r = re.match(r"([a-z]+){(.*)}", line).groups()
    rules = []
    for rule in r.split(","):
        if ":" not in rule:
            rules.append(rule)
            continue

        label, operation, value, result = re.match(r"([xmas])([<>])(\d+):([a-zAR]+)", rule).groups()
        rules.append((label, operation, int(value), result))

    return n, rules


def parse_part(line):
    for c in "xmas":
        line = line.replace(c, f"'{c}'")
    return eval(line.replace("=", ":"))


def process_input(lines=puzzle_input):
    rules = {}
    parts = []
    parse_rules = True
    for line in lines:
        if line == "":
            parse_rules = False
            continue

        if parse_rules:
            name, r = parse_rule(line)
            rules[name] = r
            continue

        parts.append(parse_part(line))

    return rules, parts


def solution1(problem=puzzle_input):
    rules, parts = process_input(problem)
    total = 0
    for part in parts:
        current = "in"
        while current not in ("A", "R"):
            for rule in rules[current]:
                if isinstance(rule, str):
                    current = rule
                    break

                label, operation, value, result = rule
                if operation == "<" and part[label] < value:
                    current = result
                    break

                if operation == ">" and part[label] > value:
                    current = result
                    break

        if current == "A":
            total += sum(part.values())
    return total
    

def solution2(problem=puzzle_input):
    positions = {
        "x": 0,
        "m": 1,
        "a": 2,
        "s": 3
    }
    rules, parts = process_input(problem)
    ranges = {"in": [[(1, 4000), (1, 4000), (1, 4000), (1, 4000)]]}
    current = [("in", ((1, 4000), (1, 4000), (1, 4000), (1, 4000)))]
    while current:
        new_workflows = []
        for workflow, bounds in current:
            if workflow not in ranges:
                ranges[workflow] = []
            ranges[workflow].append(bounds)

            if workflow in ["A", "R"]:
                continue

            b = [x for x in bounds]
            for rule in rules[workflow]:
                if isinstance(rule, str):
                    new_workflows.append((rule, tuple(b)))
                    continue

                label, operation, value, result = rule
                index = positions[label]
                if operation == "<":
                    lower, upper = b[index]
                    b[index] = (lower, value-1)
                    new_workflows.append((result, tuple(b)))
                    b[index] = (value, upper)

                if operation == ">":
                    lower, upper = b[index]
                    b[index] = (value+1, upper)
                    new_workflows.append((result, tuple(b)))
                    b[index] = (lower, value)

        current = new_workflows

    total = 0
    for r in ranges["A"]:
        combinations = 1
        for lower, upper in r:
            combinations *= (upper + 1) - lower
        total += combinations

    return total
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
