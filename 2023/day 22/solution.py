
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        c1, c2 = line.split("~")
        coordinates = [
            tuple([int(x) for x in c1.split(",")]),
            tuple([int(x) for x in c2.split(",")])
        ]
        processed_input.append(
            tuple(coordinates)
        )
    return processed_input


def update_z(x, y, z, offset):
    return x, y, z + offset


def get_direction(brick):
    p1, p2 = brick
    if p1[0] == p2[0]:
        return p1[0], 0
    return 0, p1[1]


def intersect(b1, b2):
    x_overlap = (
            b1[0][0] <= b2[0][0] <= b1[1][0] or b1[0][0] <= b2[1][0] <= b1[1][0]
            or b2[0][0] <= b1[0][0] <= b2[1][0] or b2[0][0] <= b1[1][0] <= b2[1][0]
    )
    y_overlap = (
            b1[0][1] <= b2[0][1] <= b1[1][1] or b1[0][1] <= b2[1][1] <= b1[1][1]
            or b2[0][1] <= b1[0][1] <= b2[1][1] or b2[0][1] <= b1[1][1] <= b2[1][1]
    )
    return x_overlap and y_overlap


def rests_on(brick, settled):
    bricks = []
    for b in settled:
        if intersect(brick, b):
            bricks.append(b)
    return bricks


def settle(bricks):
    sorted_bricks = list(sorted(bricks, key=lambda b: min(b[0][2], b[1][2])))
    settled = {}
    covered = set([])
    covers = {b: [] for b in sorted_bricks}
    supports = {b: [] for b in sorted_bricks}
    while sorted_bricks:
        # Read brick
        current = sorted_bricks.pop(0)
        c1, c2 = current
        min_z = min(c1[2], c2[2])
        z = 1

        # Find all blocks that would intersect
        would_intersect = set([])
        for brick, original in settled.items():
            if intersect((c1, c2), brick):
                would_intersect.add((brick, original))

        # Prune intersecting block and only keep the highest
        if would_intersect:
            z = max([max(b[0][2], b[1][2]) for b, _ in would_intersect])
            for brick, original in would_intersect:
                b1, b2 = brick
                if max(b1[2], b2[2]) == z:
                    covered.add((b1, b2))
                    covers[current].append(original)
                    supports[original].append(current)

            z += 1

        # update z
        c1 = update_z(*c1, z - min_z)
        c2 = update_z(*c2, z - min_z)
        settled[(c1, c2)] = current

    return settled, covered, covers, supports


def can_be_disintegrated(brick, covers):
    for bricks in covers.values():
        if brick in bricks and len(bricks) == 1:
            return False
    return True


def falls(brick, covers, supports, will_fall):
    let_fall = [x for x in supports[brick]]
    while let_fall:
        brick = let_fall.pop(0)
        if len(set(covers[brick]).difference(will_fall)) == 0:
            will_fall.add(brick)
            for supported in supports[brick]:
                let_fall.append(supported)

    return len(will_fall) - 1


def solution1(problem=puzzle_input):
    bricks = process_input(problem)
    _, _, covers, _ = settle(bricks)
    disintegrated = []
    for brick in covers:
        if can_be_disintegrated(brick, covers):
            disintegrated.append(brick)
    return len(disintegrated)
    

def solution2(problem=puzzle_input):
    bricks = process_input(problem)
    _, _, covers, supports = settle(bricks)
    disintegrated = []
    for brick in bricks:
        disintegrated.append(falls(brick, covers, supports, {brick}))
    return sum(disintegrated)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
