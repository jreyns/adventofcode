import heapq
from util.util import get_input, get_adjacent_coordinates, manhattan_distance


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    start = None
    end = None
    shape = (len(lines[0]), len(lines))
    sparse = {}
    for y in range(shape[1]):
        for x in range(shape[0]):
            symbol = lines[y][x]
            if symbol == "#":
                continue
            if start is None:
                start = (x, y)
            sparse[(x, y)] = symbol
            end = (x, y)
    return start, end, shape, sparse


def get_move_options(x, y, shape, trail, slippery):
    if slippery:
        symbol = trail[(x, y)]
        match symbol:
            case "<":
                return [(x-1, y)]
            case ">":
                return [(x+1, y)]
            case "^":
                return [(x, y-1)]
            case "v":
                return [(x, y+1)]

    return [p for p in get_adjacent_coordinates(x, y, shape, False) if p in trail]


def hike(start, end, shape, trail, slippery=True):
    longest_path = [(-manhattan_distance(start, end), start, 0, [])]
    options = []
    while longest_path:
        _, location, distance, seen = heapq.heappop(longest_path)
        seen.append(location)

        if location == end:
            options.append((distance, seen))
            continue

        for move in get_move_options(*location, shape, trail, slippery):
            if move in seen:
                continue

            h = -manhattan_distance(move, end)
            heapq.heappush(longest_path, (h, move, distance+1, [x for x in seen]))

    return max(options)


def print_path(trail, path, shape):
    for y in range(shape[1]):
        line = []
        for x in range(shape[0]):
            if (x, y) in path:
                line.append("O")
                continue
            line.append(trail.get((x, y), "#"))
        print(line)
    print()


def solution1(problem=puzzle_input, show_route=False):
    start, end, shape, trail = process_input(problem)
    distance, path = hike(start, end, shape, trail)
    if show_route:
        print_path(trail, path, shape)
    return distance
    

def solution2(problem=puzzle_input, show_route=False):
    start, end, shape, trail = process_input(problem)
    distance, path = hike(start, end, shape, trail, False)
    if show_route:
        print_path(trail, path, shape)
    return distance
    
    
print(f"test 1: {solution1(test_input, True)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
