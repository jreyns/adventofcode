import heapq
from util.util import get_input, get_adjacent_coordinates, manhattan_distance


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    start = None
    end = None
    shape = (len(lines[0]), len(lines))
    sparse = {}
    for y in range(shape[1]):
        for x in range(shape[0]):
            symbol = lines[y][x]
            if symbol == "#":
                continue
            if start is None:
                start = (x, y)
            sparse[(x, y)] = symbol
            end = (x, y)
    return start, end, shape, sparse


def get_move_options(x, y, shape, trail, path):
    return [(p, path) for p in get_adjacent_coordinates(x, y, shape, False) if p in trail and p not in path]


def hike(start, end, shape, trail):
    distance = 1
    max_distance = 0
    moves = get_move_options(*start, shape, trail, [start])
    while moves:
        new_moves = []
        for move, path in moves:
            if move == end:
                max_distance = distance
                continue

            new_path = path + [move]
            new_moves += get_move_options(*move, shape, trail, new_path)

        moves = new_moves
        distance += 1

    return max_distance
    

def solution2(problem=puzzle_input, show_route=False):
    start, end, shape, trail = process_input(problem)
    return hike(start, end, shape, trail)
    

print(f"test 2: {solution2(test_input)}")
    

print(f"solution 2: {solution2()}")
