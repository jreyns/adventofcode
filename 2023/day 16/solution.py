
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


class Cave:

    def __init__(self, lines):
        self.shape = len(lines[0]), len(lines)
        self.map = {}
        self.energized = set([])
        self.to_sparse(lines)

    def reset(self):
        self.energized = set([])

    def to_sparse(self, lines):
        for y in range(self.shape[1]):
            for x in range(self.shape[0]):
                symbol = lines[y][x]
                if symbol == ".":
                    continue

                self.map[(x, y)] = symbol

    def follow_path(self, x, y, dx, dy):
        if x < 0 or x >= self.shape[0] or y < 0 or y >= self.shape[1]:
            return

        if (x, y, dx, dy) in self.energized:
            return

        while 0 <= x < self.shape[0] and 0 <= y < self.shape[1]:
            self.energized.add((x, y, dx, dy))

            if (x, y) not in self.map:
                x = x + dx
                y = y + dy
                continue

            symbol = self.map.get((x, y))
            if dy == 0 and symbol == "|":
                self.follow_path(x, y-1, 0, -1)
                self.follow_path(x, y+1, 0, 1)
                return

            if dy == 0 and symbol == "\\":
                if dx == 1:
                    return self.follow_path(x, y+1, 0, 1)
                else:
                    return self.follow_path(x, y-1, 0, -1)

            if dy == 0 and symbol == "/":
                if dx == 1:
                    return self.follow_path(x, y-1, 0, -1)
                else:
                    return self.follow_path(x, y+1, 0, 1)

            if dx == 0 and symbol == "-":
                self.follow_path(x-1, y, -1, 0)
                self.follow_path(x+1, y, 1, 0)
                return

            if dx == 0 and symbol == "\\":
                if dy == 1:
                    return self.follow_path(x+1, y, 1, 0)
                else:
                    return self.follow_path(x-1, y, -1, 0)

            if dx == 0 and symbol == "/":
                if dy == 1:
                    return self.follow_path(x-1, y, -1, 0)
                else:
                    return self.follow_path(x+1, y, 1, 0)

            x = x + dx
            y = y + dy

    def score(self):
        return len(set([(x, y) for x, y, _, _ in self.energized]))

    def print(self, energized=False):
        to_print = self.map
        if energized:
            to_print = {(x, y): "#" for x, y, _, _ in self.energized}
        for y in range(self.shape[1]):
            line = []
            for x in range(self.shape[0]):
                line.append(to_print.get((x, y), "."))
            print(line)
        print()


def solution1(problem=puzzle_input):
    cave = Cave(process_input(problem))
    cave.follow_path(0, 0, 1, 0)
    return cave.score()
    

def solution2(problem=puzzle_input):
    cave = Cave(process_input(problem))
    max_x = cave.shape[0] - 1
    max_y = cave.shape[1] - 1
    options = []
    for y in range(cave.shape[1]):
        cave.reset()
        cave.follow_path(0, y, 1, 0)
        options.append(cave.score())

        cave.reset()
        cave.follow_path(max_x, y, -1, 0)
        options.append(cave.score())

    for x in range(cave.shape[0]):
        cave.reset()
        cave.follow_path(x, 0, 0, 1)
        options.append(cave.score())

        cave.reset()
        cave.follow_path(x, max_y, 0, -1)
        options.append(cave.score())

    return max(options)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
