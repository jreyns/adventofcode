from util.util import get_input

puzzle_input = get_input()
test_input = get_input("test.txt")


def parse_input(line):
    conditions, groups = line.split(" ")
    groups = [int(x) for x in groups.split(",")]
    return conditions, groups


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            parse_input(line)
        )
    return processed_input


def find_options(condition, groups, cache, condition_index, group_index, springs_found):
    if (condition_index, group_index, springs_found) in cache:
        return cache[(condition_index, group_index, springs_found)]

    if condition_index == len(condition):
        return ((group_index == len(groups) and springs_found == 0) or
                (group_index == len(groups) - 1 and springs_found == groups[group_index]))

    total = 0

    if condition[condition_index] in ".?" and springs_found == 0:
        total += find_options(condition, groups, cache, condition_index + 1, group_index, 0)

    if group_index >= len(groups):
        cache[(condition_index, group_index, springs_found)] = total
        return total

    if condition[condition_index] in ".?" and groups[group_index] == springs_found:
        total += find_options(condition, groups, cache, condition_index + 1, group_index + 1, 0)

    if condition[condition_index] in "#?" and springs_found < groups[group_index]:
        total += find_options(condition, groups, cache, condition_index + 1, group_index, springs_found + 1)

    cache[(condition_index, group_index, springs_found)] = total

    return total


def solution1(problem=puzzle_input):
    total = 0
    for condition, groups in process_input(problem):
        total += find_options(condition, groups, {}, 0, 0, 0)
    return total


def solution2(problem=puzzle_input):
    total = 0
    for condition, groups in process_input(problem):
        condition, groups = '?'.join([condition] * 5), groups * 5
        total += find_options(condition, groups, {}, 0, 0, 0)
    return total


print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")

print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
