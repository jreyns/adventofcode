
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input += line.split(",")
    return processed_input


def get_hash(word):
    current = 0
    for c in word:
        current += ord(c)
        current *= 17
        current = current % 256
    return current


def fill_hashmap(words):
    hashmap = {i: {} for i in range(256)}
    for word in words:
        if "-" in word:
            label = word.replace("-", "")
            h = get_hash(label)
            if label in hashmap[h]:
                del hashmap[h][label]
            continue
        label, count = word.split("=")
        h = get_hash(label)
        hashmap[h][label] = int(count)

    return hashmap


def solution1(problem=puzzle_input):
    return sum([get_hash(w) for w in process_input(problem)])
    

def solution2(problem=puzzle_input):
    hashmap = fill_hashmap(process_input(problem))
    total = 0
    for i in range(256):
        for j, focal_length in enumerate(hashmap[i].values()):
            total += (i+1) * (j+1) * focal_length
    return total
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
