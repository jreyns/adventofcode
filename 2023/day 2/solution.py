
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def split_line(line):
    game, cubes = line.split(":")
    cubes = [x.split(",") for x in cubes.split(";")]
    seen_cubes = {}
    for cube_pattern in cubes:
        for color_pattern in cube_pattern:
            number, color = color_pattern.strip().split(" ")
            if color not in seen_cubes:
                seen_cubes[color] = []
            seen_cubes[color].append(int(number))
    return int(game.split(" ")[1]), seen_cubes


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            split_line(line)
        )
    return processed_input


def get_power(cubes):
    return max(cubes.get("red", [0])) * max(cubes.get("green", [0])) * max(cubes.get("blue", [0]))


def solution1(problem=puzzle_input):
    valid_puzzles = []
    for game, cubes in process_input(problem):
        if max(cubes.get("red", [0])) > 12 or max(cubes.get("green", [0])) > 13 or max(cubes.get("blue", [0])) > 14:
            continue
        valid_puzzles.append(game)
    return sum(valid_puzzles)
    

def solution2(problem=puzzle_input):
    valid_puzzles = []
    for game, cubes in process_input(problem):
        valid_puzzles.append(get_power(cubes))
    return sum(valid_puzzles)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
