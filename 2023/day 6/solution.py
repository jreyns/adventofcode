import re
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def split_line(line):
    field, games = line.split(":")
    return field, [int(x) for x in re.findall(r"\d+", games)]


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            split_line(line)
        )
    return processed_input


def get_amount_of_options(time, distance):
    count = 0
    for x in range(time):
        if (time - x) * x > distance:
            count += 1
    return count


def solution1(problem=puzzle_input):
    games = process_input(problem)
    total = 1
    for time, distance in zip(games[0][1], games[1][1]):
        total *= get_amount_of_options(time, distance)
    return total
    

def solution2(problem=puzzle_input):
    games = process_input(problem)
    time = int("".join([str(x) for x in games[0][1]]))
    distance = int("".join([str(x) for x in games[1][1]]))
    return get_amount_of_options(time, distance)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
