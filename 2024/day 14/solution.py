import re
from util.util import get_input, get_adjacent_coordinates
from numpy import prod
from collections import Counter

puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            [tuple([int(x) for x in group.split(",")]) for group in re.findall(r"(-?\d+,-?\d+)", line)]
        )
    return processed_input


def move_robot(x, y, u, v, shape):
    x, y = x + u, y + v

    if x < 0 or x > (shape[0]-1):
        x = (x + shape[0]) % shape[0]

    if y < 0 or y > (shape[1]-1):
        y = (y + shape[1]) % shape[1]

    return x, y


def move_all(positions, shape):
    new_positions = []
    for p, v in positions:
        new_positions.append((move_robot(*p, *v, shape), v))
    return new_positions


def count_quarters(positions, shape):
    quarters = [0, 0, 0, 0]
    bounds = [
        [(0, 0), ((shape[0] // 2)-1, (shape[1] // 2)-1)],
        [((shape[0] // 2) + 1, 0), (shape[0] - 1, (shape[1] // 2) - 1)],
        [(0, (shape[1] // 2) + 1), ((shape[0] // 2) - 1, shape[1] - 1)],
        [((shape[0] // 2) + 1, (shape[1] // 2) + 1), (shape[0] - 1, shape[1] - 1)],
    ]
    for i, ((min_x, min_y), (max_x, max_y)) in enumerate(bounds):
        for (x, y), _ in positions:
            if min_x <= x <= max_x and min_y <= y <= max_y:
                quarters[i] += 1
    return prod(quarters)


def is_chrismas_tree(vectors, shape):
    positions = {pos for pos, _ in vectors}
    all_neighbours = set([])
    for pos in positions:
        if len([x for x in get_adjacent_coordinates(*pos, shape) if x in positions]) == 4:
            all_neighbours.add(pos)
    return len(all_neighbours) > 100


def print_floor(vectors, shape, robot="", fill="."):
    positions = Counter([x for x, _ in vectors])
    print("#" * shape[0])
    for y in range(shape[1]):
        line = []
        for x in range(shape[0]):
            if (x, y) in positions:
                line.append(robot if robot else str(positions.get((x, y))))
                continue
            line.append(fill)
        print("".join(line))
    print("#" * shape[0])


def solution1(problem=puzzle_input, shape=(101, 103)):
    processed = process_input(problem)
    for _ in range(100):
        processed = move_all(processed, shape)
    return count_quarters(processed, shape)
    

def solution2(problem=puzzle_input, shape=(101, 103)):
    processed = process_input(problem)
    counter = 0
    while not is_chrismas_tree(processed, shape):
        counter += 1
        processed = move_all(processed, shape)
    # print_floor(processed, shape, "#", " ")
    return counter
    
    
print(f"test 1: {solution1(test_input, (11, 7))}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
