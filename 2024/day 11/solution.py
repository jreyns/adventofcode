
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            {x: 1 for x in line.split(" ")}
        )
    return processed_input[0]


def blink(stones):
    new_stones = {}
    for stone, count in stones.items():
        if stone == "0":
            if "1" not in new_stones:
                new_stones["1"] = 0
            new_stones["1"] += count
            continue

        if len(stone) % 2 == 0:
            middle = len(stone) // 2
            left = stone[:middle]
            right = str(int(stone[middle:]))
            if left not in new_stones:
                new_stones[left] = 0
            if right not in new_stones:
                new_stones[right] = 0
            new_stones[left] += count
            new_stones[right] += count
            continue

        new_value = str(int(stone) * 2024)
        if new_value not in new_stones:
            new_stones[new_value] = 0
        new_stones[new_value] += count

    return new_stones


def solution1(problem=puzzle_input):
    stones = process_input(problem)
    for i in range(25):
        stones = blink(stones)
    return sum([y for _, y in stones.items()])
    

def solution2(problem=puzzle_input):
    stones = process_input(problem)
    for i in range(75):
        stones = blink(stones)
    return sum([y for _, y in stones.items()])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
