
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    files = []
    empty = []
    for i, char in enumerate(lines[0]):
        if i % 2 == 0:
            files.append((i//2, int(char)))
            continue
        empty.append(int(char))
    return files, empty


def sort_files(files, empty):
    file_id, size = files[0]
    disk_layout = [file_id] * size
    to_position = files[1:]
    while to_position:
        spots_remaining = empty[0]
        while spots_remaining > 0:
            file_id, size = to_position[-1]

            if size > spots_remaining:
                to_position[-1] = file_id, size - spots_remaining
                disk_layout += [file_id] * spots_remaining
                spots_remaining = 0
                continue

            spots_remaining = spots_remaining - size
            to_position = to_position[:-1]
            disk_layout += [file_id] * size

        empty = empty[1:]
        file_id, size = to_position[0]
        disk_layout += [file_id] * size
        to_position = to_position[1:]

    return disk_layout


def fit_files(files, empty):
    disk_layout = [files[0]]
    for e, f in zip(empty, files[1:]):
        disk_layout.append(e)
        disk_layout.append(f)
    to_position = list(reversed(files[1:]))
    while to_position:
        file_id, size = to_position[0]
        for i in range(len(disk_layout)):
            spot = disk_layout[i]

            if not isinstance(spot, int) and spot == to_position[0]:
                break

            if not isinstance(spot, int):
                continue

            if size == spot:
                disk_layout[disk_layout.index(to_position[0])] = size
                disk_layout = disk_layout[:i] + [to_position[0]] + disk_layout[i+1:]
                break

            if size < spot:
                new_empty_space = spot - size
                disk_layout[disk_layout.index(to_position[0])] = size
                disk_layout = disk_layout[:i] + [to_position[0]] + [new_empty_space] + disk_layout[i+1:]
                break

        to_position = to_position[1:]

    exploded_disk_layout = []
    for layout in disk_layout:
        if isinstance(layout, int):
            exploded_disk_layout += ["."] * layout
            continue
        file_id, size = layout
        exploded_disk_layout += [file_id] * size

    return exploded_disk_layout


def checksum(ids):
    return sum([i * v for i, v in enumerate(ids) if isinstance(v, int)])


def solution1(problem=puzzle_input):
    files, empty = process_input(problem)
    disk_layout = sort_files(files, empty)
    return checksum(disk_layout)
    

def solution2(problem=puzzle_input):
    files, empty = process_input(problem)
    disk_layout = fit_files(files, empty)
    return checksum(disk_layout)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
