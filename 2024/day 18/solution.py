import re
from util.util import get_input, get_adjacent_coordinates


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            tuple([int(x) for x in line.split(",")])
        )
    return processed_input


def find_shortest_path(start, end, sparse):
    shape = end[0] + 1, end[1] + 1
    steps = 0
    seen = set()
    current = [start]
    while current:
        new_positions = set()
        for pos in current:
            if end == pos:
                return steps

            seen.add(pos)
            [
                new_positions.add(option) for option in get_adjacent_coordinates(*pos, shape)
                if option not in sparse and option not in seen
            ]
        steps += 1
        current = list(new_positions)

    return 0


def solution1(problem=puzzle_input, byte_count=1024, shape=(70, 70)):
    corruption = process_input(problem)[:byte_count]
    sparse = {k: "#" for k in corruption}
    return find_shortest_path((0, 0), shape, sparse)
    

def solution2(problem=puzzle_input, shape=(70, 70)):
    corruption = process_input(problem)
    bound = 0, len(corruption)
    i = bound[1] // 2
    while bound[1] - bound[0] > 1:
        sparse = {k: "#" for k in corruption[:i+1]}
        if not find_shortest_path((0, 0), shape, sparse):
            bound = bound[0], i
        else:
            bound = i, bound[1]
        i = (sum(bound)) // 2
    return ",".join([str(x) for x in corruption[bound[1]]])
    
    
print(f"test 1: {solution1(test_input, 12, (6, 6))}")
print(f"test 2: {solution2(test_input, (6, 6))}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
