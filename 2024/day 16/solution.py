import heapq
from util.util import get_input, get_adjacent_coordinates

puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def process_input(lines=puzzle_input):
    sparse = {}
    start = None
    end = None
    shape = (len(lines[0]), len(lines))
    for y, line in enumerate(lines):
        for x, char in enumerate(line):
            if char == "S":
                start = (x, y)

            if char == "E":
                end = (x, y)

            if char == "#":
                sparse[(x, y)] = "#"

    return start, end, sparse, shape


def inverse(x, y):
    return x * -1, y * -1


def floodfill(start_p, start_d, end, sparse, rotation_penalty=1000):
    directions = [inverse(x, y) for x, y in get_adjacent_coordinates(0, 0, None)]
    heuristic = {(end, d): (0, []) for d in directions}
    current = [
        (1, p, d, [(end, d)]) for p, d in zip(get_adjacent_coordinates(*end, None), directions)
        if p not in sparse
    ]
    heapq.heapify(current)
    while current:
        score, point, direction, previous = heapq.heappop(current)
        if heuristic.get((point, direction), (float("inf"),))[0] < score:
            continue

        if score > heuristic.get((start_p, start_d), (float("inf"),))[0]:
            break

        if score == heuristic.get((start_p, start_d), (float("inf"),))[0]:
            heuristic[(point, direction)] = (score, list(set(previous + heuristic.get((point, direction), (0, []))[1])))
        else:
            heuristic[(point, direction)] = (score, previous + [(point, direction)])

        for d in directions:
            if d == direction:
                p = point[0] - direction[0], point[1] - direction[1]
                if p not in sparse and (p, direction) not in previous:
                    heapq.heappush(current, (score + 1, p, d, previous + [(point, direction)]))
                continue

            if (point, d) in previous:
                continue

            if d == inverse(*direction):
                continue

            heapq.heappush(current, (score + rotation_penalty, point, d, previous + [(point, direction)]))

    return heuristic[(start_p, start_d)]


def print_path(start, end, path, sparse, shape, override=None, fill="."):
    path = {k: v for k, v in path}
    directions = {
        (1, 0): ">",
        (0, 1): "v",
        (-1, 0): "<",
        (0, -1): "^",
    }
    for y in range(shape[1]):
        line = []
        for x in range(shape[0]):
            if (x, y) == start:
                line.append("S")
                continue

            if (x, y) == end:
                line.append("E")
                continue

            if (x, y) in path:
                line.append(
                    directions[path[(x, y)]] if override is None else override
                )
                continue

            if (x, y) in sparse:
                line.append("#")
                continue

            line.append(fill)

        print("".join(line))


def solution(problem=puzzle_input):
    start, end, sparse, shape = process_input(problem)
    score, path = floodfill(start, (1, 0), end, sparse)
    # print_path(start, end, path, sparse, shape, None, " ")
    return score, len(set([x for x, _ in path]))


print(f"test 1: {solution(test_input)}")
print(f"test 2: {solution(test_input2)}")

print(f"solution 1 + 2: {solution()}")
