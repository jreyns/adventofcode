import re
import numpy as np
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    claw_machine = []
    for line in lines:
        if not line:
            processed_input.append(claw_machine)
            claw_machine = []
            continue

        claw_machine.append(
            [int(x) for x in re.findall(r"\d+", line)]
        )
    if claw_machine:
        processed_input.append(claw_machine)
    return processed_input


def tokens(claw_machine, cost_a=3, cost_b=1, precision=10e-12):
    a = claw_machine[:, [0, 1]]
    b = claw_machine[:, 2]
    a_presses, b_presses = np.linalg.solve(a, b)
    close_enough = abs(round(a_presses) - a_presses) < precision and abs(round(b_presses) - b_presses) < precision
    positive_presses = a_presses >= 0 and b_presses >= 0
    if positive_presses and close_enough:
        return cost_a * round(a_presses) + cost_b * round(b_presses)
    return 0


def solution1(problem=puzzle_input):
    processed = process_input(problem)
    claw_machines = [np.transpose(x) for x in processed]
    return sum([tokens(claw_machine) for claw_machine in claw_machines])
    

def solution2(problem=puzzle_input):
    processed = process_input(problem)
    offset = 10000000000000
    for claw_machine in processed:
        claw_machine[2] = [x + offset for x in claw_machine[2]]
    claw_machines = [np.transpose(x) for x in processed]
    return sum([tokens(claw_machine, precision=10e-4) for claw_machine in claw_machines])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
