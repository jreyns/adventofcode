from collections import Counter
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = [[], []]
    for line in lines:
        [processed_input[i].append(int(x)) for i, x in enumerate(line.split("   "))]
    return processed_input


def solution1(problem=puzzle_input):
    first, second = process_input(problem)
    return sum([abs(x - y) for x, y in zip(sorted(first), sorted(second))])
    

def solution2(problem=puzzle_input):
    first, second = process_input(problem)
    counter = Counter(second)
    similarity = [x * counter.get(x) if counter.get(x) else 0 for x in first]
    return sum(similarity)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
