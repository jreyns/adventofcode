
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    sparse_set = set([])
    start_location = None
    shape = (len(lines[0]), len(lines))
    for y, line in enumerate(lines):
        for x, symbol in enumerate(line):
            if symbol == "#":
                sparse_set.add((x, y))

            if symbol == "^":
                start_location = (x, y)
    return start_location, shape, sparse_set


def move(position, direction, sparse_set):
    new_position = position[0] + direction[0], position[1] + direction[1]
    if new_position in sparse_set:
        rotation = {
            (0, -1): (1, 0),
            (1, 0): (0, 1),
            (0, 1): (-1, 0),
            (-1, 0): (0, -1)
        }
        return move(position, rotation[direction], sparse_set)
    return new_position, direction


def is_loop(start, direction, shape, sparse_set):
    states = {(start, direction)}
    pos = start
    while 0 <= pos[0] < shape[0] and 0 <= pos[1] < shape[1]:
        pos, direction = move(pos, direction, sparse_set)
        if (pos, direction) in states:
            return True
        states.add((pos, direction))
    return False


def get_all_options(pos, direction, shape, sparse_set):
    seen = {pos}
    while 0 <= pos[0] < shape[0] and 0 <= pos[1] < shape[1]:
        pos, direction = move(pos, direction, sparse_set)
        if 0 <= pos[0] < shape[0] and 0 <= pos[1] < shape[1]:
            seen.add(pos)
    return seen


def solution1(problem=puzzle_input):
    pos, shape, sparse_set = process_input(problem)
    seen = get_all_options(pos, (0, -1), shape, sparse_set)
    return len(seen)
    

def solution2(problem=puzzle_input):
    pos, shape, sparse_set = process_input(problem)
    seen = get_all_options(pos, (0, -1), shape, sparse_set) - {pos}
    total = sum([
        is_loop(pos, (0, -1), shape, set(list(sparse_set) + [(x, y)]))
        for x, y in seen
    ])
    return total
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")

print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
