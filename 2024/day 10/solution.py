
from util.util import get_input, get_adjacent_coordinates


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    sparse = {}
    start_positions = []
    for y, line in enumerate(lines):
        for x, char in enumerate(line):
            if char == ".":
                continue
            value = int(char)
            if value == 0:
                start_positions.append((x, y))
            sparse[(x, y)] = value
    return start_positions, sparse


def find_trailheads(pos, current, sparse, seen):
    if pos in seen:
        return 0

    seen.add(pos)

    if sparse[pos] == 9:
        return 1

    x, y = pos
    options = [
        option for option in get_adjacent_coordinates(x, y, None)
        if sparse.get(option, 0) - 1 == current and option not in seen
    ]

    if not options:
        return 0

    return sum([find_trailheads(option, current + 1, sparse, seen) for option in options])


def find_all_trailheads(pos, current, sparse):
    if sparse[pos] == 9:
        return 1

    x, y = pos
    options = [
        option for option in get_adjacent_coordinates(x, y, None)
        if sparse.get(option, 0) - 1 == current
    ]

    if not options:
        return 0

    return sum([find_all_trailheads(option, current + 1, sparse) for option in options])


def solution1(problem=puzzle_input):
    start_positions, sparse = process_input(problem)
    total = sum([find_trailheads(pos, 0, sparse, set([])) for pos in start_positions])
    return total
    

def solution2(problem=puzzle_input):
    start_positions, sparse = process_input(problem)
    total = sum([find_all_trailheads(pos, 0, sparse) for pos in start_positions])
    return total
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
