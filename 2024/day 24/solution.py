from collections import Counter
from util.util import get_input

puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def process_input(lines=puzzle_input):
    wires = {}
    split_seen = False
    for line in lines:
        if not line:
            split_seen = True
            continue

        if not split_seen:
            name, value = line.split(": ")
            wires[name] = int(value)
            continue

        w1, op, w2, w3 = line.replace(" -> ", " ").split(" ")
        wires[w3] = (w1, op, w2)
    return wires


def logic(w1, op, w2):
    if op == "AND":
        return w1 and w2

    if op == "OR":
        return w1 or w2

    if op == "XOR":
        return w1 ^ w2


def simulate(wires):
    values = {k: v for k, v in wires.items() if isinstance(v, int)}
    outputs = {}
    for letter in ["x", "y", "z"]:
        outputs[letter] = sorted([key for key in wires if letter in key])

    while not all([z in values for z in outputs["z"]]):
        updates = False
        for wire in filter(lambda w: w not in values, wires):
            w1, op, w2 = wires[wire]
            if w1 in values and w2 in values:
                updates = True
                values[wire] = logic(values[w1], op, values[w2])

        if not updates:
            break

    return outputs, values


def find_problems(wires, outputs, operator):
    problems = []
    xs = outputs["x"]
    ys = outputs["y"]
    zs = outputs["z"]

    for xi, yi, zi in zip(xs, ys, zs):
        if operator == "AND":
            if wires[zi] != (xi, "AND", yi):
                problems.append(zi)
            continue

        if "XOR" not in wires[zi]:
            problems.append(zi)

    if operator == "AND":
        return problems

    wires_in = {}
    for wire_out, connection in wires.items():
        if isinstance(connection, int):
            continue

        w1_in, op, w2_in = connection
        if w1_in not in wires_in:
            wires_in[w1_in] = []

        if w2_in not in wires_in:
            wires_in[w2_in] = []

        wires_in[w1_in].append((op, wire_out))
        wires_in[w2_in].append((op, wire_out))

    for wire in wires:
        if wire in xs + ys + zs:
            continue

        w1, op, w2 = wires[wire]
        op_in = (op, w1 in xs + ys and w2 in xs + ys)
        op_out = tuple([op for op, _ in sorted(wires_in[wire])])

        if wires[wire] == ("x00", "AND", "y00") and (op_in, op_out) == (("AND", True), ("AND", "XOR")):
            continue

        if (op_in, op_out) not in [
            (('OR', False), ('AND', 'XOR')),
            (('XOR', True), ('AND', 'XOR')),
            (('AND', True), ('OR',)),
            (('AND', False), ('OR',))
        ]:
            problems.append(wire)

    return problems


def solution1(problem=puzzle_input):
    wires = process_input(problem)
    outputs, values = simulate(wires)
    return int("".join(reversed([str(values[i]) for i in outputs["z"]])), 2)
    

def solution2(problem=puzzle_input, operator="+"):
    wires = process_input(problem)
    outputs = {}
    for letter in ["x", "y", "z"]:
        outputs[letter] = sorted([key for key in wires if letter in key])
    to_swap = find_problems(wires, outputs, operator)
    return ",".join(sorted(to_swap))
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input2, 'AND')}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
