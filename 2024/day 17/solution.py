import re
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def process_input(lines=puzzle_input):
    registers = {}
    operations = []
    empty_line_seen = False
    for line in lines:
        if not line:
            empty_line_seen = True
            continue

        if not empty_line_seen:
            register, value = re.search(r"Register ([ABC]): (\d+)", line).groups()
            registers[register] = int(value)
            continue

        operations += [int(x) for x in re.findall(r"\d+", line)]
    return registers, operations


class Computer:

    def __init__(self, registers, operations):
        self.instruction_pointer = 0
        self.registers = registers
        self.operations = operations
        self.outputs = []
        self.debug = []

    def reset(self, registers):
        self.instruction_pointer = 0
        self.registers = registers
        self.outputs = []

    def combo(self, value):
        if 0 <= value <= 3:
            return value

        if value == 4:
            return self.registers["A"]

        if value == 5:
            return self.registers["B"]

        if value == 6:
            return self.registers["C"]

    def next(self):
        if self.instruction_pointer >= len(self.operations):
            return False

        opcode, value = self.operations[self.instruction_pointer], self.operations[self.instruction_pointer+1]

        if opcode == 0:
            self.registers["A"] = self.registers["A"] // pow(2, self.combo(value))
            self.debug.append(("adv A", "A", self.combo(value), self.registers["A"] // pow(2, self.combo(value))))

        if opcode == 1:
            self.registers["B"] = self.registers["B"] ^ value
            self.debug.append(("bxl", "B", value, self.registers["B"] ^ value))

        if opcode == 2:
            self.registers["B"] = self.combo(value) % 8
            self.debug.append(("bst", self.combo(value), self.combo(value) % 8))

        if opcode == 3:
            if self.registers["A"]:
                self.instruction_pointer = value
                self.debug.append(("jnz", value))
                return True

        if opcode == 4:
            self.registers["B"] = self.registers["B"] ^ self.registers["C"]
            self.debug.append(("bxc", self.registers["B"], self.registers["C"], self.registers["B"] ^ self.registers["C"]))

        if opcode == 5:
            self.outputs.append(self.combo(value) % 8)
            self.debug.append(("out", value, self.combo(value), self.combo(value) % 8))

        if opcode == 6:
            self.registers["B"] = self.registers["A"] // pow(2, self.combo(value))
            self.debug.append(("bdv", self.registers["A"], self.combo(value), self.registers["A"] // pow(2, self.combo(value))))

        if opcode == 7:
            self.registers["C"] = self.registers["A"] // pow(2, self.combo(value))
            self.debug.append(("cdv", self.registers["A"], self.combo(value), self.registers["A"] // pow(2, self.combo(value))))

        self.instruction_pointer += 2

        return True

    def run(self):
        while True:
            if not self.next():
                break
        return self.outputs


def solution1(problem=puzzle_input):
    registers, operations = process_input(problem)
    computer = Computer(registers, operations)
    return ",".join([str(x) for x in computer.run()])
    

def solution2(problem=puzzle_input):
    _, operations = process_input(problem)
    options = [0]
    for i in range(1, len(operations) + 1):
        new_options = []
        for option in options:
            for offset in range(8):
                value = 8 * option + offset
                registers = {"A": value, "B": 0, "C": 0}
                computer = Computer(registers, operations)
                computer.run()
                if computer.outputs == operations[-i:]:
                    new_options.append(value)
        options = new_options
    return min(options)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input2)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
