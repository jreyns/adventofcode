# [Advent of Code](/)

  * [[About]](/2024/about)
  * [[Events]](/2024/events)
  * [[Shop]](https://cottonbureau.com/people/advent-of-code)
  * [[Settings]](/2024/settings)
  * [[Log Out]](/2024/auth/logout)

joeri reyns 6*

#        y([2024](/2024))

  * [[Calendar]](/2024)
  * [[AoC++]](/2024/support)
  * [[Sponsors]](/2024/sponsors)
  * [[Leaderboard]](/2024/leaderboard)
  * [[Stats]](/2024/stats)

Our [sponsors](/2024/sponsors) help make Advent of Code possible:

[Infi](/2024/sponsors/redirect?url=https%3A%2F%2Faoc%2Einfi%2Enl%2F%3Fmtm%5Fcampaign%3Daoc2024%26mtm%5Fsource%3Daoc)
\- Er is slecht weer op komst en het is een lange tocht vanaf de Noordpool...
Help jij de kerstman veilig door het luchtruim te navigeren?

## \--- Day 4: Ceres Search ---

"Looks like the Chief's not here. Next!" One of The Historians pulls out a
device and pushes the only button on it. After a brief flash, you recognize
the interior of the [Ceres monitoring station](/2019/day/10)!

As the search for the Chief continues, a small Elf who lives on the station
tugs on your shirt; she'd like to know if you could help her with her _word
search_ (your puzzle input). She only has to find one word: `XMAS`.

This word search allows words to be horizontal, vertical, diagonal, written
backwards, or even overlapping other words. It's a little unusual, though, as
you don't merely need to find one instance of `XMAS` \- you need to find _all
of them_. Here are a few ways `XMAS` might appear, where irrelevant characters
have been replaced with `.`:

    
    
    ..X...
    .SAMX.
    .A..A.
    XMAS.S
    .X....
    

The actual word search will be full of letters instead. For example:

    
    
    MMMSXXMASM
    MSAMXMSMSA
    AMXSXMAAMM
    MSAMASMSMX
    XMASAMXAMM
    XXAMMXXAMA
    SMSMSASXSS
    SAXAMASAAA
    MAMMMXMMMM
    MXMXAXMASX
    

In this word search, `XMAS` occurs a total of `_18_` times; here's the same
word search again, but where letters not involved in any `XMAS` have been
replaced with `.`:

    
    
    ....XXMAS.
    .SAMXMS...
    ...S..A...
    ..A.A.MS.X
    XMASAMX.MM
    X.....XA.A
    S.S.S.S.SS
    .A.A.A.A.A
    ..M.M.M.MM
    .X.X.XMASX
    

Take a look at the little Elf's word search. _How many times does`XMAS`
appear?_

To begin, [get your puzzle input](4/input).

Answer:

You can also [Shareon
[Bluesky](https://bsky.app/intent/compose?text=%22Ceres+Search%22+%2D+Day+4+%2D+Advent+of+Code+2024+%23AdventOfCode+https%3A%2F%2Fadventofcode%2Ecom%2F2024%2Fday%2F4)
[Twitter](https://twitter.com/intent/tweet?text=%22Ceres+Search%22+%2D+Day+4+%2D+Advent+of+Code+2024&url=https%3A%2F%2Fadventofcode%2Ecom%2F2024%2Fday%2F4&related=ericwastl&hashtags=AdventOfCode)
[Mastodon](javascript:void\(0\);)] this puzzle.

