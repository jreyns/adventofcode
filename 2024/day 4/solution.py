import util.util
from util.util import get_input
from collections import Counter


puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


def sparse_field(letters, starting_letter):
    sparse = {}
    positions = []
    for y in range(len(letters)):
        for x in range(len(letters[y])):
            letter = letters[y][x]
            sparse[(x, y)] = letter
            if letter == starting_letter:
                positions.append((x, y))
    return sparse, positions


def has_word(position, direction, sparse, word="XMAS"):
    if sparse.get(position, "") != word[0]:
        return 0

    if len(word) == 1:
        return 1

    x = position[0] + direction[0]
    y = position[1] + direction[1]

    return has_word((x, y), direction, sparse, word[1:])


def has_word_a(position, direction, sparse, word="MAS", a=None):
    if sparse.get(position, "") != word[0]:
        return None

    if len(word) == 1:
        return a

    x = position[0] + direction[0]
    y = position[1] + direction[1]

    return has_word_a((x, y), direction, sparse, word[1:], position if word[0] == "A" else None)


def word_count(position, directions, sparse, word="XMAS"):
    return sum([has_word(position, d, sparse, word) for d in directions])


def cross_count(position, sparse, word="MAS"):
    directions = [(-1, -1), (1, 1), (-1, 1), (1, -1)]
    return [has_word_a(position, d, sparse, word) for d in directions]


def solution1(problem=puzzle_input):
    field = process_input(problem)
    directions = util.util.get_adjacent_coordinates(0, 0, None, True)
    sparse, positions = sparse_field(field, "X")
    return sum([word_count(x, directions, sparse) for x in positions])
    

def solution2(problem=puzzle_input):
    field = process_input(problem)
    sparse, positions = sparse_field(field, "M")
    cross_counter = Counter()
    [cross_counter.update(cross_count(position, sparse)) for position in positions]
    return sum([v == 2 for _, v in cross_counter.items()])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input2)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
