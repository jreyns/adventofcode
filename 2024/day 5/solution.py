import numpy as np
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def process_input(lines=puzzle_input):
    post_rules = {}
    updates = []
    rules_seen = False
    for line in lines:
        if not line:
            rules_seen = True
            continue

        if rules_seen:
            updates.append(line.split(","))
            continue

        pre, post = line.split("|")
        if post not in post_rules:
            post_rules[post] = []
        post_rules[post].append(pre)

    return updates, post_rules


def check_order(update, post_rules):
    for index, page in enumerate(update):
        if np.isin(update[index:], post_rules.get(page)).any():
            return False
    return True


def sort_update(update, post_rules):
    if not update:
        return []

    correct, incorrect = [], []
    for index, page in enumerate(update):
        if np.isin(update[index:], post_rules.get(page)).any() or np.isin(incorrect, post_rules.get(page)).any():
            incorrect.append(page)
            continue

        correct.append(page)

    return correct + sort_update(incorrect, post_rules)


def solution1(problem=puzzle_input):
    updates, post_rules = process_input(problem)
    return sum([int(update[len(update)//2]) for update in updates if check_order(update, post_rules)])
    

def solution2(problem=puzzle_input):
    updates, post_rules = process_input(problem)
    incorrect_updates = [sort_update(update, post_rules) for update in updates if not check_order(update, post_rules)]
    return sum([int(update[len(update)//2]) for update in incorrect_updates])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
print(f"test 3: {solution2(test_input2)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
