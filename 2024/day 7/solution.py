from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        test_value, operands = line.split(": ")
        processed_input.append((int(test_value), [int(x) for x in operands.split(" ")]))
    return processed_input


def solve(test_value, current, operands, operators):
    if current > test_value:
        return False

    if not operands:
        return test_value == current

    return any([solve(test_value, op(current, operands[0]), operands[1:], operators) for op in operators])


def solution1(problem=puzzle_input):
    test_cases = process_input(problem)
    return sum([
        x for x, y in test_cases
        if solve(x, y[0], y[1:], [lambda x, y: x + y, lambda x, y: x * y])
    ])
    

def solution2(problem=puzzle_input):
    test_cases = process_input(problem)
    return sum([
        x for x, y in test_cases
        if solve(x, y[0], y[1:], [lambda x, y: x + y, lambda x, y: x * y, lambda x, y: int(f"{x}{y}")])
    ])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
