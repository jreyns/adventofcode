
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            [int(x) for x in line.split(" ")]
        )
    return processed_input


def sign(x, y):
    return (y - x) / abs(y - x) if y - x != 0 else 0


def is_safe(report, lower_bound, upper_bound, tolerance=0):
    previous = None
    for i, (x, y) in enumerate(zip(report[:-1], report[1:])):
        if previous is not None and sign(x, y) != previous:
            return (
                is_safe(report[:i-1] + report[i:], lower_bound, upper_bound, tolerance - 1) or
                is_safe(report[:i] + report[i+1:], lower_bound, upper_bound, tolerance - 1) or
                is_safe(report[:i+1] + report[i+2:], lower_bound, upper_bound, tolerance - 1)
                if tolerance > 0 else False
            )

        if lower_bound <= abs(y - x) <= upper_bound:
            previous = sign(x, y)
            continue

        return (
            is_safe(report[:i] + report[i+1:], lower_bound, upper_bound, tolerance - 1) or
            is_safe(report[:i+1] + report[i+2:], lower_bound, upper_bound, tolerance - 1)
            if tolerance > 0 else False
        )

    return True


def solution1(problem=puzzle_input):
    data = process_input(problem)
    return sum([is_safe(report, 1, 3) for report in data])
    

def solution2(problem=puzzle_input):
    data = process_input(problem)
    return sum([is_safe(report, 1, 3, 1) for report in data])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
