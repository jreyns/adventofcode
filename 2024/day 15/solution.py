
from util.util import get_input, get_sparse_map, print_sparse_map


puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def process_input(lines=puzzle_input, f=get_sparse_map):
    map_lines = []
    moves = []
    split_seen = False
    for line in lines:
        if not line:
            split_seen = True
            continue

        if not split_seen:
            map_lines.append(line)
            continue

        moves.append(line)

    return f(map_lines, ["#", "O", "@"], "@"), "".join(moves)


def get_wide_map(problem, allowed_symbols=None, actor_symbol=None):
    symbols = {}
    sparse = {}
    start_positions = []
    shape = (len(problem[0]*2), len(problem))
    for y, line in enumerate(problem):
        for x, symbol in enumerate(line):
            if allowed_symbols and symbol not in allowed_symbols:
                continue

            if symbol not in symbols:
                symbols[symbol] = {}

            if actor_symbol and symbol == actor_symbol:
                start_positions.append((x*2, y))
                continue

            symbols[symbol].update({
                (x * 2, y): ((x*2, y), (x*2 + 1, y)),
                (x * 2 + 1, y): ((x*2, y), (x*2 + 1, y))
            })
            if symbol == "O":
                sparse[(x * 2, y)] = "["
                sparse[(x * 2 + 1, y)] = "]"
                continue

            sparse[(x * 2, y)] = symbol
            sparse[(x * 2 + 1, y)] = symbol

    if actor_symbol:
        return symbols, sparse, start_positions, shape

    return symbols, sparse, shape


def simulate_push(x, y, u, v, sparse, actions):
    new_position = x + u, y + v
    actions.append(((x, y), new_position))
    if new_position not in sparse:
        return True

    if sparse[new_position] == "#":
        return False

    return simulate_push(*new_position, u, v, sparse, actions)


def try_move(x, y, move, sparse, box_positions: set):
    directions = {
        "<": (-1, 0),
        "^": (0, -1),
        ">": (1, 0),
        "v": (0, 1),
    }
    actions = []
    allowed = simulate_push(x, y, *directions[move], sparse, actions)
    if allowed:
        x, y = actions[0][1]
        for old_pos, new_pos in reversed(actions[1:]):
            symbol = sparse[old_pos]
            del sparse[old_pos]
            sparse[new_pos] = symbol
            box_positions.remove(old_pos)
            box_positions.add(new_pos)

    return x, y


def simulate_wide_push(objects, u, v, sparse, box_positions, actions):
    objects_to_check = set([])
    for x, y in objects:
        # Robot
        if isinstance(x, int):
            new_position = x + u, y + v
            actions.append(((x, y), new_position))

            if new_position not in sparse:
                return True

            if sparse[new_position] == "#":
                return False

            if new_position in box_positions:
                objects_to_check.add(box_positions[new_position])

            continue

        # Object
        new_position = ((x[0] + u, x[1] + v), (y[0] + u, y[1] + v))
        actions.append(((x, y), new_position))

        for pos in new_position:
            if sparse.get(pos, " ") == "#":
                return False

            if pos in (x, y):
                continue

            if pos in box_positions:
                objects_to_check.add(box_positions[pos])

    if objects_to_check:
        return simulate_wide_push(list(objects_to_check), u, v, sparse, box_positions, actions)

    return True


def try_wide_move(x, y, move, sparse, box_positions, shape):
    directions = {
        "<": (-1, 0),
        "^": (0, -1),
        ">": (1, 0),
        "v": (0, 1),
    }
    actions = []
    allowed = simulate_wide_push([(x, y)], *directions[move], sparse, box_positions, actions)
    if allowed:
        x, y = actions[0][1]
        for old_pos, new_pos in reversed(actions[1:]):
            p1_old, p2_old = old_pos
            p1_new, p2_new = new_pos
            s1, s2 = sparse[p1_old], sparse[p2_old]
            del sparse[p1_old]
            del sparse[p2_old]
            sparse[p1_new] = s1
            sparse[p2_new] = s2
            del box_positions[p1_old]
            del box_positions[p2_old]
            box_positions[p1_new] = new_pos
            box_positions[p2_new] = new_pos

    return x, y


def solution1(problem=puzzle_input):
    (symbol_positions, sparse_map, start_positions, shape), moves = process_input(problem)
    current = start_positions[0]
    for move in moves:
        current = try_move(*current, move, sparse_map, symbol_positions["O"])
    # print_sparse_map(sparse_map, shape, [current], ".")
    return sum([100 * y + x for x, y in symbol_positions["O"]])
    

def solution2(problem=puzzle_input):
    (symbol_positions, sparse_map, start_positions, shape), moves = process_input(problem, get_wide_map)
    current = start_positions[0]
    for move in moves:
        current = try_wide_move(*current, move, sparse_map, symbol_positions["O"], shape)
    unique_positions = list(set(symbol_positions["O"].values()))
    # print_sparse_map(sparse_map, shape, [current], ".")
    return sum([100 * y + x for (x, y), _ in unique_positions])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input2)}")
print(f"test 3: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
