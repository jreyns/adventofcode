from collections import Counter
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    connections = {}
    for line in lines:
        n1, n2 = line.split("-")
        if n1 not in connections:
            connections[n1] = set([])
        if n2 not in connections:
            connections[n2] = set([])
        connections[n1].add(n2)
        connections[n2].add(n1)
    return connections


def find_groups(connections, n):
    groups = [(key,) for key in connections]
    length = 1
    while length < n:
        new_groups = set()
        for group in groups:
            for node, connected in connections.items():
                if node in group:
                    continue

                if all([c in connected for c in group]):
                    new_groups.add(tuple(sorted(group + (node,))))

        groups = new_groups
        length += 1

    return groups


def find_biggest_group(connections):
    frequent_items = Counter()
    for key, item_list in connections.items():
        frequent_items.update(list(item_list) + [key])
    groups = [(item,) for item, _ in frequent_items.most_common()]
    length = 1
    while True:
        new_groups = set()
        for group in groups:
            for node, connected in connections.items():
                if node in group:
                    continue

                if all([c in connected for c in group]):
                    new_groups.add(tuple(sorted(group + (node,))))

        if not new_groups:
            break
        groups = new_groups
        length += 1

    return groups


def solution1(problem=puzzle_input):
    processed = process_input(problem)
    groups = find_groups(processed, 3)
    return sum([any([node.startswith("t") for node in group]) for group in groups])
    

def solution2(problem=puzzle_input):
    processed = process_input(problem)
    group = list(find_biggest_group(processed))[0]
    return ",".join(group)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
