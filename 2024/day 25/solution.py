import pandas as pd
from itertools import product
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def is_lock(df):
    return df.iloc[0, :].sum() == 5


def process_input(lines=puzzle_input):
    keys, locks = [], []
    schematic = []
    for line in lines:
        if not line:
            df = pd.DataFrame(schematic)
            if is_lock(df):
                locks.append(df.sum() - 1)
            else:
                keys.append(df.sum() - 1)
            schematic = []
            continue
        schematic.append([c == "#" for c in line])
    df = pd.DataFrame(schematic)
    if is_lock(df):
        locks.append(df.sum() - 1)
    else:
        keys.append(df.sum() - 1)
    return keys, locks


def overlaps(df1, df2):
    return pd.concat([df1, df2], axis=1).sum(axis=1).max() <= 5


def solution1(problem=puzzle_input):
    keys, locks = process_input(problem)
    return sum([overlaps(df1, df2) for df1, df2 in product(keys, locks)])

    
print(f"test 1: {solution1(test_input)}")
    
print(f"solution 1: {solution1()}")
