
from util.util import get_input, get_sparse_map, get_adjacent_coordinates, manhattan_distance


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    symbols, sparse, shape = get_sparse_map(lines, ["#", "S", "E"])
    start, end = list(symbols["S"])[0], list(symbols["E"])[0]
    for point in [start, end]:
        del sparse[point]
    return start, end, sparse, shape


def score(end, sparse, shape):
    scores = {}
    steps = 0
    current = [end]
    while current:
        new_points = set([])
        for point in current:
            scores[point] = steps
            [
                new_points.add(pos) for pos in get_adjacent_coordinates(*point, shape)
                if pos not in sparse and pos not in scores
            ]
        current = list(new_points)
        steps += 1

    return scores


def score_cheats(scores, cutoff, d_max):
    steps = list(reversed(sorted([(s, p) for p, s in scores.items()])))
    total_cheats = 0
    for i, (s1, p1) in enumerate(steps):
        if i + cutoff >= len(steps):
            break

        for s2, p2 in steps[i + cutoff + 1:]:
            d = manhattan_distance(p1, p2)
            if d <= d_max and (s1 - s2 - d) >= cutoff:
                total_cheats += 1

    return total_cheats


def solution1(problem=puzzle_input, cutoff=100):
    start, end, sparse, shape = process_input(problem)
    scores = score(end, sparse, shape)
    return score_cheats(scores, cutoff, 2)
    

def solution2(problem=puzzle_input, cutoff=100):
    start, end, sparse, shape = process_input(problem)
    scores = score(end, sparse, shape)
    return score_cheats(scores, cutoff, 20)
    
    
print(f"test 1: {solution1(test_input, 2)}")
print(f"test 2: {solution2(test_input, 76)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
