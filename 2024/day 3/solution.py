import re
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return "".join(processed_input)


def solution1(problem=puzzle_input):
    content = process_input(problem)
    matches = re.findall(r"mul\((\d{1,3}),(\d{1,3})\)", content)
    return sum([int(x) * int(y) for x, y in matches])
    

def solution2(problem=puzzle_input):
    content = process_input(problem)
    matches = re.findall(r"(do\(\))|(don't\(\))|(mul\((\d{1,3}),(\d{1,3})\))", content)

    on = True
    total = 0
    for do, dont, _, x, y in matches:
        if dont:
            on = False
            continue

        if do:
            on = True
            continue

        if on:
            total += int(x) * int(y)

    return total
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input2)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
