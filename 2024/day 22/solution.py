from collections import defaultdict
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2= get_input("test2.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            int(line)
        )
    return processed_input


def mix_value(secret, value):
    secret = secret ^ value
    return secret % 16777216


def next_secret(secret):
    secret = mix_value(secret, secret * 64)
    secret = mix_value(secret, secret // 32)
    return mix_value(secret, secret * 2048)


def nth_secret(secret, n):
    secrets = [secret]
    for _ in range(n):
        secrets.append(next_secret(secrets[-1]))
    return secrets


def solution(problem=puzzle_input):
    processed = process_input(problem)
    secrets = [nth_secret(secret, 2000) for secret in processed]
    part1 = sum([secret[-1] for secret in secrets])
    prices = [[x % 10 for x in buyer] for buyer in secrets]
    changes = [[b - a for a, b in zip(p, p[1:])] for p in prices]
    amounts = {}
    for buyer_idx, change in enumerate(changes):
        keys = set()
        for i in range(len(change) - 3):
            key = tuple(change[i: i + 4])
            if key in keys:
                continue
            if key not in amounts:
                amounts[key] = 0
            amounts[key] += prices[buyer_idx][i + 4]
            keys.add(key)
    part2 = max(amounts.values())
    return part1, part2
    
    
print(f"test 1 + 2: {solution(test_input)}")
print(f"test2 1 + 2: {solution(test_input2)}")
    
print(f"solution 1 + 2: {solution()}")
