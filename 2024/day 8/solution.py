import itertools
from util.util import get_input, is_inside


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    frequencies = {}
    shape = (len(lines[0]), len(lines))
    sparse = {}
    for y, line in enumerate(lines):
        for x, char in enumerate(line):
            if char == ".":
                continue

            sparse[(x, y)] = char
            if char not in frequencies:
                frequencies[char] = []
            frequencies[char].append((x, y))
    return sparse, frequencies, shape


def harmonic(p1, dx, dy, shape, loop):
    options = set([p1])
    k = 1
    pk1 = (p1[0] + k * dx, p1[1] + k * dy)
    pk1_inside = is_inside(pk1, shape)
    while pk1_inside and (loop or k == 1):
        if pk1_inside:
            options.add(pk1)
        k += 1
        pk1 = (p1[0] + k * dx, p1[1] + k * dy)
        pk1_inside = is_inside(pk1, shape)
    return options


def anti_node(p1, p2, shape, loop=False):
    dx, dy = p2[0] - p1[0], p2[1] - p1[1]
    if not loop:
        return harmonic(p1, -dx, -dy, shape, loop).union(harmonic(p2, dx, dy, shape, loop)) - set([p1, p2])
    return harmonic(p1, -dx, -dy, shape, loop).union(harmonic(p2, dx, dy, shape, loop))


def solution1(problem=puzzle_input):
    sparse, frequencies, shape = process_input(problem)
    anti_nodes = set([])
    for frequency, points in frequencies.items():
        for x, y in itertools.product(points, points):
            if x == y:
                continue

            anti_nodes = anti_nodes.union(anti_node(x, y, shape))

    return len(anti_nodes)
    

def solution2(problem=puzzle_input):
    sparse, frequencies, shape = process_input(problem)
    anti_nodes = set([])
    for frequency, points in frequencies.items():
        for x, y in itertools.product(points, points):
            if x == y:
                continue

            anti_nodes = anti_nodes.union(anti_node(x, y, shape, True))

    return len(anti_nodes)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
