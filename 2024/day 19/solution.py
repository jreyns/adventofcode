from util.util import get_input

puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    towels = None
    patterns = []
    empty_seen = False
    for line in lines:
        if not line:
            empty_seen = True
            continue

        if empty_seen:
            patterns.append(line)
            continue

        towels = line.split(", ")
    return towels, patterns


def is_valid(pattern, towels, cache):
    if pattern == "":
        return True

    if pattern in cache:
        return cache[pattern]

    for towel in filter(pattern.startswith, towels):
        if is_valid(pattern[len(towel):], towels, cache):
            cache[pattern] = True
            return True

    cache[pattern] = False
    return False


def count_all_options(pattern, towels, cache):
    if pattern == "":
        return 1

    if pattern in cache:
        return cache[pattern]

    options = []
    for towel in filter(pattern.startswith, towels):
        new_pattern = pattern[len(towel):]
        if new_pattern not in cache:
            cache[new_pattern] = count_all_options(new_pattern, towels, cache)
        options.append(cache[new_pattern])

    cache[pattern] = sum(options)

    return cache[pattern]


def solution1(problem=puzzle_input):
    towels, patterns = process_input(problem)
    cache = {}
    return sum([is_valid(pattern, towels, cache) for pattern in patterns])


def solution2(problem=puzzle_input):
    towels, patterns = process_input(problem)
    cache = {}
    return sum([count_all_options(pattern, towels, cache) for pattern in patterns])


print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")

print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
