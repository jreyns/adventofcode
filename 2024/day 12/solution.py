from itertools import combinations
from util.util import get_input, get_sparse_map, get_adjacent_coordinates


puzzle_input = get_input()
test_input = get_input("test.txt")


def calculate_fence_price(plant, positions, sparse):
    total_plant_price = 0
    to_check = positions
    while to_check:
        seen = set([])
        current = {to_check.pop()}
        perimeter = 0
        while current:
            position = current.pop()
            seen.add(position)
            options = [pos for pos in get_adjacent_coordinates(*position, None) if sparse.get(pos, "") == plant]
            perimeter += 4 - len(options)
            [current.add(option) for option in options if option not in seen]

        total_plant_price += len(seen) * perimeter
        to_check -= seen

    return total_plant_price


def count_corners(x, x1, y1, x2, y2, plant, sparse, multiple=False):
    # Same line so no corner
    if x1 == x2 or y1 == y2:
        return 0

    # single corner L, can't exist when there are more than 2 options
    if x1 == x and sparse.get((x2, y1), "") == plant:
        return 1 - multiple

    # single corner L, can't exist when there are more than 2 options
    if x2 == x and sparse.get((x1, y2), "") == plant:
        return 1 - multiple

    # Inner and outer edge are a corner, prevent over counting when there are more than 2 options,
    # the other side will be counted separately
    if x1 == x and sparse.get((x2, y1), "") != plant:
        return 2 - multiple

    # Inner and outer edge are a corner, prevent over counting when there are more than 2 options,
    # the other side will be counted separately
    if x2 == x and sparse.get((x1, y2), "") != plant:
        return 2 - multiple

    return 0


def find_sides(plant, positions, sparse):
    sides = 0
    for x, y in positions:
        options = [
            option for option in get_adjacent_coordinates(x, y, None) if sparse.get(option, "") == plant
        ]

        actions = {
            0: lambda u: 4,
            1: lambda u: 2,
            2: lambda u: count_corners(x, *u[0], *u[1], plant, sparse),
            3: lambda u: sum([count_corners(x, *p1, *p2, plant, sparse, True) for p1, p2 in combinations(u, 2)]),
            4: lambda u: sum([count_corners(x, *p1, *p2, plant, sparse, True) for p1, p2 in combinations(u, 2)]),
        }

        sides += actions[len(options)](options)

    return sides


def calculate_bulk_fence_price(plant, positions, sparse):
    total_plant_price = 0
    to_check = positions
    while to_check:
        seen = set([])
        current = {to_check.pop()}
        while current:
            position = current.pop()
            seen.add(position)
            options = [pos for pos in get_adjacent_coordinates(*position, None) if sparse.get(pos, "") == plant]
            [current.add(option) for option in options if option not in seen]

        total_plant_price += len(seen) * find_sides(plant, seen, sparse)
        to_check -= seen

    return total_plant_price


def solution1(problem=puzzle_input):
    plants, sparse, _ = get_sparse_map(problem)
    return sum([calculate_fence_price(plant, positions, sparse) for plant, positions in plants.items()])
    

def solution2(problem=puzzle_input):
    plants, sparse, _ = get_sparse_map(problem)
    return sum([calculate_bulk_fence_price(plant, positions, sparse) for plant, positions in plants.items()])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
