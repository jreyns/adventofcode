import re
from collections import Counter
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    numeric_keypad = {
        (0, 0): "7", (1, 0): "8", (2, 0): "9",
        (0, 1): "4", (1, 1): "5", (2, 1): "6",
        (0, 2): "1", (1, 2): "2", (2, 2): "3",
        (1, 3): "0", (2, 3): "A"
    }
    directional_keypad = {
        (1, 0): "^", (2, 0): "A",
        (0, 1): "<", (1, 1): "v", (2, 1): ">",
    }
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input, numeric_keypad, directional_keypad


def find_shortest_paths(start, end, sparse):
    x1, y1 = start
    x2,  y2 = end
    dx, dy = x2 - x1, y2 - y1
    vertical_path = "v" * dy + "^" * -dy
    horizontal_path = ">" * dx + "<" * -dx
    if dx > 0 and (x1, y2) in sparse:
        return "".join([vertical_path, horizontal_path, "A"])

    if (x2, y1) in sparse:
        return "".join([horizontal_path, vertical_path, "A"])

    return "".join([vertical_path, horizontal_path, "A"])


def find_shortest_sequence(keypad, sequence):
    lookup = {v: k for k, v in keypad.items()}
    start = lookup["A"]
    pushes = []
    for end in map(lookup.get, sequence):
        pushes.append(find_shortest_paths(start,  end, keypad))
        start = end
    return Counter(pushes)


def keypad_inception(sequences, numeric_keypad, directional_keypad, inception):
    total = 0
    for sequence in sequences:
        sub_sequences = find_shortest_sequence(numeric_keypad, sequence)
        for keypad in [directional_keypad] * inception:
            new_sub_sequences = Counter()
            for sub_sequence, count in sub_sequences.items():
                new_counts = find_shortest_sequence(keypad, sub_sequence)
                for k in new_counts:
                    new_counts[k] *= count
                new_sub_sequences.update(new_counts)
            sub_sequences = new_sub_sequences
        total += sum(len(k)*v for k, v in sub_sequences.items()) * int(re.findall(r"\d+", sequence)[0])

    return total


def solution1(problem=puzzle_input):
    return keypad_inception(*process_input(problem), 2)
    

def solution2(problem=puzzle_input):
    return keypad_inception(*process_input(problem), 25)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
