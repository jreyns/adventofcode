import re
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    potential = {}
    pattern = r"(.*) would (.*) (\d+) happiness units by sitting next to (.*)."
    lookup = {"gain": 1, "lose": -1}
    for line in lines:
        p1, sign, happiness, p2 = re.search(pattern, line).groups()
        if p1 not in potential:
            potential[p1] = {}
        potential[p1][p2] = int(happiness) * lookup[sign]
    return potential


def simulate_positions(current, seen, locations, total, f=max):
    if len(seen) == (len(locations) - 1):
        return total + locations[seen[0]][current] + locations[current][seen[0]]

    return f([
        simulate_positions(other, seen + [current], locations, total + distance + locations[other][current], f)
        for other, distance in locations[current].items() if other not in seen
    ])


def solution1(problem=puzzle_input):
    potential = process_input(problem)
    return max([simulate_positions(current, [], potential, 0, max) for current in potential])
    

def solution2(problem=puzzle_input):
    potential = process_input(problem)
    for k in potential:
        potential[k]["me"] = 0
    potential["me"] = {k: 0 for k in potential}
    return max([simulate_positions(current, [], potential, 0, max) for current in potential])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
