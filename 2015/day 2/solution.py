
day_2_input = []
with open("input.txt", "r") as file:
    for line in file:
        day_2_input.append(line[:-1])


def calculate_paper_dimensions(dimensions: str):
    """

    :param dimensions:
    :return:
    """
    l, w, h = [int(x) for x in dimensions.split("x")]
    lxw, wxh, lxh = (l*w, w*h, l*h)
    slack = min(lxw, wxh, lxh)

    return 2 * (lxw + wxh + lxh) + slack


def calculate_ribbon_length(dimensions: str):
    """

    :param dimensions:
    :return:
    """
    l, w, h = [int(x) for x in dimensions.split("x")]
    dimension_values = [l, w, h]
    volume = l*w*h
    max_value = max(dimension_values)
    dimension_values.remove(max_value)
    wrap = 2 * sum(dimension_values)
    return wrap + volume


print(f"solution part 1 is: {sum([calculate_paper_dimensions(dimensions) for dimensions in day_2_input])}")

print(f"solution part 2 is: {sum([calculate_ribbon_length(dimensions) for dimensions in day_2_input])}")
