import json

with open("input.txt", "r") as file:
    puzzle_input = json.load(file)

with open("test.txt", "r") as file:
    test_input = json.load(file)


def total_sum(structure, skip=""):
    if isinstance(structure, int):
        return structure

    if isinstance(structure, list):
        return sum([x if isinstance(x, int) else total_sum(x, skip) for x in structure])

    if isinstance(structure, dict) and (skip in structure or skip in structure.values()):
        return 0

    if isinstance(structure, dict):
        return sum([total_sum(k, skip) + total_sum(v, skip) for k, v in structure.items()])

    return 0


def solution1(problem=puzzle_input):
    return total_sum(problem)
    

def solution2(problem=puzzle_input):
    return total_sum(problem, "red")


print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
