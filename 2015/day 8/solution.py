
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


def solution1(problem=puzzle_input):
    characters = process_input(problem)
    return sum([len(c) - len(eval(c)) for c in characters])
    

def solution2(problem=puzzle_input):
    characters = process_input(problem)
    return sum([len(c.replace("\\", "\\\\").replace('"', '\\"')) + 2 - len(c) for c in characters])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
