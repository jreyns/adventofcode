

lines = []
with open("input.txt", "r") as input_file:
    for line in input_file:
        lines.append(line[:-1])


class NiceRule:

    def __init__(self):
        self._is_nice = False

    def evaluate(self, character: str):
        if self._is_nice:
            return

    def is_nice(self):
        return self._is_nice


class ThreeVowels(NiceRule):

    def __init__(self):
        super().__init__()
        self._vowels = []

    def evaluate(self, character: str):
        if self._is_nice:
            return

        if character in "aeiou":
            self._vowels.append(character)

        self._is_nice = len(self._vowels) >= 3


class Repetition(NiceRule):

    def __init__(self, repetition_threshold: int = 1):
        super().__init__()
        self._last_character = ""
        self._repetition_threshold = repetition_threshold
        self._repetition_count = 0

    def evaluate(self, character: str):
        if self._is_nice:
            return

        if character == self._last_character:
            self._repetition_count += 1
        else:
            self._repetition_count = 0

        self._last_character = character

        self._is_nice = self._repetition_count >= self._repetition_threshold


class DoesNotContain(NiceRule):

    def __init__(self, not_allowed: list = ["ab", "cd", "pq", "xy"]):
        super().__init__()
        self._not_allowed = not_allowed
        self._characters = ""

    def evaluate(self, character: str):
        self._characters += character

    def is_nice(self):
        for not_allowed in self._not_allowed:
            if not_allowed in self._characters:
                return False
        return True


def is_nice_part1(sequence: str):

    part1_rules = [
        ThreeVowels(),
        Repetition(),
        DoesNotContain()
    ]

    for c in sequence:
        for rule in part1_rules:
            rule.evaluate(c)

    for rule in part1_rules:
        if not rule.is_nice():
            return False

    return True


def do_test():
    input_sequences = [
        "ugknbfddgicrmopn",
        "aaa",
        "jchzalrnumimnmhp",
        "haegwjzuvuyypxyu",
        "dvszwmarrgswjxmb"
    ]

    for sequence in input_sequences:
        print(f"{sequence}: {is_nice_part1(sequence)}")


do_test()


def solve_part_1():
    return sum([1 for sequence in lines if is_nice_part1(sequence)])


print(f"solution part 1: {solve_part_1()}")


class PairRepetition(NiceRule):

    def __init__(self, repetition_threshold: int = 1):
        super().__init__()
        self._previous_pair = ""
        self._last_character = ""
        self._repetition_threshold = repetition_threshold
        self._repetitions = {}

    def evaluate(self, character: str):
        current_pair = self._last_character + character

        if current_pair not in self._repetitions:
            self._repetitions[current_pair] = 1
            self._previous_pair = current_pair
        elif current_pair == self._previous_pair:
            self._previous_pair = ""
        else:
            self._repetitions[current_pair] += 1
            self._previous_pair = current_pair

        self._last_character = character

    def is_nice(self):
        for pair in self._repetitions:
            if self._repetitions[pair] > self._repetition_threshold:
                return True
        return False


class SkippedRepetition(NiceRule):

    def __init__(self, repetition_threshold: int = 1):
        super().__init__()
        self._last_characters = []
        self._repetition_threshold = repetition_threshold
        self._repetition_count = 0

    def evaluate(self, character: str):
        if self._is_nice:
            return

        if len(self._last_characters) > 1 and character == self._last_characters[-2]:
            self._repetition_count += 1
        else:
            self._repetition_count = 0

        self._last_characters.append(character)

        self._is_nice = self._repetition_count >= self._repetition_threshold


def is_nice_part2(sequence: str):

    part1_rules = [
        PairRepetition(),
        SkippedRepetition()
    ]

    for c in sequence:
        for rule in part1_rules:
            rule.evaluate(c)

    for rule in part1_rules:
        if not rule.is_nice():
            return False

    return True


def do_test_2():
    input_sequences = [
        "qjhvhtzxzqqjkmpb",
        "xxyxx",
        "uurcxstgmygtbstg",
        "ieodomkazucvgmuy"
    ]

    for sequence in input_sequences:
        print(f"{sequence}: {is_nice_part2(sequence)}")


do_test_2()


def solve_part_2():
    return sum([1 for sequence in lines if is_nice_part2(sequence)])


print(f"solution part 2: {solve_part_2()}")