
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            int(line)
        )
    return processed_input


def find_combinations(containers, eggnog, used):
    if eggnog == 0:
        return [used]

    solutions = []
    for i, container in enumerate(containers):
        if container <= eggnog:
            solutions += find_combinations(containers[i+1:], eggnog - container, used + [container])

    return solutions


def solution1(problem=puzzle_input, eggnog=150):
    containers = process_input(problem)
    return len(find_combinations(containers, eggnog, []))
    

def solution2(problem=puzzle_input, eggnog=150):
    containers = process_input(problem)
    containers_used = {}
    for combination in find_combinations(containers, eggnog, []):
        used = len(combination)
        if used not in containers_used:
            containers_used[used] = []
        containers_used[used].append(combination)

    return len(containers_used[min(containers_used)])
    
    
print(f"test 1: {solution1(test_input, 25)}")
print(f"test 2: {solution2(test_input, 25)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
