from util.util import get_input
import re

puzzle_input = get_input()


def get_display():
    return [[False for _ in range(1000)] for _ in range(1000)]


def parse_operation(operation):
    match = re.findall(r"(.*) (\d+),(\d+) through (\d+),(\d+)", operation)[0]
    return match[0], (int(match[1]), int(match[3])), (int(match[2]), int(match[4]))


def toggle(leds, x, y):
    for d in range(y[0], y[1]+1):
        leds[d] = [not led if x[0] <= i <= x[1] else led for i, led in enumerate(leds[d])]


def turn_on(leds, x, y):
    for d in range(y[0], y[1]+1):
        leds[d] = [True if x[0] <= i <= x[1] else led for i, led in enumerate(leds[d])]


def turn_off(leds, x, y):
    for d in range(y[0], y[1]+1):
        leds[d] = [False if x[0] <= i <= x[1] else led for i, led in enumerate(leds[d])]


display = get_display()


for line in puzzle_input:
    op, x, y = parse_operation(line)

    if op == 'toggle':
        toggle(display, x, y)
    elif op == 'turn on':
        turn_on(display, x, y)
    elif op == 'turn off':
        turn_off(display, x, y)


print(sum([sum(x) for x in display]))


# Part 2


def get_display2():
    return [[0 for _ in range(1000)] for _ in range(1000)]


def toggle2(leds, x, y):
    for d in range(y[0], y[1]+1):
        leds[d] = [led+2 if x[0] <= i <= x[1] else led for i, led in enumerate(leds[d])]


def turn_on2(leds, x, y):
    for d in range(y[0], y[1]+1):
        leds[d] = [led+1 if x[0] <= i <= x[1] else led for i, led in enumerate(leds[d])]


def turn_off2(leds, x, y):
    for d in range(y[0], y[1]+1):
        leds[d] = [max(led-1, 0) if x[0] <= i <= x[1] else led for i, led in enumerate(leds[d])]


display = get_display2()


for line in puzzle_input:
    op, x, y = parse_operation(line)

    if op == 'toggle':
        toggle2(display, x, y)
    elif op == 'turn on':
        turn_on2(display, x, y)
    elif op == 'turn off':
        turn_off2(display, x, y)


print(sum([sum(x) for x in display]))
