import re
from util.util import get_input

sue_dna = get_input("sue.txt")
puzzle_input = get_input()


def process_input(lines=puzzle_input):
    processed_input = {}
    for line in lines:
        pairs = {k: int(v) for k, v in re.findall(r"(\w+):? (\d+)", line)}
        if "Sue" in pairs:
            processed_input[pairs["Sue"]] = pairs
            del pairs["Sue"]
            continue
        processed_input[0] = pairs
    return processed_input


def solution1(problem=puzzle_input):
    processed = process_input(problem)
    sue_hint = process_input(sue_dna)[0]
    best_match = None
    score = 0
    for sue, attributes in processed.items():
        total = sum([sue_hint[key] == attributes[key] for key in attributes])
        if score < total:
            best_match = sue
            score = total
    return best_match


def solution2(problem=puzzle_input):
    processed = process_input(problem)
    sue_hint = process_input(sue_dna)[0]
    best_match = None
    score = 0
    for sue, attributes in processed.items():
        total = sum([
            sue_hint[key] < attributes[key] if key in ["cats", "trees"] else
            sue_hint[key] > attributes[key] if key in ["pomeranians", "goldfish"] else
            sue_hint[key] == attributes[key]
            for key in attributes
        ])
        if score < total:
            best_match = sue
            score = total
    return best_match


print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
