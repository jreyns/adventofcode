import random
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")
test_input2 = get_input("test2.txt")


def process_input(lines=puzzle_input):
    rules = []
    calibration = None
    split_seen = False
    for line in lines:
        if not line:
            split_seen = True
            continue

        if not split_seen:
            rules.append(
                line.split(" => ")
            )
            continue
        calibration = line
    return rules, calibration


def perform_rewrites(rules, calibration):
    molecules = set([])
    for p, r in rules:
        length = len(p)
        index = calibration.find(p)
        while index != -1:
            molecules.add("".join([calibration[:index], r, calibration[index + length:]]))
            index = calibration.find(p, index + length)

    return len(molecules)


def synthesise(rules, calibration):
    random.shuffle(rules)
    molecule = calibration
    old_molecule = ""
    steps = 0
    while old_molecule != molecule:
        old_molecule = molecule
        for r, p in rules:
            while p in molecule:
                steps += molecule.count(p)
                molecule = molecule.replace(p, r)
    return int(molecule == 'e') * steps


def solution1(problem=puzzle_input):
    processed = process_input(problem)
    return perform_rewrites(*processed)
    

def solution2(problem=puzzle_input):
    processed = process_input(problem)
    count = 0
    while not count:
        count = synthesise(*processed)
    return count
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input2)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
