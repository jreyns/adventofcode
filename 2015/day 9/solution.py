import re
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    locations = {}
    for line in lines:
        source, destination, distance = re.search(r"(.*) to (.*) = (\d+)", line).groups()
        if source not in locations:
            locations[source] = []
        if destination not in locations:
            locations[destination] = []
        locations[source].append((destination, int(distance)))
        locations[destination].append((source, int(distance)))
    return locations


def simulate_path(current, seen, locations, total, f=min):
    if len(seen) == (len(locations) - 1):
        return total

    return f([
        simulate_path(other, seen + [current], locations, total + distance, f)
        for other, distance in locations[current] if other not in seen
    ])


def solution1(problem=puzzle_input):
    locations = process_input(problem)
    return min([simulate_path(current, [], locations, 0) for current in locations])
    

def solution2(problem=puzzle_input):
    locations = process_input(problem)
    return max([simulate_path(current, [], locations, 0, max) for current in locations])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
