import re
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    return list(map(int, re.findall(r"\d+", lines[0])))


def find_n(row, column):
    n = sum(range(1, column+1))
    for j in range(0, row-1):
        n += column + j
    return n


def incr(x):
    return x+1


def code(x):
    return (x * 252533) % 33554393


def solution1(problem=puzzle_input, start=20151125, f=code):
    processed = process_input(problem)
    result = start
    for _ in range(find_n(*processed)-1):
        result = f(result)
    return result


print(f"test 1: {solution1(test_input, 1, incr)}")

    
print(f"solution 1: {solution1()}")
