import re
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input[0]


def increment(sequence):
    s = [c for c in sequence]
    max_ord = ord("z")

    i = len(s) - 1
    while i >= 0:
        new_ord = ord(s[i]) + 1
        if new_ord <= max_ord:
            s[i] = chr(new_ord)
            break
        s[i] = "a"
        i -= 1

    return "".join(s)


def is_valid(sequence):
    if any([c in sequence for c in ["i", "o", "l"]]):
        return False

    letters = "abcdefghijklmnopqrstuvwxyz"
    increasing_count = sum([letters[i:i+3] in sequence for i in range(24)])
    if increasing_count < 1:
        return False

    pattern = "|".join([f"{c}+" for c in letters])
    double_count = sum([len(x) > 1 for x in re.findall(pattern, sequence)])
    if double_count < 2:
        return False

    return True


def solution1(problem=puzzle_input):
    processed = process_input(problem)
    while not is_valid(processed):
        processed = increment(processed)
    return processed
    

def solution2():
    processed = increment("vzbxxyzz")
    while not is_valid(processed):
        processed = increment(processed)
    return processed
    
    
print(f"test 1: {solution1(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
