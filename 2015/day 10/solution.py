import re
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input[0]


def split_and_count(sequence):
    pattern = r"|".join([f"{x}+" for x in range(1, 10)])
    return "".join([f"{len(x)}{x[0]}" for x in re.findall(pattern, sequence)])


def solution1(problem=puzzle_input, times=40):
    sequence = process_input(problem)
    for _ in range(times):
        sequence = split_and_count(sequence)
    return len(sequence)
    
    
print(f"test 1: {solution1(test_input, times=5)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution1(times=50)}")
