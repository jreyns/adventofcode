import re
from util.util import get_input
import numpy as np


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            tuple(map(int, re.findall(r"-?\d+", line)))
        )
    return processed_input


def split(total, n):
    if n <= 1:
        return [[total]]

    options = []
    for i in range(total+1):
        remaining = split(total - i, n - 1)
        for option in remaining:
            options.append([i] + option)
    return options


def score_cookie_recipe(ingredients, s, calories=False):
    stats = []
    for i in range(5):
        stats.append(sum([ingredient[i] * s[k] for k, ingredient in enumerate(ingredients)]))
    if calories and stats[-1] != 500:
        return 0
    if any([x < 0 for x in stats[:4]]):
        return 0
    return np.prod(stats[:4])


def solution1(problem=puzzle_input):
    processed = process_input(problem)
    possible_splits = split(100, len(processed))
    return max(score_cookie_recipe(processed, s) for s in possible_splits)
    

def solution2(problem=puzzle_input):
    processed = process_input(problem)
    possible_splits = split(100, len(processed))
    return max(score_cookie_recipe(processed, s, True) for s in possible_splits)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
