
from util.util import get_input, get_sparse_map, get_adjacent_coordinates


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    return get_sparse_map(lines, ["#"])


def step(lights, shape):
    new_lights = set([])
    for y in range(shape[1]):
        for x in range(shape[0]):
            neighbours = len([
                p for p in get_adjacent_coordinates(x, y, shape, True)
                if p in lights
            ])
            if (x, y) in lights and 2 <= neighbours <= 3:
                new_lights.add((x, y))
            if (x, y) not in lights and neighbours == 3:
                new_lights.add((x, y))

    return new_lights


def solution1(problem=puzzle_input, steps=100):
    symbols, _, shape = process_input(problem)
    lights = symbols["#"]
    for _ in range(steps):
        lights = step(lights, shape)
    return len(lights)
    

def solution2(problem=puzzle_input, steps=100):
    symbols, _, shape = process_input(problem)
    always_on = {(0, 0), (0, shape[1]-1), (shape[0]-1, 0), shape}
    lights = symbols["#"].union(always_on)
    for _ in range(steps):
        lights = step(lights, shape).union(always_on)
    return len(lights)
    
    
print(f"test 1: {solution1(test_input, 4)}")
print(f"test 2: {solution2(test_input, 5)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
