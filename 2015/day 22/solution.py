from copy import deepcopy
import re
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")
least_mana_used = None


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            int(re.findall(r"\d+", line)[0])
        )
    spells = {
        "missile": (53, 4, 0, 0, 0, 0),
        "drain": (73, 2, 2, 0, 0, 0),
        "shield": (113, 0, 0, 7, 0, 6),
        "poison": (173, 3, 0, 0, 0, 6),
        "recharge": (229, 0, 0, 0, 101, 5)
    }
    return processed_input, spells


def simulate_battle(player_hp, player_mp, boss_hp, boss_dmg, active_effects, mp_used, is_player_turn, spells):
    player_armour = 0
    new_active_effects = {}
    for key, (cost, dmg, hp, armor, mp, turns) in active_effects.items():
        if turns >= 0:  # spell effect applies now
            boss_hp -= dmg
            player_hp += hp
            player_armour += armor
            player_mp += mp

        if turns > 1:  # spell carries over
            new_active_effects[key] = (cost, dmg, hp, armor, mp, turns - 1)

    global least_mana_used
    if boss_hp <= 0:
        if mp_used < least_mana_used:
            least_mana_used = mp_used
        return mp_used

    if mp_used >= least_mana_used:
        return float("inf")

    if is_player_turn:
        options = []
        for spell, (cost, dmg, hp, armor, mp, turns) in spells.items():
            if cost <= player_mp and spell not in new_active_effects:
                a = {spell: (cost, dmg, hp, armor, mp, turns)}
                a.update(new_active_effects)
                options.append(
                    simulate_battle(
                        player_hp, player_mp - cost, boss_hp, boss_dmg, a, mp_used + cost, False, spells
                    )
                )
        return min(options) if options else float("inf")

    else:
        player_hp += player_armour - boss_dmg if player_armour - boss_dmg < 0 else -1
        if player_hp <= 0:
            return float("inf")
        return simulate_battle(player_hp, player_mp, boss_hp, boss_dmg, new_active_effects, mp_used, True, spells)


def solution1(problem=puzzle_input, player_hp=50, player_mp=500):
    global least_mana_used
    (boss_hp, boss_dmg), spells = process_input(problem)
    least_mana_used = float("inf")
    return simulate_battle(player_hp, player_mp, boss_hp, boss_dmg, {}, 0, True, spells)
    

def solution2(problem=puzzle_input, player_hp=50, player_mp=500):
    global least_mana_used
    (boss_hp, boss_dmg), spells = process_input(problem)
    least_mana_used = float("inf")
    active_effects = {"hard": (0, 0, -0.5, 0, 0, float("inf"))}
    return simulate_battle(player_hp, player_mp, boss_hp, boss_dmg, active_effects, 0, True, spells)
    
    
print(f"test 1: {solution1(test_input, 10, 250)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
