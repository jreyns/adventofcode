import numpy as np
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            int(line)
        )
    return processed_input


def backtrack(index, packages, current, seen, total, min_length, min_quantum):
    if total == 0:
        return current, set(seen), np.prod(current, dtype="int64")

    min_option = []
    min_seen = set([])

    if len(current) < min_length:
        for i, package in enumerate(packages[index+1:]):
            total_index = index + i + 1
            if total_index in seen:
                continue

            if package > total:
                continue

            option, s, q = backtrack(total_index, packages, current + [package], seen + [total_index], total - package, min_length, min_quantum)
            if option and len(option) <= min_length and q < min_quantum:
                min_length, min_option, min_seen, min_quantum = len(option), option, s, q

    return min_option, min_seen, min_quantum


def split_in_groups(packages, group_count):
    group_size = sum(packages) // group_count
    packages = list(reversed(packages))
    group, _, quantum = backtrack(-1, packages, [], [], group_size, float("inf"), float("inf"))
    return quantum


def solution1(problem=puzzle_input):
    packages = process_input(problem)
    return split_in_groups(packages, 3)
    

def solution2(problem=puzzle_input):
    packages = process_input(problem)
    return split_in_groups(packages, 4)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
