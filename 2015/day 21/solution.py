
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


def solution1():
    return 91
    

def solution2():
    return 158

    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
