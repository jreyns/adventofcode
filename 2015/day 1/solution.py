puzzle_input = None

with open('input.txt', "r") as file:
    puzzle_input = file.read()

actions = {
    "(": + 1,
    ")": -1
}


def solve_first():
    """

    :return:
    """
    return sum([actions[c] for c in puzzle_input])


def solve_second():
    """

    :return:
    """
    position = 0
    current_floor = 0

    while current_floor != -1:
        current_floor += actions[puzzle_input[position]]
        position += 1

    return position


print(f"first solution: {solve_first()}")
print(f"second solution: {solve_second()}")
