import math
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            int(line)
        )
    return processed_input[0]


def get_divisors(n):
    large_divisors = []
    for i in range(1, int(math.sqrt(n) + 1)):
        if n % i == 0:
            yield i
            if i*i != n:
                large_divisors.append(n / i)
    for divisor in reversed(large_divisors):
        yield divisor


def get_divisors_constrained(n):
    divisors = list(get_divisors(n))
    return [x for x in divisors if x * 50 >= n]


def presents(divisors, factor=10):
    return sum([i * factor for i in divisors])


def solution1(problem=puzzle_input):
    processed = process_input(problem)
    house = 1
    while presents(get_divisors(house)) < processed:
        house += 1
    return house
    

def solution2(problem=puzzle_input):
    processed = process_input(problem)
    house = 1
    while presents(get_divisors_constrained(house), 11) < processed:
        house += 1
    return house
    
    
print(f"test 1: {solution1(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
