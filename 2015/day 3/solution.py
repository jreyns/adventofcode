
puzzle_input = None
with open("input.txt", "r") as file:
    puzzle_input = file.read()


def move_position(current_position: tuple, action: str):
    """

    :param current_position:
    :param action:
    :return:
    """
    actions = {
        "^": (0, 1),
        ">": (1, 0),
        "v": (0, -1),
        "<": (-1, 0)
    }
    vector = actions[action]
    return current_position[0] + vector[0], current_position[1] + vector[1]


def get_all_houses_visited(actions: str, start_position: tuple = (0, 0)):
    """

    :param actions:
    :param start_position:
    :return:
    """
    current_position = start_position

    houses_visited = {
        start_position: 1
    }

    for action in actions:
        current_position = move_position(current_position, action)
        if current_position in houses_visited:
            houses_visited[current_position] += 1
        else:
            houses_visited[current_position] = 1

    return houses_visited


def get_all_houses_visited_with_robot(actions: str, start_position: tuple = (0, 0)):
    """

    :param actions:
    :param start_position:
    :return:
    """
    current_position_santa = start_position
    current_position_robot = start_position

    houses_visited = {
        start_position: 2
    }

    steps = 0
    while steps < len(actions):
        current_position_santa = move_position(current_position_santa, actions[steps])
        if current_position_santa in houses_visited:
            houses_visited[current_position_santa] += 1
        else:
            houses_visited[current_position_santa] = 1

        if steps + 1 < len(actions):
            current_position_robot = move_position(current_position_robot, actions[steps + 1])
            if current_position_robot in houses_visited:
                houses_visited[current_position_robot] += 1
            else:
                houses_visited[current_position_robot] = 1
        steps += 2

    return houses_visited


def get_all_houses_visited_generically(actions: str, deliverers: int = 2, start_position: tuple = (0, 0)):
    """

    :param actions:
    :param deliverers:
    :param start_position:
    :return:
    """
    houses_visited = {}

    def get_all_houses_from_deliverer(deliverer: int, actions: str):
        houses = [start_position]
        for action_index in range(deliverer, len(actions), deliverers):
            houses.append(move_position(houses[-1], actions[action_index]))
        return houses

    for deliverer in range(deliverers):
        for house in get_all_houses_from_deliverer(deliverer, actions):
            if house in houses_visited:
                houses_visited[house] += 1
            else:
                houses_visited[house] = 1

    return houses_visited


print(f"solution part 1: {len(get_all_houses_visited(puzzle_input))}")

print(f"solution part 2: {len(get_all_houses_visited_with_robot(puzzle_input))}")
print(f"solution part 2 (generic): {len(get_all_houses_visited_generically(puzzle_input))}")
