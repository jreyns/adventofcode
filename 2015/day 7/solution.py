
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = {}
    for line in lines:
        wire = line.replace(" -> ", " ").split(" ")
        processed_input[wire[-1]] = wire[:-1]
    return processed_input


def get_value(letter, circuit):
    if letter not in circuit:
        return int(letter)

    operation = circuit[letter]
    if isinstance(operation, int):
        return operation

    if len(operation) == 1:
        circuit[letter] = get_value(operation[0], circuit)
        return circuit[letter]

    if len(operation) == 2:
        circuit[letter] = ~get_value(operation[1], circuit)
        return circuit[letter]

    left, operator, right = operation

    if operator == "AND":
        circuit[letter] = get_value(left, circuit) & get_value(right, circuit)

    if operator == "OR":
        circuit[letter] = get_value(left, circuit) | get_value(right, circuit)

    if operator == "LSHIFT":
        circuit[letter] = get_value(left, circuit) << int(right)

    if operator == "RSHIFT":
        circuit[letter] = get_value(left, circuit) >> int(right)

    return circuit[letter]


def solution1(problem=puzzle_input):
    circuit = process_input(problem)
    if "a" in circuit:
        return get_value("a", circuit)
    return [get_value(c, circuit) for c in circuit]
    

def solution2(problem=puzzle_input):
    new_b = 46065
    circuit = process_input(problem)
    if "b" in circuit:
        circuit["b"] = new_b
    if "a" in circuit:
        return get_value("a", circuit)
    return [get_value(c, circuit) for c in circuit]
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
