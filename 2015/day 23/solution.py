
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line.replace(",", "").split(" ")
        )
    return processed_input


def run(instructions, registers):
    index = 0
    while index < len(instructions):
        instruction = instructions[index]
        if len(instruction) == 2:
            op, r = instruction
            if op == "hlf":
                registers[r] = registers[r] // 2
            elif op == "tpl":
                registers[r] = registers[r] * 3
            elif op == "inc":
                registers[r] = registers[r] + 1
            elif op == "jmp":
                index += int(r)
                continue

        if len(instruction) == 3:
            op, r, offset = instruction

            if op == "jie" and registers[r] % 2 == 0:
                index += int(offset)
                continue

            elif op == "jio" and registers[r] == 1:
                index += int(offset)
                continue

        index += 1
    return registers


def solution1(problem=puzzle_input):
    processed = process_input(problem)
    return run(processed, {"a": 0, "b": 0})
    

def solution2(problem=puzzle_input):
    processed = process_input(problem)
    return run(processed, {"a": 1, "b": 0})
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
