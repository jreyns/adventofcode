import re
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            tuple(map(int, re.findall(r"\d+", line)))
        )
    return processed_input


def get_distance(reindeer, seconds):
    speed, time, sleep = reindeer
    step_distance = speed * time
    step_seconds = time + sleep
    steps = seconds // step_seconds
    seconds_remaining = min(time, seconds % step_seconds)
    return step_distance * steps + seconds_remaining * speed


def solution1(problem=puzzle_input, seconds=2503):
    processed = process_input(problem)
    return max([get_distance(reindeer, seconds) for reindeer in processed])
    

def solution2(problem=puzzle_input, seconds=2503):
    processed = process_input(problem)
    scores = {k: 0 for k in processed}
    for s in range(1, seconds):
        in_lead = {}
        for reindeer in processed:
            distance = get_distance(reindeer, s)
            if distance not in in_lead:
                in_lead[distance] = []
            in_lead[distance].append(reindeer)
        wins = in_lead[max(in_lead)]
        for win in wins:
            scores[win] += 1
    return max(scores.values())
    
    
print(f"test 1: {solution1(test_input, 1000)}")
print(f"test 2: {solution2(test_input, 1000)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
