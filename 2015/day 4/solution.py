import hashlib

hash_key = None
with open("input.txt", "r") as file:
    hash_key = file.read()[:-1]


def get_md5_hex(key: str, digit: int):
    """

    :param key:
    :param digit:
    :return:
    """
    return hashlib.md5(f"{key}{digit}".encode()).hexdigest()


def is_valid_hash(hash: str, start: str):
    """

    :param hash:
    :param start:
    :return:
    """
    return hash[:len(start)] == start


def get_smallest_number_with_hash(key: str, start: str = "00000"):
    """

    :param key:
    :param start:
    :return:
    """
    number = 1
    while not is_valid_hash(get_md5_hex(key, number), start):
        number += 1

    return number


print(f"solution part 1: {get_smallest_number_with_hash(hash_key)}")
print(f"solution part 2: {get_smallest_number_with_hash(hash_key, '000000')}")
