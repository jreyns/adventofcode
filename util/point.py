
class Point3D:

    def __init__(self, x: int = 0, y: int = 0, z: int = 0):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return str((self.x, self.y, self.z))

    def __hash__(self):
        return (self.x, self.y, self.z).__hash__()

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.z == other.z
