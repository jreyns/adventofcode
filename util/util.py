import os
import html2text
import requests
import math


cookie = {"session": '53616c7465645f5fa92c72cade4961d14c5ef2f78b97b0894a9f0ca4910f37a2f5fbd7a86e9fce7d8fdca6e7fb342ceb6cf5b33488a7bb6327e53c7e6c959342'}

solution_template = """
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


def solution1(problem=puzzle_input):
    return
    

def solution2(problem=puzzle_input):
    return
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
"""


def least_common_multiple(x, y):
    l, h = x, y
    if x > y:
        l, h = y, x

    count = 1
    while h * count % l != 0:
        count += 1

    return count * h


def transpose(matrix):
    transposed = []

    for i in range(len(matrix[0])):
        transposed.append([])
        for j in range(len(matrix)):
            transposed[i].append(matrix[j][i])

    return transposed


def sign(value) -> int:
    """

    :return:
    """
    if value >= 0:
        return 1
    return -1


def euclidean_distance(pos1, pos2):
    if len(pos1) != len(pos2):
        raise ValueError(f"incompatible dimensions for distance: {len(pos1)} vs {len(pos2)}")
    return math.sqrt(sum([(pos1[i] - pos2[i]) ** 2 for i in range(len(pos1))]))


def manhattan_distance(pos1, pos2):
        return sum([abs(pos1[i] - pos2[i])for i in range(len(pos1))])


def get_adjacent_vector(vector: tuple) -> list:
    options = set([])
    for i, v in enumerate(vector):
        for offset in range(-1, 2):
            adjacent = tuple(list(vector[:i]) + [v + offset] + list(vector[i+1:]))
            if adjacent == vector:
                continue
            options.add(adjacent)
    return list(options)


def get_adjacent_coordinates(x: int, y: int, shape: tuple, diagonal: bool = False) -> list:
    options = [
        (x+1, y), (x, y+1), (x-1, y), (x, y-1)
    ]
    if diagonal:
        options = options + [
            (x+1, y+1), (x-1, y+1), (x-1, y-1), (x+1, y-1)
        ]

    if shape is None:
        return options

    return [option for option in options if 0 <= option[0] < shape[0] and 0 <= option[1] < shape[1]]


def get_input(file_name='input.txt'):
    """

    :param file_name:
    :return:
    """
    with open(file_name, 'r') as file:
        puzzle_input = file.read().splitlines()
    return puzzle_input


def get_puzzle_discription(year: int, day: int, part: int) -> str:
    """

    :param year:
    :param day:
    :param part:
    :return:
    """
    url = f"https://adventofcode.com/{year}/day/{day}"
    response = requests.get(url, cookies=cookie)

    if response.status_code != 200:
        raise ValueError(f"request failed with status {requests.status_code} and message: {requests.content}")

    page = response.content.decode('utf-8')

    description = html2text.html2text(page)

    return description


def get_puzzle_input(year: int, day: int) -> str:
    """

    :param year:
    :param day:
    :return:
    """
    url = f"https://adventofcode.com/{year}/day/{day}/input"
    response = requests.get(url, cookies=cookie)

    if response.status_code != 200:
        raise ValueError(f"request failed with status {requests.status_code} and message: {requests.content}")

    puzzle_input = response.content.decode("utf-8")

    return puzzle_input


def get_advent_of_code(year: int, day: int, part: int) -> None:
    """

    :param year:
    :param day:
    :param part:
    :return:
    """
    path = f"./{year}/day {day}"
    if not os.path.exists(path):
        os.makedirs(path)

    description = get_puzzle_discription(year, day, part)
    with open(os.path.join(path, f"part{part}.txt"), 'w') as puzzle:
        puzzle.write(description)

    puzzle_input = get_puzzle_input(year, day)
    with open(os.path.join(path, f"input.txt"), 'w') as puzzle_input_file:
        puzzle_input_file.write(puzzle_input)

    test_path = os.path.join(path, f"test.txt")
    if not os.path.exists(test_path):
        with open(os.path.join(path, f"test.txt"), 'w') as test_file:
            test_file.write("")

    solution_path = os.path.join(path, f"solution.py")
    if not os.path.exists(solution_path):
        with open(solution_path, 'w') as puzzle_solution_file:
            puzzle_solution_file.write(solution_template)
