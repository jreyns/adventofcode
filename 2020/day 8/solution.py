import re

test_path = "C:\\data\\git\\adventofcode\\2020\\day 8\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 8\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 8\\input.txt"


class CPU:

    def __init__(self):
        self.accumulator = 0
        self.pointer = 0
        self.program = None
        self.pointer_positions = set([0])
        self.contains_infinite_loop = False

    def load(self, program):
        self.program = program

    def reset(self):
        self.accumulator = 0
        self.pointer = 0
        self.pointer_positions = set()
        self.contains_infinite_loop = False
        self.pointer_positions.add(self.pointer)

    def do_step(self):
        operator, argument = self._parse(self.program[self.pointer])
        if operator == "nop":
            self.pointer += 1
        elif operator == "acc":
            self.accumulator += argument
            self.pointer += 1
        elif operator == "jmp":
            self.pointer += argument

        if self.pointer not in self.pointer_positions:
            self.pointer_positions.add(self.pointer)
        else:
            self.contains_infinite_loop = True

    def completed(self):
        if self.pointer >= len(self.program):
            return True
        return False

    @staticmethod
    def _parse(line: str):
        values = line.split(" ")
        return values[0], int(values[1])


def create_program(file_path):
    program = []
    with open(file_path, "r") as file:
        for line in file:
            program.append(line.strip())
    return program


def alter_program(program, start_index=0):
    for i in range(start_index, len(program)):
        line = program[i]
        new_line = None
        if "nop" in line:
            new_line = line.replace("nop", "jmp")
        elif "jmp" in line:
            new_line = line.replace("jmp", "nop")

        if new_line is not None and i+1 != len(program):
            return program[:i] + [new_line] + program[i+1:], i+1
        elif new_line is not None:
            return program[:i] + [new_line], i+1


def get_accumulator_value_at_infinite_loop(cpu, program):
    cpu.reset()
    cpu.load(program)
    while not cpu.contains_infinite_loop:
        cpu.do_step()
    return cpu.accumulator


def get_accumulator_value_at_working_program(cpu, program):
    cpu.reset()
    cpu.load(program)
    start_index = 0
    while True:
        while not cpu.contains_infinite_loop and not cpu.completed():
            cpu.do_step()

        if cpu.completed():
            break

        new_program, start_index = alter_program(program, start_index)
        cpu.reset()
        cpu.load(new_program)

    return cpu.accumulator


cpu = CPU()
test_program = create_program(test_path)
test_program2 = create_program(test_path2)
program = create_program(path)

# test
print("### TEST ###")
print(get_accumulator_value_at_infinite_loop(cpu, test_program))
print(get_accumulator_value_at_working_program(cpu, test_program))

# part 1 & 2
print("### Part 1 ###")
print(get_accumulator_value_at_infinite_loop(cpu, program))
print("### Part 2 ###")
print(get_accumulator_value_at_working_program(cpu, program))
