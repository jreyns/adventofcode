import numpy as np

filepath = "C:\\data\\git\\adventofcode\\2020\\day 1\\expense_report.txt"

numbers = []

with open(filepath, "r") as file:
    for line in file:
        numbers.append(float(line.strip()))


def general_expenses(expenses, sum_count=3, count=1, index=-1, selection=[], total=2020):
    if count > sum_count:
        return False, (None, None)

    i = index + 1
    while i < len(expenses):
        if count < sum_count:
            success, solution = general_expenses(expenses, sum_count, count + 1, i, selection + [expenses[i]], total)
            if success:
                return success, solution
        elif count == sum_count:
            new_selection = [expenses[i]] + selection
            if sum(new_selection) == total:
                return True, (new_selection, np.prod(new_selection))
        i += 1
    return False, (None, None)


print(general_expenses(numbers, 2))
print(general_expenses(numbers))
print(general_expenses(numbers, 4))


def expenses():
    for i in range(len(numbers)):
        j = i+1
        while j < len(numbers):
            if numbers[i] + numbers[j] == 2020:
                return numbers[i], numbers[j], numbers[i] * numbers[j]
            j += 1


def expenses2():
    for i in range(len(numbers)):
        j = i+1
        while j < len(numbers):
            k = j+1
            while k < len(numbers):
                if numbers[i] + numbers[j] + numbers[k] == 2020:
                    return numbers[i], numbers[j], numbers[k], numbers[i] * numbers[j] * numbers[k]
                k += 1
            j += 1


print(expenses())
print(expenses2())
