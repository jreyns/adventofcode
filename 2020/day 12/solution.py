import math

test_path = "C:\\data\\git\\adventofcode\\2020\\day 12\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 12\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 12\\input.txt"


class Ship:

    def __init__(self, filepath):
        # X, Y, angle (degrees)
        self._position = [0, 0, 0]
        self._waypoint = [10, 1]
        self._index = 0
        self._instructions = []
        with open(filepath, "r") as file:
            for line in file:
                instruction = line.strip()
                self._instructions.append((instruction[0], int(instruction[1:])))

    def reset(self):
        self._position = [0, 0, 0]
        self._waypoint = [10, 1]
        self._index = 0

    def do_step(self):
        instruction, value = self._instructions[self._index]

        if instruction == "N":
            self._position[1] += value
        elif instruction == "S":
            self._position[1] -= value
        elif instruction == "E":
            self._position[0] += value
        elif instruction == "W":
            self._position[0] -= value
        elif instruction == "L":
            self._position[2] += value % 360
        elif instruction == "R":
            self._position[2] -= value % 360
        elif instruction == "F":
            x_d = math.cos(self._position[2] * math.pi / 180) * value
            y_d = math.sin(self._position[2] * math.pi / 180) * value
            self._position[0] += round(x_d)
            self._position[1] += round(y_d)

        self._index += 1
        return self._index < len(self._instructions)

    def do_step2(self):
        instruction, value = self._instructions[self._index]

        if instruction == "N":
            self._waypoint[1] += value
        elif instruction == "S":
            self._waypoint[1] -= value
        elif instruction == "E":
            self._waypoint[0] += value
        elif instruction == "W":
            self._waypoint[0] -= value
        elif instruction == "L":
            angle_rad = (value * math.pi / 180)
            x_d = round(self._waypoint[0] * math.cos(angle_rad) - self._waypoint[1] * math.sin(angle_rad))
            y_d = round(self._waypoint[0] * math.sin(angle_rad) + self._waypoint[1] * math.cos(angle_rad))
            self._waypoint[0] = x_d
            self._waypoint[1] = y_d
        elif instruction == "R":
            angle_rad = -(value * math.pi / 180)
            x_d = round(self._waypoint[0] * math.cos(angle_rad) - self._waypoint[1] * math.sin(angle_rad))
            y_d = round(self._waypoint[0] * math.sin(angle_rad) + self._waypoint[1] * math.cos(angle_rad))
            self._waypoint[0] = x_d
            self._waypoint[1] = y_d
        elif instruction == "F":
            self._position[0] += self._waypoint[0] * value
            self._position[1] += self._waypoint[1] * value

        self._index += 1
        return self._index < len(self._instructions)

    def manhattan(self):
        return abs(self._position[0]) + abs(self._position[1])

    def solve_first(self):
        while self.do_step():
            pass
        distance = self.manhattan()
        self.reset()
        return distance

    def solve_second(self):
        while self.do_step2():
            pass
        distance = self.manhattan()
        self.reset()
        return distance


test_ship = Ship(test_path)
test_ship2 = Ship(test_path2)
ship = Ship(path)


# test
print("### TEST ###")
print(test_ship.solve_first())
print(test_ship.solve_second())

# part 1 & 2
print("### Part 1 ###")
print(ship.solve_first())
print("### Part 2 ###")
print(ship.solve_second())
