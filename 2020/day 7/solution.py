import re

test_path = "C:\\data\\git\\adventofcode\\2020\\day 7\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 7\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 7\\input.txt"


class Rule:

    def __init__(self, line):
        self._line = line
        self.name = None
        self.contains = {}
        self.parse()

    def parse(self):
        # Part 1
        self.name = re.search(r"(.*) bags contain", self._line).groups()[0]
        contains = re.findall(r"(\d+) (\S+ \S+) (bag|bags)", self._line)
        for c in contains:
            self.contains[c[1]] = int(c[0])


def create_rules(file_path):
    rules = {}
    with open(file_path, "r") as file:
        for line in file:
            rule = Rule(line)
            rules[rule.name] = rule
    return rules


def count_can_contain(rules: dict, bag_name: str = "shiny gold") -> int:
    unique = set()
    options = set([bag_name])
    while len(options) > 0:
        new_options = set()
        for option in options:
            for rule in rules.values():
                if option in rule.contains:
                    new_options.add(rule.name)
                    unique.add(rule.name)
        options = new_options
    return len(unique)


def count_must_contain(rules: dict, bag_name: str = "shiny gold", level: int = 0) -> int:
    rule = rules[bag_name]
    if not level:
        count = 0
    else:
        count = 1
    for name, amount in rule.contains.items():
        count += amount * count_must_contain(rules, name, level+1)
    return count


test_rule_dict = create_rules(test_path)
test_rule_dict2 = create_rules(test_path2)
bag_dict = create_rules(path)

# test
print("### TEST ###")
print(count_can_contain(test_rule_dict))
print(count_must_contain(test_rule_dict))
print(count_must_contain(test_rule_dict2))

# part 1 & 2
print("### Part 1 ###")
print(count_can_contain(bag_dict))
print("### Part 2 ###")
print(count_must_contain(bag_dict))
