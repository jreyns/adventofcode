

test_path = "C:\\data\\git\\adventofcode\\2020\\day 10\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 10\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 10\\input.txt"


def create_input(file_path):
    inputs = [0]
    with open(file_path, "r") as file:
        for line in file:
            inputs.append(int(line.strip()))
    return inputs


test_inputs = create_input(test_path)
test_inputs2 = create_input(test_path2)
inputs = create_input(path)


def get_adapter_distribution(adapters):
    sorted_adapters = sorted(adapters)
    distribution = {
        1: 0,
        2: 0,
        3: 1
    }
    for i, adapter in enumerate(sorted_adapters[1:]):
        difference = adapter - sorted_adapters[i]
        distribution[difference] = distribution[difference] + 1
    return distribution


def solve_first(adapters):
    distribution = get_adapter_distribution(adapters)
    return distribution[1] * distribution[3]


"""
sequence 1 = 1 -> 1
sequence 2 = 1 -> 1
sequence 3 = 2 -> 1 + 1
sequence 4 = 4 -> 1 + 1 + 2
4, 5, 6, 7, 8

4, 5, 8
4, 6, 8
4, 7, 8
4, 5, 6, 8
4, 5, 7, 8
4, 6, 7, 8
4, 5, 6, 7, 8
sequence 5 = 7 -> 1 + 2 + 4
4, 5, 6, 7, 8, 9

4, 6, 9
4, 7, 9
4, 5, 6, 9
4, 5, 7, 9
4, 5, 8, 9
4, 6, 7, 9
4, 6, 8, 9
4, 7, 8, 9
4, 5, 6, 7, 9
4, 5, 6, 8, 9
4, 5, 7, 8, 9
4, 6, 7, 8, 9
4, 5, 6, 7, 8, 9
sequence 6 = 13 -> 2 + 4 + 7

...

option count = sum of all previous options that are reachable
"""


def get_adapter_arrangement_count_at_step(sorted_adapters, previous_counts, index, jolt_range):
    # Assume that you can always reach the previous adapter
    count = previous_counts[index - 1]

    # Add the steps from the other adapters that might reach you
    # You can start at index - 2 until i < 0 or the difference between the adapters becomes larger than the jolt range
    i = index - 2
    while i >= 0 and sorted_adapters[index] - sorted_adapters[i] <= jolt_range:
        count += previous_counts[i]
        i -= 1

    return count


def get_all_adapter_arrangements_counts(adapters, jolt_range):
    # Sort
    sorted_adapters = sorted(adapters)

    # You always have 1 option to reach the smallest adapter
    option_counts = [1]

    # Get the options for each adapter
    for index in range(1, len(sorted_adapters)):
        option_counts.append(get_adapter_arrangement_count_at_step(sorted_adapters, option_counts, index, jolt_range))

    return option_counts


def solve_second(adapters):
    # Return the last count in the list of adapter option counts
    return get_all_adapter_arrangements_counts(adapters, 3)[-1]


# test
print("### TEST ###")
print(solve_first(test_inputs))
print(solve_first(test_inputs2))
print(solve_second(test_inputs))
print(solve_second(test_inputs2))

# part 1 & 2
print("### Part 1 ###")
print(solve_first(inputs))
print("### Part 2 ###")
print(solve_second(inputs))
