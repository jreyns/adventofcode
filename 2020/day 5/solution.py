import math

test_path = "C:\\data\\git\\adventofcode\\2020\\day 5\\test.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 5\\input.txt"


class BoardingPass:

    def __init__(self, line, rows=(0, 127), columns=(0, 7), spill=7):
        self._rows = rows
        self._columns = columns
        self._row = line[:spill]
        self._col = line[spill:]

    def get_row(self):
        row = list(self._rows)
        for c in self._row:
            if c == "F":
                row[1] = math.floor((row[0] + row[1]) / 2)
            elif c == "B":
                row[0] = math.ceil((row[0] + row[1]) / 2)
        return row[0]

    def get_column(self):
        col = list(self._columns)
        for c in self._col:
            if c == "L":
                col[1] = math.floor((col[0] + col[1]) / 2)
            elif c == "R":
                col[0] = math.ceil((col[0] + col[1]) / 2)
        return col[0]

    def get_seat(self):
        return self.get_row() * len(range(self._columns[1]+1)) + self.get_column()


def get_boarding_passes(file_path, *args, **kwargs):
    passes = []
    with open(file_path, "r") as file:
        for line in file:
            passes.append(BoardingPass(line.strip(), *args, **kwargs))
    return passes


# test
print("Test:")
for boarding_pass in get_boarding_passes(test_path):
    print(boarding_pass.get_seat())


# part 1
print("Part 1:")
max_seat = 0
seats = []
for boarding_pass in get_boarding_passes(path):
    seat = boarding_pass.get_seat()
    seats.append(seat)
    if seat > max_seat:
        max_seat = seat
print(max_seat)


# part 2
print("Part 2:")
my_seat = None
sorted_seats = sorted(seats)
for index in range(len(sorted_seats)):
    seat_id = sorted_seats[index]
    if sorted_seats[index + 1] != seat_id + 1:
        my_seat = seat_id + 1
        break
print(my_seat)
