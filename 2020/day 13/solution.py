import numpy as np
import sympy
from functools import reduce

test_path = "C:\\data\\git\\adventofcode\\2020\\day 13\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 13\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 13\\input.txt"


def create_input(file_path):
    inputs = []
    with open(file_path, "r") as file:
        for line in file:
            inputs.append([int(x) if x != "x" else None for x in line.strip().split(",")])
    return inputs[0][0], inputs[1]


test_bus = create_input(test_path)
test_bus2 = create_input(test_path2)
bus = create_input(path)


def solve_first(earliest, schedules):
    earliest_bus = None
    timestamp = float("inf")
    for schedule in schedules:
        if schedule is not None:
            wait_time = schedule - earliest % schedule
            if wait_time < timestamp:
                timestamp = wait_time
                earliest_bus = schedule
    return earliest_bus * timestamp


def chinese_remainder2(n, a):
    sum = 0
    prod = reduce(lambda a, b: a * b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod


def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1: return 1
    while a > 1:
        q = a // b
        a, b = b, a % b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0: x1 += b0
    return x1


def chinese_remainder(n, a):
    p = int(np.prod([float(x) for x in n]))
    values = [(int(y), int(mul_inv(p // x, x)), int(p // x)) for x, y in zip(n, a)]
    total = 0
    for a, b, c in values:
        total += a * b * c
    return total % p


def solve_second(schedules):
    values = [(x, x - i) for i, x in enumerate(schedules) if x is not None]
    return chinese_remainder([x[0] for x in values], [x[1] for x in values])


# test
print("### TEST ###")
print(solve_first(*test_bus))
print(solve_second(test_bus[1]))
print(solve_second(test_bus2[1]))

# part 1 & 2
print("### Part 1 ###")
print(solve_first(*bus))
print("### Part 2 ###")
print(solve_second(bus[1]))


total = 383869800412101403
x = [29, 41, 521, 23, 13, 17, 601, 37, 19]

print(np.prod(x, dtype="int64"))
print(np.prod([float(i) for i in x]))
print(int(np.prod([float(i) for i in x])))

print(total % np.prod(x, dtype="int64"))
print(total % np.prod([float(i) for i in x]))
print(total % int(np.prod([float(i) for i in x])))

"""
725850285300512
725850285300475

1330360937940281
1330360937940281

383869800412101403
383869800412101403
"""