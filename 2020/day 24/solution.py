from collections import Counter
from util.util import get_input
import math
import re
from functools import reduce


puzzle_input = get_input()
test_input = get_input("test.txt")
moves = {"e": (1, 0), "se": (1/2, 1), "sw": (-1/2, 1), "w": (-1, 0), "nw": (-1/2, -1), "ne": (1/2, -1)}


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line
        )
    return processed_input


def follow_path(path):
    selection = [moves[move] for move in re.findall(r"e|se|sw|w|nw|ne", path)]
    return reduce(lambda x, y: (x[0] + y[0], x[1] + y[1]), [(0, 0)] + selection)


def get_neighbours(pos):
    return [
        (x + pos[0], y + pos[1]) for x, y in moves.values()
    ]


def flip(black_positions):
    new_positions = set([])
    for pos in black_positions:
        neighbours = get_neighbours(pos)

        black_neighbours = set(filter(lambda x: x in black_positions, neighbours))
        if len(black_neighbours) in [1, 2]:
            new_positions.add(pos)

        white_neighbours = set(neighbours) - black_neighbours
        for p in white_neighbours:
            n = get_neighbours(p)
            b = set(filter(lambda x: x in black_positions, n))
            if len(b) == 2:
                new_positions.add(p)

    return new_positions


def solution1(problem=puzzle_input):
    processed = process_input(problem)
    counts = Counter([follow_path(path) for path in processed])
    return sum([c % 2 == 1 for _, c in counts.items()])
    

def solution2(problem=puzzle_input):
    processed = process_input(problem)
    counts = Counter([follow_path(path) for path in processed])
    black_positions = set([point for point, c in counts.items() if c % 2 == 1])
    for _ in range(100):
        black_positions = flip(black_positions)

    return len(black_positions)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
