import math

test_path = "C:\\data\\git\\adventofcode\\2020\\day 6\\test.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 6\\input.txt"


class GroupAnswer:

    def __init__(self, lines):
        self._answers = lines
        self._letters = [str(chr(i)) for i in range(97, 123)]
        self.count = 0
        self.count_all = 0
        self.parse()

    def parse(self):
        # Part 1
        for letter in self._letters:
            if letter in self._answers:
                self.count += 1

        # Part 2
        lines = self._answers.split("\n")
        if len(lines) == 1:
            self.count_all = len(lines[0])
        else:
            for letter in lines[0]:
                valid = True
                for line in lines:
                    if letter in line:
                        continue
                    valid = False
                    break
                if valid:
                    self.count_all += 1


def create_group_answers(file_path):
    answers = []
    with open(file_path, "r") as file:
        lines = None
        for line in file:
            value = line.strip()
            if len(value) == 0:
                answers.append(GroupAnswer(lines))
                lines = None
            elif lines is None:
                lines = value
            else:
                lines = f"{lines}\n{value}"
        answers.append(GroupAnswer(lines))
    return answers


# test
print("### TEST ###")
count = 0
count_all = 0
for answer in create_group_answers(test_path):
    count += answer.count
    count_all += answer.count_all
print(count)
print(count_all)


# part 1 & 2
count = 0
count_all = 0
for answer in create_group_answers(path):
    count += answer.count
    count_all += answer.count_all
print("### Part 1 ###")
print(count)
print("### Part 2 ###")
print(count_all)
