

test_path = "C:\\data\\git\\adventofcode\\2020\\day 14\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 14\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 14\\input.txt"


class Docking:

    def __init__(self, filepath):
        self._mask = None
        self._mem = {}
        self._index = 0
        self._instructions = []
        with open(filepath, "r") as file:
            for line in file:
                self._instructions.append(line.strip())

    def reset(self):
        self._mask = None
        self._mem = {}
        self._index = 0

    @staticmethod
    def to_bin(value, length=36):
        binary = bin(value)[2:]
        return ("0" * (length - len(binary))) + binary

    def apply_mask(self, value):
        bit_array = self.to_bin(value)
        bits = "".join([bit_array[i] if self._mask[i] == "X" else self._mask[i] for i in range(36)])
        return int(bits, 2)

    def store_value(self, loc, value):
        if self._mask is None:
            self._mem[loc] = value
        else:
            self._mem[loc] = self.apply_mask(value)

    def get_locations(self, loc, index=0):
        new_locations = []
        if index == 35:
            if self._mask[index] == "0":
                new_locations.append(loc[index])
            elif self._mask[index] == "1":
                new_locations.append("1")
            else:
                new_locations.append("0")
                new_locations.append("1")
        else:
            if self._mask[index] == "0":
                for l in self.get_locations(loc, index+1):
                    new_locations.append(loc[index]+l)
            elif self._mask[index] == "1":
                for l in self.get_locations(loc, index+1):
                    new_locations.append("1"+l)
            else:
                for l in self.get_locations(loc, index+1):
                    new_locations.append("0"+l)
                    new_locations.append("1"+l)
        return new_locations

    def do_step(self):
        instruction = self._instructions[self._index]
        if "mask" in instruction:
            self._mask = instruction[7:]
        else:
            loc_part, value = instruction.split(" = ")
            loc = loc_part[4:-1]
            self.store_value(loc, int(value))

        self._index += 1
        return self._index < len(self._instructions)

    def do_step2(self):
        instruction = self._instructions[self._index]
        if "mask" in instruction:
            self._mask = instruction[7:]
        else:
            loc_part, value = instruction.split(" = ")
            loc = loc_part[4:-1]
            locations = self.get_locations(self.to_bin(int(loc)))
            for bin_loc in locations:
                index = int(bin_loc, 2)
                self._mem[index] = int(value)

        self._index += 1
        return self._index < len(self._instructions)

    def solve_first(self):
        while self.do_step():
            pass
        total = sum(self._mem.values())
        self.reset()
        return total

    def solve_second(self):
        while self.do_step2():
            pass
        total = sum(self._mem.values())
        self.reset()
        return total


test_docking = Docking(test_path)
test_docking2 = Docking(test_path2)
docking = Docking(path)


# test
print("### TEST ###")
print(test_docking.solve_first())
print(test_docking2.solve_second())

# part 1 & 2
print("### Part 1 ###")
print(docking.solve_first())
print("### Part 2 ###")
print(docking.solve_second())
