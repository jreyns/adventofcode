import re

test_path = "C:\\data\\git\\adventofcode\\2020\\day 16\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 16\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 16\\input.txt"


class Info:

    def __init__(self):
        self.info = {}
        self.indexes = {}

    def add_info(self, line):
        try:
            groups = re.match(r"(.*): (\d+)-(\d+) or (\d+)-(\d+)", line).groups()
            ranges = [int(x) for x in groups[1:]]
            self.info[groups[0]] = lambda x: (ranges[0] <= x <= ranges[1]) or (ranges[2] <= x <= ranges[3])
        except Exception as err:
            print(line)
            raise err

    def can_be_valid(self, value):
        for _, is_valid in self.info.items():
            if is_valid(value):
                return True
        return False

    def init_index_options(self):
        self.indexes = {
            key: list(range(len(self.info))) for key in self.info
        }

    def remove_from_all(self, index, exclude):
        for key in self.indexes:
            if key != exclude and index in self.indexes[key]:
                self.indexes[key].remove(index)
                if len(self.indexes[key]) == 1:
                    self.remove_from_all(self.indexes[key][0], key)

    def prune_index(self, index, value):
        for key in self.info:
            if index in self.indexes[key] and not self.info[key](value):
                self.indexes[key].remove(index)
                if len(self.indexes[key]) == 1:
                    self.remove_from_all(self.indexes[key][0], key)


class Ticket:

    def __init__(self, line):
        self._values = [int(x) for x in line.split(",")]

    def get_invalid_values(self, info: Info):
        return [x for x in self._values if not info.can_be_valid(x)]

    def is_invalid(self, info: Info):
        return len(self.get_invalid_values(info)) > 0

    def prune_info(self, info):
        for index, value in enumerate(self._values):
            info.prune_index(index, value)

    def get_mapped_values(self, info):
        return {
            key: self._values[indexes[0]] for key, indexes in info.indexes.items()
        }


class Notes:

    def __init__(self, filepath):
        self._info = Info()
        self._ticket = None
        self._tickets = []
        self._valid_ticket_ids = []
        with open(filepath, "r") as file:
            count = 0
            for line in file:
                content = line.strip()
                if len(content) == 0:
                    count += 1
                    continue
                elif content in ["your ticket:", "nearby tickets:"]:
                    continue
                elif count == 0:
                    self._info.add_info(content)
                elif count == 1:
                    self._ticket = Ticket(content)
                elif count == 2:
                    ticket = Ticket(content)
                    self._tickets.append(ticket)
                    if not ticket.is_invalid(self._info):
                        self._valid_ticket_ids.append(len(self._tickets) - 1)

    def solve_first(self):
        invalid = []
        for i in range(len(self._tickets)):
            if i in self._valid_ticket_ids:
                continue
            invalid += self._tickets[i].get_invalid_values(self._info)
        return sum(invalid)

    def solve_second(self):
        self._info.init_index_options()
        self._ticket.prune_info(self._info)
        for i in self._valid_ticket_ids:
            self._tickets[i].prune_info(self._info)
        mapping = self._ticket.get_mapped_values(self._info)
        total = 1
        for key in mapping:
            if "departure" in key:
                total *= mapping[key]
        return total


test_docking = Notes(test_path)
test_docking2 = Notes(test_path2)
docking = Notes(path)


# test
print("### TEST ###")
print(test_docking.solve_first())
print(test_docking2.solve_second())

# part 1 & 2
print("### Part 1 ###")
print(docking.solve_first())
print("### Part 2 ###")
print(docking.solve_second())
