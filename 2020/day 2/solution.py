import re

filepath = "C:\\data\\git\\adventofcode\\2020\\day 2\\passwords.txt"

passwords = []


def parse_password(line: str) -> (int, int, str, str):
    rule = r"(\d+)-(\d+) (\S+): (\S+)"
    parsed = re.search(rule, line)
    min_count = int(parsed.groups()[0])
    max_count = int(parsed.groups()[1])
    repeating_string = parsed.groups()[2]
    password = parsed.groups()[3]
    return min_count, max_count, repeating_string, password


def is_valid_password(min_count: int, max_count: int, repeating_string: str, password: str) -> bool:
    """

    :param min_count:
    :param max_count:
    :param repeating_string:
    :param password:
    :return:
    """
    occurences = password.count(repeating_string)
    return min_count <= occurences <= max_count


def is_valid_password2(pos_1: int, pos_2: int, repeating_string: str, password: str) -> bool:
    """

    :param min_count:
    :param max_count:
    :param repeating_string:
    :param password:
    :return:
    """
    if (password[pos_1 - 1] == repeating_string and not password[pos_2 - 1] == repeating_string) or (password[pos_2 - 1] == repeating_string and not password[pos_1 - 1] == repeating_string):
        return True
    return False


def run_test():
    test_file_path = "C:\\data\\git\\adventofcode\\2020\\day 2\\test.txt"
    count = 0
    count2 = 0
    with open(test_file_path, "r") as file:
        for line in file:
            if is_valid_password(*parse_password(line)):
                count += 1
            if is_valid_password2(*parse_password(line)):
                count2 += 1
    print(count)
    print(count2)


run_test()

count = 0
count2 = 0
with open(filepath, "r") as file:
    for line in file:
        if is_valid_password(*parse_password(line)):
            count += 1
        if is_valid_password2(*parse_password(line)):
            count2 += 1
print(count)
print(count2)
