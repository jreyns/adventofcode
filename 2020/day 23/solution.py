
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            [int(x) for x in line]
        )
    return processed_input


def follow_links(linked, start, steps):
    selected = [linked[start]]
    for _ in range(1, steps):
        selected.append(linked[selected[-1]])

    return selected


def linked_list(cups, steps):
    current = cups[0]
    min_cup, max_cup = min(cups), max(cups)
    linked = dict(zip(cups, cups[1:] + [cups[0]]))

    for _ in range(steps):
        picked_up = follow_links(linked, current, 3)
        destination = current - 1
        while destination < min_cup or destination in picked_up:
            destination -= 1
            if destination < min_cup:
                destination = max_cup
        linked[current] = linked[picked_up[-1]]
        linked[picked_up[-1]] = linked[destination]
        linked[destination] = picked_up[0]
        current = linked[current]

    return linked


def solution1(problem=puzzle_input, iterations=100):
    cups = linked_list(process_input(problem)[0], iterations)
    return "".join([str(x) for x in follow_links(cups, 1, 8)])
    

def solution2(problem=puzzle_input):
    cups = linked_list(process_input(problem)[0] + list(range(10, 1000001)), 10000000)
    c1 = cups[1]
    return c1 * cups[c1]
    
    
print(f"test 1: {solution1(test_input, 10)}")
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
