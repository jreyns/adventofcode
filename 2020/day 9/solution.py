import re

test_path = "C:\\data\\git\\adventofcode\\2020\\day 9\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 9\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 9\\input.txt"


def create_stream(file_path):
    stream = []
    with open(file_path, "r") as file:
        for line in file:
            stream.append(int(line.strip()))
    return stream


test_stream = create_stream(test_path)
test_stream2 = create_stream(test_path2)
stream = create_stream(path)


def get_options(stream, index, preamble):
    options = set()
    for i in range(index - preamble, index):
        for j in range(i, index):
            options.add(stream[i] + stream[j])
    return options


def find_first_invalid(stream, preamble):
    index = preamble
    while stream[index] in get_options(stream, index, preamble):
        index += 1
    return stream[index]


def find_terms_that_equal(stream, value):
    begin = 0
    end = 1
    while True:
        total = sum(stream[begin:end])

        while total < value:
            end += 1
            total = sum(stream[begin:end])

        if total == value:
            return stream[begin:end]

        begin += 1
        end = begin + 1


def find_weakness(stream, preamble):
    incorrect = find_first_invalid(stream, preamble)
    terms = sorted(find_terms_that_equal(stream, incorrect))
    return terms[0] + terms[-1]


# test
print("### TEST ###")
print(find_first_invalid(test_stream, 5))
print(find_weakness(test_stream, 5))

# part 1 & 2
print("### Part 1 ###")
print(find_first_invalid(stream, 25))
print("### Part 2 ###")
print(find_weakness(stream, 25))
