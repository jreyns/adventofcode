
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            int(line)
        )
    return processed_input


def get_loop_count(expected):
    current = 1
    loop_count = 0
    while current != expected:
        loop_count += 1
        current = (current * 7) % 20201227

    return loop_count


def encrypt(subject, loop_count):
    current = 1
    for _ in range(loop_count):
        current = (current * subject) % 20201227

    return current


def solution1(problem=puzzle_input):
    card_pk, door_pk = process_input(problem)
    card_lc, door_lc = get_loop_count(card_pk), get_loop_count(door_pk)
    return encrypt(card_pk, door_lc)
    
    
print(f"test 1: {solution1(test_input)}")
    
print(f"solution 1: {solution1()}")
