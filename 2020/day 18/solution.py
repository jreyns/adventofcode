import re

test_path = "C:\\data\\git\\adventofcode\\2020\\day 18\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 18\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 18\\input.txt"


class Expression:

    def __init__(self, left, operation="+", right="0"):
        self._left = left
        self._operation = operation
        self._right = right

    def solve(self):
        left = self._left
        right = self._right
        if isinstance(self._left, Expression):
            left = self._left.solve()
        if isinstance(self._right, Expression):
            right = self._right.solve()
        return eval(f"{left} {self._operation} {right}")

    def __repr__(self):
        return f"{self._left} {self._operation} {self._right}"


class Homework:

    def __init__(self, filepath):
        self._expressions = []
        self._advanced_expressions = []
        with open(filepath, "r") as file:
            for line in file:
                self._expressions.append(self.parse(line))
                self._advanced_expressions.append(self.parse_advanced(line))

    @staticmethod
    def find_matching_bracket(line, index):
        count = 0
        for i, char in enumerate(line[index:]):
            if char == ")":
                count -= 1
                if count == 0:
                    return index + i
            elif char == "(":
                count += 1
        return None

    @staticmethod
    def find_next_term(line, index):
        i = index + 1
        while i < len(line):
            if i == "*":
                return i
            if i == "(":
                i == Homework.find_matching_bracket(line, i)
            i += 1

        return i

    def parse(self, line):
        index = 0
        parsed_until = 0
        expressions = []
        while index < len(line):
            if line[index] in ["+", "*"]:
                expression = line[parsed_until:index].strip()
                if index != parsed_until and expression != "":
                    expressions.append(Expression(expression))
                expressions.append(line[index])
                parsed_until = index+1
            elif line[index] in ["("]:
                end_index = self.find_matching_bracket(line, index)
                expressions.append(self.parse(line[index+1:end_index]))
                index = end_index + 1
                parsed_until = index
            index += 1

        expression = line[parsed_until:index].strip()
        if parsed_until < len(line) and expression != "":
            expressions.append(Expression(line[parsed_until:len(line)].strip()))

        left = expressions[0]
        for i in range(2, len(expressions), 2):
            left = Expression(left, expressions[i-1], expressions[i])

        return left

    def parse_advanced(self, line):
        index = 0
        parsed_until = 0
        expressions = []
        while index < len(line):
            if line[index] in ["+"]:
                expression = line[parsed_until:index].strip()
                if index != parsed_until and expression != "":
                    expressions.append(Expression(expression))
                expressions.append(line[index])
                parsed_until = index+1
            elif line[index] in ["*"]:
                expression = line[parsed_until:index].strip()
                if index != parsed_until and expression != "":
                    expressions.append(Expression(expression))
                expressions.append(line[index])
                end_index = self.find_next_term(line, index)
                expressions.append(self.parse_advanced(line[index+1:end_index]))
                index = end_index + 1
                parsed_until = index
            elif line[index] in ["("]:
                end_index = self.find_matching_bracket(line, index)
                expressions.append(self.parse_advanced(line[index+1:end_index]))
                index = end_index + 1
                parsed_until = index
            index += 1

        expression = line[parsed_until:index].strip()
        if parsed_until < len(line) and expression != "":
            expressions.append(Expression(line[parsed_until:len(line)].strip()))

        left = expressions[0]
        for i in range(2, len(expressions), 2):
            left = Expression(left, expressions[i-1], expressions[i])

        return left

    def solve_first(self):
        return sum([x.solve() for x in self._expressions])

    def solve_second(self):
        return sum([x.solve() for x in self._advanced_expressions])


test_docking = Homework(test_path)
test_docking2 = Homework(test_path2)
docking = Homework(path)


# test
print("### TEST ###")
print(test_docking.solve_first())
print(test_docking.solve_second())
print(test_docking2.solve_first())
print(test_docking2.solve_second())

# part 1 & 2
print("### Part 1 ###")
print(docking.solve_first())
print("### Part 2 ###")
print(docking.solve_second())
