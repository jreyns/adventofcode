

test_path = "C:\\data\\git\\adventofcode\\2020\\day 11\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 11\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 11\\input.txt"


class Map:

    def __init__(self, filepath):
        self._map = []
        with open(filepath, "r") as file:
            for line in file:
                self._map.append(list(line.strip()))
        self._width = len(self._map[0])
        self._height = len(self._map)

    @staticmethod
    def get_adjacent(position):
        x, y = position
        return [
            (x - 1, y - 1), (x - 1, y), (x - 1, y + 1),
            (x, y - 1), (x, y + 1),
            (x + 1, y - 1), (x + 1, y), (x + 1, y + 1),
        ]

    def get_position(self, position):
        x, y = position
        if 0 <= x < self._width and 0 <= y < self._height:
            return self._map[y][x]
        return None

    def get_first_direction(self, position, adjacent):
        x, y = adjacent
        value = self.get_position(adjacent)
        r_x = adjacent[0] - position[0]
        r_y = adjacent[1] - position[1]
        while value is not None and value not in ["L", "#"]:
            x += r_x
            y += r_y
            value = self.get_position((x, y))
        return value

    def apply_rule(self, position):
        value = self.get_position(position)

        # Floor
        if value == ".":
            return value, False

        # Get adjacent positions
        adjacent = [self.get_position(p) for p in self.get_adjacent(position)]

        # Seats
        if value == "L" and "#" not in adjacent:
            return "#", True
        elif value == "#" and adjacent.count(value) >= 4:
            return "L", True
        return value, False

    def apply_rule2(self, position):
        value = self.get_position(position)

        # Floor
        if value == ".":
            return value, False

        # Get adjacent positions
        adjacent = [self.get_first_direction(position, p) for p in self.get_adjacent(position)]

        # Seats
        if value == "L" and "#" not in adjacent:
            return "#", True
        elif value == "#" and adjacent.count(value) >= 5:
            return "L", True
        return value, False

    def do_step(self, rule="apply_rule"):
        new_map = []
        changed = False
        for y in range(self._height):
            row = []
            for x in range(self._width):
                new_value, has_changed = self.__getattribute__(rule)((x, y))
                row.append(new_value)
                changed = changed or has_changed
            new_map.append(row)
        self._map = new_map
        return changed

    def solve_first(self):
        while self.do_step():
            pass
        total = 0
        for row in self._map:
            total += row.count("#")
        return total

    def solve_second(self):
        while self.do_step("apply_rule2"):
            pass
        total = 0
        for row in self._map:
            total += row.count("#")
        return total

    def print(self):
        for line in self._map:
            print("".join(line))


# test
print("### TEST ###")
print(Map(test_path).solve_first())
print(Map(test_path).solve_second())

# part 1 & 2
print("### Part 1 ###")
print(Map(path).solve_first())
print("### Part 2 ###")
print(Map(path).solve_second())
