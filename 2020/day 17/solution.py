import re

test_path = "C:\\data\\git\\adventofcode\\2020\\day 17\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 17\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 17\\input.txt"


class Map:

    def __init__(self, filepath, z=0):
        self._initial = []
        self._map = []
        with open(filepath, "r") as file:
            for y, line in enumerate(file):
                for x, char in enumerate(line.strip()):
                    if char == "#":
                        self._map.append((x, y, z))
                        self._initial.append((x, y, z))

    def reset(self):
        self._map = [x for x in self._initial]

    @staticmethod
    def get_adjacent(position):
        x, y, z = position
        offsets = list(range(-1, 2, 1))
        adjacent = []
        for dx in offsets:
            for dy in offsets:
                for dz in offsets:
                    if dx == 0 and dy == 0 and dz == 0:
                        continue
                    adjacent.append((x + dx, y + dy, z + dz))
        return adjacent

    @staticmethod
    def get_adjacent2(position):
        x, y, z, w = position
        offsets = list(range(-1, 2, 1))
        adjacent = []
        for dx in offsets:
            for dy in offsets:
                for dz in offsets:
                    for dw in offsets:
                        adjacent.append((x + dx, y + dy, z + dz, w + dw))
        return adjacent

    def apply_rule(self, position_counts):
        new_map = []
        for position, count in position_counts.items():
            if position in self._map and count-1 in [2, 3]:
                new_map.append(position)
            elif count in [3]:
                new_map.append(position)
        self._map = new_map

    def do_step(self):
        positions_to_check = [x for x in self._map]
        for active_cube in self._map:
            positions_to_check += self.get_adjacent(active_cube)
        position_counts = {
            position: positions_to_check.count(position) for position in set(positions_to_check)
        }
        self.apply_rule(position_counts)
        return len(self._map)

    def do_step2(self):
        positions_to_check = []
        for active_cube in self._map:
            positions_to_check += self.get_adjacent2(active_cube)
        position_counts = {
            position: positions_to_check.count(position) for position in set(positions_to_check)
        }
        self.apply_rule(position_counts)
        return len(self._map)

    def solve_first(self):
        active_cubes = len(self._map)
        for _ in range(6):
            active_cubes = self.do_step()
        self.reset()
        return active_cubes

    def solve_second(self):
        self._map = [(x, y, z, 0) for x, y, z in self._map]
        active_cubes = len(self._map)
        for _ in range(6):
            active_cubes = self.do_step2()
        self.reset()
        return active_cubes

    def print(self):
        for line in self._map:
            print("".join(line))


test_docking = Map(test_path)
test_docking2 = Map(test_path2)
docking = Map(path)


# test
print("### TEST ###")
print(test_docking.solve_first())
print(test_docking.solve_second())

# part 1 & 2
print("### Part 1 ###")
print(docking.solve_first())
print("### Part 2 ###")
print(docking.solve_second())
