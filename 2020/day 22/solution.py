from copy import deepcopy
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    player = None
    for line in lines:
        if "Player" in line:
            player = (line, [])
            continue

        if not line:
            processed_input.append(
                player
            )
            continue

        player[1].append(int(line))

    processed_input.append(
        player
    )

    return processed_input


def total_score(deck):
    return sum([(i+1) * c for i, c in enumerate(reversed(deck))])


def recursive_game(deck1, deck2):
    states = set([])
    while deck1 and deck2:
        state = (tuple(deck1), tuple(deck2))
        if state in states:
            return 0

        states.add(state)

        c1, c2 = deck1.pop(0), deck2.pop(0)

        if len(deck1) >= c1 and len(deck2) >= c2:
            winner = recursive_game(deepcopy(deck1[:c1]), deepcopy(deck2[:c2]))
            if winner == 0:
                deck1 += [c1, c2]
            else:
                deck2 += [c2, c1]
        else:
            if c1 > c2:
                deck1 += [c1, c2]
            else:
                deck2 += [c2, c1]

    return not deck1


def solution1(problem=puzzle_input):
    p1, p2 = process_input(problem)
    deck1, deck2 = p1[1], p2[1]
    while deck1 and deck2:
        c1, c2 = deck1.pop(0), deck2.pop(0)
        if c1 > c2:
            deck1 += [c1, c2]
        else:
            deck2 += [c2, c1]

    return total_score(deck1) + total_score(deck2)
    

def solution2(problem=puzzle_input):
    p1, p2 = process_input(problem)
    deck1, deck2 = p1[1], p2[1]
    recursive_game(deck1, deck2)
    return total_score(deck1) + total_score(deck2)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
