import time

test_path = "C:\\data\\git\\adventofcode\\2020\\day 15\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 15\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 15\\input.txt"


class MemoryGame:

    def __init__(self, filepath):
        self._mem = {}
        self._index = 0
        self._last_spoken = None
        self._starting_numbers = []
        with open(filepath, "r") as file:
            for line in file:
                self._starting_numbers += [int(x) for x in line.strip().split(",")]

    def reset(self):
        self._mem = {}
        self._index = 0
        self._last_spoken = None

    def do_step(self):
        if self._index < len(self._starting_numbers):
            number = self._starting_numbers[self._index]
        else:
            second_last, last = self._mem[self._last_spoken]
            number = last - second_last if second_last is not None else 0
        self._last_spoken = number
        self._mem[self._last_spoken] = (self._mem.get(self._last_spoken, [None, None])[1], self._index)
        self._index += 1

    def do_step2(self):
        return False

    def solve_first(self, steps=2020):
        while self._index < steps:
            self.do_step()
        nth = self._last_spoken
        self.reset()
        return nth

    def solve_second(self, steps=30000000):
        return self.solve_first(steps)


test_docking = MemoryGame(test_path)
test_docking2 = MemoryGame(test_path2)
docking = MemoryGame(path)


# test
print("### TEST ###")
print(test_docking.solve_first())
t = time.time()
print(f"{test_docking2.solve_second()}: {time.time() - t}s")


# part 1 & 2
print("### Part 1 ###")
print(docking.solve_first())
print("### Part 2 ###")
t = time.time()
print(f"{docking.solve_second()}: {time.time() - t}s")
