import numpy as np

path = "C:\\data\\git\\adventofcode\\2020\\day 3\\map.txt"


class Map:

    def __init__(self, filepath):
        self._pointer = [0, 0]
        self._map = []
        with open(filepath, "r") as file:
            for line in file:
                self._map.append(list(line.strip()))
        self._width = len(self._map[0])
        self._height = len(self._map)

    def move(self, x, y):
        self._pointer[0] = (self._pointer[0] + x) % self._width
        self._pointer[1] = self._pointer[1] + y

    def get_current(self):
        return self._map[self._pointer[1]][self._pointer[0]]

    def reset(self):
        self._pointer = [0, 0]

    def print(self):
        for line in self._map:
            print(line)


def count_trees(tree_map, x, y):
    trees = 0
    while True:
        try:
            tree_map.move(x, y)
            if tree_map.get_current() == "#":
                trees += 1
        except:
            break
    tree_map.reset()
    return trees


tree_map = Map(path)

slope1 = [3, 1]
print(count_trees(tree_map, *slope1))

slopes = [
    [1, 1],
    [3, 1],
    [5, 1],
    [7, 1],
    [1, 2]
]

trees_per_slope = [count_trees(tree_map, *slope) for slope in slopes]
print(trees_per_slope)
print(np.prod(trees_per_slope))

