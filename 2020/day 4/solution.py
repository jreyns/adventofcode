import re
import pandas as pd

path = "C:\\data\\git\\adventofcode\\2020\\day 4\\passports.txt"


class Passport:

    def __init__(self, lines):
        self._fields = Passport.parse(lines)
        self._required = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
        self._optional = ["cid"]
        self.valid = self.is_valid()

    def is_valid(self):
        for key in self._required:
            if not self.__getattribute__(f"valid_{key}")():
                return False
        return True

    def is_valid1(self):
        for key in self._required:
            if key not in self._fields:
                return False
        return True

    def valid_byr(self):
        try:
            value = int(self._fields["byr"])
            return 1920 <= value <= 2002 and len(self._fields["byr"]) == 4
        except:
            pass
        return False

    def valid_iyr(self):
        try:
            value = int(self._fields["iyr"])
            return 2010 <= value <= 2020 and len(self._fields["iyr"]) == 4
        except:
            pass
        return False

    def valid_eyr(self):
        try:
            value = int(self._fields["eyr"])
            return 2020 <= value <= 2030 and len(self._fields["eyr"]) == 4
        except:
            pass
        return False

    def valid_hgt(self):
        try:
            value = float(self._fields["hgt"][:-2])
            if "cm" in self._fields["hgt"]:
                return 150 <= value <= 193
            elif "in" in self._fields["hgt"]:
                return 59 <= value <= 76
        except:
            pass
        return False

    def valid_hcl(self):
        try:
            value = re.match(r"#[0-9a-f]{6}$", self._fields["hcl"])
            return value is not None
        except:
            pass
        return False

    def valid_ecl(self):
        try:
            return self._fields["ecl"] in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
        except:
            pass
        return False

    def valid_pid(self):
        try:
            value = re.match(r"[0-9]{9}$", self._fields["pid"])
            return value is not None
        except:
            pass
        return False

    def valid_cid(self):
        return True

    @staticmethod
    def parse(lines):
        regex = r"(\w+):(\S+)"
        values = re.findall(regex, lines)
        return {key: value for key, value in values}

    def values(self):
        return [self._fields[key] for key in self._required]


def create_passports():
    passports = []
    with open(path, "r") as file:
        lines = ""
        for line in file:
            value = line.strip()
            if len(value) == 0:
                passports.append(Passport(lines))
                lines = ""
            else:
                lines = f"{lines} {line}"
        passports.append(Passport(lines))
    return passports


passports = create_passports()

valid = 0
valid1 = 0
count = 0
values = []
for passport in passports:
    if passport.valid:
        values.append(passport.values())
        valid += 1
    if passport.is_valid1():
        valid1 += 1
    count += 1

df = pd.DataFrame(values, columns=passport._required)

print(count)
print(valid1)
print(valid)

