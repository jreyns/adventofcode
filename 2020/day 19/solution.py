import re

test_path = "C:\\data\\git\\adventofcode\\2020\\day 19\\test.txt"
test_path2 = "C:\\data\\git\\adventofcode\\2020\\day 19\\test2.txt"
path = "C:\\data\\git\\adventofcode\\2020\\day 19\\input.txt"


class Rules:

    def __init__(self):
        self._rules = {}

    def add_rule(self, line):
        rule_number, rule = line.split(":")
        sub_rules = rule.strip().split("|")
        self._rules[int(rule_number)] = [x.strip().split(" ") for x in sub_rules] if len(sub_rules) > 1 else sub_rules[0].strip().split(" ")

    def matches_sub_rule(self, message, rules, message_index=0):
        matches = True
        if '"' in rules:
            rules = rules.replace('"', "")
        if isinstance(rules, str):
            rules = [rules]
        for rule in rules:
            if rule.isnumeric():
                m, message_index = self._matches_rule(message, int(rule), message_index)
            else:
                end = message_index + len(rule)
                m = message[message_index:end] == rule
                message_index = end
            matches = matches and m
        return matches, message_index

    @staticmethod
    def contains_list(rule):
        for x in rule:
            if isinstance(x, list):
                return True
        return False

    def _matches_rule(self, message, rule_index, message_index):
        # TODO branch over new message indexes
        rule = self._rules[rule_index]
        if self.contains_list(rule):
            matches = False
            for sub_rule in rule:
                m, new_message_index = self.matches_sub_rule(message, sub_rule, message_index)
                if m:
                    message_index = new_message_index
                matches = matches or m
        else:
            matches = True
            for sub_rule in rule:
                m, message_index = self.matches_sub_rule(message, sub_rule, message_index)
                matches = matches and m

        return matches, message_index

    def matches_rule(self, message, rule_index=0):
        message_length = len(message)
        matches, message_index = self._matches_rule(message, rule_index, 0)
        return matches and message_index == message_length


class Messages:

    def __init__(self, filepath):
        self._rules = Rules()
        self._messages = []
        with open(filepath, "r") as file:
            for line in file:
                stripped = line.strip()
                if ":" in line:
                    self._rules.add_rule(stripped)
                elif len(stripped) > 0:
                    self._messages.append(stripped)

    def solve_first(self):
        count = 0
        for message in self._messages:
            if self._rules.matches_rule(message):
                count += 1
        return count

    def solve_second(self):
        count = 0
        for message in self._messages:
            if self._rules.matches_rule(message):
                count += 1
        return count


test_docking = Messages(test_path)
test_docking2 = Messages(test_path2)
docking = Messages(path)


# test
print("### TEST ###")
print(test_docking.solve_first())
#print(test_docking.solve_second())
#print(test_docking2.solve_first())
#print(test_docking2.solve_second())

# part 1 & 2
print("### Part 1 ###")
print(docking.solve_first())
print("### Part 2 ###")
#print(docking.solve_second())
