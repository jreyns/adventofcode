from collections import Counter
import re
from functools import reduce
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    allergens = {}
    counts = Counter()
    for line in lines:
        ingredients_list, allergens_list = re.match(r"([^\(]*)\(contains (.+,?)*\)", line).groups()
        ingredients_list = set(ingredients_list.strip().split(" "))
        allergens_list = allergens_list.split(", ")
        counts.update(ingredients_list)
        for allergen in allergens_list:
            if allergen not in allergens:
                allergens[allergen] = ingredients_list
            allergens[allergen] = allergens[allergen].intersection(ingredients_list)

    return allergens, counts


def solution1(problem=puzzle_input):
    allergens, counts = process_input(problem)
    impossible = reduce(lambda x, y: x - y, [set(counts.keys())] + list(allergens.values()))
    return sum([counts.get(x) for x in impossible])
    

def solution2(problem=puzzle_input):
    allergens, _ = process_input(problem)
    unique = reduce(lambda x, y: x.union(y), [x[1] for x in filter(lambda x: len(x[1]) == 1, allergens.items())])
    while len(unique) != len(allergens):
        for allergen, options in filter(lambda x: len(x[1]) > 1, allergens.items()):
            allergens[allergen] = options - unique
        unique = reduce(lambda x, y: x.union(y), [x[1] for x in filter(lambda x: len(x[1]) == 1, allergens.items())])

    return ",".join([list(options)[0] for _, options in sorted(allergens.items())])
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
