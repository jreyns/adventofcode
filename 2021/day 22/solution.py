from util.util import get_input

validation_scenario = [
    "on x=-20..26,y=-36..17,z=-47..7",
    "on x=-20..33,y=-21..23,z=-26..28",
    "on x=-22..28,y=-29..23,z=-38..16",
    "on x=-46..7,y=-6..46,z=-50..-1",
    "on x=-49..1,y=-3..46,z=-24..28",
    "on x=2..47,y=-22..22,z=-23..27",
    "on x=-27..23,y=-28..26,z=-21..29",
    "on x=-39..5,y=-6..47,z=-3..44",
    "on x=-30..21,y=-8..43,z=-13..34",
    "on x=-22..26,y=-27..20,z=-29..19",
    "off x=-48..-32,y=26..41,z=-47..-37",
    "on x=-12..35,y=6..50,z=-50..-2",
    "off x=-48..-32,y=-32..-16,z=-15..-5",
    "on x=-18..26,y=-33..15,z=-7..46",
    "off x=-40..-22,y=-38..-28,z=23..41",
    "on x=-16..35,y=-41..10,z=-47..6",
    "off x=-32..-23,y=11..30,z=-14..3",
    "on x=-49..-5,y=-3..45,z=-29..18",
    "off x=18..30,y=-20..-8,z=-3..13",
    "on x=-41..9,y=-7..43,z=-33..15",
    "on x=-54112..-39298,y=-85059..-49293,z=-27449..7877",
    "on x=967..23432,y=45373..81175,z=27513..53682"
]

puzzle_input = get_input()


def parse_dimensions(line):
    return [tuple([int(s) for s in x[2:].split("..")]) for x in line.split(",")]


class Cube:

    def __init__(self, dx, dy, dz):
        self.dx = dx
        self.dy = dy
        self.dz = dz
        self.removed = []

    def __str__(self):
        return f"{{x: {self.dx}, y: {self.dy}, z: {self.dz}}}"

    def __repr__(self):
        return str(self)

    def overlaps(self, other):
        dx, dy, dz = other.dx, other.dy, other.dz
        if (self.dx[0] <= dx[0] <= self.dx[1] or self.dx[0] <= dx[1] <= self.dx[1]) \
                and (self.dy[0] <= dy[0] <= self.dy[1] or self.dy[0] <= dy[1] <= self.dy[1]) \
                and (self.dz[0] <= dz[0] <= self.dz[1] or self.dz[0] <= dz[1] <= self.dz[1]):
            return True
        return False

    def is_proper(self):
        return (self.dx[1] - self.dx[0]) == (self.dy[1] - self.dy[0]) \
               and (self.dx[1] - self.dx[0]) == (self.dz[1] - self.dz[0])

    def __add__(self, other):
        pass

    def __sub__(self, other):
        pass

    def __len__(self):
        return (self.dx[1] - self.dx[0]) * (self.dy[1] - self.dy[0]) * (self.dz[1] - self.dz[0])


class NaiveGrid:

    def __init__(self, dx, dy, dz):
        self.dx = dx
        self.dy = dy
        self.dz = dz
        self.on = set([])

    def overlaps(self, dx, dy, dz):
        if (self.dx[0] <= dx[0] <= self.dx[1] or self.dx[0] <= dx[1] <= self.dx[1]) \
                and (self.dy[0] <= dy[0] <= self.dy[1] or self.dy[0] <= dy[1] <= self.dy[1]) \
                and (self.dz[0] <= dz[0] <= self.dz[1] or self.dz[0] <= dz[1] <= self.dz[1]):
            return True
        return False

    def get_points(self, dx, dy, dz):
        # Return variable
        points = set([])

        # Return empty set if they are not in the configured interval
        if not self.overlaps(dx, dy, dz):
            return points

        # Generate the points
        for x in range(dx[0], dx[1] + 1):
            if x < self.dx[0] or x > self.dx[1]:
                continue

            for y in range(dy[0], dy[1] + 1):
                if y < self.dy[0] or y > self.dy[1]:
                    continue

                for z in range(dz[0], dz[1] + 1):
                    if z < self.dz[0] or z > self.dz[1]:
                        continue

                    points.add((x, y, z))
        return points

    def turn_on(self, dx, dy, dz):
        points = self.get_points(dx, dy, dz)
        self.on = self.on.union(points)

    def turn_off(self, dx, dy, dz):
        points = self.get_points(dx, dy, dz)
        self.on = self.on - points

    def __len__(self):
        return len(self.on)

    def perform_step(self, line):
        if "on" in line:
            self.turn_on(*parse_dimensions(line[3:]))
        elif "off" in line:
            self.turn_off(*parse_dimensions(line[4:]))


def solution1(scenario: list, dx: tuple = (-50, 50), dy: tuple = (-50, 50), dz: tuple = (-50, 50)):
    ng = NaiveGrid(dx, dy, dz)
    for line in scenario:
        ng.perform_step(line)
    return len(ng)


def solution2(scenario: list):
    cubes = []
    for line in scenario:
        if "on" in line:
            cubes.append(Cube(*parse_dimensions(line[3:])))
        elif "off" in line:
            cubes.append(Cube(*parse_dimensions(line[4:])))

    for cube in cubes:
        print(f"{cube} is proper: {cube.is_proper()}")


# print(f"test 1: {solution1(validation_scenario)}")
# print(f"solution 1: {solution1(puzzle_input)}")

print(f"solution 2: {solution2(puzzle_input)}")
