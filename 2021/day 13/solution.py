from util.util import get_input

validation_scenario = [
    "6,10",
    "0,14",
    "9,10",
    "0,3",
    "10,4",
    "4,11",
    "6,0",
    "6,12",
    "4,1",
    "0,13",
    "10,12",
    "3,4",
    "3,0",
    "8,4",
    "1,10",
    "2,14",
    "8,10",
    "9,0",
    "",
    "fold along y=7",
    "fold along x=5"
]

puzzle_input = get_input()


class Board:

    def __init__(self, scenario: list):
        self.points = {}
        self.shape = [0, 0]
        self.folds = []
        self.folds_applied = 0

        for line in scenario:
            if "fold along" in line:
                direction = line[11:12]
                coordinate = int(line[13:])
                if direction == "x":
                    self.folds.append((coordinate, None))
                elif direction == "y":
                    self.folds.append((None, coordinate))
                continue
            if "," in line:
                x, y = [int(c) for c in line.split(",")]
                self.update_shape(x, y)
                self.points[(x, y)] = True

    def update_shape(self, x, y):
        if x > self.shape[0]:
            self.shape[0] = x
        if y > self.shape[1]:
            self.shape[1] = y

    def fold_x(self, fold_x):
        new_points = {}
        self.shape = [0, 0]
        for x, y in self.points:
            if x > fold_x:
                x_new = 2 * fold_x - x
                self.update_shape(x_new, y)
                new_points[(x_new, y)] = True
            else:
                self.update_shape(x, y)
                new_points[(x, y)] = True
        self.points = new_points

    def fold_y(self, fold_y):
        new_points = {}
        self.shape = [0, 0]
        for x, y in self.points:
            if y > fold_y:
                y_new = 2 * fold_y - y
                self.update_shape(x, y_new)
                new_points[(x, y_new)] = True
            else:
                self.update_shape(x, y)
                new_points[(x, y)] = True
        self.points = new_points

    def perform_fold(self) -> (bool, int):
        if self.folds_applied >= len(self.folds):
            return False, len(self.points)

        fold = self.folds[self.folds_applied]
        self.folds_applied += 1

        if fold[0] is None:
            self.fold_y(fold[1])

        if fold[1] is None:
            self.fold_x(fold[0])

        return self.folds_applied < len(self.points), len(self.points)

    def print(self):
        for y in range(self.shape[1]+1):
            print("".join(["#" if (x, y) in self.points else " " for x in range(self.shape[0]+1)]))


def solution1(scenario: list) -> int:
    board = Board(scenario)
    _, in_vision = board.perform_fold()
    # board.print()
    return in_vision


def solution2(scenario: list) -> None:
    board = Board(scenario)
    can_continue, _ = board.perform_fold()
    while can_continue:
        can_continue, _ = board.perform_fold()
    board.print()


print(f"test 1: {solution1(validation_scenario)}")
print(f"solution 1: {solution1(puzzle_input)}")

print(f"test 2: {solution2(validation_scenario)}")
print(f"solution 2: {solution2(puzzle_input)}")
