from util.util import get_input

validation_input = [
    "start-A",
    "start-b",
    "A-c",
    "A-b",
    "b-d",
    "A-end",
    "b-end"
]

puzzle_input = get_input()


class CavePaths:

    def __init__(self, start: str = "start", end: str = "end"):
        self.rooms = {start: [], end: []}
        self.start = start
        self.end = end

    def add_connection(self, connection: str) -> None:
        room1, room2 = connection.split("-")

        if room1 not in self.rooms:
            self.rooms[room1] = []

        if room2 not in self.rooms:
            self.rooms[room2] = []

        self.rooms[room1].append(room2)
        self.rooms[room2].append(room1)

    def get_paths(self, point: str = None, passed: list = []) -> list:
        paths = []
        if not point:
            point = self.start

        for option in self.rooms[point]:
            if option == option.lower() and option in passed:
                continue

            if option == self.end:
                paths.append([point, self.end])
            else:
                sub_paths = self.get_paths(option, passed + [point])
                for path in sub_paths:
                    if path:
                        paths.append([point] + path)

        return paths

    @staticmethod
    def lower_repeated(point, passed):
        points_to_check = [x for x in passed + [point] if x == x.lower()]
        return (len(points_to_check) - len(set(points_to_check))) > 1

    def get_paths2(self, point: str = None, passed: list = []) -> list:
        paths = []
        if not point:
            point = self.start

        for option in self.rooms[point]:
            if option == self.start:
                continue

            if self.lower_repeated(option, passed + [point]):
                continue

            if option == self.end:
                paths.append([point, self.end])
            else:
                sub_paths = self.get_paths2(option, passed + [point])
                for path in sub_paths:
                    if path:
                        paths.append([point] + path)

        return paths


def solution1(scenario: list) -> int:
    layout = CavePaths()
    for line in scenario:
        layout.add_connection(line)
    paths = layout.get_paths()
    return len(paths)


def solution2(scenario: list) -> int:
    layout = CavePaths()
    for line in scenario:
        layout.add_connection(line)
    paths = layout.get_paths2()
    return len(paths)


print(f"test 1: {solution1(validation_input)}")
print(f"solution 1: {solution1(puzzle_input)}")

print(f"test 2: {solution2(validation_input)}")
print(f"solution 2: {solution2(puzzle_input)}")
