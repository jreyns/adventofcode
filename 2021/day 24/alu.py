from typing import List

from util.util import get_input


puzzle_input = get_input()


class ALU:

    def __init__(self, model_number: str, instructions: List[str]):
        self.input_count = 0
        self.model_number = model_number
        self.instructions = instructions
        self.registers = {
            "w": 0,
            "x": 0,
            "y": 0,
            "z": 0
        }
        self.operations = {
            "inp": self.inp,
            "add": self.add,
            "mul": self.mul,
            "div": self.div,
            "mod": self.mod,
            "eql": self.eql
        }

    def reset(self, model_number: str):
        self.model_number = model_number
        self.input_count = 0
        self.registers = {
            "w": 0,
            "x": 0,
            "y": 0,
            "z": 0
        }

    def get(self, register):
        if register in self.registers:
            return self.registers[register]

        return int(register)

    def inp(self, register, _):
        self.registers[register] = int(self.model_number[self.input_count])
        self.input_count += 1

    def add(self, register_a, register_b):
        self.registers[register_a] = self.registers[register_a] + self.get(register_b)

    def mul(self, register_a, register_b):
        self.registers[register_a] = self.registers[register_a] * self.get(register_b)

    def div(self, register_a, register_b):
        self.registers[register_a] = self.registers[register_a] // self.get(register_b)

    def mod(self, register_a, register_b):
        self.registers[register_a] = self.registers[register_a] % self.get(register_b)

    def eql(self, register_a, register_b):
        self.registers[register_a] = self.registers[register_a] == self.get(register_b)

    def run(self):
        for instruction in [x.split(" ") for x in self.instructions]:
            self.operations[instruction[0]](instruction[1], instruction[-1])

        return self.isvalid()

    def isvalid(self):
        return self.registers["z"] == 0


def solution1(scenario: List[str]):
    alu = ALU(str(53999995829399), scenario)
    return alu.run()


def solution2(scenario: List[str]):
    alu = ALU(str(11721151118175), scenario)
    return alu.run()


print(f"solution 1: {solution1(puzzle_input)}")
print(f"solution 2: {solution2(puzzle_input)}")
