
from util.util import get_input


puzzle_input = get_input()


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(
            line.split(" ")
        )
    return processed_input


def get_relevant_adds(operations):
    adds = []
    for i in range(0, len(operations), 18):
        if operations[i + 4][2] == "1":
            adds.append((int(operations[i + 15][2]), None))
        else:
            adds.append((None, int(operations[i + 5][2])))
    return adds


def get_model_no(operations, lowest=False):
    relevant_adds = get_relevant_adds(operations)
    model_no = [0] * 14
    stack = []
    start_digit = 1 if lowest else 9
    for index, (a, b) in enumerate(relevant_adds):
        if a:
            stack.append((index, a))
        else:
            i, a = stack.pop()
            diff = a + b
            if not lowest:
                model_no[i] = min(start_digit, start_digit - diff)
                model_no[index] = min(start_digit, start_digit + diff)
            else:
                model_no[i] = max(start_digit, start_digit - diff)
                model_no[index] = max(start_digit, start_digit + diff)
    return int("".join([str(x) for x in model_no]))


def solution1(problem=puzzle_input):
    operations = process_input(problem)
    return get_model_no(operations)
    

def solution2(problem=puzzle_input):
    operations = process_input(problem)
    return get_model_no(operations, True)


print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
