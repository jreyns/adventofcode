from util.util import get_input
from collections import Counter

validation_scenario = [
    "NNCB",
    "",
    "CH -> B",
    "HH -> N",
    "CB -> H",
    "NH -> C",
    "HB -> C",
    "HC -> B",
    "HN -> C",
    "NN -> C",
    "BH -> H",
    "NC -> B",
    "NB -> B",
    "BN -> B",
    "BB -> N",
    "BC -> B",
    "CC -> N",
    "CN -> C"
]

puzzle_input = get_input()


class Polymer:

    def __init__(self, scenario):
        self.strand = Counter(["".join(x) for x in zip(scenario[0][:-1], scenario[0][1:])])
        self.elements = Counter(scenario[0])
        self.rewrite_rules = {k: v for k, v in [x.split(" -> ") for x in scenario[2:]]}

    def rewrite(self) -> None:
        new_strand = Counter()
        for pair, count in self.strand.items():
            new_char = self.rewrite_rules[pair]
            self.elements[new_char] += count
            new_strand[pair[0] + new_char] += count
            new_strand[new_char + pair[1]] += count
        self.strand = new_strand


def solution(scenario: list, iterations: int = 10) -> int:
    polymer = Polymer(scenario)
    for _ in range(iterations):
        polymer.rewrite()
    counts = polymer.elements
    sorted_by_count = counts.most_common()
    return sorted_by_count[0][1] - sorted_by_count[-1][1]


print(f"test 1: {solution(validation_scenario)}")
print(f"solution 1: {solution(puzzle_input)}")

print(f"test 2: {solution(validation_scenario, 40)}")
print(f"solution 2: {solution(puzzle_input, 40)}")
