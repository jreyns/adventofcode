from util.util import get_input
import math

validation_scenario = [
    "--- scanner 0 ---",
    "0,2",
    "4,1",
    "3,3",
    "",
    "--- scanner 1 ---",
    "-1,-1",
    "-5,0",
    "-2,1"
]

puzzle_input = get_input()


class Scanner:

    def __init__(self, identifier: str, measurements: list):
        self.id = identifier
        self.measurements = measurements
        self.distances = {m: set([]) for m in self.measurements}

        for i, p1 in enumerate(self.measurements[:-1]):
            for p2 in self.measurements[i + 1:]:
                distance = self.distance(p1, p2)
                self.distances[p1].add(distance)
                self.distances[p2].add(distance)

    @staticmethod
    def distance(p1, p2):
        return math.sqrt(sum([(p2[i] - p1[i]) ** 2 for i in range(len(p1))]))

    def get_similar_points(self, other, expected: int):
        similarities = {}

        for p1, distances in self.distances.items():
            similarities[p1] = []
            for p2, other_distances in other.distances.items():
                if len(distances.intersection(other_distances)) >= expected:
                    similarities[p1].append(p2)
        return similarities


class TrenchMap:

    def __init__(self, scenario: list):
        self.scanners = []

        name = None
        measurements = []
        for line in scenario:
            if not name:
                name = line
                continue

            if line == "":
                self.scanners.append(Scanner(name, measurements))
                name = None
                measurements = []
                continue

            measurements.append(tuple([int(x) for x in line.split(",")]))

        self.scanners.append(Scanner(name, measurements))

    def find_similar_points(self, expected: int):
        reference_scanner = self.scanners[0]
        similarities = []

        for scanner in self.scanners[1:]:
            similarities.append(reference_scanner.get_similar_points(scanner, expected))

        return similarities


def solution1(scenario: list, expected: int = 12) -> int:
    trench_map = TrenchMap(scenario)
    return trench_map.find_similar_points(expected)


print(f"test 1: {solution1(validation_scenario, 2)}")
