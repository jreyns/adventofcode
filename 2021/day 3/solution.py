

class SubmarineDiagnostics:

    def __init__(self):
        self._lines = []
        self._bitwise_counts = {}

    def append(self, line: str) -> None:
        self._lines.append(line)

        if not self._bitwise_counts:
            self._init_counts(len(line))

        for index, bit in enumerate(line):
            self._bitwise_counts[index][bit] += 1

    def _init_counts(self, bit_length):
        self._bitwise_counts = {
            x: {'0': 0, '1': 0} for x in range(bit_length)
        }

    @staticmethod
    def _filter_selective_counts(options: list, index: int = 0, preferred_bit: str = '0', most_common: bool = True) \
            -> list:
        count = {
            '0': [0, []],
            '1': [0, []]
        }
        for option in options:
            count[option[index]][0] += 1
            count[option[index]][1].append(option)

        if count['0'][0] == count['1'][0]:
            return count[preferred_bit][1]

        if most_common and count['0'][0] > count['1'][0]:
            return count['0'][1]

        if most_common and count['0'][0] < count['1'][0]:
            return count['1'][1]

        if count['0'][0] > count['1'][0]:
            return count['1'][1]

        if count['0'][0] < count['1'][0]:
            return count['0'][1]

        return None

    @property
    def gamma(self):
        binary_gamma = "".join(
            [
                '0' if self._bitwise_counts[x]['0'] > self._bitwise_counts[x]['1'] else '1'
                for x in range(len(self._bitwise_counts))
            ]
        )
        return int(binary_gamma, 2)

    @property
    def epsilon(self):
        binary_epsilon = "".join(
            [
                '0' if self._bitwise_counts[x]['0'] < self._bitwise_counts[x]['1'] else '1'
                for x in range(len(self._bitwise_counts))
            ]
        )
        return int(binary_epsilon, 2)

    @property
    def oxygen_generator_rating(self):
        options = self._lines
        bit_index = 0
        while len(options) > 1 and bit_index < len(self._bitwise_counts):
            options = self._filter_selective_counts(options, bit_index, preferred_bit='1', most_common=True)
            bit_index += 1
        return int(options[0], 2)

    @property
    def co2_scrubber_rating(self):
        options = self._lines
        bit_index = 0
        while len(options) > 1 and bit_index < len(self._bitwise_counts):
            options = self._filter_selective_counts(options, bit_index, preferred_bit='0', most_common=False)
            bit_index += 1
        return int(options[0], 2)

    def reset(self):
        self._lines = []
        self._bitwise_counts = {}


test_diagnostics = SubmarineDiagnostics()
diagnostics = SubmarineDiagnostics()

with open('input.txt', 'r') as file:
    for line in file.read().splitlines():
        diagnostics.append(line)

test_scenario = [
    '00100',
    '11110',
    '10110',
    '10111',
    '10101',
    '01111',
    '00111',
    '11100',
    '10000',
    '11001',
    '00010',
    '01010'
]


def run_test_scenario():
    test_diagnostics.reset()
    for l in test_scenario:
        test_diagnostics.append(l)

    print(
        f"diagnostics gamma: {test_diagnostics.gamma}, epsilon: {test_diagnostics.epsilon} "
        f"-> {test_diagnostics.gamma * test_diagnostics.epsilon}"
    )
    print(
        f"diagnostics oxygen_generator_rating: {test_diagnostics.oxygen_generator_rating}, "
        f"co2_scrubber_rating: {test_diagnostics.co2_scrubber_rating} "
        f"-> {test_diagnostics.oxygen_generator_rating * test_diagnostics.co2_scrubber_rating}"
    )


run_test_scenario()

print(
    f"solution 1: diagnostics gamma: {diagnostics.gamma}, epsilon: {diagnostics.epsilon} "
    f"-> {diagnostics.gamma * diagnostics.epsilon}"
)
print(
    f"solution 2: diagnostics oxygen_generator_rating: {diagnostics.oxygen_generator_rating}, "
    f"co2_scrubber_rating: {diagnostics.co2_scrubber_rating} "
    f"-> {diagnostics.oxygen_generator_rating * diagnostics.co2_scrubber_rating}"
)