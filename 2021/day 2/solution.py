
def split(value: str, on: str = ' '):
    parts = value.split(on)
    return parts[0], int(parts[1])


with open('input.txt', 'r') as file:
    route = [split(x) for x in file.readlines()]


def travel_route(some_route: list) -> (int, int):
    horizontal_position = 0
    depth = 0
    for direction, amount in some_route:
        if direction == 'forward':
            horizontal_position += amount
        elif direction == 'down':
            depth += amount
        elif direction == 'up':
            depth -= amount
    return horizontal_position, depth


def travel_route_with_aim(some_route: list) -> (int, int):
    horizontal_position = 0
    depth = 0
    aim = 0
    for direction, amount in some_route:
        if direction == 'forward':
            horizontal_position += amount
            depth += aim * amount
        elif direction == 'down':
            aim += amount
        elif direction == 'up':
            aim -= amount
    return horizontal_position, depth


def example():
    r = [
        ('forward', 5),
        ('down', 5),
        ('forward', 8),
        ('up', 3),
        ('down', 8),
        ('forward', 2)
    ]
    pos, depth = travel_route(r)
    print(f"got {pos}, {depth}. expected 15, 10")


def example2():
    r = [
        ('forward', 5),
        ('down', 5),
        ('forward', 8),
        ('up', 3),
        ('down', 8),
        ('forward', 2)
    ]
    pos, depth = travel_route_with_aim(r)
    print(f"got {pos}, {depth}. expected 15, 60")


example()

pos1, depth1 = travel_route(route)
print(f"solution 1: {pos1 * depth1}")

example2()

pos2, depth2 = travel_route_with_aim(route)
print(f"solution 2: {pos2 * depth2}")
