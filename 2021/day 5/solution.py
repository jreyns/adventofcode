import re

from util.util import get_input, sign
from util.point import Point3D

validation = [
    '0,9 -> 5,9',
    '8,0 -> 0,8',
    '9,4 -> 3,4',
    '2,2 -> 2,1',
    '7,0 -> 7,4',
    '6,4 -> 2,0',
    '0,9 -> 2,9',
    '3,4 -> 1,4',
    '0,0 -> 8,8',
    '5,5 -> 8,2'
]

puzzle_input = get_input()


class GeothermalMap:

    def __init__(self, lines: list, diagonal: bool = False):
        self._map = {}
        self._min = Point3D(float('inf'), float('inf'))
        self._max = Point3D(0, 0)
        self._diagonal = diagonal

        self._parse(lines)

    def _parse(self, lines: list) -> None:
        """

        :param lines:
        :return:
        """
        for line in lines:
            x1, y1, x2, y2 = [int(x) for x in re.findall(r"(\d+)", line)]

            self._check_bounds(x1, y1, x2, y2)

            points = self.get_points((x1, y1), (x2, y2))

            for point in points:
                if point not in self._map:
                    self._map[point] = 1
                else:
                    self._map[point] += 1

    def _check_bounds(self, x1, y1, x2, y2):
        """

        :param x1:
        :param y1:
        :param x2:
        :param y2:
        :return:
        """
        if x1 < self._min.x:
            self._min.x = x1

        if x1 > self._max.x:
            self._max.x = x1

        if y1 < self._min.y:
            self._min.y = y1

        if y1 > self._max.y:
            self._max.y = y1

        if x2 < self._min.x:
            self._min.x = x2

        if x2 > self._max.x:
            self._max.x = x2

        if y2 < self._min.y:
            self._min.y = y2

        if y2 > self._max.y:
            self._max.y = y2

    def get_points(self, p1, p2) -> list:
        """

        :param p1:
        :param p2:
        :return:
        """
        if not self._diagonal and not (p1[0] == p2[0] or p1[1] == p2[1]):
            return []

        # Reverse order to check if coordinate is increasing
        sign_x = sign(p2[0] - p1[0])
        sign_y = sign(p2[1] - p1[1])

        if p1[0] == p2[0]:
            return [(p1[0], y) for y in range(p1[1], p2[1] + sign_y, sign_y)]

        if p1[1] == p2[1]:
            return [(x, p1[1]) for x in range(p1[0], p2[0] + sign_x, sign_x)]

        return zip([x for x in range(p1[0], p2[0] + sign_x, sign_x)], [y for y in range(p1[1], p2[1] + sign_y, sign_y)])

    def print(self):
        """

        :return:
        """
        for y in range(self._min.y, self._max.y + 1, 1):
            line = []
            for x in range(self._min.x, self._max.x + 1, 1):
                line.append(str(self._map.get((x, y), '.')))
            print(''.join(line))

    def solution1(self) -> int:
        """

        :return:
        """
        return sum([1 if value >= 2 else 0 for _, value in self._map.items()])


validation_map = GeothermalMap(validation)
validation_map.print()
print(f"test1: {validation_map.solution1()} should be 5\n")

validation_map = GeothermalMap(validation, True)
validation_map.print()
print(f"test2: {validation_map.solution1()} should be 12\n")

solution1 = GeothermalMap(puzzle_input)
print(f"solution1: {solution1.solution1()}")

solution2 = GeothermalMap(puzzle_input, True)
print(f"solution1: {solution2.solution1()}")
