
validation_scenario = [
    [2, 1, 9, 9, 9, 4, 3, 2, 1, 0],
    [3, 9, 8, 7, 8, 9, 4, 9, 2, 1],
    [9, 8, 5, 6, 7, 8, 9, 8, 9, 2],
    [8, 7, 6, 7, 8, 9, 6, 7, 8, 9],
    [9, 8, 9, 9, 9, 6, 5, 6, 7, 8]
]

puzzle_input = []
with open('input.txt', 'r') as file:
    for line in file.read().splitlines():
        puzzle_input.append([int(x) for x in line])


def get_adjacent_coordinates(x: int, y: int, shape: tuple, diagonal: bool = False) -> list:
    options = [
        (x+1, y), (x, y+1), (x-1, y), (x, y-1)
    ]
    if diagonal:
        options = options + [
            (x+1, y+1), (x-1, y+1), (x-1, y-1), (x+1, y-1)
        ]
    return [option for option in options if 0 <= option[0] < shape[0] and 0 <= option[1] < shape[1]]


def is_lower(value: int, options: list) -> bool:
    for option in options:
        if option <= value:
            return False
    return True


def get_low_points(scenario: list) -> list:
    low_points = []
    shape = (len(scenario[0]), len(scenario))
    for y in range(shape[1]):
        for x in range(shape[0]):
            height = scenario[y][x]
            options = [scenario[c[1]][c[0]] for c in get_adjacent_coordinates(x, y, shape)]
            if is_lower(height, options):
                low_points.append((x, y, height))
    return low_points


def solution1(scenario: list) -> int:
    total_risk_level = 0
    for _, _, height in get_low_points(scenario):
        total_risk_level += 1 + height
    return total_risk_level


print(f"test1: {solution1(validation_scenario)}")
print(f"solution1: {solution1(puzzle_input)}")


def find_basin_size(coordinate: tuple, scenario: list, shape: tuple):
    points = set([])
    points_to_check = set([coordinate])
    boundaries = set([])

    while points_to_check:
        new_points_to_check = set([])
        for point in points_to_check:
            height = scenario[point[1]][point[0]]

            # Check if this coordinate is a boundary
            if height == 9:
                boundaries.add((point[0], point[1]))
                continue

            # Point is not a boundary so add it to the points
            points.add(point)

            # Add all adjacent points that haven't been checked yet
            options = set(get_adjacent_coordinates(point[0], point[1], shape))
            new_points_to_check = new_points_to_check.union(options - points - boundaries)

        # Update points to check
        points_to_check = new_points_to_check

    return len(points)


def solution2(scenario: list) -> int:
    shape = (len(scenario[0]), len(scenario))

    # Find lowest points
    low_points = get_low_points(scenario)

    # Find basin sizes
    sizes = [
        find_basin_size((l[0], l[1]), scenario, shape) for l in low_points
    ]

    # Only keep the 3 largest
    largest = sorted(sizes)[-3:]
    print(largest)

    # Calculate result
    result = 1
    for x in largest:
        result *= x
    return result


print(f"test2: {solution2(validation_scenario)}")
print(f"solution2: {solution2(puzzle_input)}")
