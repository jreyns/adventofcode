from util.util import get_input

validation_scenario = [
    "[({(<(())[]>[[{[]{<()<>>",
    "[(()[<>])]({[<{<<[]>>(",
    "{([(<{}[<>[]}>{[]{[(<()>",
    "(((({<>}<{<{<>}{[]{[]{}",
    "[[<[([]))<([[{}[[()]]]",
    "[{[{({}]{}}([{[{{{}}([]",
    "{<[[]]>}<{[{[{[]{()[[[]",
    "[<(<(<(<{}))><([]([]()",
    "<{([([[(<>()){}]>(<<{{",
    "<{([{{}}[<[[[<>{}]]]>[]]"
]

puzzle_input = get_input()


score = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137
}

matches = {
    ")": "(",
    "]": "[",
    "}": "{",
    ">": "<"
}

complete_score = {
    ")": 1,
    "]": 2,
    "}": 3,
    ">": 4
}

inverse_matches = {v: k for k, v in matches.items()}


def solution1(scenario: list) -> (int, list):
    total_score = 0
    lines_to_complete = []
    for line in scenario:
        queue = []
        valid = True
        for c in line:
            if c in ['(', '[', '{', '<']:
                queue.append(c)
            elif queue[-1] == matches[c]:
                queue.pop()
            else:
                total_score += score[c]
                valid = False
                break
        if valid and queue:
            lines_to_complete.append(queue)
    return total_score, lines_to_complete


def solution2(lines_to_complete: list) -> int:
    scores = []
    for queue in lines_to_complete:
        total_score = 0
        while queue:
            total_score *= 5
            total_score += complete_score[inverse_matches[queue.pop()]]
        scores.append(total_score)
    return sorted(scores)[len(scores) // 2]


validation_score, validation_lines = solution1(validation_scenario)
print(f"test1: {validation_score}")
print(f"test2: {solution2(validation_lines)}")

puzzle_score, puzzle_lines = solution1(puzzle_input)
print(f"solution 1: {puzzle_score}")
print(f"solution 2: {solution2(puzzle_lines)}")

