from util.util import get_input

validation_scenario = [
    "5483143223",
    "2745854711",
    "5264556173",
    "6141336146",
    "6357385478",
    "4167524645",
    "2176841721",
    "6882881134",
    "4846848554",
    "5283751526"
]

puzzle_input = get_input()


def get_adjacent_coordinates(x: int, y: int, shape: tuple, diagonal: bool = False) -> list:
    options = [
        (x+1, y), (x, y+1), (x-1, y), (x, y-1)
    ]
    if diagonal:
        options = options + [
            (x+1, y+1), (x-1, y+1), (x-1, y-1), (x+1, y-1)
        ]
    return [option for option in options if 0 <= option[0] < shape[0] and 0 <= option[1] < shape[1]]


def perform_step(board: list) -> (list, int):
    shape = (len(board[0]), len(board))
    flash_points = set([])

    # Increment energy by one and keep track of points that will flash by themselves
    for y, row in enumerate(board):
        for x, value in enumerate(row):
            board[y][x] = value+1
            if value+1 > 9:
                flash_points.add((x, y))

    # Stop here if no point is going to flash
    if not flash_points:
        return board, 0

    # Flashing logic
    flashes = set(flash_points)
    while flashes:
        new_flashes = set([])
        for point in flashes:
            flash_points.add(point)
            adjacent_points = get_adjacent_coordinates(*point, shape, True)
            for x, y in adjacent_points:
                board[y][x] += 1
                if board[y][x] > 9:
                    new_flashes.add((x, y))
        flashes = new_flashes - flash_points

    # Reset all points that flashed
    for x, y in flash_points:
        board[y][x] = 0

    return board, len(flash_points)


def solution1(scenario: list) -> int:
    board = [[int(x) for x in line] for line in scenario]
    total_flashes = 0
    for _ in range(100):
        board, flashes = perform_step(board)
        total_flashes += flashes
    return total_flashes


def solution2(scenario: list) -> int:
    board = [[int(x) for x in line] for line in scenario]
    total_points = len(board[0]) * len(board)
    iteration = 0
    flashed = 0
    while flashed != total_points:
        board, flashed = perform_step(board)
        iteration += 1
    return iteration


print(f"test 1: {solution1(validation_scenario)}")
print(f"solution 1: {solution1(puzzle_input)}")


print(f"test 2: {solution2(validation_scenario)}")
print(f"solution 2: {solution2(puzzle_input)}")