
depth_measurements = []

with open('input.txt', 'r') as file:
    for line in file:
        depth_measurements.append(int(line[:-1]) if "\n" in line else int(line))

grouped = [sum(depth_measurements[pointer:pointer+3]) for pointer in range(len(depth_measurements)-2)]

print(f"solution 1: {sum([x > y for x, y in zip(depth_measurements[1:], depth_measurements)])}")
print(f"solution 2: {sum([x > y for x, y in zip(grouped[1:], grouped)])}")
