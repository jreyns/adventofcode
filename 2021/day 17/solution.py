from util.util import get_input
from itertools import product

validation_scenario = [
    "target area: x=20..30, y=-10..-5"
]

puzzle_input = get_input()


def parse(formula):
    return [int(x) for part in formula.replace("target area: ", "").split(", ") for x in part[2:].split("..")]


def check_velocity_valid(dx, dy, target):
    tminx, tmaxx, tminy, tmaxy = target
    pos_x, pos_y = 0, 0
    while pos_x <= tmaxx and pos_y >= tminy:
        pos_x, pos_y, dx, dy = pos_x+dx, pos_y+dy, max(0, dx-1), dy-1
        if tminx <= pos_x <= tmaxx and tminy <= pos_y <= tmaxy:
            return True
    return False


def highest_y(y):
    return (y+1) * y // 2


def solution1(scenario: list):
    target = parse(scenario[0])
    velocities = [dy for dx, dy in product(range(1000), range(-1000, 1000)) if check_velocity_valid(dx, dy, target)]
    return highest_y(max(velocities))


def solution2(scenario: list):
    target = parse(scenario[0])
    velocities = [dy for dx, dy in product(range(1000), range(-1000, 1000)) if check_velocity_valid(dx, dy, target)]
    return len(velocities)


print(f"test 1: {solution1(validation_scenario)}")

print(f"solution 1: {solution1(puzzle_input)}")
print(f"solution 2: {solution2(puzzle_input)}")
