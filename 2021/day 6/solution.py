from util.util import get_input

validation_input = ['3,4,3,1,2']

puzzle_input = get_input()


# Naive
class LanternFish:

    def __init__(self, timer: int, spawn: dict = {'default': 6, 0: 8}, age: int = 0):
        self._timer = timer
        self._spawn = spawn

        # Age in cycles
        self._age = age
        if timer > spawn['default']:
            self._age = 0

    def iterate(self):
        if self._timer == 0:
            self._timer = self._spawn['default']
            self._age += 1
            return LanternFish(self._spawn.get(0, self._spawn['default']), self._spawn)

        self._timer -= 1

        return None


def solution1(scenario: list, iterations: int):
    fish_ages = [int(x) for x in scenario[0].split(',')]
    fish = [LanternFish(age, age=1) for age in fish_ages]
    for i in range(iterations):
        new_fish = []
        for f in fish:
            offspring = f.iterate()
            if offspring:
                new_fish.append(offspring)
        fish = fish + new_fish
        # print(f"day {i+1}: " + ','.join([str(f._timer) for f in fish]))
    return len(fish)


def solution2(scenario: list, iterations: int):
    # trackers
    adult_modulo_tracker = {x: 0 for x in range(7)}
    child_tracker = [0] * 9

    # Set initial state of the adult tracker
    for age in [int(x) for x in scenario[0].split(',')]:
        adult_modulo_tracker[age] += 1

    # Get new children per day
    for i in range(iterations):
        offset = i % 7
        adult_modulo_tracker[offset] += child_tracker[i]
        new_offspring_count = adult_modulo_tracker[i % 7]
        child_tracker.append(new_offspring_count)

    return sum(adult_modulo_tracker.values()) + sum(child_tracker[i+1:])


# Exponential function
"""
    x = current age
    start count = 1
    y(t0) = 1
    y(tx) = 2
    y(tx+7) = 3
    y(tx+9) = 4
    
    amount of offspring from current fish
    1 + (iterations - x) // 7
    
    fish creation:
    if age = 0 -> 9
    else every 7 days
    
    1 + 
    

"""

# print(f"test1: {solution1(validation_input, 80)}")
# print(f"solution 1: {solution1(puzzle_input, 80)}")


print(f"test1.0: {solution2(validation_input, 18)}")
print(f"test1.1: {solution2(validation_input, 80)}")
print(f"test2: {solution2(validation_input, 256)}")
print(f"solution 1.1: {solution2(puzzle_input, 80)}")
print(f"solution 2: {solution2(puzzle_input, 256)}")
