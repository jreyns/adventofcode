from util.util import get_input, get_adjacent_coordinates

validation_scenario = [
    "1163751742",
    "1381373672",
    "2136511328",
    "3694931569",
    "7463417111",
    "1319128137",
    "1359912421",
    "3125421639",
    "1293138521",
    "2311944581"
]

puzzle_input = get_input()


def get_lowest_risk(adjacent_points: list, total_risk: list) -> int:
    try:
        return min([total_risk[y1][x1] for x1, y1 in adjacent_points if total_risk[y1][x1] > 0])
    except ValueError:
        return 0


def print_risk(risk: list):
    for line in risk:
        print("".join([str(x) for x in line]))


def solution1(scenario: list) -> int:
    shape = len(scenario)
    cave_map = [[int(x) for x in line] for line in scenario]
    total_risk = [[0] * shape for _ in range(shape)]
    changed = True
    while changed:
        changed = False
        points = set([(shape - 1, shape - 1)])
        visited = set([])
        while points:
            new_points = set([])
            for point in points:
                x, y = point
                visited.add(point)
                adjacent_points = get_adjacent_coordinates(x, y, (shape, shape))
                old_risk = total_risk[y][x]
                if point == (shape-1, shape-1):
                    total_risk[y][x] = cave_map[y][x]
                else:
                    total_risk[y][x] = cave_map[y][x] + get_lowest_risk(adjacent_points, total_risk)

                if old_risk != total_risk[y][x]:
                    changed = True

                for new_point in adjacent_points:
                    new_points.add(new_point)
            points = new_points - visited
    return total_risk[0][0] - cave_map[0][0]


def extend_board(scenario: list) -> list:
    board = []

    for i in range(5):
        for row in scenario:
            new_row = []
            for j in range(5):
                new_row += [(x + i + j) if (x + i + j) <= 9 else (x + i + j) % 9 for x in row]
            board.append(new_row)

    return board


def solution2(scenario: list) -> int:
    cave_map = extend_board([[int(x) for x in line] for line in scenario])
    shape = len(cave_map)
    total_risk = [[0] * shape for _ in range(shape)]
    changed = True
    while changed:
        changed = False
        points = set([(shape - 1, shape - 1)])
        visited = set([])
        while points:
            new_points = set([])
            for point in points:
                x, y = point
                visited.add(point)
                adjacent_points = get_adjacent_coordinates(x, y, (shape, shape))
                old_risk = total_risk[y][x]
                if point == (shape-1, shape-1):
                    total_risk[y][x] = cave_map[y][x]
                else:
                    total_risk[y][x] = cave_map[y][x] + get_lowest_risk(adjacent_points, total_risk)

                if old_risk != total_risk[y][x]:
                    changed = True

                for new_point in adjacent_points:
                    new_points.add(new_point)
            points = new_points - visited
    return total_risk[0][0] - cave_map[0][0]


print(f"test 1: {solution1(validation_scenario)}")
print(f"solution 1: {solution1(puzzle_input)}")

print(f"test 2: {solution2(validation_scenario)}")
print(f"solution 2: {solution2(puzzle_input)}")
