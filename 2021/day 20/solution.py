from util.util import get_input

validation_scenario = [
    "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#",
    "",
    "#..#.",
    "#....",
    "##..#",
    "..#..",
    "..###"
]

puzzle_input = get_input()


class TrenchMap:

    def __init__(self, scenario: list):
        self.mask = scenario[0]
        self.enhancements = 0
        self.points = set([])
        self.shape = None
        self.infinite = False

        for y, line in enumerate(scenario[2:]):
            for x, value in enumerate(line):
                if value == "#":
                    self.points.add((x, y))

        self.update_shape()

    def update_shape(self):
        x_s = sorted([x for x, _ in self.points])
        y_s = sorted([y for _, y in self.points])
        self.shape = ((x_s[0], x_s[-1]), (y_s[0], y_s[-1]))

    @property
    def count(self) -> float:
        if self.infinite:
            return float('inf')
        return len(self.points)

    @staticmethod
    def get_adjacent_coordinates(x: int, y: int) -> list:
        return [
            (x - 1, y - 1), (x, y - 1), (x + 1, y - 1),
            (x - 1, y), (x, y), (x + 1, y),
            (x - 1, y + 1), (x, y + 1), (x + 1, y + 1),
        ]

    def is_on(self, x, y):
        if self.shape[0][0] <= x <= self.shape[0][1] and self.shape[1][0] <= y <= self.shape[1][1]:
            return (x, y) in self.points
        return self.infinite

    def enhance_image(self):
        # Check local region
        enhanced = set([])
        x1, x2 = self.shape[0]
        y1, y2 = self.shape[1]
        for x in range(x1-1, x2 + 2):
            for y in range(y1-1, y2 + 2):
                mask_points = self.get_adjacent_coordinates(x, y)
                mask_index = int("".join(["1" if self.is_on(*p) else "0" for p in mask_points]), 2)
                if self.mask[mask_index] == "#":
                    enhanced.add((x, y))

        # Check behavior on infinity
        self.enhancements += 1
        if self.enhancements % 2 == 1 and not self.infinite and self.mask[0] == "#":
            self.infinite = True
        elif self.enhancements % 2 == 0 and self.infinite and self.mask[511] == ".":
            self.infinite = False

        # Update internals
        self.points = enhanced
        self.update_shape()

    def print(self):
        x1, x2 = self.shape[0]
        y1, y2 = self.shape[1]
        for y in range(y1, y2 + 1):
            print("".join([" #"[(x, y) in self.points] for x in range(x1, x2 + 1)]))


def solution1(scenario: list) -> int:
    trench_map = TrenchMap(scenario)
    for _ in range(2):
        trench_map.enhance_image()
    return trench_map.count


def solution2(scenario: list) -> int:
    trench_map = TrenchMap(scenario)
    for _ in range(50):
        trench_map.enhance_image()
    return trench_map.count


print(f"test 1: {solution1(validation_scenario)}")
print(f"solution 1: {solution1(puzzle_input)}")

print(f"test 2: {solution2(validation_scenario)}")
print(f"solution 2: {solution2(puzzle_input)}")
