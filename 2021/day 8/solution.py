from util.util import get_input

# Input
validation_scenario = [
    "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | _fdgacbe_ cefdb cefbgd _gcbe_",
    "edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb _cgb_ _dgebacf_ _gc_",
    "fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | _cg_ _cg_ fdcagb _cbg_",
    "fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec _cb_",
    "aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | _gecf_ _egdcabf_ _bgf_ bfgea",
    "fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | _gebdcfa_ _ecba_ _ca_ _fadegcb_",
    "dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | _cefg_ dcbef _fcge_ _gbcadfe_",
    "bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | _ed_ bcgafe cdgba cbgef",
    "egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | _gbdfcae_ _bgc_ _cg_ _cgb_",
    "gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | _fgae_ cfgab _fg_ bagce"
]

puzzle_input = get_input()

# General info
signals = {
    0: ['a', 'b', 'c', 'e', 'f', 'g'],
    1: ['c', 'f'],
    2: ['a', 'c', 'd', 'e', 'g'],
    3: ['a', 'c', 'd', 'f', 'g'],
    4: ['b', 'c', 'd', 'f'],
    5: ['a', 'b', 'd', 'f', 'g'],
    6: ['a', 'b', 'd', 'e', 'f', 'g'],
    7: ['a', 'c', 'f'],
    8: ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
    9: ['a', 'b', 'c', 'd', 'f', 'g']
}

lookup = {}
for k, v in signals.items():
    signal_count = len(v)
    if len(v) not in lookup:
        lookup[signal_count] = []
    lookup[signal_count].append(k)

unique_patterns = {v[0]: k for k, v in lookup.items() if len(v) == 1}
inverse_patterns = {v: k for k, v in unique_patterns.items()}


def solution1(scenario: list):
    total = 0
    for line in scenario:
        _, display = line.split(' | ')
        for n in display.split(' '):
            sc = len(n.replace('_', ''))
            if len(lookup[sc]) == 1:
                total += 1
    return total


print(f"test1: {solution1(validation_scenario)}")
print(f"solution1: {solution1(puzzle_input)}")


def sort(values: list) -> list:
    return [''.join(sorted(v)) for v in values]


def split_displays_output(line: str) -> (list, list):
    displays, display = line.split(' | ')
    return sort(displays.split(' ')), sort([x.replace('_', '') for x in display.split(' ')])


def get_difference(first: str, second: list, mapped: list) -> list:
    diff = set(first) - set(''.join(second + mapped))
    return list(diff)


def find_sum(first, result, options) -> str:
    for option in options:
        if option != result and set(first).union(set(option)) == set(result):
            return option
    return None


def solve_mapping(displays: list, display: list) -> int:
    # Fill in 1, 4, 7, 8
    display_mapping = {d: inverse_patterns.get(len(d), None) for d in displays}
    number_mapping = {v: k for k, v in display_mapping.items() if v is not None}

    # Find a
    line_mapping = {}
    line_mapping['a'] = get_difference(number_mapping[7], [number_mapping[1]], list(line_mapping.values()))[0]

    # Find c
    six = find_sum(number_mapping[1], number_mapping[8], displays)
    display_mapping[six] = 6
    number_mapping[6] = six
    line_mapping['c'] = get_difference(number_mapping[1], [number_mapping[6]],  list(line_mapping.values()))[0]

    # Find f
    line_mapping['f'] = get_difference(number_mapping[7], [],  list(line_mapping.values()))[0]

    # Find e
    five = None
    for option in displays:
        if option != six and not (set(option) - set(six)):
            five = option
    display_mapping[five] = 5
    number_mapping[5] = five
    line_mapping['e'] = get_difference(number_mapping[6], [number_mapping[5]],  list(line_mapping.values()))[0]

    # Find g
    line_mapping['g'] = get_difference(number_mapping[6], [number_mapping[4]],  list(line_mapping.values()))[0]

    # Find b
    guiding_options = [x for x in displays if display_mapping[x] is None and len(x) == 5]
    line_mapping['b'] = get_difference(number_mapping[8], guiding_options,  list(line_mapping.values()))[0]

    # Find d
    line_mapping['d'] = list(set('abcdefg') - set(line_mapping.values()))[0]

    inverse_line_mapping = {v: k for k, v in line_mapping.items()}

    for d, v in display_mapping.items():
        if v is None:
            translated = sorted([inverse_line_mapping[c] for c in d])
            for number, pattern in signals.items():
                if translated == pattern:
                    display_mapping[d] = number
                    break

    return int(''.join([str(display_mapping[d]) for d in display]))


def solution2(scenario: list) -> int:
    total = 0
    for line in scenario:
        displays, display = split_displays_output(line)
        total += solve_mapping(displays, display)
    return total


print(f"test2: {solution2(['acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf'])}")
print(f"solution2: {solution2(puzzle_input)}")