from util.util import get_input

validation_scenario = [
    "D2FE28",
    "8A004A801A8002F478",
    "A0016C880162017C3686B18A3D4780",
    "9C0141080250320F1802104A08"
]

puzzle_input = get_input()


class Packet:

    def __init__(self, bits: str, version: int, packet_type: int, length: int, content: list):
        self.bits = bits
        self.version = version
        self.type = packet_type
        self.length = length
        self.content = content

    @property
    def value(self):
        if self.type == 4:
            return self.content[0]
        elif self.type == 0:
            return sum([x.value for x in self.content])
        elif self.type == 1:
            product = 1
            for x in self.content:
                product *= x.value
            return product
        elif self.type == 2:
            return min([x.value for x in self.content])
        elif self.type == 3:
            return max([x.value for x in self.content])
        elif self.type == 5:
            return self.content[0].value > self.content[1].value
        elif self.type == 6:
            return self.content[0].value < self.content[1].value
        elif self.type == 7:
            return self.content[0].value == self.content[1].value
        return None

    def __str__(self):
        return f"{{V: {self.version}, T: {self.type}, L: {self.length}, C: {self.content}}}"

    def __repr__(self):
        return f"{{V: {self.version}, T: {self.type}, L: {self.length}, C: {self.content}}}"

    @staticmethod
    def to_bits(value: str) -> str:
        return (bin(int(value, 16))[2:]).zfill(len(value) * 4)

    @staticmethod
    def decode(bits: str, to_parse=None):
        packets = []
        while bits and int(bits, 2) and (to_parse is None or len(packets) < to_parse):
            version = int(bits[0:3], 2)
            packet_type = int(bits[3:6], 2)
            length_field_size = int(bits[6:7], 2)
            if packet_type == 4:
                bit_value = ""
                pointer = 6
                while int(bits[pointer]):
                    bit_value += bits[pointer+1: pointer+5]
                    pointer += 5
                bit_value += bits[pointer+1: pointer+5]
                pointer += 5
                packet_bits = bits[0:pointer]
                content = [int(bit_value, 2)]
                length = pointer - 6
                bits = bits[pointer:]
            elif length_field_size:
                number_of_packets = int(bits[7: 7 + 11], 2)
                content = Packet.decode(bits[7 + 11:], number_of_packets)
                length = sum([len(x.bits) for x in content])
                packet_bits = bits[:7 + 11 + length]
                bits = bits[7 + 11 + length:]
            else:
                length = int(bits[7: 7 + 15], 2)
                packet_bits = bits[:7 + 15 + length]
                content_bits = bits[7 + 15: 7 + 15 + length]
                content = Packet.decode(content_bits)[:length]
                bits = bits[7 + 15 + length:]
            packets.append(Packet(packet_bits, version, packet_type, length, content))
        return packets


def solution1(hex_string: str) -> int:
    bits = Packet.to_bits(hex_string)
    packets = Packet.decode(bits)
    version_total = 0
    while packets:
        new_packets = []
        for packet in packets:
            version_total += packet.version
            new_packets += [x for x in packet.content if isinstance(x, Packet)]
        packets = new_packets
    return version_total


def solution2(hex_string: str) -> int:
    bits = Packet.to_bits(hex_string)
    packets = Packet.decode(bits)
    return sum([x.value for x in packets])


print(f"test 1: {solution1(validation_scenario[0])} -> 6")
print(f"test 1: {solution1(validation_scenario[1])} -> 16")
print(f"test 1: {solution1(validation_scenario[2])} -> 31")
print(f"solution 1: {solution1(puzzle_input[0])}")

print(f"test 2: {solution2(validation_scenario[3])}")
print(f"solution 2: {solution2(puzzle_input[0])}")
