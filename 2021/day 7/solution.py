from util.util import get_input
import math

validation_scenario = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]
puzzle_input = [int(x) for x in get_input()[0].split(',')]


class CrabSubmarine:

    def __init__(self, position):
        self._horizontal = position

    def simulate_move(self, to: int) -> int:
        return abs(self._horizontal - to)


def solution1(scenario: list) -> int:
    crabs = [CrabSubmarine(x) for x in scenario]
    min_pos = 0
    min_fuel = float('inf')
    for x in range(min(scenario), max(scenario), 1):
        fuel_cost = sum([c.simulate_move(x) for c in crabs])
        if fuel_cost < min_fuel:
            min_pos = x
            min_fuel = fuel_cost
    return min_pos, min_fuel


def median_scenario(scenario: list) -> int:
    median = sorted(scenario)[len(scenario) // 2]
    return median, sum([abs(x - median) for x in scenario])


def cost(position, to):
    if position == to:
        return 0
    n = abs(position - to)
    return math.ceil((n * (n+1)) / 2)


def avg_scenario(scenario: list) -> int:
    avg = sum(scenario) / len(scenario)
    lowest = sum([cost(x, math.floor(avg)) for x in scenario])
    highest = sum([cost(x, math.ceil(avg)) for x in scenario])

    if lowest < highest:
        return math.floor(avg), lowest

    return math.ceil(avg), highest


def naive(scenario: list) -> int:
    min_pos = 0
    min_fuel = float('inf')
    for x in range(min(scenario), max(scenario), 1):
        fuel_cost = sum([cost(c, x) for c in scenario])
        if fuel_cost < min_fuel:
            min_pos = x
            min_fuel = fuel_cost
    return min_pos, min_fuel


# print(f"test1: {solution1(validation_scenario)}")
print(f"test1.1: {median_scenario(validation_scenario)}")
print(f"test2: {avg_scenario(validation_scenario)}")

# print(f"solution1: {solution1(puzzle_input)}")
print(f"solution1.1: {median_scenario(puzzle_input)}")
print(f"solution2: {avg_scenario(puzzle_input)}")
