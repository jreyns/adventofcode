from util.util import get_input
import math
from itertools import permutations


class SnailFishNumber:

    def __init__(self, pair, parent=None):
        self.x, self.y = pair
        self.depth = 0
        self.parent: SnailFishNumber = parent

        # Recursive init so that we don't have to expand a nested list ourselves if we provide one
        if isinstance(self.x, list):
            self.x = SnailFishNumber(self.x,  self)
            self.depth = max(self.depth, self.x.depth + 1)
        elif isinstance(self.x, SnailFishNumber):
            self.x.parent = self
            self.depth = max(self.depth, self.x.depth + 1)

        if isinstance(self.y, list):
            self.y = SnailFishNumber(self.y, self)
            self.depth = max(self.depth, self.y.depth + 1)
        elif isinstance(self.y, SnailFishNumber):
            self.y.parent = self
            self.depth = max(self.depth, self.y.depth + 1)

    def __str__(self):
        return f"[{self.x}, {self.y}]"

    def __repr__(self):
        return str(self)

    def copy(self):
        return SnailFishNumber(eval(str(self)))

    def __add__(self, other):
        added = SnailFishNumber((self.copy(), other.copy()))
        while added.reduce():
            pass
        return added

    def update_depth(self):
        x_depth = self.x.depth if isinstance(self.x, SnailFishNumber) else 0
        y_depth = self.y.depth if isinstance(self.y, SnailFishNumber) else 0
        self.depth = max(x_depth, y_depth) + 1
        if self.parent:
            self.parent.update_depth()

    def left_most_pair(self, sides: dict, depth: int = 0, last_updated: str = None):
        # If you are a leaf and reached then you are the left most
        if isinstance(self.x, int) and isinstance(self.y, int):
            return sides, self, last_updated

        # Check the left hand side whether it is larger than allowed
        if isinstance(self.x, SnailFishNumber) and depth + 1 + self.x.depth > 3:
            sides.update({"x": self})
            return self.x.left_most_pair(sides, depth + 1, "x")

        # If x isn't deep enough than left must be the deeper path
        sides.update({"y": self})
        return self.y.left_most_pair(sides, depth + 1, "y")

    def left_most(self):
        if isinstance(self.x, int):
            return self
        return self.x.left_most()

    def right_most(self):
        if isinstance(self.y, int):
            return self
        return self.y.right_most()

    @staticmethod
    def update_left_most(sides, value):
        ancestor = sides["x"]

        # Return if no valid ancestor exists
        if not ancestor:
            return

        # Update the appropriate field of the ancestor
        if isinstance(ancestor.y, int):
            ancestor.y += value
        else:
            left_most = ancestor.y.left_most()
            left_most.x += value

    @staticmethod
    def update_right_most(sides, value):
        ancestor = sides["y"]

        # Return if no valid ancestor exists
        if not ancestor:
            return

        # Update the appropriate field of the ancestor
        if isinstance(ancestor.x, int):
            ancestor.x += value
        else:
            right_most = ancestor.x.right_most()
            right_most.y += value

    def reduce(self) -> bool:
        # Check for explodes
        if self.depth > 3:
            # Find first pair to explode
            sides, left_most_pair, side = self.left_most_pair({"x": None, "y": None})
            left_most_pair.parent.explode(side, sides, left_most_pair.x, left_most_pair.y)
            return True

        # Check for splits on the left hand side
        if isinstance(self.x, int) and self.x >= 10:
            self.x = SnailFishNumber([self.x // 2, math.ceil(self.x / 2)], self)
            self.check_split_depth()
            return True

        if isinstance(self.x, SnailFishNumber) and self.x.reduce():
            return True

        # Check for splits on the right hand side
        if isinstance(self.y, int) and self.y >= 10:
            self.y = SnailFishNumber([self.y // 2, math.ceil(self.y / 2)], self)
            self.check_split_depth()
            return True

        if isinstance(self.y, SnailFishNumber) and self.y.reduce():
            return True

        return False

    def explode(self, side: str, sides: dict, x, y):
        # Set value to 0
        if side == "x":
            self.x = 0
        else:
            self.y = 0

        # Update depth if needed
        self.check_explode_depth()

        # Update the left most value of the right hand side with y
        self.update_left_most(sides, y)

        # Update the right most value of the left hand side with x
        self.update_right_most(sides, x)

    def check_explode_depth(self):
        new_depth = max(
            0 if isinstance(self.x, int) else self.x.depth + 1,
            0 if isinstance(self.y, int) else self.y.depth + 1,
        )
        if new_depth != self.depth:
            self.depth = new_depth
            self.parent.update_depth()

    def check_split_depth(self):
        if self.depth == 0:
            self.depth = 1
            self.parent.update_depth()

    @property
    def magnitude(self):
        return (3 * self.x if isinstance(self.x, int) else 3 * self.x.magnitude) \
               + (2 * self.y if isinstance(self.y, int) else 2 * self.y.magnitude)


def get_snailfish_numbers(scenario: list) -> list:
    numbers = []
    for x in scenario:
        numbers.append(SnailFishNumber(eval(x)))
    return numbers


validation_scenario = get_snailfish_numbers([
    "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]",
    "[[[5,[2,8]],4],[5,[[9,9],0]]]",
    "[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]",
    "[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]",
    "[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]",
    "[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]",
    "[[[[5,4],[7,7]],8],[[8,3],8]]",
    "[[9,3],[[9,9],[6,[4,9]]]]",
    "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]",
    "[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]"
])

puzzle_input = get_snailfish_numbers(get_input())


def solution1(scenario: list) -> int:
    total = scenario[0]
    for x in scenario[1:]:
        total = total + x
    return total.magnitude


def solution2(scenario: list) -> int:
    max_magnitude = 0
    for x, y in permutations(scenario, 2):
        max_magnitude = max((x + y).magnitude, max_magnitude)
    return max_magnitude


# print(f"test 1: {solution1(sum_test)}")
print(f"test 1: {solution1(validation_scenario)} -> 4140")
print(f"solution 1: {solution1(puzzle_input)}")

print(f"test 2: {solution2(validation_scenario)} -> 3993")
print(f"solution 2: {solution2(puzzle_input)}")
