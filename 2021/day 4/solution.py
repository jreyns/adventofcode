
with open('input.txt', 'r') as file:
    lines = file.read().splitlines()


class BingoCard:

    def __init__(self, board: list, shape: tuple = (5, 5)):
        self._shape = shape
        self._board = [[int(column) for column in row.split(' ') if column is not ''] for row in board]
        self._crossed = [[0 for _ in range(shape[1])] for _ in range(shape[0])]
        self._bingo = False

    def check_number(self, value: int) -> bool:
        for i in range(self._shape[0]):
            for j in range(self._shape[1]):
                if self._board[i][j] == value:
                    self._crossed[i][j] = 1
                    return True
        return False

    @property
    def bingo(self) -> bool:
        if self._bingo:
            return True

        rows = [0 for _ in range(self._shape[0])]
        columns = [0 for _ in range(self._shape[1])]
        for i in range(self._shape[0]):
            for j in range(self._shape[1]):
                if self._crossed[i][j]:
                    rows[i] += 1
                    columns[j] += 1

        self._bingo = self._shape[0] in rows or self._shape[1] in columns

        return self._bingo

    @property
    def uncrossed_sum(self) -> int:
        total = 0
        for i in range(self._shape[0]):
            for j in range(self._shape[1]):
                if not self._crossed[i][j]:
                    total += self._board[i][j]
        return total


class Bingo:

    def __init__(self, input_lines, shape=(5, 5)):
        self._number_order = [int(x) for x in input_lines[0].split(',')]
        self._cards = [
            BingoCard(input_lines[x + 1: x + 1 + shape[0]], shape) for x in range(1, len(input_lines), shape[0]+1)
        ]

    def solution1(self) -> int:
        for value in self._number_order:
            bingo_count = []
            for card in self._cards:
                if card.check_number(value) and card.bingo:
                    bingo_count.append(card.uncrossed_sum)
                else:
                    bingo_count.append(0)
            turn_score = max(bingo_count)
            if turn_score > 0:
                return value * turn_score
        return 0

    def solution2(self) -> int:
        last_value = 0
        for value in self._number_order:
            new_cards = []
            for card in self._cards:
                if card.check_number(value) and card.bingo:
                    last_value = card.uncrossed_sum * value
                else:
                    new_cards.append(card)
            self._cards = new_cards
            if not self._cards:
                return last_value
        return last_value


input_test = [
    '7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1',
    '',
    '22 13 17 11  0',
    '8  2 23  4 24',
    '21  9 14 16  7',
    '6 10  3 18  5',
    '1 12 20 15 19',
    '',
    '3 15  0  2 22',
    '9 18 13 17  5',
    '19  8  7 25 23',
    '20 11 10 24  4',
    '14 21 16 12  6',
    '',
    '14 21 17 24  4',
    '10 16 15  9 19',
    '18  8 23 26 20',
    '22 11 13  6  5',
    '2  0 12  3  7'
]


def trial_run():
    print(f"test output {Bingo(input_test).solution1()}")
    print(f"test output {Bingo(input_test).solution2()}")


trial_run()

print(f"solution1: {Bingo(lines).solution1()}")

print(f"solution2: {Bingo(lines).solution2()}")
