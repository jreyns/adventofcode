from typing import List, Dict

from util.util import get_input
from util.point import Point3D

validation_scenario = [
    "v...>>.vv>",
    ".vv>>.vv..",
    ">>.>v>...v",
    ">>v>>.>.v.",
    "v>v.vv.v..",
    ">.>>..v...",
    ".vv..>.>v.",
    "v.v..>>v.v",
    "....v..v.>"
]

puzzle_input = get_input()


class SeaFloorMap:

    def __init__(self, state: List[str]):
        self.cucumbers: Dict[str, List[SeaCucumber]] = {}
        self.shape = (len(state[0]), len(state))

        for y, row in enumerate(state):
            for x, direction in enumerate(row):
                if direction != ".":
                    if direction not in self.cucumbers:
                        self.cucumbers[direction] = []
                    self.cucumbers[direction].append(SeaCucumber(Point3D(x, y, 0), direction))

    def __str__(self):
        positions = self.get_positions()
        rows = []
        for y in range(self.shape[1]):
            row = []
            for x in range(self.shape[0]):
                location = Point3D(x, y, 0)
                if location in positions:
                    row.append(positions[location].direction)
                else:
                    row.append(".")
            rows.append("".join(row))
        return "\n".join(rows)

    def get_positions(self):
        positions = {}
        for key in self.cucumbers:
            for cucumber in self.cucumbers[key]:
                positions[cucumber.location] = cucumber

        return positions

    def adjust_position(self, location: Point3D):
        return Point3D(location.x % self.shape[0], location.y % self.shape[1], 0)

    def move(self) -> bool:
        moved = False

        for direction in [">", "v"]:
            current_positions = self.get_positions()

            for cucumber in self.cucumbers[direction]:
                new_location = self.adjust_position(cucumber.move())
                if current_positions.get(new_location) is None:
                    cucumber.location = new_location
                    moved = True

        return moved


class SeaCucumber:

    movement = {
        "v": lambda location: Point3D(location.x, location.y + 1, 0),
        ">": lambda location: Point3D(location.x + 1, location.y, 0)
    }

    def __init__(self, location: Point3D, direction: str):
        self.location = location
        self.direction = direction
        self.move_function = self.movement[direction]

    def __str__(self):
        return f"cucumber at {self.location} moving {self.direction}"

    def move(self):
        return self.move_function(self.location)


def solution1(state: List[str]):
    sea_floor = SeaFloorMap(state)
    # print(sea_floor)

    count = 0
    while sea_floor.move():
        count += 1
        # print()
        # print(sea_floor)

    return count + 1


print(f"test 1: {solution1(validation_scenario)}")

print(f"solution 1: {solution1(puzzle_input)}")
