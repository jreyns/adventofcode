import copy
def part1(p1,p2):
    score1 = 0
    score2 = 0
    cur_val = 1
    rolls = 0
    while True:
        rolls += 3
        p1 = (p1 + 3*cur_val+ 3) % 10
        cur_val =(cur_val+3) % 100
        score1 += p1 if p1 else 10
        if score1 > 999:
            break
        p2 = (p2 + 3*cur_val+ 3) % 10
        rolls += 3
        cur_val =(cur_val+3) % 100
        score2 += p2 if p2 else 10
        if score2 > 999:
            break
    return min(score1,score2)*rolls

def calc_turns(start):
    turns = {(start,0,0):1}
    tot_turns = {}
    probs = {3:1,4:3,5:6,6:7,7:6,8:3,9:1}
    while len(turns):
        new_turns = {}
        for space,score,turn_num in turns:
            for roll in probs:
                key = ((space+roll)%10, score+(((space+roll)%10)if(space+roll)%10 else 10),turn_num+1)
                if key[1]> 20:
                    if key[2] in tot_turns:
                       tot_turns[key[2]] += probs[roll]*turns[(space,score,turn_num)]
                    else:
                       tot_turns[key[2]] = probs[roll]*turns[(space,score,turn_num)]
                    continue
                if key in new_turns:
                   new_turns[key] += probs[roll]*turns[(space,score,turn_num)]
                else:
                   new_turns[key] = probs[roll]*turns[(space,score,turn_num)]
        turns = copy.deepcopy(new_turns)
    return tot_turns

def get_counts(p1_tot_turns, p2_tot_turns):
    p1_cts = 0
    p2_cts = 0
    p1_rem_scenarios = {0:1}
    p2_rem_scenarios = {0:1}
    for num_rolls in range(1,max(p1_tot_turns)+2):
        if num_rolls in p1_tot_turns:
           p1_rem_scenarios[num_rolls] = 27*p1_rem_scenarios[num_rolls-1]-p1_tot_turns[num_rolls]
        else:
           p1_rem_scenarios[num_rolls] = 27*p1_rem_scenarios[num_rolls-1]
        if num_rolls in p2_tot_turns:
           p2_rem_scenarios[num_rolls] = 27*p2_rem_scenarios[num_rolls-1]-p2_tot_turns[num_rolls]
        else:
           p2_rem_scenarios[num_rolls] = 27*p2_rem_scenarios[num_rolls-1]
    for i in p1_tot_turns:
        p1_cts +=p1_tot_turns[i]*p2_rem_scenarios[i-1]
        p2_cts +=p2_tot_turns[i]*p1_rem_scenarios[i]
    return p1_cts, p2_cts

def part2(p1,p2):
    p1_tot_turns = calc_turns(p1)
    p2_tot_turns = calc_turns(p2)
    return max(get_counts(p1_tot_turns,p2_tot_turns))

if __name__ == "__main__":
    p1 = 1
    p2 = 5
    sol1 = part1(p1,p2)
    sol2 = part2(p1,p2)
    print("Solution 1:%d\nSolution 2: %d" % (sol1, sol2))