
validation_scenario = [4, 8]

puzzle_input = [1, 5]


class DeterministicDie:

    def __init__(self):
        self.count = 0

    def __str__(self):
        return str(self.count)

    def throw(self) -> int:
        self.count += 1
        throw = self.count % 100
        if not throw:
            return 100
        return throw

    def throw_dirac(self, count: int = 3):
        return sum([self.throw() for _ in range(count)])


class DiracDice:

    def __init__(self, start_positions):
        self.players = list(range(len(start_positions)))
        self.positions = start_positions
        self.score = [0, 0]
        self.die = DeterministicDie()
        self.turn = 0

    def __str__(self):
        return f"""{self.players}\n{self.positions}\n{self.score}\n{self.die}\n{self.turn}"""

    def move(self, index, steps):
        total = self.positions[index] + steps
        if total > 10:
            total = total % 10
        if not total:
            total = 10
        self.positions[index] = total
        self.score[index] += total

    def play(self):
        index = self.turn % 2
        steps = self.die.throw_dirac()
        self.move(index, steps)
        self.turn += 1


def solution1(scenario: list) -> int:
    game = DiracDice(scenario)
    while max(game.score) < 1000:
        game.play()
    return min(game.score) * game.die.count


print(f"test 1: {solution1(validation_scenario)}")
print(f"solution 1: {solution1(puzzle_input)}")

"""
    max throws = 6
"""