import math
import copy


class IntCode:

    def __init__(self):
        self.inputs = []
        self.outputs = []
        self.memory = []
        self.fp = 0
        self.ip = 0
        self.rb = 0

    def load_program(self, program):
        self.memory = program

    def load_input(self, inp):
        self.inputs = inp

    def add_input(self, inp):
        if type(inp) is not list:
            inp = [inp]
        self.inputs += inp

    def reset(self):
        self.fp = 0

    def get_parameter(self, index, mode=0):
        if mode == 0:
            self.mem_alloc(self.memory[index])
            return self.memory[self.memory[index]]
        elif mode == 1:
            self.mem_alloc(index)
            return self.memory[index]
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            return self.memory[self.rb + offset]

    def write_value(self, index, value, mode=0):
        if mode == 0:
            self.write_to_memory(self.memory[index], value)
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            self.write_to_memory(self.rb + offset, value)

    @staticmethod
    def parse_op_code(code: int):
        code_str = str(code)
        while len(code_str) < 5:
            code_str = "0" + code_str
        tokens = [int(c) for c in code_str]
        op_code = tokens[-2] * 10 + tokens[-1]
        modes = tokens[:-2]
        modes.reverse()
        return op_code, modes

    def mem_alloc(self, mem_loc):
        memory_diff = mem_loc - len(self.memory) + 1
        if memory_diff > 0:
            self.memory += [0] * memory_diff

    def write_to_memory(self, mem_loc, value):
        self.mem_alloc(mem_loc)
        self.memory[mem_loc] = value

    def do_sum(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) + self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_mult(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) * self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_store(self, modes):
        if self.ip >= len(self.inputs):
            return False
        self.write_value(self.fp + 1, self.inputs[self.ip], modes[0])
        # self.write_to_memory(self.get_parameter(self.fp + 1, modes[0]), self.inputs[self.ip])
        self.ip += 1
        self.fp += 2
        return True

    def do_output(self, modes):
        self.outputs.append(self.get_parameter(self.fp + 1, modes[0]))
        self.fp += 2
        return True

    def do_jump_if_true(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) > 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_jump_if_false(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) == 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_less_than(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) < self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_equals(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) == self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_adjust_relative_base(self, modes):
        self.rb += self.get_parameter(self.fp + 1, modes[0])
        self.fp += 2
        return True

    def do_op(self):
        op_code, modes = self.parse_op_code(self.memory[self.fp])
        if op_code == 1:
            return self.do_sum(modes)
        elif op_code == 2:
            return self.do_mult(modes)
        elif op_code == 3:
            return self.do_store(modes)
        elif op_code == 4:
            return self.do_output(modes)
        elif op_code == 5:
            return self.do_jump_if_true(modes)
        elif op_code == 6:
            return self.do_jump_if_false(modes)
        elif op_code == 7:
            return self.do_less_than(modes)
        elif op_code == 8:
            return self.do_equals(modes)
        elif op_code == 9:
            return self.do_adjust_relative_base(modes)
        elif op_code == 99:
            return False
        else:
            raise ValueError(f"unexpected operation code: {(op_code, self.memory)}")

    def run(self):
        can_continue = True
        while can_continue:
            can_continue = self.do_op()


def str_to_program(text: str):
    return [int(v) for v in text.split(",")]


class Grid:

    black = " "
    white = "#"

    def __init__(self):
        self.X = []
        self.Y = []
        self.panels = {}
        self.base_color = self.black

    def paint_panel(self, x, y, color):
        if x not in self.X:
            self.X.append(x)
        if y not in self.Y:
            self.Y.append(y)
        self.panels[(x, y)] = color

    def get_color_of_panel(self, x, y):
        return self.panels.get((x, y), self.base_color)

    def is_black(self, x, y):
        return self.get_color_of_panel(x, y) == self.black

    def is_white(self, x, y):
        return self.get_color_of_panel(x, y) == self.white

    def count_plates_colored_at_least_once(self):
        return len(self.panels)

    def print_grid(self, x, y, symbol):
        if x not in self.X:
            self.X.append(x)
        if y not in self.Y:
            self.Y.append(y)
        for y2 in range(max(self.Y), min(self.Y) - 1, -1):
            x_axis = list(range(min(self.X), max(self.X) + 1))
            line = [symbol if x2 == x and y2 == y else self.get_color_of_panel(x2, y2) for x2 in x_axis]
            print(f"{''.join(line)}")


class HullPaintingRobot:

    def __init__(self):
        self.brain = IntCode()
        self.x = 0
        self.y = 0
        self.direction = math.pi / 2
        self.symbols = {
            math.pi / 2: "^",
            0: ">",
            math.pi: "<",
            - math.pi: "<",
            - math.pi / 2: "v"
        }

    def load(self, operating_system):
        self.brain.load_program(operating_system)

    def get_symbol(self):
        symbol = self.symbols.get(self.direction)
        if symbol is None:
            print(self.direction)
        return self.symbols.get(self.direction)

    def rescale_direction(self):
        if self.direction > math.pi:
            self.direction += - 2*math.pi
        elif self.direction < - math.pi:
            self.direction += 2*math.pi

    def rotate(self, direction):
        if direction == 0:
            self.direction += math.pi / 2
        elif direction == 1:
            self.direction -= math.pi / 2
        self.rescale_direction()

    def move(self, distance=1):
        self.x += int(distance * math.cos(self.direction))
        self.y += int(distance * math.sin(self.direction))

    def get_color(self, grid):
        color = int(grid.is_white(self.x, self.y))
        self.brain.add_input(color)

    def paint(self, grid, color_value):
        if color_value == 0:
            color = grid.black
        elif color_value == 1:
            color = grid.white
        grid.paint_panel(self.x, self.y, color)

    def run(self, grid, print_grid=False):
        self.get_color(grid)
        self.brain.run()
        outputs = self.brain.outputs[-2:]
        self.brain.outputs = []
        while len(outputs) != 0:
            self.paint(grid, outputs[-2])
            self.rotate(outputs[-1])
            self.move()
            self.get_color(grid)
            self.brain.run()
            outputs = self.brain.outputs[-2:]
            self.brain.outputs = []
        if print_grid:
            grid.print_grid(self.x, self.y, self.get_symbol())
            print("\n")


def solve_part1():
    programs = []
    with open("input.txt", "r") as file:
        for line in file:
            programs.append(str_to_program(line))
    for program in programs:
        grid = Grid()
        paint_robot = HullPaintingRobot()
        paint_robot.load(program)
        paint_robot.run(grid)
        print(f"solution part 1: {grid.count_plates_colored_at_least_once()}")


def solve_part2():
    programs = []
    with open("input2.txt", "r") as file:
        for line in file:
            programs.append(str_to_program(line))
    for program in programs:
        grid = Grid()
        grid.paint_panel(0, 0, grid.white)
        paint_robot = HullPaintingRobot()
        paint_robot.load(program)
        print(f"solution part 2:")
        paint_robot.run(grid, True)


def main():
    solve_part1()
    solve_part2()


if __name__ == "__main__":
    main()
