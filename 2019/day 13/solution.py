import time
from win32api import GetKeyState


class IntCode:

    def __init__(self):
        self.inputs = []
        self.outputs = []
        self.memory = []
        self.fp = 0
        self.ip = 0
        self.rb = 0

    def load_program(self, program):
        self.memory = program

    def load_input(self, inp):
        self.inputs = inp

    def add_input(self, inp):
        if type(inp) is not list:
            inp = [inp]
        self.inputs += inp

    def reset(self):
        self.fp = 0

    def get_parameter(self, index, mode=0):
        if mode == 0:
            self.mem_alloc(self.memory[index])
            return self.memory[self.memory[index]]
        elif mode == 1:
            self.mem_alloc(index)
            return self.memory[index]
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            return self.memory[self.rb + offset]

    def write_value(self, index, value, mode=0):
        if mode == 0:
            self.write_to_memory(self.memory[index], value)
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            self.write_to_memory(self.rb + offset, value)

    @staticmethod
    def parse_op_code(code: int):
        code_str = str(code)
        while len(code_str) < 5:
            code_str = "0" + code_str
        tokens = [int(c) for c in code_str]
        op_code = tokens[-2] * 10 + tokens[-1]
        modes = tokens[:-2]
        modes.reverse()
        return op_code, modes

    def mem_alloc(self, mem_loc):
        memory_diff = mem_loc - len(self.memory) + 1
        if memory_diff > 0:
            self.memory += [0] * memory_diff

    def write_to_memory(self, mem_loc, value):
        self.mem_alloc(mem_loc)
        self.memory[mem_loc] = value

    def do_sum(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) + self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_mult(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) * self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_store(self, modes):
        if self.ip >= len(self.inputs):
            return False
        self.write_value(self.fp + 1, self.inputs[self.ip], modes[0])
        # self.write_to_memory(self.get_parameter(self.fp + 1, modes[0]), self.inputs[self.ip])
        self.ip += 1
        self.fp += 2
        return True

    def do_output(self, modes):
        self.outputs.append(self.get_parameter(self.fp + 1, modes[0]))
        self.fp += 2
        return True

    def do_jump_if_true(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) > 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_jump_if_false(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) == 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_less_than(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) < self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_equals(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) == self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_adjust_relative_base(self, modes):
        self.rb += self.get_parameter(self.fp + 1, modes[0])
        self.fp += 2
        return True

    def do_op(self):
        op_code, modes = self.parse_op_code(self.memory[self.fp])
        if op_code == 1:
            return self.do_sum(modes)
        elif op_code == 2:
            return self.do_mult(modes)
        elif op_code == 3:
            return self.do_store(modes)
        elif op_code == 4:
            return self.do_output(modes)
        elif op_code == 5:
            return self.do_jump_if_true(modes)
        elif op_code == 6:
            return self.do_jump_if_false(modes)
        elif op_code == 7:
            return self.do_less_than(modes)
        elif op_code == 8:
            return self.do_equals(modes)
        elif op_code == 9:
            return self.do_adjust_relative_base(modes)
        elif op_code == 99:
            return False
        else:
            raise ValueError(f"unexpected operation code: {(op_code, self.memory)}")

    def run(self):
        can_continue = True
        while can_continue:
            can_continue = self.do_op()


def str_to_program(text: str):
    return [int(v) for v in text.split(",")]


class Grid:

    tile_ids = {
        0: " ",
        1: "+",
        2: "#",
        3: "-",
        4: "o"
    }

    def __init__(self):
        self.X = []
        self.Y = []
        self.panels = {}
        self.base_type = self.tile_ids[0]

    def paint_panel(self, x, y, tile_type):
        if x == -1 and y == 0:
            x = 0
            y = -1
            self.panels[(x, y)] = str(tile_type)
        else:
            self.panels[(x, y)] = self.tile_ids[tile_type]
        if x not in self.X:
            self.X.append(x)
        if y not in self.Y:
            self.Y.append(y)

    def get_score(self):
        return self.get_color_of_panel(0, -1)

    def get_ball_position(self):
        for position in self.panels:
            if self.panels[position] == self.tile_ids[4]:
                return position

    def get_pallet_position(self):
        for position in self.panels:
            if self.panels[position] == self.tile_ids[3]:
                return position

    def get_color_of_panel(self, x, y):
        return self.panels.get((x, y), self.base_type)

    def count_tile_type(self, tile_type):
        count = 0
        for position in self.panels:
            if self.panels[position] == self.tile_ids[tile_type]:
                count += 1
        return count

    def print_grid(self):
        for y2 in range(min(self.Y), max(self.Y) + 1, 1):
            x_axis = list(range(min(self.X), max(self.X) + 1))
            line = [self.get_color_of_panel(x2, y2) for x2 in x_axis]
            print(f"{''.join(line)}")


class JoyStick:

    @staticmethod
    def keyIsDown(key):
        keystate = GetKeyState(key)
        if (keystate != 0) and (keystate != 1):
            return True
        else:
            return False

    @staticmethod
    def get_input():
        while True:
            if JoyStick.keyIsDown(37):
                return -1
            elif JoyStick.keyIsDown(39):
                return 1
            elif JoyStick.keyIsDown(38):
                return 0

    @staticmethod
    def get_input2(grid):
        pos_ball = grid.get_ball_position()
        pos_pallet = grid.get_pallet_position()
        return pos_ball[0] - pos_pallet[0]


class ArcadeCabinet:

    def __init__(self):
        self.brain = IntCode()

    def load(self, operating_system):
        self.brain.load_program(operating_system)

    def set_for_free(self):
        self.brain.memory[0] = 2

    def paint(self, grid, output):
        grid.paint_panel(output[0], output[1], grid.tile_ids[output[2]])

    def load_board(self, grid):
        self.brain.run()
        outputs = self.brain.outputs
        for i in range(0, len(outputs), 3):
            grid.paint_panel(outputs[i], outputs[i+1], outputs[i+2])

    def get_score(self, grid):
        return grid.get_color_of_panel(0, -1)

    def run(self, grid, sleep_time=0.0):
        self.load_board(grid)
        grid.print_grid()
        print(f"score: {grid.get_score()} count: {grid.count_tile_type(2)}")
        while len(self.brain.outputs) != 0:
            self.brain.outputs = []
            joystick_input = JoyStick.get_input2(grid)
            self.brain.add_input(joystick_input)
            self.brain.run()
            outputs = self.brain.outputs
            for i in range(0, len(outputs), 3):
                grid.paint_panel(outputs[i], outputs[i+1], outputs[i+2])
            grid.print_grid()
            time.sleep(sleep_time)
            print(f"score: {grid.get_score()} count: {grid.count_tile_type(2)}")


def solve_part1():
    programs = []
    with open("input.txt", "r") as file:
        for line in file:
            programs.append(str_to_program(line))
    for program in programs:
        grid = Grid()
        paint_robot = ArcadeCabinet()
        paint_robot.load(program)
        paint_robot.load_board(grid)
        print(f"solution part 1: {grid.count_tile_type(2)}")
        grid.print_grid()


def solve_part2():
    programs = []
    with open("input2.txt", "r") as file:
        for line in file:
            programs.append(str_to_program(line))
    for program in programs:
        grid = Grid()
        paint_robot = ArcadeCabinet()
        paint_robot.load(program)
        paint_robot.set_for_free()
        paint_robot.run(grid)
        print(f"solution part 2: {paint_robot.get_score(grid)}")


def main():
    solve_part1()
    solve_part2()


if __name__ == "__main__":
    main()
