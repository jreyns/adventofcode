from functools import lru_cache
from util.util import get_input, get_adjacent_coordinates, get_sparse_map


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    _, sparse, shape = get_sparse_map(lines, ["#"])
    return sparse, shape


def game_of_life(sparse: dict, shape):
    new_state = {}
    for x in range(shape[0]):
        for y in range(shape[1]):
            neighbours = len([p for p in get_adjacent_coordinates(x, y, shape) if p in sparse])
            if (x, y) in sparse and neighbours == 1:
                new_state[(x, y)] = "#"
                continue

            if (x, y) not in sparse and neighbours in [1, 2]:
                new_state[(x, y)] = "#"
                continue

    return new_state


@lru_cache
def get_neighbours(depth, x, y, shape):
    neighbours = [(depth, (u, v)) for u, v in get_adjacent_coordinates(x, y, shape) if (u, v) != (shape[0]//2, shape[1]//2)]
    if x == 0:  # outer border
        neighbours += [(depth - 1, (1, 2))]
    elif x == 4:
        neighbours += [(depth - 1, (3, 2))]

    if y == 0:  # outer border
        neighbours += [(depth - 1, (2, 1))]
    elif y == 4:
        neighbours += [(depth - 1, (2, 3))]

    if (x, y) == (2, 1):
        neighbours += [(depth + 1, (u, 0)) for u in range(shape[0])]
    elif (x, y) == (1, 2):
        neighbours += [(depth + 1, (0, v)) for v in range(shape[1])]
    elif (x, y) == (2, 3):
        neighbours += [(depth + 1, (u, 4)) for u in range(shape[0])]
    elif (x, y) == (3, 2):
        neighbours += [(depth + 1, (4, v)) for v in range(shape[1])]

    return neighbours


def recursive_game_of_life(sparse: list, shape):
    new_sparse = set([])
    checked = set([])
    for depth, (x, y) in sparse:
        neighbours = get_neighbours(depth, x, y, shape)
        bug_count = len([n for n in neighbours if n in sparse])
        # check if stays alive
        if bug_count == 1:
            new_sparse.add((depth, (x, y)))

        # check if neighbour becomes alive
        for d, (u, v) in [n for n in neighbours if n not in sparse and n not in checked]:
            checked.add((d, (u, v)))
            neighbours = get_neighbours(d, u, v, shape)
            bug_count = len([n for n in neighbours if n in sparse])
            if bug_count in [1, 2]:
                new_sparse.add((d, (u, v)))

    return list(new_sparse)


def biodiversity(state):
    return sum([pow(2, y*5 + x) for x, y in state])


def print_recursive(sparse, shape):
    depths = sorted(list(set([depth for depth, _ in sparse])))
    for depth in depths:
        print(f"Depth {depth}:")
        for y in range(shape[1]):
            line = []
            for x in range(shape[0]):
                if (depth, (x, y)) in sparse:
                    line.append("#")
                    continue
                if (x, y) == (shape[0]//2, shape[1]//2):
                    line.append("?")
                    continue
                line.append(".")
            print("".join(line))
        print()


def solution1(problem=puzzle_input):
    sparse, shape = process_input(problem)
    states = set()
    state = tuple(sorted(sparse.keys()))
    while state not in states:
        states.add(state)
        sparse = game_of_life(sparse, shape)
        state = tuple(sorted(sparse.keys()))
    return biodiversity(state)
    

def solution2(problem=puzzle_input, minutes=200):
    sparse, shape = process_input(problem)
    sparse = [(0, p) for p in sparse]
    for _ in range(minutes):
        sparse = recursive_game_of_life(sparse, shape)
    # print_recursive(sparse, shape)
    return len(sparse)
    
    
print(f"test 1: {solution1(test_input)}")
print(f"test 2: {solution2(test_input, 10)}")
    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
