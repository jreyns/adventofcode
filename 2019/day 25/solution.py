import re
import json
from util.util import get_input
import copy


class IntCode:

    def __init__(self):
        self.inputs = []
        self.outputs = []
        self.memory = []
        self.fp = 0
        self.ip = 0
        self.rb = 0

    def load_program(self, program):
        self.memory = copy.deepcopy(program)

    def load_input(self, inp):
        self.inputs = inp

    def add_input(self, inp):
        if type(inp) is not list:
            inp = [inp]
        self.inputs += inp

    def reset(self):
        self.fp = 0

    def get_parameter(self, index, mode=0):
        if mode == 0:
            self.mem_alloc(self.memory[index])
            return self.memory[self.memory[index]]
        elif mode == 1:
            self.mem_alloc(index)
            return self.memory[index]
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            return self.memory[self.rb + offset]

    def write_value(self, index, value, mode=0):
        if mode == 0:
            self.write_to_memory(self.memory[index], value)
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            self.write_to_memory(self.rb + offset, value)

    @staticmethod
    def parse_op_code(code: int):
        code_str = str(code)
        while len(code_str) < 5:
            code_str = "0" + code_str
        tokens = [int(c) for c in code_str]
        op_code = tokens[-2] * 10 + tokens[-1]
        modes = tokens[:-2]
        modes.reverse()
        return op_code, modes

    def mem_alloc(self, mem_loc):
        memory_diff = mem_loc - len(self.memory) + 1
        if memory_diff > 0:
            self.memory += [0] * memory_diff

    def write_to_memory(self, mem_loc, value):
        self.mem_alloc(mem_loc)
        self.memory[mem_loc] = value

    def do_sum(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) + self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_mult(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) * self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_store(self, modes):
        if self.ip >= len(self.inputs):
            return False
        self.write_value(self.fp + 1, self.inputs[self.ip], modes[0])
        # self.write_to_memory(self.get_parameter(self.fp + 1, modes[0]), self.inputs[self.ip])
        self.ip += 1
        self.fp += 2
        return True

    def do_output(self, modes):
        self.outputs.append(self.get_parameter(self.fp + 1, modes[0]))
        self.fp += 2
        return True

    def do_jump_if_true(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) > 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_jump_if_false(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) == 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_less_than(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) < self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_equals(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) == self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_adjust_relative_base(self, modes):
        self.rb += self.get_parameter(self.fp + 1, modes[0])
        self.fp += 2
        return True

    def do_op(self):
        op_code, modes = self.parse_op_code(self.memory[self.fp])
        if op_code == 1:
            return self.do_sum(modes)
        elif op_code == 2:
            return self.do_mult(modes)
        elif op_code == 3:
            return self.do_store(modes)
        elif op_code == 4:
            return self.do_output(modes)
        elif op_code == 5:
            return self.do_jump_if_true(modes)
        elif op_code == 6:
            return self.do_jump_if_false(modes)
        elif op_code == 7:
            return self.do_less_than(modes)
        elif op_code == 8:
            return self.do_equals(modes)
        elif op_code == 9:
            return self.do_adjust_relative_base(modes)
        elif op_code == 99:
            return False
        else:
            raise ValueError(f"unexpected operation code: {(op_code, self.memory)}")

    def run(self):
        can_continue = True
        while can_continue:
            can_continue = self.do_op()


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    program = [int(x) for x in lines[0].split(",")]
    return program


def read_output(droid):
    output = "".join([chr(x) if x < 256 else str(x) for x in droid.outputs])
    droid.outputs = []
    return output


def parse_output(output):
    room = re.findall(r"== (.*) ==", output)[0]
    info = re.findall(r"- ([^\n]+)\n", output)
    directions = [i for i in info if i in ["north", "south", "east", "west"]]
    items = [
        i for i in info
        if i not in directions
        and i not in ["escape pod", "giant electromagnet", "photons", "infinite loop", "molten lava"]
    ]
    return room, directions, items


def room_not_mapped(room):
    return any([direction is None for direction in room.values()])


def all_rooms_mapped(environment):
    for room, directions in environment.items():
        if room_not_mapped(directions):
            return False
    return True


def select_unmapped_room(environment):
    for room, directions in environment.items():
        if room_not_mapped(directions):
            return room


def move_to(start, dst, environment):
    seen = set([])
    current = [(start, [])]
    while current:
        room, commands = current.pop(0)

        if room == dst:
            return commands

        seen.add(room)

        for direction, other in environment[room].items():
            if direction == "items" or other is None:
                continue

            if other in seen:
                continue

            current.append((other, commands + [f"{direction}\n"]))


def map_environment(droid):
    current = read_output(droid)
    environment = {}
    inventory = []
    inverse = {
        "north": "south",
        "south": "north",
        "east": "west",
        "west": "east"
    }
    previous_room = None
    previous_direction = None
    while True:
        room, directions, items = parse_output(current)
        if room not in environment:
            environment[room] = {k: None for k in directions}

        if items:
            inventory += items
            commands = [f"take {item}\n" for item in items]
            droid.add_input([ord(c) for c in "".join(commands)])
            droid.run()
            droid.outputs = []

        if previous_room:
            environment[previous_room][previous_direction] = room
            environment[room][inverse[previous_direction]] = previous_room

        if all_rooms_mapped(environment):
            return room, environment, inventory

        if not room_not_mapped(environment[room]):
            previous_room = None
            previous_direction = None
            dst = select_unmapped_room(environment)
            commands = move_to(room, dst, environment)
            droid.add_input([ord(c) for c in "".join(commands[:-1])])
            droid.run()
            droid.outputs = []
            commands = commands[-1:]
        else:
            previous_room = room
            previous_direction = [k for k, v in environment[room].items() if v is None][0]
            commands = [f"{previous_direction}\n"]

        droid.add_input([ord(c) for c in "".join(commands)])
        droid.run()
        current = read_output(droid)


def solution1(problem=puzzle_input):
    program = process_input(problem)
    droid = IntCode()
    droid.load_program(program)
    droid.run()

    # Map environment
    current_room, environment, inventory = map_environment(droid)
    if current_room == "Pressure-Sensitive Floor":
        current_room = "Security Checkpoint"

    # Move to Security Checkpoint
    commands = move_to(current_room, "Security Checkpoint", environment)
    if commands:
        droid.add_input([ord(c) for c in "".join(commands)])
        droid.run()
        output = read_output(droid)

    # Inventory: ['planetoid', 'spool of cat6', 'sand', 'dark matter', 'coin', 'jam', 'wreath', 'fuel cell']:
    to_drop = ['planetoid', 'dark matter', 'coin', 'wreath']
    commands = [f"drop {item}\n" for item in to_drop]
    droid.add_input([ord(c) for c in "".join(commands)])
    droid.run()
    droid.outputs = []

    commands = move_to(current_room, "Pressure-Sensitive Floor", environment)
    droid.add_input([ord(c) for c in "".join(commands)])
    droid.run()
    output = read_output(droid)

    return output


print(f"solution 1: {solution1()}")
