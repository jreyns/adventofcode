
from util.util import get_input
import copy


class IntCode:

    def __init__(self):
        self.inputs = []
        self.outputs = []
        self.memory = []
        self.fp = 0
        self.ip = 0
        self.rb = 0

    def load_program(self, program):
        self.memory = copy.deepcopy(program)

    def load_input(self, inp):
        self.inputs = inp

    def add_input(self, inp):
        if type(inp) is not list:
            inp = [inp]
        self.inputs += inp

    def reset(self):
        self.fp = 0

    def get_parameter(self, index, mode=0):
        if mode == 0:
            self.mem_alloc(self.memory[index])
            return self.memory[self.memory[index]]
        elif mode == 1:
            self.mem_alloc(index)
            return self.memory[index]
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            return self.memory[self.rb + offset]

    def write_value(self, index, value, mode=0):
        if mode == 0:
            self.write_to_memory(self.memory[index], value)
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            self.write_to_memory(self.rb + offset, value)

    @staticmethod
    def parse_op_code(code: int):
        code_str = str(code)
        while len(code_str) < 5:
            code_str = "0" + code_str
        tokens = [int(c) for c in code_str]
        op_code = tokens[-2] * 10 + tokens[-1]
        modes = tokens[:-2]
        modes.reverse()
        return op_code, modes

    def mem_alloc(self, mem_loc):
        memory_diff = mem_loc - len(self.memory) + 1
        if memory_diff > 0:
            self.memory += [0] * memory_diff

    def write_to_memory(self, mem_loc, value):
        self.mem_alloc(mem_loc)
        self.memory[mem_loc] = value

    def do_sum(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) + self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_mult(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) * self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_store(self, modes):
        if self.ip >= len(self.inputs):
            return False
        self.write_value(self.fp + 1, self.inputs[self.ip], modes[0])
        # self.write_to_memory(self.get_parameter(self.fp + 1, modes[0]), self.inputs[self.ip])
        self.ip += 1
        self.fp += 2
        return True

    def do_output(self, modes):
        self.outputs.append(self.get_parameter(self.fp + 1, modes[0]))
        self.fp += 2
        return True

    def do_jump_if_true(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) > 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_jump_if_false(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) == 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_less_than(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) < self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_equals(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) == self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_adjust_relative_base(self, modes):
        self.rb += self.get_parameter(self.fp + 1, modes[0])
        self.fp += 2
        return True

    def do_op(self):
        op_code, modes = self.parse_op_code(self.memory[self.fp])
        if op_code == 1:
            return self.do_sum(modes)
        elif op_code == 2:
            return self.do_mult(modes)
        elif op_code == 3:
            return self.do_store(modes)
        elif op_code == 4:
            return self.do_output(modes)
        elif op_code == 5:
            return self.do_jump_if_true(modes)
        elif op_code == 6:
            return self.do_jump_if_false(modes)
        elif op_code == 7:
            return self.do_less_than(modes)
        elif op_code == 8:
            return self.do_equals(modes)
        elif op_code == 9:
            return self.do_adjust_relative_base(modes)
        elif op_code == 99:
            return False
        else:
            raise ValueError(f"unexpected operation code: {(op_code, self.memory)}")

    def run(self):
        can_continue = True
        while can_continue:
            can_continue = self.do_op()


puzzle_input = get_input()


def process_input(lines=puzzle_input):
    program = [int(x) for x in lines[0].split(",")]
    return program


def load_network(program):
    network = []
    for i in range(50):
        pc = IntCode()
        pc.load_program(program)
        pc.run()
        pc.add_input(i)
        pc.run()
        network.append(pc)
    return network


def split(outputs):
    return [(outputs[i], tuple(outputs[i+1:i+3])) for i in range(0, len(outputs), 3)]


def send_packets(network, queue, nat_enabled=False):
    new_queue = {}
    packet_sent = None

    if nat_enabled and 255 in queue:
        packet_count = sum([len(queue[k]) for k in range(50)])
        if not packet_count:
            packet_sent = queue[255][-1]
            queue[0].append(packet_sent)

    for i, pc in enumerate(network):
        if not queue[i]:
            pc.add_input(-1)
            pc.run()
        else:
            x, y = queue[i].pop(0)
            pc.add_input([x, y])
            pc.run()

        for dst, packet in split(pc.outputs):
            if dst not in new_queue:
                new_queue[dst] = []

            new_queue[dst].append(packet)
        pc.outputs = []

    for k, v in new_queue.items():
        if k not in queue:
            queue[k] = v
        else:
            queue[k] += v

    return packet_sent


def solution1(problem=puzzle_input):
    program = process_input(problem)
    network = load_network(program)
    queue = {k: [] for k in range(50)}
    while 255 not in queue:
        send_packets(network, queue)
    return queue[255][0][1]
    

def solution2(problem=puzzle_input):
    program = process_input(problem)
    network = load_network(program)
    queue = {k: [] for k in range(50)}
    nat_packets_sent = set()
    while True:
        nat_packet = send_packets(network, queue, True)
        if nat_packet and nat_packet in nat_packets_sent:
            return nat_packet[1]

        if nat_packet:
            nat_packets_sent.add(nat_packet)


print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
