class IntCode:

    def __init__(self):
        self.inputs = []
        self.outputs = []
        self.memory = []
        self.fp = 0
        self.ip = 0
        self.rb = 0

    def load_program(self, program):
        self.memory = program

    def load_input(self, inp):
        self.inputs = inp

    def add_input(self, inp):
        if type(inp) is not list:
            inp = [inp]
        self.inputs += inp

    def reset(self):
        self.fp = 0

    def get_parameter(self, index, mode=0):
        if mode == 0:
            self.mem_alloc(self.memory[index])
            return self.memory[self.memory[index]]
        elif mode == 1:
            self.mem_alloc(index)
            return self.memory[index]
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            return self.memory[self.rb + offset]

    def write_value(self, index, value, mode=0):
        if mode == 0:
            self.write_to_memory(self.memory[index], value)
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            self.write_to_memory(self.rb + offset, value)

    @staticmethod
    def parse_op_code(code: int):
        code_str = str(code)
        while len(code_str) < 5:
            code_str = "0" + code_str
        tokens = [int(c) for c in code_str]
        op_code = tokens[-2] * 10 + tokens[-1]
        modes = tokens[:-2]
        modes.reverse()
        return op_code, modes

    def mem_alloc(self, mem_loc):
        memory_diff = mem_loc - len(self.memory) + 1
        if memory_diff > 0:
            self.memory += [0] * memory_diff

    def write_to_memory(self, mem_loc, value):
        self.mem_alloc(mem_loc)
        self.memory[mem_loc] = value

    def do_sum(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) + self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_mult(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) * self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_store(self, modes):
        if self.ip >= len(self.inputs):
            return False
        self.write_value(self.fp + 1, self.inputs[self.ip], modes[0])
        # self.write_to_memory(self.get_parameter(self.fp + 1, modes[0]), self.inputs[self.ip])
        self.ip += 1
        self.fp += 2
        return True

    def do_output(self, modes):
        self.outputs.append(self.get_parameter(self.fp + 1, modes[0]))
        self.fp += 2
        return True

    def do_jump_if_true(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) > 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_jump_if_false(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) == 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_less_than(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) < self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_equals(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) == self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_adjust_relative_base(self, modes):
        self.rb += self.get_parameter(self.fp + 1, modes[0])
        self.fp += 2
        return True

    def do_op(self):
        op_code, modes = self.parse_op_code(self.memory[self.fp])
        if op_code == 1:
            return self.do_sum(modes)
        elif op_code == 2:
            return self.do_mult(modes)
        elif op_code == 3:
            return self.do_store(modes)
        elif op_code == 4:
            return self.do_output(modes)
        elif op_code == 5:
            return self.do_jump_if_true(modes)
        elif op_code == 6:
            return self.do_jump_if_false(modes)
        elif op_code == 7:
            return self.do_less_than(modes)
        elif op_code == 8:
            return self.do_equals(modes)
        elif op_code == 9:
            return self.do_adjust_relative_base(modes)
        elif op_code == 99:
            return False
        else:
            raise ValueError(f"unexpected operation code: {(op_code, self.memory)}")

    def run(self):
        can_continue = True
        while can_continue:
            can_continue = self.do_op()


def str_to_program(text: str):
    return [int(v) for v in text.split(",")]


def cases():
    tests = [
        {
            "program": "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99",
            "context": "part 1",
            "solution": [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]
        },
        {
            "program": "1102,34915192,34915192,7,4,7,99,0",
            "context": "part 1",
            "solution": [1102, 34915192, 34915192, 7, 4, 7, 99, 0]
        },
        {
            "program": "104,1125899906842624,99",
            "context": "part 1",
            "solution": [1125899906842624]
        }
    ]

    for i, test in enumerate(tests):
        program = str_to_program(test["program"])
        int_code_machine = IntCode()
        int_code_machine.load_program(program)
        int_code_machine.run()
        context = test["context"]
        solution = test["solution"]
        if context == "part 1":
            print(f"test {i}.0: {solution} {int_code_machine.outputs}")
        elif context == "part 2":
            print(f"test {i}.0: {solution} {int_code_machine.outputs}")


def solve_part1():
    programs = []
    with open("input.txt", "r") as file:
        for line in file:
            programs.append(str_to_program(line))
    for program in programs:
        int_code_machine = IntCode()
        int_code_machine.load_program(program)
        int_code_machine.add_input(1)
        int_code_machine.run()
        print(f"solution part 1: {int_code_machine.outputs}")


def solve_part2():
    programs = []
    with open("input.txt", "r") as file:
        for line in file:
            programs.append(str_to_program(line))
    for program in programs:
        int_code_machine = IntCode()
        int_code_machine.load_program(program)
        int_code_machine.add_input(2)
        int_code_machine.run()
        print(f"solution part 2: {int_code_machine.outputs}")


def main():
    cases()
    solve_part1()
    solve_part2()


if __name__ == "__main__":
    main()
