from typing import List
from math import ceil, floor


class Reaction:

    def __init__(self, input_chemicals: List[tuple]=None, output_chemical: tuple=None):
        self.output_chemical = output_chemical
        self.input_chemicals = input_chemicals

    def name(self):
        return self.output_chemical[0]

    def count(self):
        return self.output_chemical[1]

    @staticmethod
    def parse_chemical(chemical_string: str):
        amount, name = chemical_string.strip().split(" ")
        return name, int(amount)

    @staticmethod
    def parse_reaction(input_str: str):
        input_chemicals_str, output_chemical_str = input_str.split("=>")
        output_chemical = Reaction.parse_chemical(output_chemical_str)
        input_chemicals = [Reaction.parse_chemical(input_chemical) for input_chemical in input_chemicals_str.split(", ")]
        return Reaction(input_chemicals, output_chemical)


def create_reaction_dictionary(reactions: List[str]):
    reaction_dict = {}
    for reaction_str in reactions:
        reaction = Reaction.parse_reaction(reaction_str)
        reaction_dict[reaction.name()] = reaction
    return reaction_dict


def resource_needed(resource_name, input_list, reaction_dictionary):
    if len(input_list) == 0:
        return False
    else:
        for name in [inp[0] for inp in input_list]:
            if name == resource_name:
                return True
            if name not in reaction_dictionary:
                continue
            new_input_list = reaction_dictionary[name].input_chemicals
            if resource_needed(resource_name, new_input_list, reaction_dictionary):
                return True
    return False


def has_only_ore(resources: list):
    for resource in resources:
        if resource != "ORE":
            return False
    return True


def is_a_dependency(resource, resources, reaction_dictionary):
    for other_resource in resources:
        if resource == other_resource:
            return True
        elif other_resource not in reaction_dictionary:
            continue
        reaction = reaction_dictionary[other_resource]
        new_resources = [inp[0] for inp in reaction.input_chemicals]
        if is_a_dependency(resource, new_resources, reaction_dictionary):
            return True

    return False


def is_not_a_dependency(resource, resources, reaction_dictionary):
    if resource == "ORE":
        return False
    for other_resource in resources:
        if other_resource not in reaction_dictionary:
            continue
        reaction = reaction_dictionary[other_resource]
        new_resources = [inp[0] for inp in reaction.input_chemicals]
        if is_a_dependency(resource, new_resources, reaction_dictionary):
            return False

    return True


def calculate_amount_of_ore_for_fuel(reaction_dictionary: dict, fuel_amount=1):
    reaction = reaction_dictionary["FUEL"]
    resource_counts = {inp[0]: fuel_amount * inp[1] for inp in reaction.input_chemicals}
    resources = set(resource_counts.keys())
    while not has_only_ore(resources):
        new_resources = set([])
        for resource in resources:
            if is_not_a_dependency(resource, resources, reaction_dictionary):
                reaction = reaction_dictionary[resource]
                for new_resource in reaction.input_chemicals:
                    if new_resource[0] not in resource_counts:
                        resource_counts[new_resource[0]] = \
                            new_resource[1] * ceil(resource_counts[resource] / reaction.count())
                    else:
                        resource_counts[new_resource[0]] += \
                            new_resource[1] * ceil(resource_counts[resource] / reaction.count())
                    if new_resource[0] not in new_resources:
                        new_resources.add(new_resource[0])
            else:
                new_resources.add(resource)
        resources = new_resources

    return resource_counts["ORE"]


def calculate_amount_of_fuel_with_ore(ore, reaction_dictionary):
    max_ore = calculate_amount_of_ore_for_fuel(reaction_dictionary)
    min_fuel = ore / max_ore
    max_fuel = ore
    while max_fuel - min_fuel > 1:
        current_fuel = (max_fuel + min_fuel) // 2
        current_ore = calculate_amount_of_ore_for_fuel(reaction_dictionary, fuel_amount=current_fuel)
        if current_ore > ore:
            max_fuel = current_fuel
        else:
            min_fuel = current_fuel
    return min_fuel


def str_to_start_positions(string):
    return string.split("\n")


def cases():
    tests = [
        {
            "program": "10 ORE => 10 A\n1 ORE => 1 B\n7 A, 1 B => 1 C\n7 A, 1 C => 1 D\n7 A, 1 D => 1 E\n7 A, 1 E => 1 FUEL",
            "context": "part 1",
            "solution": 31
        },
        {
            "program": "9 ORE => 2 A\n8 ORE => 3 B\n7 ORE => 5 C\n3 A, 4 B => 1 AB\n5 B, 7 C => 1 BC\n4 C, 1 A => 1 CA\n2 AB, 3 BC, 4 CA => 1 FUEL",
            "context": "part 1",
            "solution": 165
        },
        {
            "program": """157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT""",
            "context": "part 1",
            "solution": 13312
        },
        {
            "program": """2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF""",
            "context": "part 1",
            "solution": 180697
        },
        {
            "program": """171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX""",
            "context": "part 1",
            "solution": 2210736
        },
        {
            "program": """157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT""",
            "context": "part 2",
            "solution": 82892753
        },
        {
            "program": """2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF""",
            "context": "part 2",
            "solution": 5586022
        },
        {
            "program": """171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX""",
            "context": "part 2",
            "solution": 460664
        }
    ]

    for i, test in enumerate(tests):
        reactions = str_to_start_positions(test["program"])
        reaction_dictionary = create_reaction_dictionary(reactions)
        context = test["context"]
        solution = test["solution"]
        if context == "part 1":
            print(f"test {i}.0: {solution} {calculate_amount_of_ore_for_fuel(reaction_dictionary)}")
        elif context == "part 2":
            ore = 1000000000000
            print(f"test {i}.0: {solution} {calculate_amount_of_fuel_with_ore(ore, reaction_dictionary)}")


def solve_part1():
    reactions = []
    with open("input.txt", "r") as file:
        for line in file:
            reactions.append(line)
    reaction_dictionary = create_reaction_dictionary(reactions)
    print(f"solution 1: {calculate_amount_of_ore_for_fuel(reaction_dictionary)}")


def solve_part2():
    reactions = []
    with open("input.txt", "r") as file:
        for line in file:
            reactions.append(line)
    reaction_dictionary = create_reaction_dictionary(reactions)
    ore = 1000000000000
    print(f"solution 2: {calculate_amount_of_fuel_with_ore(ore, reaction_dictionary)}")


def main():
    cases()
    solve_part1()
    solve_part2()


if __name__ == "__main__":
    main()
