class Node:

    def __init__(self, locations, collected_keys):
        self.locations = locations
        self.collected_keys = collected_keys
        self.parents = set([])

    def __hash__(self):
        return hash((self.locations, str(self.collected_keys)))

    def __eq__(self, other):
        return (self.locations, self.collected_keys) == (other.locations, other.collected_keys)

    def __ne__(self, other):
        # Not strictly necessary, but to avoid having both x==y and x!=y
        # True at the same time
        return not(self == other)

    def __repr__(self):
        return f"({self.locations}, {', '.join(self.collected_keys)})"


class Grid:

    def __init__(self):
        self.X = []
        self.Y = []
        self.panels = {}
        self.keys = []
        self.has_a_reachable_key = []

    def remove_unreachable_keys(self):
        for location in self.panels:
            if self.is_door([location]) and self.panels[location].lower() not in self.keys:
                self.panels[location] = "."

    def is_start_position(self, locations):
        for location in locations:
            if self.panels[location] != "@":
                return False
        return True

    def is_wall(self, locations):
        for location in locations:
            if self.panels[location] == "#":
                return True
        return False

    def is_door(self, locations):
        for location in locations:
            if self.panels[location] in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
                return True
        return False

    def is_key(self, locations):
        for location in locations:
            if self.panels[location] in "abcdefghijklmnopqrstuvwxyz":
                return True
        return False

    def is_empty(self, locations):
        for location in locations:
            if self.panels[location] != ".":
                return False
        return True

    def paint_panel(self, x, y, tile_type):
        self.panels[(x, y)] = tile_type
        if x not in self.X:
            self.X.append(x)
        if y not in self.Y:
            self.Y.append(y)

    def get_start_position(self):
        start_positions = []
        for y in range(min(self.Y), max(self.Y) + 1, 1):
            for x in range(min(self.X), max(self.X) + 1):
                if self.is_start_position([(x, y)]):
                    start_positions.append((x, y))
        return tuple(start_positions)

    def change_start_position(self):
        x, y = self.get_start_position()[0]
        self.panels[(x, y)] = "#"
        self.panels[(x, y+1)] = "#"
        self.panels[(x, y-1)] = "#"
        self.panels[(x+1, y)] = "#"
        self.panels[(x-1, y)] = "#"
        self.panels[(x+1, y+1)] = "@"
        self.panels[(x+1, y-1)] = "@"
        self.panels[(x-1, y+1)] = "@"
        self.panels[(x-1, y-1)] = "@"

    @staticmethod
    def build_location_state(locations, option, index):
        if index == len(locations) - 1:
            return tuple(locations[:-1] + [option])
        elif index == 0:
            return tuple([option] + locations[1:])
        else:
            return tuple(locations[:index] + [option] + locations[index+1:])

    def location_has_reachable_key(self, location, collected_keys):
        reachable = set([location])
        visited = set([])
        while len(reachable) > 0:
            new_reachable = set([])
            for x, y in reachable:
                visited.add((x, y))
                options = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
                for option in options:
                    if self.is_wall([option]) or (self.is_door([option]) and self.panels[option].lower() not in collected_keys):
                        continue
                    elif self.is_key([option]) and self.panels[option] not in collected_keys:
                        return True
                    elif option not in visited:
                        new_reachable.add(option)
            reachable = new_reachable
        return False

    def update_reachable_keys(self, node):
        self.has_a_reachable_key = [self.location_has_reachable_key(loc, node.collected_keys) for loc in node.locations]

    def get_next_node_per_location(self, node, index):
        x, y = node.locations[index]
        options = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
        nodes = []
        for location in options:
            location_state = self.build_location_state(list(node.locations), location, index)
            if self.is_wall([location]):
                continue
            elif self.is_start_position([location]):
                nodes.append(Node(location_state, node.collected_keys))
            elif self.is_empty([location]):
                nodes.append(Node(location_state, node.collected_keys))
            elif self.is_key([location]):
                if self.panels[location] not in node.collected_keys:
                    nodes.append(Node(location_state, sorted(node.collected_keys + [self.panels[location]])))
                else:
                    nodes.append(Node(location_state, node.collected_keys))
            elif self.is_door([location]):
                if self.panels[location].lower() in node.collected_keys:
                    nodes.append(Node(location_state, node.collected_keys))
        return nodes

    def get_next_nodes(self, node):
        nodes = []
        if len(node.locations) > 1:
            self.update_reachable_keys(node)
        for index in range(len(node.locations)):
            if self.has_a_reachable_key[index]:
                nodes += self.get_next_node_per_location(node, index)
        return nodes

    def to_str(self):
        str = ""
        for y2 in range(min(self.Y), max(self.Y) + 1, 1):
            x_axis = list(range(min(self.X), max(self.X) + 1))
            line = [self.panels[(x2, y2)] for x2 in x_axis]
            str += f"{''.join(line)}\n"
        return str

    def print_grid(self):
        for y2 in range(min(self.Y), max(self.Y) + 1, 1):
            x_axis = list(range(min(self.X), max(self.X) + 1))
            line = [self.panels[(x2, y2)] for x2 in x_axis]
            print(f"{''.join(line)}")

    @staticmethod
    def create_grid(lines, lower_bound=None, upper_bound=None):
        grid = Grid()
        y_min = 0
        y_max = len(lines)
        if lower_bound is not None:
            y_min = lower_bound[1]
        if upper_bound is not None:
            y_max = upper_bound[1]
        for y in range(y_min, y_max):
            x_min = 0
            x_max = len(lines[y])
            if lower_bound is not None:
                x_min = lower_bound[0]
            if upper_bound is not None:
                x_max = upper_bound[0]
            for x in range(x_min, x_max):
                grid.paint_panel(x, y, lines[y][x])
                if grid.is_key([(x, y)]):
                    grid.keys = sorted(grid.keys + [lines[y][x]])
        return grid


class UndergroundVault:

    def __init__(self, grid):
        self.grid = grid
        self.start_position = grid.get_start_position()
        self.start_node = Node(self.start_position, [])
        self.grid.update_reachable_keys(self.start_node)
        self.end_node = None
        self.connections = {self.start_node: (set([]), 0)}
        self.nodes = [self.start_node]

    def find_end_node(self, print_every_key=False):
        keys = self.grid.keys
        key_count = 0
        nodes = set([self.start_node])
        while len(nodes) > 0:
            new_nodes = set([])
            for node in nodes:
                reachable_nodes = self.grid.get_next_nodes(node)
                for reachable_node in reachable_nodes:
                    self.connections[node][0].add(reachable_node)
                    if reachable_node not in self.connections:
                        reachable_node.parents.add(node)
                        self.connections[reachable_node] = (set([node]), len(self.nodes))
                        self.nodes.append(reachable_node)
                        new_nodes.add(reachable_node)
                    else:
                        self.nodes[self.connections[reachable_node][1]].parents.add(node)
                if len(node.collected_keys) > key_count and print_every_key:
                    print(f"found {len(node.collected_keys)}/{len(keys)} keys")
                    key_count = len(node.collected_keys)
                if node.collected_keys == keys:
                    print(f"found {len(node.collected_keys)}/{len(keys)} keys")
                    return node
            nodes = new_nodes

    def find_shortest_path(self):
        nodes = set([self.end_node])
        visited = set([])
        count = 0
        while self.start_node not in nodes:
            new_nodes = set([])
            for node in nodes:
                for parent in node.parents:
                    if parent not in visited:
                        new_nodes.add(parent)
                visited.add(node)
            count += 1
            nodes = new_nodes
        return count

    def solve(self, print_every_key=False):
        self.end_node = self.find_end_node(print_every_key)
        return self.find_shortest_path()


def cases():
    tests = [
        {
            "program": """#########
#b.A.@.a#
#########""",
            "context": "part 1",
            "solution": 8
        },
        {
            "program": """########################
#f.D.E.e.C.b.A.@.a.B.c.#
######################.#
#d.....................#
########################""",
            "context": "part 1",
            "solution": 86
        },
        {
            "program": """########################
#...............b.C.D.f#
#.######################
#.....@.a.B.c.d.A.e.F.g#
########################""",
            "context": "part 1",
            "solution": 132
        },
        {
            "program": """#################
#i.G..c...e..H.p#
########.########
#j.A..b...f..D.o#
########@########
#k.E..a...g..B.n#
########.########
#l.F..d...h..C.m#
#################""",
            "context": "part 1",
            "solution": 136
        },
        {
            "program": """########################
#@..............ac.GI.b#
###d#e#f################
###A#B#C################
###g#h#i################
########################""",
            "context": "part 1",
            "solution": 81
        },
        {
            "program": """#############
#g#f.D#..h#l#
#F###e#E###.#
#dCba@#@BcIJ#
#############
#nK.L@#@G...#
#M###N#H###.#
#o#m..#i#jk.#
#############""",
            "context": "part 2",
            "solution": 72
        }
    ]

    for i, test in enumerate(tests):
        grid = Grid.create_grid(test["program"].split("\n"))
        vault = UndergroundVault(grid)
        context = test["context"]
        solution = test["solution"]
        print(f"test {i}.0: start")
        if context == "part 1":
            print(f"test {i}.0: {solution} {vault.solve()}")
        elif context == "part 2":
            print(f"test {i}.0: {solution} {vault.solve(True)}")


def solve_part1():
    print(f"solving part 1")
    vault_map = []
    with open("input.txt", "r") as file:
        for line in file:
            vault_map.append(line.replace("\n", ""))
    grid = Grid.create_grid(vault_map)
    vault = UndergroundVault(grid)
    print(f"solution 1: {vault.solve()}")


def solve_part2():
    print(f"solving part 2")
    vault_map = []
    with open("input2.txt", "r") as file:
        for line in file:
            vault_map.append(line.replace("\n", ""))
    grid = Grid.create_grid(vault_map)
    grid.change_start_position()
    new_grid = grid.to_str()
    vaults = []
    for lower_bound, upper_bound in [[(0, 0), (41, 41)], [(0, 40), (41, 81)], [(40, 0), (81, 41)], [(40, 40), (81, 81)]]:
        grid = Grid.create_grid(new_grid.split("\n"), lower_bound, upper_bound)
        grid.remove_unreachable_keys()
        vault = UndergroundVault(grid)
        vaults.append(vault)
    print(f"solution 2: {sum([vault.solve(True) for vault in vaults])}")


def main():
    cases()
    solve_part1()
    solve_part2()


if __name__ == "__main__":
    main()
