def parse_move(move_str: str):
    return move_str[0], int(move_str[1:])


def get_coordinates_after_move(coord: tuple, move: tuple):
    if move[0] == "U":
        return [(coord[0], coord[1] + i) for i in range(1, move[1] + 1)]
    elif move[0] == "D":
        return [(coord[0], coord[1] - i) for i in range(1, move[1] + 1)]
    elif move[0] == "L":
        return [(coord[0] - i, coord[1]) for i in range(1, move[1] + 1)]
    elif move[0] == "R":
        return [(coord[0] + i, coord[1]) for i in range(1, move[1] + 1)]


def get_coordinates(coord: tuple, path: list):
    coordinates = [coord]
    for move_str in path:
        coordinates = coordinates + get_coordinates_after_move(coordinates[-1], parse_move(move_str))
    return coordinates


def manhatten_distance(p1, p2):
    return abs(p1[0] - p2[0]) + abs(p1[1] - p2[1])


def amount_of_steps(coord, path):
    return path.index(coord)


def calculate_closest_intersect(coord: tuple, paths: str):
    set_intersect = set(get_coordinates(coord, paths[0].split(",")))
    for path in paths:
        set_intersect = set_intersect.intersection(set(get_coordinates(coord, path.split(","))))
    minimum = None
    for s in set_intersect:
        if s != coord:
            distance = manhatten_distance(coord, s)
            if minimum is None or distance < minimum:
                minimum = distance
    return minimum


def calculate_fastest_intersect(coord: tuple, paths: str):
    coordinates = []
    set_intersect = set(get_coordinates(coord, paths[0].split(",")))
    for path in paths:
        coordinates.append(get_coordinates(coord, path.split(",")))
        set_intersect = set_intersect.intersection(set(coordinates[-1]))
    minimum = None
    for s in set_intersect:
        if s != coord:
            distance = sum([amount_of_steps(s, path) for path in coordinates])
            if minimum is None or distance < minimum:
                minimum = distance
    return minimum


def cases():
    tests = [
        {
            "coord": (0, 0),
            "path1": "R8,U5,L5,D3",
            "path2": "U7,R6,D4,L4",
            "distance": 6,
            "steps": 30
        },
        {
            "coord": (0, 0),
            "path1": "R75,D30,R83,U83,L12,D49,R71,U7,L72",
            "path2": "U62,R66,U55,R34,D71,R55,D58,R83",
            "distance": 159,
            "steps": 610
        },
        {
            "coord": (0, 0),
            "path1": "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
            "path2": "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7",
            "distance": 135,
            "steps": 410
        }
    ]

    for i, test in enumerate(tests):
        coord = test["coord"]
        paths = [test["path1"], test["path2"]]
        distance = test["distance"]
        steps = test["steps"]
        print(f"test {i}.0: {distance} {calculate_closest_intersect(coord, paths)}")
        print(f"test {i}.1: {steps} {calculate_fastest_intersect(coord, paths)}")


def solve_part1():
    coord = (0, 0)
    paths = []
    with open("input.txt", "r") as file:
        for line in file:
            paths.append(line)
    print(calculate_closest_intersect(coord, paths))


def solve_part2():
    coord = (0, 0)
    paths = []
    with open("input2.txt", "r") as file:
        for line in file:
            paths.append(line)
    print(calculate_fastest_intersect(coord, paths))


def main():
    cases()
    solve_part1()
    solve_part2()


if __name__ == "__main__":
    main()

