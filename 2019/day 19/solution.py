import copy


class IntCode:

    def __init__(self):
        self.inputs = []
        self.outputs = []
        self.memory = []
        self.fp = 0
        self.ip = 0
        self.rb = 0

    def load_program(self, program):
        self.memory = copy.deepcopy(program)

    def load_input(self, inp):
        self.inputs = inp

    def add_input(self, inp):
        if type(inp) is not list:
            inp = [inp]
        self.inputs += inp

    def reset(self):
        self.fp = 0

    def get_parameter(self, index, mode=0):
        if mode == 0:
            self.mem_alloc(self.memory[index])
            return self.memory[self.memory[index]]
        elif mode == 1:
            self.mem_alloc(index)
            return self.memory[index]
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            return self.memory[self.rb + offset]

    def write_value(self, index, value, mode=0):
        if mode == 0:
            self.write_to_memory(self.memory[index], value)
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            self.write_to_memory(self.rb + offset, value)

    @staticmethod
    def parse_op_code(code: int):
        code_str = str(code)
        while len(code_str) < 5:
            code_str = "0" + code_str
        tokens = [int(c) for c in code_str]
        op_code = tokens[-2] * 10 + tokens[-1]
        modes = tokens[:-2]
        modes.reverse()
        return op_code, modes

    def mem_alloc(self, mem_loc):
        memory_diff = mem_loc - len(self.memory) + 1
        if memory_diff > 0:
            self.memory += [0] * memory_diff

    def write_to_memory(self, mem_loc, value):
        self.mem_alloc(mem_loc)
        self.memory[mem_loc] = value

    def do_sum(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) + self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_mult(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) * self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_store(self, modes):
        if self.ip >= len(self.inputs):
            return False
        self.write_value(self.fp + 1, self.inputs[self.ip], modes[0])
        # self.write_to_memory(self.get_parameter(self.fp + 1, modes[0]), self.inputs[self.ip])
        self.ip += 1
        self.fp += 2
        return True

    def do_output(self, modes):
        self.outputs.append(self.get_parameter(self.fp + 1, modes[0]))
        self.fp += 2
        return True

    def do_jump_if_true(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) > 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_jump_if_false(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) == 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_less_than(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) < self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_equals(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) == self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_adjust_relative_base(self, modes):
        self.rb += self.get_parameter(self.fp + 1, modes[0])
        self.fp += 2
        return True

    def do_op(self):
        op_code, modes = self.parse_op_code(self.memory[self.fp])
        if op_code == 1:
            return self.do_sum(modes)
        elif op_code == 2:
            return self.do_mult(modes)
        elif op_code == 3:
            return self.do_store(modes)
        elif op_code == 4:
            return self.do_output(modes)
        elif op_code == 5:
            return self.do_jump_if_true(modes)
        elif op_code == 6:
            return self.do_jump_if_false(modes)
        elif op_code == 7:
            return self.do_less_than(modes)
        elif op_code == 8:
            return self.do_equals(modes)
        elif op_code == 9:
            return self.do_adjust_relative_base(modes)
        elif op_code == 99:
            return False
        else:
            raise ValueError(f"unexpected operation code: {(op_code, self.memory)}")

    def run(self):
        can_continue = True
        while can_continue:
            can_continue = self.do_op()


def str_to_program(text: str):
    return [int(v) for v in text.split(",")]


def distance(point):
    x, y = point
    return x + y


def compare_coordinates(p1, p2):
    d = distance(p1) - distance(p2)
    if d > 0:
        return 1
    elif d == 0:
        return 0
    else:
        return -1


class Grid:

    tile_ids = {
        0: ".",
        1: "#"
    }

    def __init__(self):
        self.X = []
        self.Y = []
        self.panels = {}

    def paint_panel(self, x, y, tile_type):
        if tile_type == 1:
            self.panels[(x, y)] = self.tile_ids[tile_type]
        if x not in self.X:
            self.X.append(x)
        if y not in self.Y:
            self.Y.append(y)

    def get_color_of_panel(self, x, y):
        return self.panels.get((x, y), self.tile_ids[0])

    def count_tile_type(self, tile_type):
        count = 0
        for position in self.panels:
            if self.panels[position] == self.tile_ids[tile_type]:
                count += 1
        return count

    def get_sorted_panels(self):
        panels = list(self.panels.keys())
        return sorted(panels, key=distance)

    def find_closest_in_grid(self, x_dir, y_dir):
        for coordinate in self.get_sorted_panels():
            x, y = coordinate
            positions = [(x + x_dir - 1, y), (x, y + y_dir - 1), (x + x_dir - 1, y + y_dir - 1)]
            valid = True
            for position in positions:
                if position not in self.panels:
                    valid = False
            if valid:
                return coordinate

    def print_grid(self):
        for y2 in range(min(self.Y), max(self.Y) + 1, 1):
            x_axis = list(range(min(self.X), max(self.X) + 1))
            line = [self.get_color_of_panel(x2, y2) for x2 in x_axis]
            print(f"{''.join(line)}")


class TractorBeamDrone:

    def __init__(self):
        self.brain = IntCode()

    def load(self, operating_system):
        self.brain.load_program(operating_system)

    @staticmethod
    def check_point(x, y, grid, program):
        drone = TractorBeamDrone()
        drone.load(program)
        drone.brain.add_input([x, y])
        drone.brain.run()
        if drone.brain.outputs[-1] == 1:
            grid.paint_panel(x, y, 1)
            return True
        return False

    @staticmethod
    def check_grid(x, y, x_range, y_range, grid, program):
        options = [(x + x_range - 1, y), (x, y + y_range - 1), (x + x_range - 1, y + y_range - 1)]
        for option in options:
            if not TractorBeamDrone.check_point(option[0], option[1], grid, program):
                return False
        return True

    @staticmethod
    def map_tractor_beam(x_range, y_range, grid, program, step=10):
        print_at = step
        if TractorBeamDrone.check_point(0, 0, grid, program):
            has_traction = [[0, 0]]
        else:
            has_traction = [[]]
        for y in range(y_range[0] + 1, y_range[1]):
            positions = []
            found = False
            for x in range(y - 1, min(y + len(has_traction[y-1]) + (y // 10) + 2, x_range[1])):
                if TractorBeamDrone.check_point(x, y, grid, program):
                    positions.append((x, y))
                    found = True
                    break
            if found:
                x += 1
                while x < x_range[1] and TractorBeamDrone.check_point(x, y, grid, program):
                    positions.append((x, y))
                    x += 1
            has_traction.append(positions)
            if y == print_at:
                print(f"reached row {y} with {len(positions)} valid locations")
                print_at *= step

    @staticmethod
    def find_closest_in_grid(x_range, y_range, grid, program, step=10):
        print_at = step
        if TractorBeamDrone.check_point(0, 0, grid, program):
            has_traction = [[0, 0]]
        else:
            has_traction = [[]]

        y = 1
        while True:
            positions = []
            found = False
            for x in range(y - 1, y + len(has_traction[y-1]) + (y // 10) + 2):
                if (x, y) in grid.panels or TractorBeamDrone.check_point(x, y, grid, program):
                    positions.append((x, y))
                    if TractorBeamDrone.check_grid(x, y, x_range, y_range, grid, program):
                        return x, y
                    found = True
                    break
            if found:
                x += 1
                while (x, y) in grid.panels or TractorBeamDrone.check_point(x, y, grid, program):
                    positions.append((x, y))
                    if TractorBeamDrone.check_grid(x, y, x_range, y_range, grid, program):
                        return x, y
                    x += 1
            has_traction.append(positions)
            if y == print_at:
                print(f"reached row {y} with {len(positions)} valid locations")
                print_at *= step
            y += 1

    def run(self, grid):
        self.brain.run()
        grid.paint_panels(self.brain.outputs)
        grid.print_grid()


def solve_part1():
    programs = []
    with open("input.txt", "r") as file:
        for line in file:
            programs.append(str_to_program(line))
    for program in programs:
        grid = Grid()
        TractorBeamDrone.map_tractor_beam((0, 50), (0, 50), grid, program)
        grid.print_grid()
        print(f"solution 1: {grid.count_tile_type(1)}")


def solve_part2():
    programs = []
    with open("input2.txt", "r") as file:
        for line in file:
            programs.append(str_to_program(line))
    for program in programs:
        grid = Grid()
        point = TractorBeamDrone.find_closest_in_grid(100, 100, grid, program)
        grid.print_grid()
        print(f"solution 2: {point[0] * 10000 + point[1]}")


def main():
    solve_part1()
    solve_part2()


if __name__ == "__main__":
    main()
