import random


class IntCode:

    def __init__(self):
        self.inputs = []
        self.outputs = []
        self.memory = []
        self.fp = 0
        self.ip = 0
        self.rb = 0

    def load_program(self, program):
        self.memory = program

    def load_input(self, inp):
        self.inputs = inp

    def add_input(self, inp):
        if type(inp) is not list:
            inp = [inp]
        self.inputs += inp

    def reset(self):
        self.fp = 0

    def get_parameter(self, index, mode=0):
        if mode == 0:
            self.mem_alloc(self.memory[index])
            return self.memory[self.memory[index]]
        elif mode == 1:
            self.mem_alloc(index)
            return self.memory[index]
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            return self.memory[self.rb + offset]

    def write_value(self, index, value, mode=0):
        if mode == 0:
            self.write_to_memory(self.memory[index], value)
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            self.write_to_memory(self.rb + offset, value)

    @staticmethod
    def parse_op_code(code: int):
        code_str = str(code)
        while len(code_str) < 5:
            code_str = "0" + code_str
        tokens = [int(c) for c in code_str]
        op_code = tokens[-2] * 10 + tokens[-1]
        modes = tokens[:-2]
        modes.reverse()
        return op_code, modes

    def mem_alloc(self, mem_loc):
        memory_diff = mem_loc - len(self.memory) + 1
        if memory_diff > 0:
            self.memory += [0] * memory_diff

    def write_to_memory(self, mem_loc, value):
        self.mem_alloc(mem_loc)
        self.memory[mem_loc] = value

    def do_sum(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) + self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_mult(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) * self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_store(self, modes):
        if self.ip >= len(self.inputs):
            return False
        self.write_value(self.fp + 1, self.inputs[self.ip], modes[0])
        # self.write_to_memory(self.get_parameter(self.fp + 1, modes[0]), self.inputs[self.ip])
        self.ip += 1
        self.fp += 2
        return True

    def do_output(self, modes):
        self.outputs.append(self.get_parameter(self.fp + 1, modes[0]))
        self.fp += 2
        return True

    def do_jump_if_true(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) > 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_jump_if_false(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) == 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_less_than(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) < self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_equals(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) == self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_adjust_relative_base(self, modes):
        self.rb += self.get_parameter(self.fp + 1, modes[0])
        self.fp += 2
        return True

    def do_op(self):
        op_code, modes = self.parse_op_code(self.memory[self.fp])
        if op_code == 1:
            return self.do_sum(modes)
        elif op_code == 2:
            return self.do_mult(modes)
        elif op_code == 3:
            return self.do_store(modes)
        elif op_code == 4:
            return self.do_output(modes)
        elif op_code == 5:
            return self.do_jump_if_true(modes)
        elif op_code == 6:
            return self.do_jump_if_false(modes)
        elif op_code == 7:
            return self.do_less_than(modes)
        elif op_code == 8:
            return self.do_equals(modes)
        elif op_code == 9:
            return self.do_adjust_relative_base(modes)
        elif op_code == 99:
            return False
        else:
            raise ValueError(f"unexpected operation code: {(op_code, self.memory)}")

    def run(self):
        can_continue = True
        while can_continue:
            can_continue = self.do_op()


def str_to_program(text: str):
    return [int(v) for v in text.split(",")]


class Grid:

    tile_ids = {
        0: "#",
        1: "D",
        2: "+",
        3: ".",
        4: " ",
        5: "T",
        6: "O",
    }

    def __init__(self):
        self.X = []
        self.Y = []
        self.panels = {}
        self.base_type = 4

    def paint_panel(self, x, y, tile_type):
        if x == 0 and y == 0:
            self.panels[(x, y)] = 2
        elif (x, y) in self.panels and self.panels[(x, y)] == 6:
            return
        else:
            self.panels[(x, y)] = tile_type
        if x not in self.X:
            self.X.append(x)
        if y not in self.Y:
            self.Y.append(y)

    def in_grid(self, point, strikt=False):
        if strikt:
            return min(self.X) <= point[0] <= max(self.X) and min(self.Y) <= point[1] <= max(self.Y)
        return min(self.X)-1 <= point[0] <= max(self.X) + 1 and min(self.Y)-1 <= point[1] <= max(self.Y) + 1

    def get_allowed_directions(self, x, y, dijkstra=None, strikt=False):
        directions = []
        if self.get_color_of_panel(x, y) == 0:
            return directions
        options = [(x, y+1), (x, y-1), (x-1, y), (x+1, y)]
        for direction in options:
            if self.get_color_of_panel(direction[0], direction[1]) != 0 and (dijkstra is None or (direction not in dijkstra and self.in_grid(direction, strikt))):
                directions.append(direction)
        return directions

    def get_color_of_panel(self, x, y):
        return self.panels.get((x, y), self.base_type)

    def points_of_interests(self, skip_o=False, strikt=False):
        max_interest = 4
        points_of_interest = {k: set([]) for k in self.tile_ids}
        for x1, y1 in self.panels:
            if self.get_color_of_panel(x1, y1) == 0:
                continue
            for x, y in self.get_allowed_directions(x1, y1, strikt=strikt):
                level_of_interest = self.get_color_of_panel(x, y)
                if level_of_interest == 0 or (skip_o and (4 > level_of_interest or level_of_interest >= 6)):
                    continue
                if level_of_interest >= max_interest:
                    max_interest = level_of_interest
                    points_of_interest[level_of_interest].add((x, y))
        return points_of_interest[max_interest]

    def apply_dijkstra(self, x, y):
        loc_type = self.get_color_of_panel(x, y)
        if loc_type == 0:
            raise ValueError("i dont want to walk in a wall")
        elif loc_type != 6:
            self.paint_panel(x, y, 5)
        score = 1
        dijkstra = {(x, y): 0}
        options = self.get_allowed_directions(x, y, dijkstra)
        while len(options) > 0:
            new_options = set()
            for option in options:
                dijkstra[option] = score
                for new_option in self.get_allowed_directions(option[0], option[1], dijkstra):
                    new_options.add(new_option)
            score += 1
            options = new_options
        return dijkstra

    def count_tile_type(self, tile_type):
        count = 0
        for position in self.panels:
            if self.panels[position] == self.tile_ids[tile_type]:
                count += 1
        return count

    def print_grid(self):
        for y2 in range(max(self.Y), min(self.Y) - 1, -1):
            x_axis = list(range(min(self.X), max(self.X) + 1))
            line = [self.tile_ids[self.get_color_of_panel(x2, y2)] for x2 in x_axis]
            print(f"{''.join(line)}")


class RepairDroid:

    def __init__(self):
        self.brain = IntCode()
        self.x = 0
        self.y = 0
        self.positions = [(0, 0)]

    def load(self, operating_system):
        self.brain.load_program(operating_system)

    def north(self):
        self.y += 1
        self.positions.append((self.x, self.y))
        self.brain.add_input(1)

    def south(self):
        self.y -= 1
        self.positions.append((self.x, self.y))
        self.brain.add_input(2)

    def west(self):
        self.x -= 1
        self.positions.append((self.x, self.y))
        self.brain.add_input(3)

    def east(self):
        self.x += 1
        self.positions.append((self.x, self.y))
        self.brain.add_input(4)

    def move(self, direction, grid):
        self.__getattribute__(direction)()
        self.brain.run()
        output = self.brain.outputs[-1]
        if output == 0:
            grid.paint_panel(self.x, self.y, 0)
            self.x, self.y = self.positions[-2]
            self.positions.pop()
        elif output == 1:
            previous = self.positions[-2]
            grid.paint_panel(previous[0], previous[1], 3)
            grid.paint_panel(self.x, self.y, 1)
        elif output == 2:
            previous = self.positions[-2]
            grid.paint_panel(previous[0], previous[1], 3)
            grid.paint_panel(self.x, self.y, 6)
        return output

    def get_direction_string(self, position):
        options = {
            (self.x, self.y+1): "north",
            (self.x, self.y-1): "south",
            (self.x-1, self.y): "west",
            (self.x+1, self.y): "east",
        }
        return options[position]

    def get_dijkstra(self, grid, dijkstra_info=None, previous=None, point_of_interest=None, backtrack_location=None, skip_o=False, strikt=False):
        if backtrack_location is not None and previous is not None and previous == 0:
            return grid.apply_dijkstra(backtrack_location[0], backtrack_location[1])
        elif dijkstra_info is None or dijkstra_info[(self.x, self.y)] == 0 or (previous is not None and previous == 0):
            if point_of_interest is None:
                points_of_interest = grid.points_of_interests(skip_o, strikt)
                point_of_interest = random.choice(list(points_of_interest))
            return grid.apply_dijkstra(point_of_interest[0], point_of_interest[1])
        return dijkstra_info

    def get_direction(self, grid, dijkstra_info):
        options = grid.get_allowed_directions(self.x, self.y)
        lowest_score = None
        best_option = None
        for option in options:
            if option not in dijkstra_info:
                continue
            score = dijkstra_info[option]
            if lowest_score is None or score < lowest_score:
                lowest_score = score
                best_option = option

        return self.move(self.get_direction_string(best_option), grid), dijkstra_info

    @staticmethod
    def print_line():
        print("-------------------------------------------------------------------------------------------------------")

    def run(self, grid):
        grid.paint_panel(self.x, self.y, 3)
        dijkstra_info = None
        previous = None
        while grid.get_color_of_panel(self.x, self.y) != 6:
            dijkstra_info = self.get_dijkstra(grid, dijkstra_info, previous)
            previous, dijkstra_info = self.get_direction(grid, dijkstra_info)
        self.print_line()
        grid.print_grid()
        tank_location = (self.x, self.y)
        while (self.x, self.y) != (0, 0):
            dijkstra_info = self.get_dijkstra(grid, dijkstra_info, previous, (0, 0), tank_location)
            previous, dijkstra_info = self.get_direction(grid, dijkstra_info)
        self.print_line()
        grid.print_grid()
        solution_1 = grid.apply_dijkstra(0, 0)[tank_location]
        try:
            while True:
                dijkstra_info = self.get_dijkstra(grid, dijkstra_info, previous, skip_o=True, strikt=True)
                previous, dijkstra_info = self.get_direction(grid, dijkstra_info)
        except IndexError:
            pass
        self.print_line()
        grid.print_grid()
        solution_2 = max(grid.apply_dijkstra(tank_location[0], tank_location[1]).values())
        return solution_1, solution_2


def solve():
    programs = []
    with open("input.txt", "r") as file:
        for line in file:
            programs.append(str_to_program(line))
    for program in programs:
        grid = Grid()
        repair_robot = RepairDroid()
        repair_robot.load(program)
        print(f"solution: {repair_robot.run(grid)}")


def main():
    solve()


if __name__ == "__main__":
    main()
