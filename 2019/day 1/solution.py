def calculate_fuel(mass: int):
	return (mass // 3) - 2
	
def calculate_total_fuel(mass: int):
	fuel = 0
	temp_value = calculate_fuel(mass)
	while temp_value > 0:
		fuel += temp_value
		temp_value = calculate_fuel(temp_value)
	return fuel

module_fuel = 0
total_fuel = 0

with open("module_masses.txt", "r") as masses:
	for line in masses:
		module_fuel += calculate_fuel(int(line))
		total_fuel += calculate_total_fuel(int(line))
		
print(f"amount of fuel for modules: {module_fuel}")
print(f"total amount of fuel: {total_fuel}")