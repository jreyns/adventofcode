def do_sum(locations, values):
	values[locations[2]] = values[locations[0]] + values[locations[1]]
	return values


def do_mult(locations, values):
	values[locations[2]] = values[locations[0]] * values[locations[1]]
	return values


def do_op(index, values):
	op_code = values[index]
	if op_code == 1:
		return True, do_sum(values[index+1:index+4], values)
	elif op_code == 2:
		return True, do_mult(values[index+1:index+4], values)
	elif op_code == 99:
		return False, values
	else:
		raise ValueError(f"unexpected operation code: {(op_code, index, values)}")


def load_program():
	return [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,19,9,23,1,23,6,27,2,27,13,31,1,10,31,35,1,10,35,39,2,39,6,43,1,43,5,47,2,10,47,51,1,5,51,55,1,55,13,59,1,59,9,63,2,9,63,67,1,6,67,71,1,71,13,75,1,75,10,79,1,5,79,83,1,10,83,87,1,5,87,91,1,91,9,95,2,13,95,99,1,5,99,103,2,103,9,107,1,5,107,111,2,111,9,115,1,115,6,119,2,13,119,123,1,123,5,127,1,127,9,131,1,131,10,135,1,13,135,139,2,9,139,143,1,5,143,147,1,13,147,151,1,151,2,155,1,10,155,0,99,2,14,0,0]


def reload_program():
	return [1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,19,9,23,1,23,6,27,2,27,13,31,1,10,31,35,1,10,35,39,2,39,6,43,1,43,5,47,2,10,47,51,1,5,51,55,1,55,13,59,1,59,9,63,2,9,63,67,1,6,67,71,1,71,13,75,1,75,10,79,1,5,79,83,1,10,83,87,1,5,87,91,1,91,9,95,2,13,95,99,1,5,99,103,2,103,9,107,1,5,107,111,2,111,9,115,1,115,6,119,2,13,119,123,1,123,5,127,1,127,9,131,1,131,10,135,1,13,135,139,2,9,139,143,1,5,143,147,1,13,147,151,1,151,2,155,1,10,155,0,99,2,14,0,0]


def reload_program2():
	return [1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,19,9,23,1,23,6,27,2,27,13,31,1,10,31,35,1,10,35,39,2,39,6,43,1,43,5,47,2,10,47,51,1,5,51,55,1,55,13,59,1,59,9,63,2,9,63,67,1,6,67,71,1,71,13,75,1,75,10,79,1,5,79,83,1,10,83,87,1,5,87,91,1,91,9,95,2,13,95,99,1,5,99,103,2,103,9,107,1,5,107,111,2,111,9,115,1,115,6,119,2,13,119,123,1,123,5,127,1,127,9,131,1,131,10,135,1,13,135,139,2,9,139,143,1,5,143,147,1,13,147,151,1,151,2,155,1,10,155,0,99,2,14,0,0]


def run_program(program):	
	current_index = 0
	can_continue, values = True, program
	while can_continue:
		can_continue, values = do_op(current_index, values)
		current_index += 4
	return program


values_before_engine_failure = run_program(reload_program2())

print(f"values: {values_before_engine_failure}")
print(f"result = {values_before_engine_failure[0]}")

def find_noun_and_verb(solution: int):
	noun_index = 1
	verb_index = 2
	for noun in range(100):
		for verb in range(100):
			program = load_program()
			program[noun_index] = noun
			program[verb_index] = verb
			result = run_program(program)
			if result[0] == solution:
				return noun, verb

noun, verb = find_noun_and_verb(19690720)

print(f"100 * noun + verb: {100 * noun + verb}")