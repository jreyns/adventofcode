def print_representation(representation, depth=0, name="COM", indent=4):
	print(f"{' ' * depth * indent}{name} {representation[name]['count']}")
	for sub_orb in representation[name]["orbited_by"]:
		print_representation(representation, depth+1, sub_orb)


def update_counts(representation: dict, center: str, orbit: str, increase_by: int):
	if orbit not in representation:
		representation[orbit] = {"count": increase_by, "orbited_by": [], "orbits": [center]}
	else:
		if center not in representation[orbit]["orbits"]:
			representation[orbit]["orbits"].append(center)
		representation[orbit]["count"] += increase_by
		for indirect_orbit in representation[orbit]["orbited_by"]:
			update_counts(representation, orbit, indirect_orbit, increase_by)


def build_representation(orbit_map: list):
	representation = {}
	for line in orbit_map:
		center, orbit = line.split(")")
		if center not in representation:
			representation[center] = {"count": 0, "orbited_by": [orbit], "orbits": []}
		else:
			representation[center]["orbited_by"].append(orbit)
		update_counts(representation, center, orbit, representation[center]["count"] + 1)
	return representation


def count_direct_and_indirect_orbits(orbit_map: list):
	representation = build_representation(orbit_map)
	# print_representation(representation)
	count = 0
	for obj in representation:
		count += representation[obj]["count"]
	return count


def count_orbital_transfers(representation, orbit, destination="SAN", orbital_transfers=0, passed=[]):
	if destination in representation[orbit]["orbited_by"]:
		return orbital_transfers
	else:
		passed.append(orbit)
		options = [len(representation)]
		for next_orbit in representation[orbit]["orbited_by"] + representation[orbit]["orbits"]:
			if next_orbit not in passed:
				options.append(count_orbital_transfers(representation, next_orbit, destination, orbital_transfers+1, passed))
		return min(options)


def count_orbital_hops(orbit_map: list, start="YOU", destination="SAN"):
	representation = build_representation(orbit_map)
	# print_representation(representation)
	counts = []
	for obj in representation[start]["orbits"]:
		counts.append(count_orbital_transfers(representation, obj, destination))
	return min(counts)


def cases():
	tests = [
		{
			"map": "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L",
			"solution": 42,
			"context": "part 1"
		},
		{
			"map": "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN",
			"solution": 4,
			"context": "part 2"
		}
	]

	for i, test in enumerate(tests):
		orbit_map = test["map"].split("\n")
		solution = test["solution"]
		context = test["context"]
		if context == "part 1":
			print(f"test {i}.0: {solution} {count_direct_and_indirect_orbits(orbit_map)}")
		elif context == "part 2":
			print(f"test {i}.0: {solution} {count_orbital_hops(orbit_map)}")


def solve_part1():
	orbit_map = []
	with open("input.txt", "r") as file:
		for line in file:
			orbit_map.append(line.replace("\n", ""))
	print(f"solution part 1: {count_direct_and_indirect_orbits(orbit_map)}")


def solve_part2():
	orbit_map = []
	with open("input2.txt", "r") as file:
		for line in file:
			orbit_map.append(line.replace("\n", ""))
	print(f"solution part 1: {count_orbital_hops(orbit_map)}")


def main():
	cases()
	solve_part1()
	solve_part2()


if __name__ == "__main__":
	main()