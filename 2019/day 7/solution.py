import itertools


class IntCode:

	def __init__(self):
		self.inputs = []
		self.outputs = []
		self.memory = []
		self.fp = 0
		self.ip = 0

	def load_program(self, program):
		self.memory = program

	def load_input(self, inp):
		self.inputs = inp

	def add_input(self, inp):
		self.inputs += inp

	def reset(self):
		self.fp = 0

	def get_parameter(self, index, mode=0):
		if mode == 0:
			return self.memory[self.memory[index]]
		elif mode == 1:
			return self.memory[index]

	@staticmethod
	def parse_op_code(code: int):
		code_str = str(code)
		while len(code_str) < 5:
			code_str = "0" + code_str
		tokens = [int(c) for c in code_str]
		op_code = tokens[-2] * 10 + tokens[-1]
		modes = tokens[:-2]
		modes.reverse()
		return op_code, modes

	def do_sum(self, modes):
		self.memory[self.memory[self.fp+3]] = \
			self.get_parameter(self.fp+1, modes[0]) + self.get_parameter(self.fp+2, modes[1])
		self.fp += 4
		return True

	def do_mult(self, modes):
		self.memory[self.memory[self.fp+3]] = \
			self.get_parameter(self.fp+1, modes[0]) * self.get_parameter(self.fp+2, modes[1])
		self.fp += 4
		return True

	def do_store(self):
		if self.ip >= len(self.inputs):
			return False
		self.memory[self.memory[self.fp+1]] = self.inputs[self.ip]
		self.ip += 1
		self.fp += 2
		return True

	def do_output(self, modes):
		self.outputs.append(self.get_parameter(self.fp+1, modes[0]))
		self.fp += 2
		return True

	def do_jump_if_true(self, modes):
		if self.get_parameter(self.fp+1, modes[0]) > 0:
			self.fp = self.get_parameter(self.fp+2, modes[1])
		else:
			self.fp += 3
		return True

	def do_jump_if_false(self, modes):
		if self.get_parameter(self.fp+1, modes[0]) == 0:
			self.fp = self.get_parameter(self.fp+2, modes[1])
		else:
			self.fp += 3
		return True

	def do_less_than(self, modes):
		self.memory[self.memory[self.fp+3]] = \
			int(self.get_parameter(self.fp+1, modes[0]) < self.get_parameter(self.fp+2, modes[1]))
		self.fp += 4
		return True

	def do_equals(self, modes):
		self.memory[self.memory[self.fp+3]] = \
			int(self.get_parameter(self.fp+1, modes[0]) == self.get_parameter(self.fp+2, modes[1]))
		self.fp += 4
		return True

	def do_op(self):
		op_code, modes = self.parse_op_code(self.memory[self.fp])
		if op_code == 1:
			return self.do_sum(modes)
		elif op_code == 2:
			return self.do_mult(modes)
		elif op_code == 3:
			return self.do_store()
		elif op_code == 4:
			return self.do_output(modes)
		elif op_code == 5:
			return self.do_jump_if_true(modes)
		elif op_code == 6:
			return self.do_jump_if_false(modes)
		elif op_code == 7:
			return self.do_less_than(modes)
		elif op_code == 8:
			return self.do_equals(modes)
		elif op_code == 99:
			return False
		else:
			raise ValueError(f"unexpected operation code: {(op_code, self.memory)}")

	def run(self):
		can_continue = True
		while can_continue:
			can_continue = self.do_op()


class Amplifier:

	def __init__(self, program):
		self.int_code = IntCode()
		self.int_code.load_program(program)

	def load_input(self, inp):
		self.int_code.load_input(inp)

	def add_input(self, inp):
		self.int_code.add_input(inp)

	def run(self):
		self.int_code.run()
		return self.int_code.outputs[-1]


def maximize_thruster_output(program, amplifier_count: int = 5):
	phases = list(range(amplifier_count))
	max_output = 0
	phase_sequence = []
	for permutation in itertools.permutations(phases):
		amplifier_input = 0
		for phase in permutation:
			amplifier = Amplifier(program)
			amplifier.load_input([phase])
			amplifier.add_input([amplifier_input])
			amplifier_input = amplifier.run()
		if amplifier_input > max_output:
			max_output = amplifier_input
			phase_sequence = permutation

	return max_output, phase_sequence


def maximize_thruster_output_with_feed_back(program, amplifier_count: int = 5):
	phases = list(range(5, 5+amplifier_count))
	max_output = 0
	phase_sequence = []
	for permutation in itertools.permutations(phases):
		amplifiers = [Amplifier(program.copy()) for _ in range(amplifier_count)]
		last_amplifier_input = 0
		amplifier_input = 0
		for i, phase in enumerate(permutation):
			amplifiers[i].load_input([phase, amplifier_input])
			amplifier_input = amplifiers[i].run()
		if amplifier_input > max_output:
			max_output = amplifier_input
			phase_sequence = permutation
		while last_amplifier_input != amplifier_input:
			last_amplifier_input = amplifier_input
			for i, phase in enumerate(permutation):
				amplifiers[i].add_input([amplifier_input])
				amplifier_input = amplifiers[i].run()
			if amplifier_input > max_output:
				max_output = amplifier_input
				phase_sequence = permutation
	return max_output, phase_sequence


def str_to_program(text: str):
	return [int(v) for v in text.split(",")]


def cases():
	tests = [
		{
			"program": "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0",
			"context": "part 1",
			"solution": [43210, [4, 3, 2, 1, 0]]
		},
		{
			"program": "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0",
			"context": "part 1",
			"solution": [54321, [0, 1, 2, 3, 4]]
		},
		{
			"program": "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0",
			"context": "part 1",
			"solution": [65210, [1, 0, 4, 3, 2]]
		},
		{
			"program": "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5",
			"context": "part 2",
			"solution": [139629729, [9, 8, 7, 6, 5]]
		},
		{
			"program": "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10",
			"context": "part 2",
			"solution": [18216, [9, 7, 8, 5, 6]]
		}
	]

	for i, test in enumerate(tests):
		program = str_to_program(test["program"])
		context = test["context"]
		solution = test["solution"]
		if context == "part 1":
			print(f"test {i}.0: {solution} {maximize_thruster_output(program)}")
		elif context == "part 2":
			print(f"test {i}.0: {solution} {maximize_thruster_output_with_feed_back(program)}")


def solve_part1():
	programs = []
	with open("input.txt", "r") as file:
		for line in file:
			programs.append(str_to_program(line))
	for program in programs:
		print(f"solution part 1: {maximize_thruster_output(program)}")


def solve_part2():
	programs = []
	with open("input2.txt", "r") as file:
		for line in file:
			programs.append(str_to_program(line))
	for program in programs:
		print(f"solution part 2: {maximize_thruster_output_with_feed_back(program)}")


def main():
	cases()
	solve_part1()
	solve_part2()


if __name__ == "__main__":
	main()