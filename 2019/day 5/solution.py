inputs = []
outputs = []


class REG:
	fp = 0
	ip = 0

	@staticmethod
	def reset():
		REG.fp = 0
		REG.ip = 0
		global outputs
		outputs = []


def get_parameter(values, index, mode=0):
	if mode == 0:
		return values[values[index]]
	elif mode == 1:
		return values[index]


def parse_op_code(code: int):
	code_str = str(code)
	while len(code_str) < 5:
		code_str = "0" + code_str
	tokens = [int(c) for c in code_str]
	op_code = tokens[-2] * 10 + tokens[-1]
	modes = tokens[:-2]
	modes.reverse()
	return op_code, modes


def do_sum(values, modes):
	values[values[REG.fp+3]] = get_parameter(values, REG.fp+1, modes[0]) + get_parameter(values, REG.fp+2, modes[1])
	REG.fp += 4


def do_mult(values, modes):
	values[values[REG.fp+3]] = get_parameter(values, REG.fp+1, modes[0]) * get_parameter(values, REG.fp+2, modes[1])
	REG.fp += 4


def do_store(values):
	values[values[REG.fp+1]] = inputs[REG.ip]
	REG.ip += 1
	REG.fp += 2


def do_output(values, modes):
	outputs.append(get_parameter(values, REG.fp+1, modes[0]))
	REG.fp += 2


def do_jump_if_true(values, modes):
	if get_parameter(values, REG.fp+1, modes[0]) > 0:
		REG.fp = get_parameter(values, REG.fp+2, modes[1])
	else:
		REG.fp += 3


def do_jump_if_false(values, modes):
	if get_parameter(values, REG.fp+1, modes[0]) == 0:
		REG.fp = get_parameter(values, REG.fp+2, modes[1])
	else:
		REG.fp += 3


def do_less_than(values, modes):
	values[values[REG.fp+3]] = int(get_parameter(values, REG.fp+1, modes[0]) < get_parameter(values, REG.fp+2, modes[1]))
	REG.fp += 4


def do_equals(values, modes):
	values[values[REG.fp+3]] = int(get_parameter(values, REG.fp+1, modes[0]) == get_parameter(values, REG.fp+2, modes[1]))
	REG.fp += 4


def do_op(values):
	op_code, modes = parse_op_code(values[REG.fp])
	if op_code == 1:
		do_sum(values, modes)
		return True
	elif op_code == 2:
		do_mult(values, modes)
		return True
	elif op_code == 3:
		do_store(values)
		return True
	elif op_code == 4:
		do_output(values, modes)
		return True
	elif op_code == 5:
		do_jump_if_true(values, modes)
		return True
	elif op_code == 6:
		do_jump_if_false(values, modes)
		return True
	elif op_code == 7:
		do_less_than(values, modes)
		return True
	elif op_code == 8:
		do_equals(values, modes)
		return True
	elif op_code == 99:
		return False
	else:
		raise ValueError(f"unexpected operation code: {(op_code, values)}")


def run_program(program):
	REG.reset()
	can_continue = True
	while can_continue:
		can_continue = do_op(program)
	return program


def str_to_program(text: str):
	return [int(v) for v in text.split(",")]


def cases():
	tests = [
		{
			"program": "1,0,0,0,99",
			"location": "program",
			"index": 0,
			"solution": 2
		},
		{
			"program": "2,3,0,3,99",
			"location": "program",
			"index": 3,
			"solution": 6
		},
		{
			"program": "2,4,4,5,99,0",
			"location": "program",
			"index": 5,
			"solution": 9801
		},
		{
			"program": "1,1,1,4,99,5,6,0,99",
			"location": "program",
			"index": 0,
			"solution": 30
		},
		{
			"program": "3,9,8,9,10,9,4,9,99,-1,8",
			"inputs": [[8], [7]],
			"location": "output",
			"index": -1,
			"solution": [1, 0]
		},
		{
			"program": "3,9,7,9,10,9,4,9,99,-1,8",
			"inputs": [[8], [7]],
			"location": "output",
			"index": -1,
			"solution": [0, 1]
		},
		{
			"program": "3,3,1108,-1,8,3,4,3,99",
			"inputs": [[8], [7]],
			"location": "output",
			"index": -1,
			"solution": [1, 0]
		},
		{
			"program": "3,3,1107,-1,8,3,4,3,99",
			"inputs": [[8], [7]],
			"location": "output",
			"index": -1,
			"solution": [0, 1]
		},
		{
			"program": "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9",
			"inputs": [[0], [200]],
			"location": "output",
			"index": -1,
			"solution": [0, 1]
		},
		{
			"program": "3,3,1105,-1,9,1101,0,0,12,4,12,99,1",
			"inputs": [[0], [200]],
			"location": "output",
			"index": -1,
			"solution": [0, 1]
		},
		{
			"program": "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
			"inputs": [[7], [8], [9]],
			"location": "output",
			"index": -1,
			"solution": [999, 1000, 1001]
		}
	]

	for i, test in enumerate(tests):
		program = str_to_program(test["program"])
		location = test["location"]
		solution = test["solution"]
		index = test["index"]
		if location == "program":
			print(f"test {i}.0: {solution} {run_program(program)[index]}")
		elif location == "output":
			program_outputs = []
			local_inputs = test["inputs"]
			for scenario in local_inputs:
				global inputs
				inputs = scenario
				run_program(program.copy())
				program_outputs.append(outputs[index])
			print(f"test {i}.0: {solution} {program_outputs}")


def solve_part1():
	global inputs
	inputs = [1]
	programs = []
	with open("input.txt", "r") as file:
		for line in file:
			programs.append(str_to_program(line))
	for program in programs:
		run_program(program)
		print(outputs)


def solve_part2():
	global inputs
	inputs = [5]
	programs = []
	with open("input.txt", "r") as file:
		for line in file:
			programs.append(str_to_program(line))
	for program in programs:
		run_program(program)
		print(outputs)


def main():
	cases()
	solve_part1()
	solve_part2()


if __name__ == "__main__":
	main()