import numpy as np
from sympy import lcm


class Moon:

    def __init__(self, name, position):
        self.name = name
        self.dimensions = len(position)
        self.position = np.array(position)
        self.velocity = np.zeros(self.dimensions, dtype="int32")

    def apply_gravity(self, other):
        for dim in range(self.dimensions):
            if self.position[dim] > other.position[dim]:
                self.velocity[dim] -= 1
            elif self.position[dim] < other.position[dim]:
                self.velocity[dim] += 1

    def apply_velocity(self):
        self.position += self.velocity

    def print_status(self):
        print(f"pos=<x={self.position[0]}, y={self.position[1]}, z={self.position[2]}>, "
              f"vel=<x={self.velocity[0]}, y={self.velocity[1]}, z={self.velocity[2]}>, "
              f"pot={self.potential_energy()}, kin={self.kinetic_energy()}, tot={self.total_energy()}, "
              f"name={self.name}")

    def potential_energy(self):
        return np.sum(np.abs(self.position))

    def kinetic_energy(self):
        return np.sum(np.abs(self.velocity))

    def total_energy(self):
        return self.potential_energy() * self.kinetic_energy()


def do_step(moons: list):
    # Apply gravity
    for moon1 in moons:
        for moon2 in moons:
            if moon1 != moon2:
                moon1.apply_gravity(moon2)
    # Apply velocity
    for moon in moons:
        moon.apply_velocity()
    return moons


def parse_coordinate(coordinate_str: str):
    dimensions = coordinate_str.replace("<", "").replace(">", "").split(", ")
    coordinates = []
    for dim in dimensions:
        _, value = dim.split("=")
        coordinates.append(int(value))
    return coordinates


def get_moons(start_positions):
    moon_names = ["Io", "Europa", "Ganymede", "Callisto"]
    moons = []
    for moon_name, start_position in zip(moon_names, start_positions):
        position = parse_coordinate(start_position)
        moons.append(Moon(moon_name, position))
    return moons


def str_to_start_positions(string):
    return string.split("\n")


def get_per_axis_states(moons):
    if len(moons) == 0:
        return None
    dimensions = moons[0].dimensions
    state = []
    for dim in range(dimensions):
        state.append(tuple([(moon.position[dim], moon.velocity[dim]) for moon in moons]))
    return state


def test_cycled(state, states, hasnt_cycled):
    for dim in range(len(state)):
        if not hasnt_cycled[dim]:
            continue
        elif state[dim] in states[dim]:
            hasnt_cycled[dim] = False
        else:
            states[dim].add(state[dim])
    return hasnt_cycled, states


def calculate_cycle_time(moons):
    state = get_per_axis_states(moons)
    states = [set([state_along_axis]) for state_along_axis in state]
    hasnt_cycled = [True for _ in state]
    while True in hasnt_cycled:
        moons = do_step(moons)
        state = get_per_axis_states(moons)
        hasnt_cycled, states = test_cycled(state, states, hasnt_cycled)
    return lcm([len(states_along_axis) for states_along_axis in states])


def cases():
    tests = [
        {
            "program": "<x=-1, y=0, z=2>\n<x=2, y=-10, z=-7>\n<x=4, y=-8, z=8>\n<x=3, y=5, z=-1>",
            "context": "part 1",
            "solution": ""
        },
        {
            "program": "<x=-1, y=0, z=2>\n<x=2, y=-10, z=-7>\n<x=4, y=-8, z=8>\n<x=3, y=5, z=-1>",
            "context": "part 2",
            "solution": "2772"
        }
    ]

    for i, test in enumerate(tests):
        positions = str_to_start_positions(test["program"])
        moons = get_moons(positions)
        context = test["context"]
        solution = test["solution"]
        if context == "part 1":
            for j in range(11):
                print(f"after {j} steps:")
                for moon in moons:
                    moon.print_status()
                moons = do_step(moons)
        elif context == "part 2":
            print(f"test {i}.0: {solution} {calculate_cycle_time(moons)}")


def solve_part1():
    positions = []
    with open("input.txt", "r") as file:
        for line in file:
            positions.append(line)
    moons = get_moons(positions)
    for i in range(1000):
        moons = do_step(moons)
    print(f"solution 1: {sum([m.total_energy() for m in moons])}")


def solve_part2():
    positions = []
    with open("input.txt", "r") as file:
        for line in file:
            positions.append(line)
    moons = get_moons(positions)
    print(f"solution 2: {calculate_cycle_time(moons)}")


def main():
    cases()
    solve_part1()
    solve_part2()


if __name__ == "__main__":
    main()
