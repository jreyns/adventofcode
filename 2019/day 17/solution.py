import random


class IntCode:

    def __init__(self):
        self.inputs = []
        self.outputs = []
        self.memory = []
        self.fp = 0
        self.ip = 0
        self.rb = 0

    def load_program(self, program):
        self.memory = program

    def load_input(self, inp):
        self.inputs = inp

    def add_input(self, inp):
        if type(inp) is not list:
            inp = [inp]
        self.inputs += inp

    def reset(self):
        self.fp = 0

    def get_parameter(self, index, mode=0):
        if mode == 0:
            self.mem_alloc(self.memory[index])
            return self.memory[self.memory[index]]
        elif mode == 1:
            self.mem_alloc(index)
            return self.memory[index]
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            return self.memory[self.rb + offset]

    def write_value(self, index, value, mode=0):
        if mode == 0:
            self.write_to_memory(self.memory[index], value)
        elif mode == 2:
            offset = self.memory[index]
            self.mem_alloc(self.rb + offset)
            self.write_to_memory(self.rb + offset, value)

    @staticmethod
    def parse_op_code(code: int):
        code_str = str(code)
        while len(code_str) < 5:
            code_str = "0" + code_str
        tokens = [int(c) for c in code_str]
        op_code = tokens[-2] * 10 + tokens[-1]
        modes = tokens[:-2]
        modes.reverse()
        return op_code, modes

    def mem_alloc(self, mem_loc):
        memory_diff = mem_loc - len(self.memory) + 1
        if memory_diff > 0:
            self.memory += [0] * memory_diff

    def write_to_memory(self, mem_loc, value):
        self.mem_alloc(mem_loc)
        self.memory[mem_loc] = value

    def do_sum(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) + self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_mult(self, modes):
        self.write_value(
            self.fp + 3,
            self.get_parameter(self.fp + 1, modes[0]) * self.get_parameter(self.fp + 2, modes[1]),
            modes[2]
        )
        self.fp += 4
        return True

    def do_store(self, modes):
        if self.ip >= len(self.inputs):
            return False
        self.write_value(self.fp + 1, self.inputs[self.ip], modes[0])
        # self.write_to_memory(self.get_parameter(self.fp + 1, modes[0]), self.inputs[self.ip])
        self.ip += 1
        self.fp += 2
        return True

    def do_output(self, modes):
        self.outputs.append(self.get_parameter(self.fp + 1, modes[0]))
        self.fp += 2
        return True

    def do_jump_if_true(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) > 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_jump_if_false(self, modes):
        if self.get_parameter(self.fp + 1, modes[0]) == 0:
            self.fp = self.get_parameter(self.fp + 2, modes[1])
        else:
            self.fp += 3
        return True

    def do_less_than(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) < self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_equals(self, modes):
        self.write_value(
            self.fp + 3,
            int(self.get_parameter(self.fp + 1, modes[0]) == self.get_parameter(self.fp + 2, modes[1])),
            modes[2]
        )
        self.fp += 4
        return True

    def do_adjust_relative_base(self, modes):
        self.rb += self.get_parameter(self.fp + 1, modes[0])
        self.fp += 2
        return True

    def do_op(self):
        op_code, modes = self.parse_op_code(self.memory[self.fp])
        if op_code == 1:
            return self.do_sum(modes)
        elif op_code == 2:
            return self.do_mult(modes)
        elif op_code == 3:
            return self.do_store(modes)
        elif op_code == 4:
            return self.do_output(modes)
        elif op_code == 5:
            return self.do_jump_if_true(modes)
        elif op_code == 6:
            return self.do_jump_if_false(modes)
        elif op_code == 7:
            return self.do_less_than(modes)
        elif op_code == 8:
            return self.do_equals(modes)
        elif op_code == 9:
            return self.do_adjust_relative_base(modes)
        elif op_code == 99:
            return False
        else:
            raise ValueError(f"unexpected operation code: {(op_code, self.memory)}")

    def run(self):
        can_continue = True
        while can_continue:
            can_continue = self.do_op()


def str_to_program(text: str):
    return [int(v) for v in text.split(",")]


class Grid:

    def __init__(self):
        self.X = []
        self.Y = []
        self.panels = {}
        self.tile = "#"
        self.robot_positions = ["^", "v", "<", ">"]

    def paint_panel(self, x, y, tile_type):
        self.panels[(x, y)] = chr(tile_type)
        if x not in self.X:
            self.X.append(x)
        if y not in self.Y:
            self.Y.append(y)

    def paint_panels(self, output):
        x = 0
        y = 0
        for code in output:
            if code == 10:
                x = 0
                y += 1
            else:
                self.paint_panel(x, y, code)
                x += 1

    def is_intersection(self, x, y):
        if (x, y) not in self.panels or self.panels[(x, y)] not in [self.tile] + self.robot_positions:
            return False
        neighbours = [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]
        for location in neighbours:
            if location not in self.panels or self.panels[location] not in [self.tile] + self.robot_positions:
                return False
        return True

    @staticmethod
    def get_intersection_value(point):
        return point[0] * point[1]

    def get_calibration_value(self):
        intersections = []
        for y in range(min(self.Y), max(self.Y) + 1):
            for x in range(min(self.X), max(self.X) + 1):
                if self.is_intersection(x, y):
                    intersections.append([x, y])
        values = [self.get_intersection_value(p) for p in intersections]
        return sum(values)

    def get_color_of_panel(self, x, y):
        return self.panels.get((x, y), " ")

    def count_tile_type(self, tile_type):
        count = 0
        for position in self.panels:
            if self.panels[position] == self.tile_ids[tile_type]:
                count += 1
        return count

    def print_grid(self):
        for y2 in range(min(self.Y), max(self.Y) + 1, 1):
            x_axis = list(range(min(self.X), max(self.X) + 1))
            line = [self.get_color_of_panel(x2, y2) for x2 in x_axis]
            print(f"{''.join(line)}")


class VacuumDroid:

    def __init__(self):
        self.brain = IntCode()
        self.x = 0
        self.y = 0
        self.positions = []

    def load(self, operating_system):
        self.brain.load_program(operating_system)

    def wake_up_and_configure(self):
        self.brain.memory[0] = 2
        self.brain.add_input(self.main_routine())
        self.brain.add_input(self.A_routine())
        self.brain.add_input(self.B_routine())
        self.brain.add_input(self.C_routine())
        self.brain.add_input(self.continues_feed())

    def run(self, grid):
        self.brain.run()
        grid.paint_panels(self.brain.outputs)
        grid.print_grid()

    @staticmethod
    def main_routine():
        routine = "A,A,B,C,B,C,B,C,B,A\n"
        return [ord(c) for c in routine]

    @staticmethod
    def A_routine():
        routine = "R,6,L,9,3,R,6\n"
        return [ord(c) for c in routine]

    @staticmethod
    def B_routine():
        routine = "L,9,3,R,6,L,8,L,9,3\n"
        return [ord(c) for c in routine]

    @staticmethod
    def C_routine():
        routine = "R,9,3,L,9,1,L,9,1\n"
        return [ord(c) for c in routine]

    @staticmethod
    def continues_feed():
        routine = "n\n"
        return [ord(c) for c in routine]


def solve_part1():
    programs = []
    with open("input.txt", "r") as file:
        for line in file:
            programs.append(str_to_program(line))
    for program in programs:
        grid = Grid()
        repair_robot = VacuumDroid()
        repair_robot.load(program)
        repair_robot.run(grid)
        print(f"solution 1: {grid.get_calibration_value()}")


def solve_part2():
    programs = []
    with open("input2.txt", "r") as file:
        for line in file:
            programs.append(str_to_program(line))
    for program in programs:
        grid = Grid()
        repair_robot = VacuumDroid()
        repair_robot.load(program)
        repair_robot.wake_up_and_configure()
        repair_robot.brain.run()
        print(f"solution 2: {repair_robot.brain.outputs[-1]}")


def main():
    solve_part1()
    solve_part2()


if __name__ == "__main__":
    main()
