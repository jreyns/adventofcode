import numpy as np


def decode_image(img, width, height):
	return np.array(img).reshape((-1, height, width))


def count_digit_in_layer(layer):
	return np.unique(layer, return_counts=True)


def get_zero_count_and_solution(layer):
	stats = count_digit_in_layer(layer)
	zero_count = stats[1][0]
	solution = stats[1][1] * stats[1][2]
	return zero_count, solution


def parse_color(value):
	if value == 0:
		return " "
	if value == 1:
		return "X"
	return None


def print_image(img, width, height):
	printable = [[" " for _ in range(width)] for _ in range(height)]
	layer_count = img.shape[0]
	string_rep = ""
	while layer_count > 0:
		layer = img[layer_count-1]
		for i, row in enumerate(layer):
			for j, value in enumerate(row):
				color = parse_color(value)
				if color is not None:
					printable[i][j] = color
		layer_count -= 1
	for row in printable:
		for value in row:
			string_rep += value
		string_rep += "\n"
	return string_rep


def cases():
	tests = [
		{
			"image": [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2],
			"width": 3,
			"height": 2,
			"context": "part 1",
			"solution": [[[1, 2, 3], [4, 5, 6]], [[7, 8, 9], [0, 1, 2]]]
		},
		{
			"image": [0, 2, 2, 2, 1, 1, 2, 2, 2, 2, 1, 2, 0, 0, 0, 0],
			"width": 2,
			"height": 2,
			"context": "part 2",
			"solution": [[0, 1], [1, 0]]
		}
	]

	for i, test in enumerate(tests):
		img = test["image"]
		width = test["width"]
		height = test["height"]
		context = test["context"]
		solution = test["solution"]
		if context == "part 1":
			print(f"test {i}.0: {solution}\n{decode_image(img, width, height)}")
		elif context == "part 2":
			print(f"test {i}.0: {solution}\n{print_image(decode_image(img, width, height), width, height)}")


def solve_part1():
	imgs = []
	with open("input.txt", "r") as file:
		for line in file:
			imgs.append([int(c) for c in line if c.isdigit()])
	for img in imgs:
		decoded = decode_image(img, 25, 6)
		minimal_count = None
		solution = 0
		for layer in decoded:
			zero_count, sol = get_zero_count_and_solution(layer)
			if minimal_count is None or minimal_count > zero_count:
				minimal_count = zero_count
				solution = sol
		print(f"solution part 1: {solution}")


def solve_part2():
	imgs = []
	with open("input.txt", "r") as file:
		for line in file:
			imgs.append([int(c) for c in line if c.isdigit()])
	for img in imgs:
		decoded = decode_image(img, 25, 6)
		minimal_count = None
		str_rep = print_image(decoded, 25, 6)
		print(f"solution part 2: \n{str_rep}")


def main():
	cases()
	solve_part1()
	solve_part2()


if __name__ == "__main__":
	main()