def generate_pattern_coeff(index, i, base_pattern):
    return base_pattern[((i + 1) % (len(base_pattern) * (index + 1))) // (index + 1)]


def apply_pattern(fft_input, index, base_pattern, repeat_time=None):
    output = 0
    if repeat_time is None or repeat_time < len(base_pattern) * (index + 1):
        needed_steps = len(fft_input)
    else:
        needed_steps = int(len(fft_input) / repeat_time)
    for i in range(needed_steps):
        pattern_coeff = generate_pattern_coeff(index, i, base_pattern)
        output += fft_input[i] * pattern_coeff
    if repeat_time is not None:
        output *= repeat_time

    return abs(output) % 10


def FFT(fft_input, base_pattern, repeat_time=None):
    output = []
    input_length = len(fft_input)
    for i in range(input_length):
        output_number = apply_pattern(fft_input, i, base_pattern, repeat_time)
        output.append(output_number)
    return output


def do_phases(phases, fft_input, base_pattern=[0, 1, 0, -1], repeat_time=None, print_phase=False):
    output = fft_input
    for i in range(phases):
        output = FFT(output, base_pattern, repeat_time)
        if print_phase:
            print(f"reached phase {i+1}")
    return output


def output_to_number(output):
    return "".join([str(i) for i in output])


def str_to_fft_input(input_str):
    return [int(c) for c in input_str]


def do_phases_if_offset_in_last_half(phases, fft_input, offset):
    """
        Last half of output is always an upper triangular matrix with ones
        which implies that if our numbers of interest have higher index than len(fft_input) // 2
        we can calculate the numbers of interest for each phase:
            output[-1] = input[-1] (always < 10, so no need for the % 10)
            output[-2] = (input[-2] + output[-1]) % 10
    :param phases:
    :param fft_input:
    :param offset:
    :return:
    """
    length = len(fft_input)
    for _ in range(phases):
        output = [0] * length
        output[length - 1] = fft_input[length - 1]
        for i in range(length - 2, offset - 1, -1):
            value = fft_input[i]
            result = value + output[i + 1]
            output[i] = result % 10
        fft_input = output
    return fft_input


def cases():
    tests = [
        {
            "program": "12345678",
            "phases": 1,
            "context": "part 1",
            "solution": "48226158"
        },
        {
            "program": "12345678",
            "phases": 2,
            "context": "part 1",
            "solution": "34040438"
        },
        {
            "program": "12345678",
            "phases": 3,
            "context": "part 1",
            "solution": "03415518"
        },
        {
            "program": "12345678",
            "phases": 4,
            "context": "part 1",
            "solution": "01029498"
        },
        {
            "program": "80871224585914546619083218645595",
            "phases": 100,
            "context": "part 1",
            "solution": "24176176"
        },
        {
            "program": "19617804207202209144916044189917",
            "phases": 100,
            "context": "part 1",
            "solution": "73745418"
        },
        {
            "program": "69317163492948606335995924319873",
            "phases": 100,
            "context": "part 1",
            "solution": "52432133"
        },
        {
            "program": "03036732577212944063491565474664",
            "phases": 100,
            "context": "part 2",
            "solution": "84462026"
        }
    ]

    for i, test in enumerate(tests):
        fft_input = str_to_fft_input(test["program"])
        phases = test["phases"]
        context = test["context"]
        solution = test["solution"]
        if context == "part 1":
            print(f"test {i}.0: {solution} {output_to_number(do_phases(phases, fft_input))[:8]}")
        elif context == "part 2":
            fft_input = fft_input * 10000
            offset = int(test["program"][:7])
            print(f"test {i}.0: {solution} {output_to_number(do_phases_if_offset_in_last_half(phases, fft_input, offset))[offset:offset+8]}")


def solve_part1():
    fft_inputs = []
    with open("input.txt", "r") as file:
        for line in file:
            fft_inputs.append(line)
    for fft_input_str in fft_inputs:
        fft_input = str_to_fft_input(fft_input_str)
        phases = 100
        print(f"solution 1: {output_to_number(do_phases(phases, fft_input))[:8]}")


def solve_part2():
    fft_inputs = []
    with open("input2.txt", "r") as file:
        for line in file:
            fft_inputs.append(line)
    for fft_input_str in fft_inputs:
        fft_input = str_to_fft_input(fft_input_str) * 10000
        offset = int(fft_input_str[:7])
        phases = 100
        print(f"solution 2: {output_to_number(do_phases_if_offset_in_last_half(phases, fft_input, offset))[offset:offset+8]} "
              f"{len(fft_input), offset}")


def main():
    cases()
    solve_part1()
    solve_part2()


if __name__ == "__main__":
    main()
