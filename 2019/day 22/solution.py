import re
from util.util import get_input


puzzle_input = get_input()
test_input = get_input("test.txt")


def process_input(lines=puzzle_input):
    processed_input = []
    for line in lines:
        processed_input.append(re.findall(r"(cut -?\d+)|(deal into new stack)|deal with increment (\d+)", line)[0])
    return processed_input


def flatten(cut, new_stack, increment):
    if cut:
        return lambda c, length, n: (c + length - n) % length, int(cut.split(" ")[-1])

    if new_stack:
        return lambda c, length, n: length - c - 1, None

    return lambda c, length, n: (c * n % length), int(increment)


def f(a, b, length, cut, new_stack, increment):
    if cut:
        n = int(cut.split(" ")[-1])
        return a, (b + n) % length

    if new_stack:
        return -a, length - b - 1

    n = int(increment)
    z = pow(n, length - 2, length)

    return a * z % length, b * z % length


def poly_pow(a, b, m, n):
    if m == 0:
        return 1, 0

    if m % 2 == 0:
        return poly_pow(a * a % n, (a * b + b) % n, m//2, n)

    else:
        c, d = poly_pow(a, b, m-1, n)
        return a * c % n, (a * d + b) % n


def solution1(problem=puzzle_input, count=10007):
    processed = process_input(problem)
    card = 2019
    for op, n in [flatten(*process) for process in processed]:
        card = op(card, count, n)

    return card
    

def solution2(problem=puzzle_input):
    processed = process_input(problem)
    card = 2020
    length = 119315717514047
    shuffles = 101741582076661
    a, b = 1, 0
    for process in reversed(processed):
        a, b = f(a, b, length, *process)
    a, b = poly_pow(a, b, shuffles, length)

    return (card * a + b) % length

    
print(f"solution 1: {solution1()}")
print(f"solution 2: {solution2()}")
