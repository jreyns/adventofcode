

def is_valid_password(password: str, lower_bound: str=None, upper_bound: str=None):
    if not password.isdigit():
        return False
    if lower_bound is not None and password < lower_bound:
        return False
    if upper_bound is not None and password > upper_bound:
        return False
    if len(password) != 6:
        return False
    if len(set(password)) == len(password):
        return False
    for i in range(len(password) - 1):
        if password[i+1] < password[i]:
            return False

    return True


def is_valid_restricted_password(password: str, lower_bound: str=None, upper_bound: str=None):
    if not password.isdigit():
        return False
    if lower_bound is not None and password < lower_bound:
        return False
    if upper_bound is not None and password > upper_bound:
        return False
    if len(password) != 6:
        return False
    if len(set(password)) == len(password):
        return False

    char_dict = {str(c): 0 for c in range(10)}
    char_dict[password[-1]] += 1
    for i in range(len(password) - 1):
        char_dict[password[i]] += 1
        if password[i+1] < password[i]:
            return False

    for c in char_dict:
        if char_dict[c] == 2:
            return True

    return False


def cases():
    tests = [
        {
            "password": "111111",
            "valid1": True,
            "valid2": False
        },
        {
            "password": "223450",
            "valid1": False,
            "valid2": False
        },
        {
            "password": "123789",
            "valid1": False,
            "valid2": False
        },
        {
            "password": "112233",
            "valid1": True,
            "valid2": True
        },
        {
            "password": "123444",
            "valid2": False,
            "valid1": True
        },
        {
            "password": "111122",
            "valid2": True,
            "valid1": True
        }
    ]

    for i, test in enumerate(tests):
        password = test["password"]
        valid1 = test["valid1"]
        valid2 = test["valid2"]
        print(f"test {i}.0: {valid1} {is_valid_password(password)}")
        print(f"test {i}.1: {valid2} {is_valid_restricted_password(password)}")


def solve_part1():
    ranges = []
    with open("input.txt", "r") as file:
        for line in file:
            ranges.append(line)
    for r in ranges:
        lower_bound, upper_bound = r.split("-")
        valid_password_count = 0
        for password in range(int(lower_bound), int(upper_bound) + 1):
            if is_valid_password(str(password)):
                valid_password_count += 1
        print(f"amount of valid passwords between {r}: {valid_password_count}")


def solve_part2():
    ranges = []
    with open("input2.txt", "r") as file:
        for line in file:
            ranges.append(line)
    for r in ranges:
        lower_bound, upper_bound = r.split("-")
        valid_password_count = 0
        for password in range(int(lower_bound), int(upper_bound) + 1):
            if is_valid_restricted_password(str(password)):
                valid_password_count += 1
        print(f"amount of valid passwords between {r}: {valid_password_count}")


def main():
    cases()
    solve_part1()
    solve_part2()


if __name__ == "__main__":
    main()

