import math
import copy
import time


def calculate_distance(p1, p2):
    return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)


def calculate_angle(center, p):
    """
    Calculate angle between y - axis (optimisation for part 2)
    :param center:
    :param p:
    :return:
    """
    return math.atan2(center[0] - p[0], center[1] - p[1])


def has_astroid(p: tuple, grid: list):
    return grid[p[1]][p[0]] == "#"


def flatten_grid(grid):
    coordinates = []
    for y in range(len(grid)):
        row = grid[y]
        for x in range(len(row)):
            p = (x, y)
            if has_astroid(p, grid):
                coordinates.append(p)
    return coordinates


def get_unique_angles(center, coordinates):
    angles = {}
    for point in coordinates:
        if point == center:
            continue
        angle = calculate_angle(center, point)
        distance = calculate_distance(center, point)
        if angle not in angles or angles[angle]["distance"] > distance:
            angles[angle] = {
                "distance": distance,
                "coordinate": point
            }
    return angles


def get_sorted_angles(angle_dict):
    angles = list(angle_dict.keys())
    angles.sort(key=lambda x: abs(x) if x <= 0.0 else 2*math.pi - x)
    return angles


def get_best_position(grid, show=False):
    score_grid = copy.deepcopy(grid)
    flattened = flatten_grid(grid)
    best_coord = None
    best_score = 0
    best_angles = {}
    for point in flattened:
        angles = get_unique_angles(point, flattened)
        score = len(angles)
        score_grid[point[1]][point[0]] = str(score)
        if score > best_score:
            best_score = score
            best_coord = point
            best_angles = angles
    if show:
        print("score map:")
        for i in score_grid:
            print(i)
        print(f"sorted angles: {get_sorted_angles(best_angles)}")
        print("angle map:")
        angle_grid = copy.deepcopy(grid)
        for angle in best_angles:
            point = best_angles[angle]["coordinate"]
            angle_grid[point[1]][point[0]] = angle
        for i in angle_grid:
            print(i)
    return best_coord, best_score


def get_nth_point(grid, point, n):
    count = 0
    flattened = flatten_grid(grid)
    angles_dict = get_unique_angles(point, flattened)
    angles = get_sorted_angles(angles_dict)
    angle_count = 0
    while count < n and len(flattened) > 0:
        count += 1
        point_to_remove = angles_dict[angles[angle_count]]["coordinate"]
        angle_count += 1
        flattened.remove(point_to_remove)
        if angle_count >= len(angles):
            angles_dict = get_unique_angles(point, flattened)
            angles = get_sorted_angles(angles_dict)
            angle_count = 0
    return point_to_remove


def string_to_grid(string):
    grid = []
    for line in string.split("\n"):
        grid.append([c for c in line])
    return grid


def cases():
    tests = [
        {
            "program": ".#..#\n.....\n#####\n....#\n...##",
            "context": "part 1",
            "solution": [(3, 4), 8]
        },
        {
            "program": "......#.#.\n#..#.#....\n..#######.\n.#.#.###..\n.#..#.....\n..#....#.#\n#..#....#.\n.##.#..###\n##...#..#.\n.#....####",
            "context": "part 1",
            "solution": [(5, 8), 33]
        },
        {
            "program": "#.#...#.#.\n.###....#.\n.#....#...\n##.#.#.#.#\n....#.#.#.\n.##..###.#\n..#...##..\n..##....##\n......#...\n.####.###.",
            "context": "part 1",
            "solution": [(1, 2), 35]
        },
        {
            "program": ".#..#..###\n####.###.#\n....###.#.\n..###.##.#\n##.##.#.#.\n....###..#\n..#.#..#.#\n#..#.#.###\n.##...##.#\n.....#.#..",
            "context": "part 1",
            "solution": [(6, 3), 41]
        },
        {
            "program": ".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##",
            "context": "part 1",
            "solution": [(11, 13), 210]
        },
        {
            "program": ".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##",
            "point": (11, 13),
            "n": 1,
            "context": "part 2",
            "solution": (11, 12)
        },
        {
            "program": ".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##",
            "point": (11, 13),
            "n": 2,
            "context": "part 2",
            "solution": (12, 1)
        },
        {
            "program": ".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##",
            "point": (11, 13),
            "n": 3,
            "context": "part 2",
            "solution": (12, 2)
        },
        {
            "program": ".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##",
            "point": (11, 13),
            "n": 10,
            "context": "part 2",
            "solution": (12, 8)
        },
        {
            "program": ".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##",
            "point": (11, 13),
            "n": 20,
            "context": "part 2",
            "solution": (16, 0)
        },
        {
            "program": ".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##",
            "point": (11, 13),
            "n": 100,
            "context": "part 2",
            "solution": (10, 16)
        },
        {
            "program": ".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##",
            "point": (11, 13),
            "n": 200,
            "context": "part 2",
            "solution": (8, 2)
        },
        {
            "program": ".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##",
            "point": (11, 13),
            "n": 299,
            "context": "part 2",
            "solution": (11, 1)
        }
    ]

    for i, test in enumerate(tests):
        grid = string_to_grid(test["program"])
        context = test["context"]
        solution = test["solution"]
        if context == "part 1":
            print(f"test {i}.0: {solution} {get_best_position(grid)}")
        elif context == "part 2":
            point = test["point"]
            n = test["n"]
            print(f"test {i}.0: {solution} {get_nth_point(grid, point, n)}")


def solve_part1():
    grid = []
    with open("input.txt", "r") as file:
        for line in file:
            grid.append([c for c in line])
    print(f"solution 1: {get_best_position(grid)}")


def solve_part2():
    grid = []
    with open("input.txt", "r") as file:
        for line in file:
            grid.append([c for c in line])
    print(f"solution 2: {get_nth_point(grid, (27, 19), 200)}")


def main():
    now = time.time()
    # cases()
    solve_part1()
    solve_part2()
    print(f"needed {time.time() - now} seconds")


if __name__ == "__main__":
    main()
